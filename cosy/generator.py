# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import math
import random
import igraph as ig
from .algorithms import min_depths, max_depths, uniform_rule_probabilities
from .tree import TreeAttributes




class ASTGenerator:
    """An Iterator that generates a potentially infinite amount of ASTs.

    Note:
        It is highly (!) recommend to specify a maximal depth.

    Args:
        bnf (BNF or NumericBNF): The Backus-Naur form of a context-free grammar.
        min_depth (int): The minium depth of all generated programs.
        max_depth (int): The maximum depth of all generated programs.
        rule_probabilities (dict): The probabilites with which each rule will be picked.
        seed (object): A seed for the random generator, see random.seed for options.

    Raises:
        AssertionError: If min_depth > max_depth
    """

    _UNIFORM_PROBABILITIES_STRING = "uniform"

    def __init__(self,
                 bnf,
                 min_depth=0,
                 max_depth=math.inf,
                 rule_probabilities="uniform",
                 seed=None):
        assert min_depth <= max_depth, "min_depth must be greater or equal then max_depth."

        self._bnf = bnf
        self._min_depth = min_depth
        self._max_depth = max_depth
        self._rule_probabilities = rule_probabilities
        self._seed = seed

        if rule_probabilities == self._UNIFORM_PROBABILITIES_STRING:
            self._rule_probabilities = uniform_rule_probabilities(self._bnf)

        self._min_depth_map = self._compute_depth_extrema(min_depths(self._bnf), min)
        self._max_depth_map = self._compute_depth_extrema(max_depths(self._bnf), max)

        self._vertices = []
        self._edges = []

        self._iter_counter = 0
        self._max_iter = math.inf


    @property
    def bnf(self):
        return self._bnf

    @property
    def min_depth(self):
        return self._min_depth

    @property
    def max_depth(self):
        return self._max_depth

    @property
    def rule_probabilities(self):
        return self._rule_probabilities

    @property
    def seed(self):
        return self._seed


    def generate(self, num_asts):
        """Returns an iterator that generates the specified amount of ASTs.

        Args:
            num_asts (int): The number of ASTs to generate.

        Returns:
            (ASTGenerator) self
        """
        self._iter_counter = 0
        self._max_iter = num_asts
        return self


    def __iter__(self):
        """Returns itself"""
        return self


    def __next__(self):
        """Generate and return another ASTs."""
        self._iter_counter += 1

        if self._iter_counter > self._max_iter:
            self._iter_counter = 0
            self._max_iter = math.inf
            raise StopIteration

        self._vertices = []
        self._edges = []

        self._recursive_generate_ast(self._bnf.start_symbol, 1)

        ast = ig.Graph(directed=True)
        ast.add_vertices(len(self._vertices))
        ast.vs[TreeAttributes.SYMBOL] = self._vertices
        ast.add_edges(self._edges)

        return ast


    def _recursive_generate_ast(self, symbol, current_depth):
        # end recursion at terminal symbols
        if not symbol in self._bnf.nonterminal_symbols:
            self._vertices.append(symbol)
            return

        candidate_rules, candidate_probs = self._compute_candidates(symbol, current_depth)

        selected_rule = self._select_rule(candidate_rules, candidate_probs)

        # add vertices + edges and process child vertices recursively
        source_id = len(self._vertices)
        self._vertices.append(symbol)

        for child_symbol in selected_rule:
            self._edges.append((source_id, len(self._vertices)))
            self._recursive_generate_ast(child_symbol, current_depth + 1)


    def _compute_depth_extrema(self, extrema, extrema_fn):
        depth_map = {}

        for nonterminal in self._bnf:
            depth_map[nonterminal] = []

            for rule in self._bnf[nonterminal]:
                rule_depth = [extrema[s] + 1 for s in rule if s in self._bnf.nonterminal_symbols]
                depth_map[nonterminal].append(extrema_fn(rule_depth) if rule_depth else 1)

        return depth_map


    def _compute_candidates(self, symbol, current_depth):
        min_depth = self._min_depth - current_depth
        max_depth = self._max_depth - current_depth

        candidate_rules = []
        candidate_probs = []

        for idx, rule in enumerate(self._bnf[symbol]):
            if self._min_depth_map[symbol][idx] > max_depth:
                continue

            if self._max_depth_map[symbol][idx] < min_depth:
                continue

            candidate_rules.append(rule)
            candidate_probs.append(self._rule_probabilities[symbol][idx])

        if not candidate_rules:
            raise ValueError("Unable to find derivation under the given constraints.")

        return candidate_rules, candidate_probs


    def _select_rule(self, candidate_rules, candidate_probs):
        return random.choices(candidate_rules, candidate_probs, k=1)[0]
