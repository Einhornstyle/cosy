# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from collections import defaultdict
import itertools as it
import igraph as ig




class TreeAttributes:
    """A collection of attribute names that should be used if appropriate."""

    MAX_CHILD_INDEX = "max_child_index"
    CHILD_INDEX = "child_index"
    SYMBOL = "symbol"




def _global_index_key(edge):
    return edge.index


def _child_index_key(edge):
    return edge[TreeAttributes.CHILD_INDEX]


class EdgeSort:
    """Sorts a set of edges.

    This is particularly important for concrete syntax tree, because without order the sentence
    associated with the tree is not well defined. By default the `global_index` that is the
    edge ids are used to sort them. Every cosy algorithm that creates a (pseudo-) concrete syntax
    tree will ensure that sorting the edges by there `global_index` provides them in the correct
    order.

    Args:
        key (str or function): Specify how the edges will be sorted. Must either be 'global_index',
            'child_index' or a function that takes an edge as an argument and returns the value by
            which to sort it.
        reverse (bool): If True the edges will be return in descending order, otherwise ascending.

    """
    SORT_DISPATCHER = {
        "global_index": _global_index_key,
        "child_index": _child_index_key
    }


    def __init__(self, key="global_index", reverse=False):
        self.reverse = reverse

        if key in self.SORT_DISPATCHER:
            self.key = self.SORT_DISPATCHER[key]
        else:
            self.key = key


    def __call__(self, edges):
        """Sort the edges.

        Args:
            edges (list): A list of igraph.Edge objects.

        Returns:
            (list) A sorted list of igraph.Edge objects.
        """
        return sorted(edges, key=self.key, reverse=self.reverse)




def is_root(vertex):
    """Check if a vertex is the root of a tree.

    Args:
        vertex (igraph.Vertex): The vertex to investigate.

    Returns:
        (bool) True if the vertex is the root, false otherwise.
    """
    return vertex.indegree() == 0




def is_leaf(vertex):
    """Check if a vertex is a leaf of a tree.

    Args:
        vertex (igraph.Vertex): The vertex to investigate.

    Returns:
        (bool) True if the vertex is a leaf, false otherwise.
    """
    return vertex.outdegree() == 0




def parent(vertex):
    """Gets the parent of a vertex in a tree.

    Args:
        vertex (igraph.Vertex): The vertex for which we want the parent.

    Returns:
        (igraph.Vertex) The parent of the given vertex.

    Raises:
        (AssertionError) if the parent does not exist or is not unique.
    """
    predecessors = vertex.predecessors()

    assert len(predecessors) == 1, "The parent must exist and be unique."

    return predecessors[0]




def children(vertex, sort=EdgeSort()):
    """Get all children of a vertex in the specified order.

    By default the children are ordered by the `global_index` (aka edge ID) of their incoming
    edges.

    Args:
        vertex (igraph.Vertex): The vertex for which we want the children.
        sort (function): A function that takes a list of edges and returns them in a sorted order.

    Returns:
        (list) The children of the given vertex, represented as igraph.Vertex objects.
    """
    result = []

    for edge in sort(vertex.out_edges()):
        result.append(edge.target_vertex)

    return result




def root(tree):
    """Get the root of a tree.

    Args:
        tree (igraph.Graph): A directed tree.

    Returns:
        (igraph.Vertex) The root vertex of the tree.
    """
    return tree.vs.find(_indegree=0)




def _recursive_leafs(vertex, sort):
    if is_leaf(vertex):
        return [vertex]

    result = []

    for child in children(vertex, sort):
        result.extend(_recursive_leafs(child, sort))

    return result


def leafs(tree, sort=EdgeSort()):
    """Get all leaf nodes of a (sub-)tree.

    The leafs are discovered in a depth-first pattern, where the expansion order is determined
    by the `sort` argument. By default the children are expanded in order of the `global_index`
    (aka edge ID) of their incoming edges.

    Args:
        tree (igraph.Graph or igraph.Vertex): Must be either a directed igraph.Graph that represents
            a tree or a igraph.Vertex of such a graph. In the latter case only the leafs of the
            subtree rooted at the given vertex are computed.
        sort (function): A function that takes a list of edges and returns them in a sorted order.

    Returns:
        (list) A list of all leafs, represented as igraph.Vertex objects.
    """
    if isinstance(tree, ig.Graph):
        return _recursive_leafs(root(tree), sort)

    return _recursive_leafs(tree, sort)




def add_child_index(tree, sort=EdgeSort()):
    """Add the child index attribute to every edge in a tree.

    The `tree` provided as input should have ordered edges as defined by the `sort` argument.
    Every edge will then receive the attribute 'child_index' which stores this order explicitly.

    Args:
        tree (igraph.Graph): A directed tree.
        sort (function): A function that takes a list of edges and returns them in a sorted order.
    """
    tree[TreeAttributes.MAX_CHILD_INDEX] = 0

    for vertex in tree.vs:
        for child_index, edge in enumerate(sort(vertex.out_edges())):
            edge[TreeAttributes.CHILD_INDEX] = child_index

        max_child_index = vertex.outdegree() - 1

        if max_child_index > tree[TreeAttributes.MAX_CHILD_INDEX]:
            tree[TreeAttributes.MAX_CHILD_INDEX] = max_child_index




def root_path(vertex, reverse=False):
    """Compute the shortest path from any vertex in a tree to the root.

    Args:
        vertex (igraph.Vertex): The starting point of the path.
        reverse (bool): If true the path will be reversed (== start with the root).

    Returns:
        (list) A list of igraph.Vertex objects.

    Raises:
        (AssertionError) If the path is not unique.
    """
    path = [vertex]

    while not is_root(vertex):
        vertex = parent(vertex)
        path.append(vertex)

    if reverse:
        path.reverse()

    return path




def leaf_path(start_leaf, stop_leaf):
    """Compute the shortest undirected path between two leafs in a tree.

    Args:
        start_leaf (igraph.Vertex): The first vertex in the path, must be a leaf.
        stop_leaf (igraph.Vertex): The last vertex in the path, must be a leaf.

    Returns:
        (list) A list of igraph.Vertex objects.

    Raises:
        (AssertionError): If either of the vertices is not a leaf.
        (AssertionError): If the leafs do not belong to the same tree.
        (ValueError): If the provided graph it not a directed tree.
    """
    assert is_leaf(start_leaf), "The node provided as `start_leaf` is not a leaf."
    assert is_leaf(stop_leaf), "The node provided as `stop_leaf` is not a leaf."
    assert start_leaf.graph == stop_leaf.graph, "The leafs do not belong to the same tree."

    if start_leaf.index == stop_leaf.index:
        return [start_leaf]

    start_path = root_path(start_leaf, reverse=False)
    lookup = {vertex.index: position for position, vertex in enumerate(start_path)}

    stop_path = [stop_leaf]

    while not is_root(stop_leaf):
        stop_path.append(parent(stop_path[-1]))
        vertex_index = stop_path[-1].index

        if vertex_index in lookup:
            return list(it.chain(start_path[:lookup[vertex_index]], reversed(stop_path)))

    raise ValueError("The provided graph is not a directed tree.")




def all_root_paths(tree, reverse=False):
    """Compute the shortest paths from all leafs to the root of the tree

    Args:
        tree (igraph.Graph): A directed tree.

    Returns:
        (list) A list of paths, where each path is a list of igraph.Vertex objects.
    """
    return [root_path(leaf, reverse) for leaf in tree.vs.select(_outdegree=0)]




def _helper_all_leaf_paths(start_path, stop_path, reverse_start_path, reverse_stop_path):
    """Helper function, which computes the path (and its reverse) between two leafs.

    Args:
        start_path (list): The path from the start vertex to the root of the tree as a list.
        stop_path (list): The path from the stop vertex to the root of the tree as a list.
        reverse_start_path (list): The reverse of `start_path`.
        reverse_start_path (list): The reverse of `stop_path`.

    Returns:
        (tuple) The path from start to stop and its reverse.

    Raises:
        ValueError: If the leafs do not belong to the same tree.
    """
    for start_cutoff, start_node in enumerate(start_path[1:]):
        for stop_cutoff, stop_node in enumerate(stop_path[1:]):
            if start_node.index != stop_node.index:
                continue

            # add offset due to skipping of leafs
            start_cutoff += 1
            stop_cutoff += 1

            # compute the path from start to stop (start/stop nodes not included)
            cutoff = len(stop_path) - stop_cutoff
            path = start_path[:start_cutoff+1] + reverse_stop_path[cutoff:]

            # compute the path from stop to start (start/stop nodes not included)
            rev_cutoff = len(start_path) - start_cutoff
            rev_path = stop_path[:stop_cutoff+1] + reverse_start_path[rev_cutoff:]

            return path, rev_path

    raise ValueError("leafs do not belong to the same tree")




def all_leaf_paths(tree, reverse=False, full_path=False):
    """Compute the shortest undirected paths between all leafs in a tree.

    For every leaf we compute all paths from any other leaf to its parent and return them
    as a dictionary mapping from vertex IDs (aka igraph.Vertex.index) to a list of said paths.

    Args:
        tree (igraph.Graph): A directed tree.
        reverse (bool): If true the paths will be reversed
        full_path (bool): If true the full paths (including both leafs) are returned.

    Returns:
        (dict) A map from igraph.Vertex IDs to a list of lists containing igraph.Vertex objects.
    """
    forward_paths = all_root_paths(tree)
    # We cache the reverse path, because we need each of them O(len(leafs)**2) times.
    reverse_paths = [list(reversed(p)) for p in forward_paths]

    num_leafs = len(forward_paths)

    result = defaultdict(list)

    for start_idx in range(num_leafs):
        # We exploit the symmetry of the problem to omit half of the path searches.
        for stop_idx in range(start_idx+1, num_leafs):
            start2stop, stop2start = _helper_all_leaf_paths(
                forward_paths[start_idx],
                forward_paths[stop_idx],
                reverse_paths[start_idx],
                reverse_paths[stop_idx]
            )

            start2stop_index = start2stop[-1].index
            stop2start_index = stop2start[-1].index


            if full_path and reverse:
                start2stop, stop2start = stop2start, start2stop

            elif (not full_path) and reverse:
                start2stop, stop2start = stop2start[1:], start2stop[1:]

            elif (not full_path) and (not reverse):
                start2stop, stop2start = start2stop[:-1], stop2start[:-1]

            result[start2stop_index].append(start2stop)
            result[stop2start_index].append(stop2start)

    return result




def dfs(tree, sort=EdgeSort()):
    """Iterate over the vertices in a (sub-)tree in depth first search (DFS) order.

    Args:

        tree (igraph.Graph or igraph.Vertex): Must be either a directed igraph.Graph that represents
            a tree or a igraph.Vertex of such a graph. In the latter case only the vertices of the
            subtree rooted at the given vertex are traversed.
        sort (function): A function that takes a list of edges and returns them in a sorted order.

    Yields:
        (igraph.Vertex) The next vertex in depth first traversal order.
    """
    root_vertex = root(tree) if isinstance(tree, ig.Graph) else tree

    yield root_vertex

    stack = [children(root_vertex, sort)]
    added = {root_vertex.index}

    while stack:
        children_list = stack[-1]

        if children_list:
            child = children_list.pop(0)

            if not child.index in added:
                stack.append(children(child, sort))
                added.add(child.index)
                yield child
        else:
            stack.pop()
