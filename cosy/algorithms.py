# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import math
import itertools as it
import tensorflow as tf




def min_depths(bnf):
    """Compute the minimal possible remaining parse tree depth for every nonterminal symbol.

    Args:
        bnf (BNF or NumericBNF): An grammar in Backus-Naur-Form.

    Returns:
        (dict) A mapping from nonterminals do minimal parse tree depths.
    """
    grammar_copy = bnf.grammar

    processed = set(bnf.terminal_symbols)
    remaining = set(bnf.nonterminal_symbols)

    depths = {symbol: math.inf for symbol in bnf.nonterminal_symbols}

    for current_depth in it.count(1):
        updated = []

        for nt in remaining:
            # We know the minimal depth if we have a derivation where all symbols
            # on the right hand side are processed (aka the depth is known)
            if any([all([s in processed for s in rule]) for rule in grammar_copy[nt]]):
                depths[nt] = current_depth
                updated.append(nt)

        if not updated:
            break

        processed.update(updated)
        remaining.difference_update(updated)

    return depths




def max_depths(bnf):
    """Compute the maximal possible remaining parse tree depth for every nonterminal sybmol.

    Note:
        This depth might be infinity, in which case math.inf is returned
        for the respective vertex.

    Args:
        bnf (BNF or NumericBNF): An grammar in Backus-Naur-Form.

    Returns:
        (dict) A mapping from nonterminal do maximal parse tree depths.
    """
    grammar_copy = bnf.grammar

    remaining = set(bnf.nonterminal_symbols)

    depths = {symbol: math.inf for symbol in bnf.nonterminal_symbols}
    depths.update({symbol: 0 for symbol in bnf.terminal_symbols})

    while True:
        updated = []

        for nt in remaining:
            max_depth = max([max([depths[s] for s in rule]) for rule in grammar_copy[nt]])

            if max_depth != math.inf:
                depths[nt] = max_depth + 1
                updated.append(nt)

        if not updated:
            break

        remaining.difference_update(updated)

    return {s: d for s, d in depths.items() if s in bnf.nonterminal_symbols}




def min_widths(bnf):
    """Compute the minimal possible remaining sentence length for every nonterminal symbol.

    Args:
        bnf (BNF or NumericBNF): An grammar in Backus-Naur-Form.

    Returns:
        (dict) A mapping from nonterminals do minimal sentence length.

    Raises:
        ValueError: On existence of nonterminals without a minimum width.
    """
    grammar_copy = bnf.grammar

    widths = {symbol: math.inf for symbol in bnf.nonterminal_symbols}
    widths.update({symbol: 1 for symbol in bnf.terminal_symbols})

    updated = True

    while updated:
        updated = False

        for nt in bnf.nonterminal_symbols:
            min_width = min([sum([widths[s] for s in rule]) for rule in grammar_copy[nt]])

            if min_width < widths[nt]:
                widths[nt] = min_width
                updated = True

    return {s: w for s, w in widths.items() if s in bnf.nonterminal_symbols}




def max_widths(bnf):
    """Compute the maximal possible remaining sentence length for every nonterminal symbol.

    Note:
        This width might be infinity, in which case math.inf is returned
        for the respective symbol.

    Args:
        bnf (BNF or NumericBNF): An grammar in Backus-Naur-Form.

    Returns:
        (dict) A mapping from nonterminals do maximal sentence lengths.

    Raises:
        ValueError: On existence of nonterminals without a minimum width.
    """
    grammar_copy = bnf.grammar

    widths = {symbol: 0 for symbol in bnf.nonterminal_symbols}
    widths.update({symbol: 1 for symbol in bnf.terminal_symbols})

    updated = True

    while updated:
        updated = False

        for nt in bnf.nonterminal_symbols:
            # strict optimization, does not impact result if omitted
            if widths[nt] == math.inf:
                break

            rule_widths = []

            for rule in grammar_copy[nt]:
                rule_widths.append(sum([widths[s] for s in rule]))

                # detect infinite max depths and handle them properly
                if nt in rule and len(rule) > 1 and rule_widths[-1] > 0:
                    widths[nt] = math.inf
                    updated = True
                    break

            max_width = max(rule_widths)

            if max_width > widths[nt]:
                widths[nt] = max_width
                updated = True

    return {s: w for s, w in widths.items() if s in bnf.nonterminal_symbols}




def uniform_rule_probabilities(bnf):
    """Computes the probabilites for each rule application in a context-free grammar

    The probabilites are uniform in the sense that each rule that may be applied to a given
    nonterminal symbol has the same probability as its peers. Specifically, if N rules may
    be applied to a nonterminal symbol each rule will be assigned the probability 1/N.

    Args:
        bnf (BNF or NumericBNF): A context-free grammar in Backus-Naur form.

    Returns:
        (dict) A mapping from nonterminal symbols to a list of probabilities.
    """
    return {nt: [1 / len(bnf[nt])] * len(bnf[nt]) for nt in bnf}




def greedy_sequence_prediction(model, input_sequence, end_token, max_length=1000000):
    """Predict extend a sequence with the given model using a greedy algorithm.

    Args:
        model (function): Any function that takes a sequence, represented as a rank 1 tensor
            of integers as input, and returns a sequence of predictions over the same vocabulary.
        input_sequence (Tensor): A rank 1 integer tensor that represent the initial sequence.
        end_token (int): The token that represent the end of the sequence.
        max_length (int): The maximum length of the predicted sequence. Must be provided to ensure
            the algorithm always terminates. (default = 1e6)
    """
    assert tf.rank(input_sequence) <= 2

    if tf.rank(input_sequence) == 2:
        raise ValueError("Batches of sequences are currently not supported.")

    output, success = _greedy_sequence_prediction(model, input_sequence, end_token, max_length)

    if tf.reduce_any(tf.equal(success, False)):
        raise ValueError("Model did not produce the end_token.")

    return output


def _greedy_sequence_prediction(model, input_sequence, end_token, max_length):
    position = 0
    success = False

    while tf.reduce_all(tf.less(position, max_length)):
        predictions = model(input_sequence)

        # get rid of size 1 batches if the exists.
        if tf.reduce_all(tf.equal(tf.rank(predictions), 3)):
            assert tf.shape(predictions)[0] == 1
            predictions = predictions[0, ...]

        predicted_token = tf.argmax(predictions[-1:,:], axis=-1, output_type=input_sequence.dtype)
        input_sequence = tf.concat([input_sequence, predicted_token], axis=-1)

        # return the generated sequence if our model generated the end_token
        if tf.reduce_all(tf.equal(predicted_token, end_token)):
            input_sequence = input_sequence[-(position + 1):]
            success = True
            break

        position += 1

    return input_sequence, success




class FeedbackBeamSearch:
    """***EXPERIMENTAL***

    A massively parallel version of the beam search algorithm that runs on GPUs as well CPUs.

    Note:
        Currently the final result is provided as a batch of embedded sequences.
        This will be changed in a future version.

    The score of a beam b = (b_1, ..., b_N) is computed as:

        sum_{i=1,...N} log(P(b_i)) / ((beta + N)**alpha / (beta + 1)**alpha)

    Where P(b_i) denotes the probability of token b_i according to the `feedback_fn`.

    Args:
        feedback_fn (function): A (tf-)function that predicts the next token for each element
            in a batch on sequences.
        embedding_layer (tf.keras.Layer): An embedding layer that will be applied to input
            sequences as well as the predicted tokens.
        beam_width (int): The width of the beam.
        end_token (int): The token that indicates the end of sequence.
        padding_token (int): The token used to pad the sequences in the batches.
        alpha (float): Scale factor for the beam score.
        beta (float): Scale factor for the beam score.
        max_length (int): The maximum length of the output sequences. If None output sequences
            may be of any length. If not provided this algorithm is not guaranteed to terminate.
        num_parallel_beams (int): The number of beams that will be feed into the feedback_fn in
            parallel. The default is batch_size * beam_width.
        memory_growth_speed (int): The speed by which the memory that holds the feedback sequences
            will grow. Measured in number of tokens.
    """

    _SCORE_PENALTY = 1e20


    def __init__(self,
            feedback_fn,
            embedding_layer,
            beam_width,
            end_token,
            padding_token=0,
            alpha=1.0,
            beta=0.0,
            max_length=None,
            num_parallel_beams=None,
            memory_growth_speed=1):
        self._feedback_fn = feedback_fn
        self._embedding_layer = embedding_layer
        self._beam_width = beam_width
        self._end_token = end_token
        self._padding_token = padding_token
        self._alpha = alpha
        self._beta = beta
        self._max_length = max_length
        self._num_parallel_beams = num_parallel_beams
        self._memory_growth_speed = memory_growth_speed

        var_kwargs = {"trainable": False, "validate_shape": False, "shape": [None]}

        self._inputs_offset = tf.Variable([0], dtype=tf.int64, **var_kwargs)
        self._pred_lengths = tf.Variable([0], dtype=tf.int64, **var_kwargs)
        self._active_beams = tf.Variable([True], dtype=tf.bool, **var_kwargs)
        self._log_probs = tf.Variable([0.0], dtype=tf.float32, **var_kwargs)
        self._scores = tf.Variable([0.0], dtype=tf.float32, **var_kwargs)


    def __call__(self, inputs, **kwargs):
        """Run the beam search on the provided input sequences.

        Args:
            inputs (Tensor): The input sequences. shape = (batch_size, seq_length)
            **kwargs (keyword arguments): Additional keyword argument that will be passed
                to each call of the `feedback_fn`.

        Returns:
            (tuple) The output_beams, their mask and the scores.
        """
        emb_inputs = self._embedding_layer(inputs)
        mask = self._embedding_layer.compute_mask(inputs)

        input_beams, mask_beams = self._initialize_beams(emb_inputs, mask, **kwargs)

        current_length = 1

        while tf.reduce_any(self._active_beams):
            if (self._max_length is not None) and (current_length >= self._max_length):
                break

            # feed beam into model and get predictions
            pred_beams = self._feed(input_beams, mask_beams, **kwargs)
            # evaluate each prediction
            beam_indices, tokens, log_probs, scores = self._evaluate_beams(pred_beams)
            # update beams
            input_beams =  self._update_beams(input_beams, beam_indices, tokens, log_probs, scores)
            # compute mask (may be None)
            mask_beams = self._embedding_layer.compute_mask(input_beams)

            current_length += 1

        output_beams = self._merge_beams(input_beams)
        output_mask_beams = self._merge_beams(mask_beams)

        return output_beams, output_mask_beams, self._scores


    def _initialize_variables(self, inputs, mask=None, beam_width=None):
        """Initialize the variables for a certain beam width.

        Args:
            inputs (Tensor): The embedded input sequences.
                shape = (batch_shape, seq_length, embedding_dim)
            mask (Tensor): The mask of the input sequences. shape (batch_shape, seq_length).
                May be None.
            beam_width (int): The beam width. Defaults to self._beam_width
        """
        if beam_width is None:
            beam_width = self._beam_width

        num_seq = inputs.shape[0] * beam_width

        if mask is None:
            self._inputs_offset.assign([(inputs.shape[-2] - 1)] * num_seq)
        else:
            inputs_end = tf.repeat(
                tf.reduce_sum(tf.cast(mask, dtype=tf.int64), axis=-1),
                beam_width,
                axis=0
            )
            not_empty = tf.cast(tf.logical_not(inputs_end == 0), dtype=tf.int64)

            self._inputs_offset.assign((inputs_end - 1) * not_empty)

        self._active_beams.assign(tf.ones([num_seq], dtype=tf.bool))
        self._pred_lengths.assign(tf.zeros([num_seq], dtype=tf.int64))
        self._log_probs.assign(tf.zeros([num_seq], dtype=tf.float32))
        self._scores.assign(tf.zeros([num_seq], dtype=tf.float32))


    def _initialize_beams(self, inputs, mask=None, **kwargs):
        """Initialize the beams.

        Args:
            inputs (Tensor): The embedded input sequences.
                shape = (batch_shape, seq_length, embedding_dim)
            mask (Tensor): The mask of the input sequences. shape = (batch_shape, seq_length).

        Returns:
            (tuple) The beams and their mask. mask_shape = (batch_shape * beam_width, seq_length)
                beam_shape = (batch_shape * beam_width, seq_length, embedding_dim)
        """
        predictions = self._feed(inputs, mask, **kwargs)

        self._initialize_variables(inputs, mask, beam_width=1)

        token_log_probs = tf.math.log(tf.gather(predictions, self._inputs_offset, batch_dims=1))
        scores = self._score(token_log_probs)

        scores, tokens = tf.math.top_k(scores, k=self._beam_width)
        log_probs = tf.gather(token_log_probs, tokens, batch_dims=1)

        input_beams = self._split_beams(self._create_beams(inputs))
        beam_indices = tf.repeat(tf.range(inputs.shape[0]) * self._beam_width, self._beam_width)
        tokens = tf.reshape(tokens, [-1])
        log_probs = tf.reshape(log_probs, [-1])
        scores = tf.reshape(scores, [-1])

        self._initialize_variables(inputs, mask, beam_width=self._beam_width)

        beam = self._update_beams(input_beams, beam_indices, tokens, log_probs, scores)
        mask = self._embedding_layer.compute_mask(beam)

        return beam, mask


    def _feed(self, inputs, mask=None, **kwargs):
        """Feed the current beams into the `feedback_fn`.

        Args:
            inputs (Tensor): The input beams.
                shape = (batch_shape * beam_width, seq_length, embedding_dim)
            mask (Tensor): The mask of the beams. shape = (batch_shape * beam_width, seq_length)

        Returns:
            (Tensor) The beams of next token predictions.
                shape = (batch_shape * beam_width, seq_length, vocab_size)
        """
        if mask is not None:
            kwargs["mask"] = mask

        if self._num_parallel_beams is None:
            return self._feedback_fn(inputs, **kwargs)

        step_count = 0
        pred_list = []

        inputs_bunch = inputs[:self._num_parallel_beams, ...]

        if mask is not None:
            kwargs["mask"] = mask[:self._num_parallel_beams, ...]

        while tf.not_equal(tf.size(inputs_bunch), 0):
            step_count += 1
            pred_list.append(self._feedback_fn(inputs_bunch, **kwargs))

            start = step_count * self._num_parallel_beams
            stop = (step_count + 1) * self._num_parallel_beams

            inputs_bunch = inputs[start:stop, ...]

            if mask is not None:
                kwargs["mask"] = mask[start:stop, ...]

        return tf.concat(pred_list, axis=0)


    def _score(self, token_log_probs):
        """Compute the scores for the potential next tokens.

        The score of a beam b = (b_1, ..., b_N) is computed as:

            sum_{i=1,...N} log(P(b_i)) / ((beta + N)**alpha / (beta + 1)**alpha)

        Where P(b_i) denotes the probability of token b_i according to the `feedback_fn`.

        Args:
            token_log_probs (Tensor): The logarithm of the_log_probs for each potential next
                token. shape = (batch_size * beam_width, vocab_size)

        Returns:
            (Tensor) The scores for each token. shape = (batch_size * beam_width, vocab_size)
        """
        probs = tf.tile(self._log_probs[:, tf.newaxis], [1, token_log_probs.shape[-1]])

        lengths = tf.cast(self._pred_lengths, dtype=token_log_probs.dtype)
        lengths = tf.tile(lengths[:, tf.newaxis], [1, token_log_probs.shape[-1]])

        active_beams = tf.cast(self._active_beams, dtype=token_log_probs.dtype)
        active_beams = tf.tile(active_beams[..., tf.newaxis], [1, token_log_probs.shape[-1]])

        # lengths of active beams will increase by one
        lengths += active_beams

        # only add probabilites for beams that are still active
        seq_score = probs + (token_log_probs * active_beams)

        weight = (self._beta + lengths)**self._alpha / (self._beta + 1)**self._alpha

        # If beams are not active from the start we set their score to zero
        return tf.math.divide_no_nan(seq_score, weight)


    def _compute_score_mask(self, scores):
        """Compute the mask for the given score.

        The score mask penalizes the score of every non padding for every inactive beam.

        Args:
            score (Tensor): The score tensor. shape = (batch_size * beam_width, vocab_size)

        Returns:
            (Tensor): The score mask. shape = (batch_size * beam_width, vocab_size)
        """
        inactive_beams = tf.logical_not(self._active_beams)

        mask = tf.tile(inactive_beams[..., tf.newaxis], [1, scores.shape[-1]])
        mask = tf.cast(mask, dtype=tf.float32) * self._SCORE_PENALTY

        start_indices = tf.cast(tf.where(inactive_beams), tf.int64)
        stop_indices = tf.ones((tf.size(start_indices), 1), dtype=tf.int64) * self._padding_token
        indices = tf.concat([start_indices, stop_indices], axis=-1)

        updates = tf.ones((tf.size(start_indices)), dtype=tf.float32)

        return tf.tensor_scatter_nd_update(mask + 1, indices,  updates)


    def _evaluate_beams(self, prediction_beams):
        """Evaluate the predictions for every beam.

        Args:
            prediction_beams (Tensor): The predictions.
                shape = (batch_shape * beam_width, seq_length, vocab_size)

        Returns:
            (tuple) The beam indices, tokens, log probabilities and scores.
        """
        offset = tf.add(self._inputs_offset, self._pred_lengths)
        token_probs = tf.gather(prediction_beams, offset, batch_dims=1)

        # store, since we need them later to update the beam probabilities
        token_log_probs = tf.math.log(token_probs)

        # map: [batch_size * beam_width, vocab_size] -> [batch_size, beam_width * vocab_size]
        scores = self._score(token_log_probs)
        # avoid duplicate usage of inactive beams
        scores *= self._compute_score_mask(scores)

        merged_scores = self._merge_beams(scores)
        merged_scores = tf.reshape(merged_scores, [merged_scores.shape[0], -1])

        # top_k per beam
        vocab_size = prediction_beams.shape.as_list()[-1]
        scores, indices = tf.math.top_k(merged_scores, k=self._beam_width)

        # convert: index(beam_width*vocab_size) -> index(beam_width) and flatten
        beam_offset = tf.range(0, prediction_beams.shape[0], self._beam_width)[:, tf.newaxis]
        beam_indices = (indices // vocab_size) + tf.tile(beam_offset, [1, self._beam_width])
        beam_indices = tf.reshape(beam_indices, [-1])

        # convert: index(beam_width*vocab_size) -> index(vocab_size) and flatten
        tokens = indices % vocab_size
        tokens = tf.reshape(tokens, [-1])

        log_probs = tf.gather(token_log_probs, tokens, batch_dims=1)
        scores = self._split_beams(scores)

        return beam_indices, tokens, log_probs, scores


    def _update_beams(self, inputs, beam_indices, tokens, token_log_probs, scores):
        """Update the beams.

        Args:
            inputs (Tensor): The embedded input sequences.
                shape = (batch_shape, seq_length, embedding_dim)
            beam_indices (Tensor): The beam indices. shape = (batch_shape * beam_width)
            tokens (Tensor): The tokens. shape = (batch_shape * beam_width)
            token_log_probs (Tensor): The log probabilites of the tokens.
                shape = (batch_shape * beam_width)
            scores (Tensor): The scores. shape = (batch_shape * beam_width)

        Returns:
            (Tensor): The updated inputs. shape = (batch_shape, seq_length + N, embedding_dim).
            N is either 0 or self._memory_growth_speed
        """
        # select the new beams and associated meta data
        beams = tf.gather(inputs, beam_indices)
        active = tf.gather(self._active_beams, beam_indices)
        probs = tf.gather(self._log_probs, beam_indices)
        lengths = tf.gather(self._pred_lengths, beam_indices)
        offset = tf.gather(self._inputs_offset, beam_indices)

        token_position = offset + lengths + 1

        # if the beam is not active set token to padding
        tokens = tf.where(active, tokens, self._padding_token)
        not_last_tokens = tf.logical_and(tokens != self._end_token, tokens != self._padding_token)

        # maybe grow beams
        if tf.reduce_any(token_position >= inputs.shape[1]):
            beams = self._grow_sequences(beams)

        # scatter update new tokens
        indices = tf.stack([tf.range(tokens.shape[0], dtype=tf.int64), token_position], axis=-1)
        updates = tf.cast(self._embedding_layer(tokens), dtype=beams.dtype)
        beams = tf.tensor_scatter_nd_update(beams, indices, updates)

        # update meta data
        self._log_probs.assign(probs + tf.where(active, token_log_probs, 0))
        self._pred_lengths.assign(lengths + tf.cast(active, dtype=self._pred_lengths.dtype))
        self._active_beams.assign(not_last_tokens)
        self._scores.assign(scores)

        return beams


    def _create_beams(self, inputs):
        """Create a set of beams by repetition of sequences.

        Args:
            inputs (Tensor): The input tensor. shape = (batch_shape, ...)

        Returns:
            (Tensor) The beams. shape = (batch_shape, beam_width, ...)
        """
        multiples = [1, self._beam_width] + [1] * (len(inputs.shape) - 1)
        return tf.tile(inputs[:, tf.newaxis, ...], multiples)


    @classmethod
    def _split_beams(cls, inputs):
        """Split a tensor of beams.

        Args:
            inputs (Tensor): The merged input beams. shape = (batch_size, beam_width, ...)

        Returns:
            (Tensor) The input beams. shape = (batch_size * beam_width, ...)
        """
        return tf.reshape(inputs, [-1] + inputs.shape.as_list()[2:])


    def _merge_beams(self, inputs):
        """Merge continuous beams.

        Args:
            inputs (Tensor): The input beams. shape = (batch_size * beam_width, ...)

        Returns:
            (Tensor) The merged beams. shape = (batch_size, beam_width, ...)
        """
        return tf.reshape(inputs, [-1, self._beam_width] + inputs.shape.as_list()[1:])


    def _grow_sequences(self, inputs):
        """Grow the input sequences.

        Args:
            inputs (Tensor): A tensor containing a batch of (possibly embedded) sequences.
                shape = (batch_shape, seq_len, ...)

        Returns:
            (Tensor): The elongated sequences. shape = (batch_shape, seq_len, ...)
        """
        paddings = [[0, 0], [0, self._memory_growth_speed]] + [[0, 0]] * (len(inputs.shape) - 2)
        return tf.pad(inputs, paddings, constant_values=self._padding_token)
