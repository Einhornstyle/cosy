# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import inspect




def arg_names(function):
    """Get a list of all argument names from function.

    Args:
        function (function): The function from which the argument names will be extracted.

    Returns:
        (list) A list of all argument names, represented as strings.
    """
    return list(inspect.signature(function).parameters.keys())


def has_arg(function, arg_name):
    """Check if the function has an argument with the specified name.

    Args:
        function (function): The function to investigate.
        arg_name (str): The argument to search for.

    Returns:
        (bool) True if the function has the argument `arg_name`, False otherwise.
    """
    return arg_name in inspect.signature(function).parameters


def applicable_kwargs(function, **kwargs):
    """Remove every keyword argument that is not applicable to the function.

    Args:
        function (function): The function to investigate.
        **kwargs (keyword arguments): The keyword arguments in question.

    Returns:
        (dict) This dict contains only the keyword arguments that are applicable to function.
    """
    return {
        key: value
        for key, value in kwargs.items()
        if has_arg(function, key)
    }
