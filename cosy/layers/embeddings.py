# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import copy

import tensorflow as tf
from tensorflow import keras as K

import cosy.utils as cu
from .utils import LSTMStack, GRUStack




class SymbolEmbedding(K.layers.Layer):
    """Embed a symbol together with positional information.

    Args:
        vocab_size (int): The size of the symbol vocabulary
        max_index (int): The largest integer used to represent position information.
        vocab_dimension (int): The dimensionality of symbol embedding.
        index_dimension (int): The dimensionality of positional embedding.
        mask_zero (bool): If True a padding mask for symbols with value zero will be created.
    """
    def __init__(self,
                 vocab_size,
                 max_index,
                 vocab_dimension,
                 index_dimension,
                 mask_zero=True,
                 **kwargs):
        super().__init__(**kwargs)

        self.symbol_embedding = K.layers.Embedding(
            vocab_size,
            vocab_dimension,
            mask_zero=mask_zero
        )

        self.index_embedding = K.layers.Embedding(
            max_index,
            index_dimension,
            mask_zero=mask_zero
        )


    def call(self, inputs):
        """Embed a symbol and index/positional tensor.

        At first each tensor will be embedded seperately and then the will be stacked along the
        last dimension, such that the first `vocab_dimension` dimensions are occupied by the
        symbol embedding and the remaining `index_dimension` dimensions by the embedding of the
        index/positional tensor.

        Args:
            inputs (tuple): A pair of integer tensor that have the same shape.

        Returns:
            (Tensor) A single tensor containing the embedding.
        """
        symbols, indices = inputs[0], inputs[1]

        embedded_symbols = self.symbol_embedding(symbols)
        embedded_indices = self.index_embedding(indices)

        result = tf.concat([embedded_symbols, embedded_indices], -1)

        return result


    def compute_mask(self, inputs, mask=None):
        """Compute the mask.

        Args:
            inputs (tuple): A pair of input tensors.

        Returns.
            (Tensor) The mask as a tensor.
        """
        if mask is not None:
            return self.symbol_embedding.compute_mask(inputs[0], mask=mask[0])

        return self.symbol_embedding.compute_mask(inputs[0])


    def compute_output_shape(self, input_shape):
        """Compute the shape of the output tensor.

        Args:
            input_shape (tuple): The shape of the input tensor.

        Returns.
            (tuple) The shape of the output tensor.
        """
        sym_shape = self.symbol_embedding.compute_output_shape(input_shape[0])
        idx_shape = self.index_embedding.compute_output_shape(input_shape[1])
        return sym_shape[:-1] + (sym_shape[-1] + idx_shape[-1],)




class PathRNN(K.layers.Layer):
    """Encode several sets of paths with a RNN.

    Args:
        units (int): Dimensionality of the RNN output.
        rnn_type (string): The type of rnn, must be either 'lstm' or 'gru'
        activation (activation function): The activation to use.
        recurrent_activation (activation function): The activation to use for recurrent step.
        dropout (float): Fraction of units to drop out for linear transformation of the inputs.
            Must be between 0.0 and 1.0.
        recurrent_dropout (float): Fraction of units to drop out for linear transformation of
            the recurrent state. Must be between 0.0 and 1.0.
        allow_cudnn_kernel (bool): If True the faster cuDNN kernel may be used if applicable.
    """
    _CUDNN_ERROR_TYPE = "CUDNN_STATUS_BAD_PARAM"
    _CUDNN_ERROR_MSG = "The mask is incompatible with the cuDNN kernel, please set "\
        "`allow_cudnn_kernel` to False during construction of this layer."

    _LAYER_DISPATCHER = {
        "lstm": K.layers.LSTM,
        "gru": K.layers.GRU
    }

    _CELL_DISPATCHER = {
        "lstm": K.layers.LSTMCell,
        "gru": K.layers.GRUCell
    }


    def __init__(self,
                 units,
                 rnn_type="lstm",
                 activation="tanh",
                 recurrent_activation="sigmoid",
                 dropout=0.0,
                 recurrent_dropout=0.0,
                 allow_cudnn_kernel=True,
                 **kwargs):
        super().__init__(**kwargs)

        if allow_cudnn_kernel:
            self.rnn = self._LAYER_DISPATCHER[rnn_type](
                units=units,
                activation=activation,
                recurrent_activation=recurrent_activation,
                dropout=dropout,
                recurrent_dropout=recurrent_dropout
            )
        else:
            self.rnn = K.layers.RNN(cell=self._CELL_DISPATCHER[rnn_type](
                units=units,
                activation=activation,
                recurrent_activation=recurrent_activation,
                dropout=dropout,
                recurrent_dropout=recurrent_dropout
            ))

        self.distributed_rnn = K.layers.TimeDistributed(self.rnn)


    def call(self, inputs, training=None, mask=None):
        """Encode a tensor containing several sets of paths.

        Args:
            inputs (Tensor): The path tensor. Must be of shape
                (batch_size, num_paths, max_path_len, embedding_dim)
            training (bool): If True the layer will be run in trainings mode. This parameter
                only matters if dropout or recurrent_dropout are not 0.0.
            mask (Tensor): Mask tensor of shape (batch_size, num_paths, max_path_len)

        Returns:
            (Tensor) Tensor of shape (batch_size, num_paths, embedding_dim)
        """
        try:
            return self.distributed_rnn(inputs, training=training, mask=mask)

        except tf.python.framework.errors_impl.UnknownError as err:
            # this mask prohibits the use of the cudnn implementation
            if str(err).startswith(self._CUDNN_ERROR_TYPE):
                raise ValueError(self._CUDNN_ERROR_MSG)

            # actually unknown error
            raise err


    def compute_mask(self, inputs, mask=None):
        """Compute the updated mask.

        Args:
            inputs (Tensor): The path tensor. Must be of shape
                (batch_size, num_paths, max_path_len, embedding_dim)
            mask (Tensor): Mask tensor of shape (batch_size, num_paths, max_path_len)

        Returns:
            (Tensor) Mask tensor of shape (batch_size, num_paths)
        """
        if mask is None:
            return mask

        return tf.math.reduce_any(mask, axis=-1)




class PathPooling(K.layers.Layer):
    """Encode several sets of paths with global 1D pooling.

    Args:
        pooling_type (string): The type of pooling, must be either 'max' or 'average'.
    """
    _LAYER_DISPATCHER = {
        "average": K.layers.GlobalAveragePooling1D,
        "max": K.layers.GlobalMaxPooling1D
    }


    def __init__(self, pooling_type="max", **kwargs):
        super().__init__(**kwargs)

        self._pooling_type = pooling_type

        self.pooling = self._LAYER_DISPATCHER[pooling_type]()

        self.distributed_pooling = K.layers.TimeDistributed(self.pooling)


    def call(self, inputs, mask=None):
        """Encode a tensor containing several sets of paths.

        Args:
            inputs (Tensor): The path tensor. Must be of shape
                (batch_size, num_paths, max_path_len, embedding_dim)
            mask (Tensor): Mask tensor of shape (batch_size, num_paths, max_path_len)

        Returns:
            (Tensor) Tensor of shape (batch_size, num_paths, embedding_dim)
        """
        if self._pooling_type == "average":
            return self.distributed_pooling(inputs, mask=mask)

        return self.distributed_pooling(inputs)


    def compute_mask(self, inputs, mask=None):
        """Compute the updated mask.

        Args:
            inputs (Tensor): The path tensor. Must be of shape
                (batch_size, num_paths, max_path_len, embedding_dim)
            mask (Tensor): Mask tensor of shape (batch_size, num_paths, max_path_len)

        Returns:
            (Tensor) Mask tensor of shape (batch_size, num_paths)
        """
        if mask is None:
            return mask

        return tf.math.reduce_any(mask, axis=-1)




class NCE(K.layers.Layer):
    """The Node Context Embedding layer embeds an entire node context.

    The node context of a node A consist out of the following:

    1. root_path: A path of nonterminal symbols from the root of the AST to the parent of A.
    2. leaf_paths: A set of paths where the first symbol is a terminal symbol and the remaining
        symbols are nonterminal symbol.

    Typically each leaf path is either a path from the leaf to the root of the AST or from the
    leaf to the parent of A. The terminal and nonterminal symbols of a leaf path may either be
    embedded jointly or seperately and concatenate afterwards. It is also common to add some
    information regarding the position in the AST to each symbol (often called the index of
    the symbol.) Common positional information for a symbol is its position among its siblings
    or an enumeration based on breadth or depth first search.

    In case positional information is provided, we recommend to use the cosy.layers.SymbolEmbedding
    layer. Otherwise the tensorflow.keras.layers.Embedding layer should suffice.

    The leaf path and root path embedding layers need to reduce the dimensionality of the input
    tensor. Common candidates are pooling or recurrent layers.

    Args:
        terminal_embedding (Layer): The layer used to embed the terminal symbols.
        nonterminal_embedding (Layer): The layer used to embed the nonterminal symbols.
        leaf_path_embedding (Layer): The layer used to contract the leaf paths.
        root_path_embedding (Layer): The layer used to contract the root paths.
        apply_leaf_mask (bool): If the terminal/nonterminal embedding layers produce a mask
            and this is True the leaf paths will be masked
        apply_root_mask (bool): If the terminal/nonterminal embedding layers produce a mask
            and this is True the root path will be masked
        use_joint_embedding (bool): If True the terminal and nonterminal symbols in a leaf path
            will be embedded jointly.
    """
    def __init__(self,
                 terminal_embedding,
                 nonterminal_embedding,
                 leaf_path_embedding,
                 root_path_embedding,
                 apply_leaf_mask=False,
                 apply_root_mask=True,
                 use_joint_embedding=False,
                 **kwargs):
        super().__init__(**kwargs)
        self.terminal_embedding = terminal_embedding
        self.nonterminal_embedding = nonterminal_embedding
        self.leaf_path_embedding = leaf_path_embedding

        self.root_path_embedding = root_path_embedding

        self._apply_leaf_mask = apply_leaf_mask
        self._apply_root_mask = apply_root_mask
        self._use_joint_embedding = use_joint_embedding


    @property
    def use_joint_embedding(self):
        """If True the terminal and nonterminal symbols in a leaf path will be embedded jointly."""
        return self._use_joint_embedding

    @property
    def apply_leaf_mask(self):
        """If True the leaf paths will be masked.

        If the terminal/nonterminal embedding layers do not produce a mask this has no effect.
        """
        return self._apply_leaf_mask

    @property
    def apply_root_mask(self):
        """If True the root path will be masked.

        If the terminal/nonterminal embedding layers do not produce a mask this has no effect.
        """
        return self._apply_root_mask


    def call(self, inputs):
        """Compute the node context embedding of the inputs.

        Args:
            inputs (pair) A triple of either tensors or pairs of tensors.

        Returns:
            (tuple) A pair of tensors containing the leaf path embedding and root path embedding.
        """
        terminals, nonterminals, root = inputs

        if self._use_joint_embedding:
            leaf_paths = self._compute_joint_paths(terminals, nonterminals)
        else:
            leaf_paths = self._compute_paths(terminals, nonterminals)

        emb_root = self.nonterminal_embedding(root)

        if self._apply_root_mask:
            mask = self.nonterminal_embedding.compute_mask(root)
            root_path = self.root_path_embedding(emb_root, mask=mask)
        else:
            root_path = self.root_path_embedding(emb_root)

        return leaf_paths, root_path


    def compute_output_shape(self, input_shape):
        """Compute the shape of the output tensor.

        Args:
            input_shape (tuple): The shape of the input tensor.

        Returns.
            (tuple) The shape of the output tensor.
        """
        if self._use_joint_embedding:
            leaf_path_shape = self._compute_joint_shape(input_shape[0], input_shape[1])
        else:
            leaf_path_shape = self._compute_shape(input_shape[0], input_shape[1])

        root_shape = self.nonterminal_embedding.compute_output_shape(input_shape[2])
        root_path_shape = self.root_path_embedding.compute_output_shape(root_shape)

        return leaf_path_shape, root_path_shape


    def _compute_joint_paths(self, terminals, nonterminals):
        emb_terminals = self.terminal_embedding(terminals)
        emb_nonterminals = self.nonterminal_embedding(nonterminals)

        symbols = tf.concat([emb_terminals[:,:,tf.newaxis,...], emb_nonterminals], 2)

        if not self._apply_leaf_mask:
            return self._time_distributed(symbols)

        mask = self._compute_joint_mask(terminals, nonterminals)
        return self._time_distributed(symbols, mask=mask)


    def _compute_paths(self, terminals, nonterminals):
        emb_terminals = self.terminal_embedding(terminals)
        emb_nonterminals = self.nonterminal_embedding(nonterminals)

        if self._apply_leaf_mask:
            mask = self.nonterminal_embedding.compute_mask(nonterminals)
            nonterminal_paths = self._time_distributed(emb_nonterminals, mask=mask)
        else:
            nonterminal_paths = self._time_distributed(emb_nonterminals)

        return tf.concat([emb_terminals, nonterminal_paths], -1)


    def _compute_joint_shape(self, terminal_shape, nonterminal_shape):
        shape = list(self.nonterminal_embedding.compute_output_shape(nonterminal_shape))
        shape[2] += 1 # Take concatenation with terminal symbols into account

        return (shape[0],) + self._time_distributed_shape(tuple(shape))[1:]


    def _compute_shape(self, terminal_shape, nonterminal_shape):
        tshape = self.terminal_embedding.compute_output_shape(terminal_shape)
        nshape = self.nonterminal_embedding.compute_output_shape(nonterminal_shape)

        lshape = (nshape[0],) + self._time_distributed_shape(nshape)[1:]

        return lshape[:-1] + (tshape[-1] + lshape[-1],)


    def _compute_joint_mask(self, terminals, nonterminals):
        terminal_mask = self.terminal_embedding.compute_mask(terminals)
        nonterminal_mask = self.nonterminal_embedding.compute_mask(nonterminals)

        if (terminal_mask is None) and (nonterminal_mask is None):
            return None

        if terminal_mask is None:
            terminal_mask = tf.ones(terminals.shape, tf.bool)

        if nonterminal_mask is None:
            nonterminal_mask = tf.ones(nonterminals.shape, tf.bool)

        return tf.concat([terminal_mask[:,:,tf.newaxis,...], nonterminal_mask], 2)


    def _time_distributed(self, inputs, mask=None):
        original_inputs = inputs

        inputs = tf.reshape(inputs, self._get_input_shape_tuple(inputs))

        if cu.has_arg(self.leaf_path_embedding.call, "mask") and mask is not None:
            mask = tf.reshape(mask, self._get_input_shape_tuple(mask))
            output = self.leaf_path_embedding(inputs, mask=mask)
        else:
            output = self.leaf_path_embedding(inputs)

        output_shape = self._time_distributed_shape(K.backend.int_shape(original_inputs))
        output = tf.reshape(output, self._get_output_shape_tuple(original_inputs, output_shape))

        return output


    def _get_input_shape_tuple(self, tensor, start_idx=2):
        int_shape = K.backend.int_shape(tensor)[start_idx:]

        if not any(not s for s in int_shape):
            return (-1,) + tuple(int_shape)

        shape = K.backend.shape(tensor)
        int_shape = list(int_shape)

        for i, s in enumerate(int_shape):
            if not s:
                int_shape[i] = shape[start_idx + i]

        return (-1,) + tuple(int_shape)


    def _get_output_shape_tuple(self, tensor, output_shape):
        output_shape = list(output_shape)

        output_shape[0] = -1
        output_shape[1] = K.backend.shape(tensor)[1]

        return tuple(output_shape)


    def _time_distributed_shape(self, input_shape):
        time_step_shape = (input_shape[0],) + input_shape[2:]
        output_shape = self.leaf_path_embedding.compute_output_shape(time_step_shape)
        return (-1, input_shape[1]) + tuple(output_shape[1:])




class PoolingNCE(NCE):
    """A variation of the NCE embedding that uses pooling to embed the paths.

    Args:
        terminal_embedding (Layer): The layer used to embed the terminal symbols.
        nonterminal_embedding (Layer): The layer used to embed the nonterminal symbols.
        use_joint_embedding (bool): If True the terminal and nonterminal symbols in a leaf path
            will be embedded jointly.
        mask_paths (bool): If the terminal/nonterminal embedding layers produce a mask
            and this is True the paths will be masked.
        pooling_type (str): Must be either 'average' or 'max'.
    """
    LAYER_DISPATCHER = {
        "average": K.layers.GlobalAveragePooling1D,
        "max": K.layers.GlobalMaxPooling1D
    }

    ROOT_MASK_DISPATCHER = {
        "average": True,
        "max": False
    }


    def __init__(self,
                 terminal_embedding,
                 nonterminal_embedding,
                 use_joint_embedding=False,
                 mask_paths=False,
                 pooling_type="average",
                 **kwargs):

        if pooling_type not in self.LAYER_DISPATCHER:
            raise ValueError("`pooling_type` is {}, but must be one of {}.".format(
                pooling_type,
                self.LAYER_DISPATCHER
            ))

        pooling = self.LAYER_DISPATCHER[pooling_type]()

        super().__init__(
            terminal_embedding=terminal_embedding,
            nonterminal_embedding=nonterminal_embedding,
            leaf_path_embedding=pooling,
            root_path_embedding=pooling,
            apply_leaf_mask=(self.ROOT_MASK_DISPATCHER[pooling_type] and mask_paths),
            apply_root_mask=(self.ROOT_MASK_DISPATCHER[pooling_type] and mask_paths),
            use_joint_embedding=use_joint_embedding,
            **kwargs
        )




class RecurrentNCE(NCE):
    """A variation of the NCE embedding that uses recurrent layers to embed the paths.

    Args:
        terminal_embedding (Layer): The layer used to embed the terminal symbols.
        nonterminal_embedding (Layer): The layer used to embed the nonterminal symbols.
        use_joint_embedding (bool): If True the terminal and nonterminal symbols in a leaf path
            will be embedded jointly.
        mask_paths (bool): If the terminal/nonterminal embedding layers produce a mask
            and this is True the paths will be masked.
        rnn_type (str): Must be either 'lstm' or 'gru'.
        num_layers (int): The number of recurrent layers.
        dimensions (int or list): The dimensionality of the individual recurrent layers.
            If this is an integer every layer will have the same dimensionality.
    """
    LAYER_DISPATCHER = {
        "lstm": LSTMStack,
        "gru": GRUStack
    }


    def __init__(self,
                 terminal_embedding,
                 nonterminal_embedding,
                 use_joint_embedding=False,
                 mask_paths=False,
                 rnn_type="lstm",
                 num_layers=1,
                 dimensions=256,
                 **kwargs):

        if rnn_type not in self.LAYER_DISPATCHER:
            raise ValueError("`rnn_type` is {}, but must be one of {}.".format(
                rnn_type,
                self.LAYER_DISPATCHER
            ))

        rnn = self.LAYER_DISPATCHER[rnn_type](
            num_layers=num_layers,
            dimensions=dimensions,
            use_cudnn_kernel=False
        )

        super().__init__(
            terminal_embedding=terminal_embedding,
            nonterminal_embedding=nonterminal_embedding,
            leaf_path_embedding=rnn,
            root_path_embedding=rnn,
            apply_leaf_mask=mask_paths,
            apply_root_mask=True,
            use_joint_embedding=use_joint_embedding,
            **kwargs
        )
