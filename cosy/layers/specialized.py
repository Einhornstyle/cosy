# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import tensorflow as tf
from tensorflow import keras as K




class KarelResidualBlock(K.layers.Layer):
    """A residual block with 3 identical layers, used by the KarelModel.

    Args:
        **kwargs: Keyword arguments passed to the parent class tf.keras.layers.Layer.
    """
    PARAMETERS = {
        "filters": 64,
        "kernel_size": (3, 3),
        "padding": "same",
        "activation": K.activations.relu
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.conv1 = K.layers.Conv2D(**self.PARAMETERS)
        self.add1 = K.layers.Add()

        self.conv2 = K.layers.Conv2D(**self.PARAMETERS)
        self.add2 = K.layers.Add()

        self.conv3 = K.layers.Conv2D(**self.PARAMETERS)
        self.add3 = K.layers.Add()


    def call(self, inputs):
        """Feed the inputs through the residual block.

        Args:
            inputs (Tensor): A 4D inputs tensor.

        Returns:
            (Tensor) Another 4D tensor with the same shape as the input tensor.
        """
        result1 = self.conv1(inputs)
        result1 = self.add1([inputs, result1])

        result2 = self.conv2(result1)
        result2 = self.add2([result1, result2])

        result3 = self.conv3(result2)
        result3 = self.add3([result2, result3])

        return result3




class KarelEncoder(K.layers.Layer):
    """An encoder for I/O examples containing grid worlds.

    Args:
        **kwargs: Keyword arguments passed to the parent class tf.keras.layers.Layer.
    """
    PARAMETERS = {
        "filters": 32,
        "kernel_size": (3, 3),
        "padding": "same",
        "activation": K.activations.relu
    }

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.iconv = K.layers.Conv2D(**self.PARAMETERS)
        self.oconv = K.layers.Conv2D(**self.PARAMETERS)
        self.concatenate = K.layers.Concatenate()

        self.rblock1 = KarelResidualBlock()
        self.add1 = K.layers.Add()

        self.rblock2 = KarelResidualBlock()
        self.add2 = K.layers.Add()

        self.flatten = K.layers.Flatten()
        self.dense = K.layers.Dense(512)


    def call(self, inputs, reshape_inputs=True):
        """Encode the given I/O examples.

        The `inputs` argument must be a list or tuple with 2 tensors with the folowing shapes

        * input examples: inputs[0].shape = [batch_size, num_examples, width, height, 16]
        * output examples: inputs[1].shape = [batch_size, num_examples, width, height, 16]

        Args:
            inputs (list or tuple): A list of Tensors (see above).
            reshape_inputs (bool): If True the reshapes I/O tensors to (batch_size x num_examples).

        Returns:
            (Tensor) Encoding with the shape [batch_size x num_examples, width, height, 16].
        """
        idata, odata = inputs

        if reshape_inputs:
            idata = tf.reshape(idata, [-1] + list(idata.shape)[2:])
            odata = tf.reshape(odata, [-1] + list(odata.shape)[2:])

        iconv_result = self.iconv(idata)
        oconv_result = self.oconv(odata)
        ioconv_result = self.concatenate([iconv_result, oconv_result])

        rblock1_result = self.rblock1(ioconv_result)
        rblock1_result = self.add1([ioconv_result, rblock1_result])

        rblock2_result = self.rblock2(rblock1_result)
        rblock2_result = self.add2([rblock1_result, rblock2_result])

        result = self.flatten(rblock2_result)
        result = self.dense(result)
        return  result




class KarelDecoder(K.layers.Layer):
    """A decoder for a encoded I/O sequence and a Karel program.

    First the programm will be embedded and together with the encoded I/O examples
    it will be feed trough an RNN.

    Args:
        num_tokens (int): The maximal number of tokens in per input sentence.
        num_examples (int): The number of I/O examples per sentence.
        **kwargs: Keyword arguments passed to the parent class tf.keras.layers.Layer.
    """
    PARAMETERS = {
        "units": 256
    }

    def __init__(self, num_tokens, num_examples, **kwargs):
        super().__init__(**kwargs)

        self.num_tokens = num_tokens
        self.num_examples = num_examples

        self.embedding = K.layers.Embedding(num_tokens, self.PARAMETERS["units"], mask_zero=True)
        self.concatenate = K.layers.Concatenate()

        self.lstm1 = K.layers.LSTM(self.PARAMETERS["units"], return_sequences=True)
        self.lstm2 = K.layers.LSTM(self.PARAMETERS["units"])

        self.maxpool = K.layers.GlobalMaxPool1D()
        self.dense = K.layers.Dense(num_tokens, activation=K.activations.softmax)


    def call(self, inputs, mask=None):
        """Decode the I/O pair encoding alongside the corresponding programs.

        Note:

        The `inputs` argument must be a list or tuple with 2 tensors with the folowing shapes

        * I/O embedding: inputs[0].shape = [batch_size x num_examples, width, height, 16]
        * programs: inputs[1].shape = [batch_size, num_tokens]

        Args:
            inputs (list or tuple): A list of Tensors (see above).
            mask (Tensor): Mask of the programs tensor. Will be compute if not provided.

        Returns:
            (Tensor): Probabilities each grammar token with shape [batch_size, 52].
        """
        encoding, programs = inputs

        # [BxN, 512] -> [BxN, L, 512]
        encoding = K.backend.tile(K.backend.expand_dims(encoding, 1), [1, programs.shape[1], 1])
        # [B, L] -> [B, L, 256] -> [BxN, L, 256]
        programs = K.backend.tile(self.embedding(programs), [self.num_examples, 1, 1])
        # [BxN, L, 512], [BxN, L, 256] -> [BxN, L, 768]
        combined = self.concatenate([encoding, programs])

        if mask is None:
            mask = self.embedding.compute_mask(combined)[:, :, 0]
        else:
            mask = K.backend.tile(mask, [self.num_examples, 1])

        # [BxN, L, 768] -> [BxN, L, 256]
        result = self.lstm1(combined, mask=mask)
        # [BxN, L, 256] -> [BxN, 256]
        result = self.lstm2(result, mask=mask)

        # [BxN, 256] -> [B, N, 256]
        result = tf.reshape(result, [-1, self.num_examples,256])
        # [B, N, 256] -> [B, 256]
        result = self.maxpool(result)
        # [B, 256] -> [B, 52]
        result = self.dense(result)

        return result
