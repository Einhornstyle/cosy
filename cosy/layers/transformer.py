# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import numpy as np
import tensorflow as tf
from tensorflow import keras as K




class ScaledDotProductAttention(K.layers.Layer):
    """Computes scaled dot product attention as used in the original transformer.

    Specifically, scaled dot product attention is defined as::

            Attention(Q, K, V) := softmax_k((Q * K^T) / sqrt(d_k)) * V

    Q, K and V stand for query, key and value and are all tensors.
    d_k is the depth dimension of the key tensor.
    """
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


    def call(self, query, key, value, mask=None):
        """Calculate the scaled attention output and attention weights.

        Query, key and value must have matching leading dimensions.
        Furthermore, key and value must have matching penultimate dimension.
        The mask may be a padding or look ahead mask, each of which has different shapes.
        In either case the mask shape must be broadcastable for addition tough.

        Args:
            query (Tensor): The query tensor with shape = (..., seq_len_q, depth)
            key (Tensor): The key tensor with shape = (..., seq_len_k, depth)
            value (Tensor): The value tensor with shape = (..., seq_len_v, depth_v)
            mask (Tensor): Float tensor with shape broadcastable to (..., seq_len_q, seq_len_k).

        Returns:
            ((Tensor, Tensor)): The output and the attention weights.
        """
        query_times_key = tf.matmul(query, key, transpose_b=True) # (..., seq_len_q, seq_len_k)

        # scale query_times_key
        scaling = tf.cast(tf.shape(key)[-1], tf.float32)
        scaled_logits = query_times_key / tf.math.sqrt(scaling)

        # we scale the mask with -1e9 (close to negative infinity) to also zero out cells
        # with large negative output after applying the softmax later on.
        if mask is not None:
            mask = tf.cast(tf.logical_not(mask), dtype=scaled_logits.dtype)
            scaled_logits += (mask * -1e9)

        # softmax is normalized on last axis so thate the scores add up to 1
        attention_weights = tf.nn.softmax(scaled_logits, axis=-1) # (..., seq_len_q, seq_len_k)

        output = tf.matmul(attention_weights, value) # (..., seq_len_q, depth_v)

        return output, attention_weights




class MultiHeadAttention(K.layers.Layer):
    """Computes multi-head attention as it used in the transformer architecture.

    Similar to regular attention we start with a query, key and value triple. Each tensor
    is feed through a linear layer and consecutively split into several `heads`.
    Each head is then feed through an implementation of attention. By default scaled dot product
    attention is used. Splitting query, key and value into multiple heads allows to jointly attend
    to information at different positions.

    Args:
        dimension (int): The output dimension of the linear layers.
        num_heads (int): The number of heads.
        attention (Layer): The attention layer that will be used internally. Default is scaled dot
            product attention.
    """
    def __init__(self, dimension, num_heads, attention=ScaledDotProductAttention(), **kwargs):
        super().__init__(**kwargs)

        assert dimension % num_heads == 0

        self.dimension = dimension
        self.num_heads = num_heads
        self.depth = dimension // num_heads

        self.dense_query = K.layers.Dense(dimension)
        self.dense_key = K.layers.Dense(dimension)
        self.dense_values = K.layers.Dense(dimension)

        self.attention = attention

        self.dense_final = K.layers.Dense(dimension)


    def call(self, query, key, value, mask=None, attention_kwargs=None):
        """Calculate the multi-head attention output and attention weights.

        The shapes of the input tensors a given in there most general form.
        For self attention we obviously have::

            seq_len_q = seq_len_k = seq_len_v and depth = depth_v

        Args:
            query (Tensor): The query tensor with shape = (..., seq_len_q, depth)
            key (Tensor): The key tensor with shape = (..., seq_len_k, depth)
            value (Tensor): The value tensor with shape = (..., seq_len_v, depth_v)
            mask (Tensor): Float tensor with shape broadcastable to (..., seq_len_q, seq_len_k).
            attention_kwargs (dict): A dictionary of keywords argument that are forwarded to
                internal attention layer. The attention layer will always receive the query,
                key, value and mask as an argument.
        Returns:
            ((Tensor, Tensor)): The output and the attention weights.
        """
        # {} is a problematic default argument, we use None instead
        if attention_kwargs is None:
            attention_kwargs = {}

        batch_size = tf.shape(query)[0]

        query = self.dense_query(query) # (..., seq_len_q, dimension)
        key = self.dense_key(key)     # (..., seq_len_k, dimension)
        value = self.dense_values(value) # (..., seq_len_v, dimension)

        query = self.split_heads(query, batch_size) # (..., num_heads, seq_len_q, depth)
        key = self.split_heads(key, batch_size)     # (..., num_heads, seq_len_k, depth)
        value = self.split_heads(value, batch_size) # (..., num_heads, seq_len_v, depth)

        # attention output shape == (..., num_heads, seq_len_q, depth)
        # attention weight shape == (..., num_heads, seq_len_q, seq_len_k)
        output, weights = self.attention(query, key, value, mask, **attention_kwargs)

        # attention output shape == (..., seq_len_q, num_heads, depth)
        output = tf.transpose(output, perm=[0, 2, 1, 3])

        # attention output shape == (..., seq_len_q, dimension)
        output = tf.reshape(output, (batch_size, -1, self.dimension))

        # attention output shape == (..., seq_len_q, dimension)
        output = self.dense_final(output)

        return output, weights


    def split_heads(self, x, batch_size):
        """Split the last dimension into (num_heads, depth).

        Transpose the result such that the shape is (..., num_heads, seq_len, depth).

        Args:
            x (Tensor): The tensor to be split with shape (..., seq_len, dimension)

        Returns:
            (Tensor) The splitted and reshaped tensor.
        """
        x = tf.reshape(x, (batch_size, -1, self.num_heads, self.depth))
        return tf.transpose(x, perm=[0, 2, 1, 3])




class PointWiseFeedForward(K.layers.Layer):
    """A simple network consisting out of two feed forward layers.

    The first layer use a ReLu and the second a linear activation function.

    Args:
        dimension (int): The dimension of the output layer.
        hidden_units (int): The dimension of the hidden layer.
    """
    def __init__(self, dimension, hidden_units, **kwargs):
        super().__init__(**kwargs)

        self.dense_hidden = K.layers.Dense(hidden_units, activation="relu")
        self.dense_output = K.layers.Dense(dimension)


    def call(self, inputs):
        """Feed the input through the two layers.

        Args:
            inputs (Tensor): Any non scalar tensor.

        Returns:
            (Tensor) The processed tensor.
        """
        output = self.dense_hidden(inputs)
        output = self.dense_output(output)
        return output




class TransformerEmbedding(K.layers.Layer):
    """The positional encoding as used in the original transformer.

    In contrast to a RNN we use the positional information of the tokens in the input sequence
    when employing a transformer. To compensate for this we add an explicit positional encoding.


        The positional encodings are defined as::

            pos_encoding_2i = sin(pos * 10000**((-2 * i) / dimension_model))
            pos_encoding_2i+1 = cos(pos * 10000**((-2 * i) / dimension_model))

    Args:
        dimension (int): The dimension of the embedding.
        vocab_size (int): The size of the embedded vocabulary.
        max_position (int): The largest possible position.
    """
    def __init__(self, dimension, vocab_size, max_position, **kwargs):
        super().__init__(**kwargs)

        self.dimension = dimension

        self.embedding = K.layers.Embedding(vocab_size, dimension)
        self.pos_encoding = self.positional_encoding(max_position)


    def call(self, inputs):
        """Compute the embedding of tensor.

        Args:
            inputs (Tensor): A tensor containing encoded tokens.

        Returns:
            (Tensor) The embedded tensor with positional encoding.
        """
        seq_len = tf.shape(inputs)[1]

        # add embedding and position encoding
        output = self.embedding(inputs) # (..., input_seq_len, dimension)
        output *= tf.math.sqrt(tf.cast(self.dimension, tf.float32))
        output += self.pos_encoding[:, :seq_len, :]

        return output


    def get_angles(self, position, idx):
        """Get the angles for a given position and index.

        The angles are defined as::

            pos * 10000**((-2 * idx) / dimension_model)

        Args:
            position (numpy.array): The array of positions.
            idx (numpy.array): The index array.

        Returns:
            (numpy.array) The array of angles.
        """
        angle_rates = 1 / np.power(10000, (2 * (idx//2)) / np.float32(self.dimension))
        return position * angle_rates


    def positional_encoding(self, max_position):
        """Compute the positional encodings.

        The positional encodings are defined as::

            pos_encoding_2i = sin(pos * 10000**((-2 * i) / dimension_model))
            pos_encoding_2i+1 = cos(pos * 10000**((-2 * i) / dimension_model))

        Args:
            max_position (int): The largest possible position.

        Returns:
            (Tensor) The tensor of positional encodings.
        """
        angle_rads = self.get_angles(
            np.arange(max_position)[:, np.newaxis],
            np.arange(self.dimension)[np.newaxis, :]
        )

        # apply sinus to even indices in the array; 2i
        angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])

        # apply cosinus to even indices in the array; 2i+1
        angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])

        positional_encoding = angle_rads[np.newaxis, ...]
        return tf.cast(positional_encoding, dtype=tf.float32)




class TransformerEncoderLayer(K.layers.Layer):
    """The encoder layer as used in the original transformer.

    It consists out of two sublayers:

    1. masked multi-head self attention
    2. point wise feed forward

    Each layer is wrapped with a residual connection followed by a layer normalization on the
    last dimension. This helps to avoid the vanishing gradient problem which arises when stacking
    these encoding layers.

    Args:
        dimension (int): The output dimension of the multi-head attention and point wise feed
            forward network.
        num_heads (int): The number of heads used in the multi-head attention layer.
        hidden_units (int): The dimension of the hidden layer inside the point wise feed forward
            network.
        dropout_rate (float): The dropout rate to be used. Default is 0.1.
        prevent_look_ahead (bool): If True a look ahead mask will be added to the input mask.
        attention (Layer): The attention layer that will be used internally. Default is scaled dot
            product attention.
    """
    def __init__(self,
                 dimension,
                 num_heads,
                 hidden_units,
                 dropout_rate=0.1,
                 prevent_look_ahead=False,
                 attention=ScaledDotProductAttention(),
                 **kwargs):
        super().__init__(**kwargs)

        self._prevent_look_ahead = prevent_look_ahead

        self.mha = MultiHeadAttention(dimension, num_heads, attention)
        self.ffn = PointWiseFeedForward(dimension, hidden_units)

        self.layernorm1 = K.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = K.layers.LayerNormalization(epsilon=1e-6)

        self.dropout1 = K.layers.Dropout(dropout_rate)
        self.dropout2 = K.layers.Dropout(dropout_rate)

        self.supports_masking = True


    def call(self, inputs, training=None, mask=None, attention_kwargs=None):
        """Feed the inputs through the transformer encoding layer.

        Args:
            inputs (Tensor): A tensor containing a sequences of token embeddings
                of shape = (..., seq_len, embedding_dim)
            training (bool): Indicates whether the call is meant for training or inference.
            mask (Tensor): A mask indicating the padding in the input sequence.
            attention_kwargs (dict): A dictionary of keywords argument that are forwarded to
                internal attention layer. The attention layer will always receive the query,
                key, value and mask as an argument.

        Returns:
            (Tensor) The result of feeding the inputs tensor through all sublayers.
        """
        if mask is not None:
            # make mask broadcastable to shape required by mha
            mask = mask[:, tf.newaxis, tf.newaxis, :]

        if self._prevent_look_ahead:
            seq_len = tf.shape(inputs)[1]
            look_ahead_mask = tf.ones((seq_len, seq_len), dtype=tf.bool)
            look_ahead_mask = tf.linalg.band_part(look_ahead_mask, -1, 0)

            mask = look_ahead_mask if mask is None else tf.logical_and(mask, look_ahead_mask)

        # multi head attention stage, attn_output.shape == (..., input_seq_len, dimension)
        attn_output, _ = self.mha(inputs, inputs, inputs, mask, attention_kwargs)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.layernorm1(inputs + attn_output) # (..., input_seq_len, dimension)

        # point wise feed forward stage
        ffn_output = self.ffn(out1) # (..., input_seq_len, dimension)
        ffn_output = self.dropout2(ffn_output, training=training)
        out2 = self.layernorm2(out1 + ffn_output) # (..., input_seq_len, dimension)

        return out2




class TransformerDecoderLayer(K.layers.Layer):
    """The decoder layer as used in the original transformer.

    It consists out of two sublayers:

    1. masked multi-head self attention (with look ahead and padding mask)
    2. masked multi-head attention (only padding mask).
    3. point wise feed forward

    The second multi-head attention layers gets it query from the previous layer, but the key
    and value are the output of the encoder. Each layer is wrapped with a residual connection
    followed by a layer normalization on the last dimension. This helps to avoid the vanishing
    gradient problem which arises when stacking these encoding layers.

    Args:
        dimension (int): The output dimension of the multi-head attention and point wise feed
            forward network.
        num_heads (int): The number of heads used in the multi-head attention layer.
        hidden_units (int): The dimension of the hidden layer inside the point wise feed forward
            network.
        dropout_rate (float): The dropout rate to be used. Default is 0.1.
        attention (Layer): The attention layer that will be used internally. Default is scaled dot
            product attention.
    """
    def __init__(self,
                 dimension,
                 num_heads,
                 hidden_units,
                 dropout_rate=0.1,
                 attention=ScaledDotProductAttention(),
                 **kwargs):
        super().__init__(**kwargs)

        self.mha1 = MultiHeadAttention(dimension, num_heads, attention)
        self.mha2 = MultiHeadAttention(dimension, num_heads, attention)

        self.ffn = PointWiseFeedForward(dimension, hidden_units)

        self.layernorm1 = K.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm2 = K.layers.LayerNormalization(epsilon=1e-6)
        self.layernorm3 = K.layers.LayerNormalization(epsilon=1e-6)

        self.dropout1 = K.layers.Dropout(dropout_rate)
        self.dropout2 = K.layers.Dropout(dropout_rate)
        self.dropout3 = K.layers.Dropout(dropout_rate)


    def call(self,
             inputs,
             training=None,
             mask=None,
             attention_kwargs=None):
        """Feed the inputs through the transformer decoding layer.

        Args:
            inputs (Tuple): A pair containing the tensor with the embedded target sequences
                as first element and the output of the TransformerEncoder as second argument.
            training (bool): Indicates whether the call is meant for training or inference.
            mask (Tuple): A pair containing the padding mask of the target sequences as first
                element and the look ahead mask as second argument.
            attention_kwargs (dict): A dictionary of keywords argument that are forwarded to
                internal attention layer. The attention layer will always receive the query,
                key, value and mask as an argument.

        Returns:
            (tuple) A triple containing the predicted sequence, the first MHA weights and the
                second MHA weights.
        """
        inputs, encoder_output = inputs

        if mask is None:
            padding_mask = None
            look_ahead_mask = None
        else:
            padding_mask = None if mask[0] is None else mask[0][:, tf.newaxis, tf.newaxis, :]
            look_ahead_mask = mask[1]

        # (..., target_seq_len, dimension)
        attn_output1, attn_weights1 = self.mha1(
            inputs,
            inputs,
            inputs,
            look_ahead_mask,
            attention_kwargs
        )

        attn_output1 = self.dropout1(attn_output1, training=training)
        out1 = self.layernorm1(attn_output1 + inputs)

        # (..., target_seq_len, dimension)
        attn_output2, attn_weights2 = self.mha2(
            out1,
            encoder_output,
            encoder_output,
            padding_mask,
            attention_kwargs
        )

        attn_output2 = self.dropout2(attn_output2, training=training)
        out2 = self.layernorm2(attn_output2 + out1)

        # (..., target_seq_len, dimension)
        ffn_output = self.ffn(out2)
        ffn_output = self.dropout3(ffn_output, training=training)
        out3 = self.layernorm3(ffn_output + out2)

        return out3, attn_weights1, attn_weights2


    def compute_mask(self, inputs, mask=None):
        if mask is None:
            return mask

        return (mask[0], None, None)




class TransformerEncoder(K.layers.Layer):
    """The encoder of as used in the original transformer.

    This encoder is just a stack of `num_layers` encoder layers, it will not embed the inputs.

    Args:
        num_layers (int): The number of `EncoderLayers` that are used.
        dimension (int): The output dimension of the multi-head attention and point wise feed
            forward network.
        num_heads (int): The number of heads used in the multi-head attention layer.
        hidden_units (int): The dimension of the hidden layer inside the point wise feed forward
            network.
        dropout_rate (float): The dropout rate to be used. Default is 0.1.
        prevent_look_ahead (bool): If True a look ahead mask will be added to the input mask.
        attention (Layer): The attention layer that will be used internally. Default is scaled dot
            product attention.
    """
    def __init__(self,
                 num_layers,
                 dimension,
                 num_heads,
                 hidden_units,
                 dropout_rate=0.1,
                 prevent_look_ahead=False,
                 attention=ScaledDotProductAttention(),
                 **kwargs):
        super().__init__(**kwargs)

        self.num_layers = num_layers
        self.dimension = dimension

        self.dropout = K.layers.Dropout(dropout_rate)

        self.encoder_layers = [
            TransformerEncoderLayer(
                dimension,
                num_heads,
                hidden_units,
                dropout_rate,
                prevent_look_ahead,
                attention
            )
            for _ in range(num_layers)
        ]

        self.supports_masking = True


    def call(self, inputs, training=None, mask=None, attention_kwargs=None):
        """Feed the inputs through the transformer encoder.

        Args:
            inputs (Tensor): A tensor containing a sequences of token embeddings
                of shape = (..., seq_len, embedding_dim)
            training (bool): Indicates whether the call is meant for training or inference.
            mask (Tensor): A mask indicating the padding in the input sequence.
            attention_kwargs (dict): A dictionary of keywords argument that are forwarded to
                internal attention layer. The attention layer will always receive the query,
                key, value and mask as an argument.

        Returns:
            (Tensor) The result of feeding the inputs tensor through all sublayers.
        """
        output = self.dropout(inputs, training=training)

        for encoder in self.encoder_layers:
            output = encoder(output, training, mask, attention_kwargs)

        return output # (..., input_seq_len, dimension)




class TransformerDecoder(K.layers.Layer):
    """The decoder of as used in the original transformer.

    This decoder is just a stack of `num_layers` encoder layers, it will not embed the inputs.

    Args:
        num_layers (int): The number of `DecoderLayers` that are used.
        dimension (int): The output dimension of the multi-head attention and point wise feed
            forward network.
        num_heads (int): The number of heads used in the multi-head attention layer.
        hidden_units (int): The dimension of the hidden layer inside the point wise feed forward
            network.
        dropout_rate (float): The dropout rate to be used. Default is 0.1.
        attention (Layer): The attention layer that will be used internally. Default is scaled dot
            product attention.
    """
    def __init__(self,
                 num_layers,
                 dimension,
                 num_heads,
                 hidden_units,
                 dropout_rate=0.1,
                 attention=ScaledDotProductAttention(),
                 **kwargs):
        super().__init__(**kwargs)

        self.num_layers = num_layers
        self.dimension = dimension

        self.dropout = K.layers.Dropout(dropout_rate)

        self.decoder_layers = [
            TransformerDecoderLayer(dimension, num_heads, hidden_units, dropout_rate, attention)
            for _ in range(num_layers)
        ]


    def call(self,
             inputs,
             training=None,
             mask=None,
             attention_kwargs=None):
        """Feed the inputs through the transformer decoder.

        Args:
            inputs (Tuple): A pair containing the tensor with the embedded target sequences
                as first element and the output of the TransformerEncoder as second argument.
            training (bool): Indicates whether the call is meant for training or inference.
            mask (Tuple): A pair containing the padding mask of the target sequences as first
                element and the look ahead mask as second argument.
            attention_kwargs (dict): A dictionary of keywords argument that are forwarded to
                internal attention layer. The attention layer will always receive the query,
                key, value and mask as an argument.

        Returns:
            (Tuple) A pair containing the predicted sequences as first elements and the dictionary
                that maps the keys 'decoder_layer_{i}_block_{j}' (i = 1, num_layers, j = 1, 2)
                to the corresponding attention weights.
        """
        inputs, encoder_output = inputs

        attention_weights = {}

        output = self.dropout(inputs, training=training)

        for idx, decoder in enumerate(self.decoder_layers):
            output, block1, block2 = decoder(
                (output, encoder_output),
                training,
                mask,
                attention_kwargs
            )

            attention_weights["decoder_layer_{}_block_1".format(idx+1)] = block1
            attention_weights["decoder_layer_{}_block_2".format(idx+1)] = block2

        return output, attention_weights


    def compute_mask(self, inputs, mask=None):
        if mask is None:
            return mask

        return (mask[0], None)
