# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import tensorflow as tf
from tensorflow import keras as K




class InputSelector(K.layers.Wrapper):
    """Allowes the wrapped layer to perform a index based selection of the input tensors.

    If the inputs argument to layer is a tuple or lists of tensors, this wrapper selects the
    only the tensor at position `index` and feed it into the wrapped layer.

    Args:
        layer (Layer): The layer that will be wrapped.
        index (int): The index of the tensor that should be selected from the inputs when
            calling the layer.
    """
    def __init__(self, layer, index=0, **kwargs):
        super().__init__(layer, **kwargs)
        self.index = index


    def call(self, inputs, *args, **kwargs):
        """Call the internal layer on inputs[self.index].

        Args:
            inputs (tuple or list): A tuple or list of tensors.

        Returns:
            (Tensor) The output tensor of the wrapped layer, if it exists.
        """
        return self.layer(inputs[self.index], *args, **kwargs)


    def compute_mask(self, inputs, mask=None):
        """Compute the mask.

        Args:
            inputs (tuple or list): A tuple or list of tensors.

        Returns.
            (Tensor) The mask as a tensor.
        """
        if mask is not None:
            mask = mask[self.index]

        return self.layer.compute_mask(inputs[self.index], mask=mask)


    def compute_output_shape(self, input_shape):
        """Compute the shape of the output tensor.

        Args:
            input_shape (tuple): The shape of the input tensor.

        Returns.
            (tuple) The shape of the output tensor.
        """
        return self.layer.compute_output_shape(input_shape[self.index])




class InputSeperator(K.layers.Wrapper):
    def __init__(self, layer, **kwargs):
        """Slice a tensor along its last dimension and feed every segment into the wrapped layer.

        Args:
            layer (Layer): The layer that will be wrapped.
        """
        super().__init__(layer, **kwargs)


    def call(self, inputs, *args, **kwargs):
        """Call the internal layer on inputs[...,i] for every i in inputs.shape[-1].

        Args:
            inputs (Tensor): Any tensor with at least 2 dimensions.

        Returns:
            (Tensor) The output tensor of the wrapped layer, if it exists.
        """
        inputs_list = [inputs[..., i] for i in range(inputs.shape[-1])]
        return self.layer(inputs_list, *args, **kwargs)


    def compute_mask(self, inputs, mask=None):
        """Compute the mask.

        Args:
            inputs (Tensor): Any tensor with at least 2 dimensions.

        Returns.
            (Tensor) The mask as a tensor.
        """
        if mask is not None:
            mask = [mask[..., i] for i in range(mask.shape[-1])]

        inputs = [inputs[..., i] for i in range(inputs.shape[-1])]

        return self.layer.compute_mask(inputs, mask=mask)


    def compute_output_shape(self, input_shape):
        """Compute the shape of the output tensor.

        Args:
            input_shape (tuple): The shape of the input tensor.

        Returns.
            (tuple) The shape of the output tensor.
        """
        input_shape_list = [input_shape[:-1] for _ in range(input_shape[-1])]
        return self.layer.compute_output_shape(input_shape_list)




class MultiTimeDistributed(K.layers.Wrapper):
    """A variation of the the TimeDistributed layer that works with tuples and lists of tensors.

    Args:
        layer (Layer): The layer that will be wrapped.
    """
    def __init__(self, layer, **kwargs):
        super().__init__(layer, **kwargs)
        self.wrapped_layer = InputSeperator(layer)
        self.time_distributed = K.layers.TimeDistributed(self.wrapped_layer)


    def call(self, inputs, mask=None):
        """Applies the wrapped layer along time axis of every tensor in the inputs.

        Args:
            inputs (tuple or list): A tuple or list of tensors.

        Returns:
            (Tensor) The output tensor of the wrapped layer, if it exists.
        """
        combined_inputs = tf.concat([t[..., tf.newaxis] for t in inputs], axis=-1)
        return self.time_distributed(combined_inputs, mask=mask)


    def compute_output_shape(self, input_shape):
        """Compute the shape of the output tensor.

        Args:
            input_shape (tuple): The shape of the input tensor.

        Returns.
            (tuple) The shape of the output tensor.
        """
        shape = input_shape[0] + (len(input_shape),)
        return self.time_distributed.compute_output_shape(shape)




class LSTMStack(K.layers.Layer):
    """A stack of LSTM layers.

    Args:
        num_layers (int): The number of lstm layers.
        dimensions (int or list): The dimensionality of the individual LSTM layers.
            If this is an integer every layer will have the same dimensionality.
        use_cudnn_kenel (bool): If True the faster cudnn kernel will be used. However,
            this kernel is not applicable for every input and might raise an error.
    """
    def __init__(self, num_layers, dimensions, use_cudnn_kernel=True, **kwargs):
        super().__init__(**kwargs)

        assert num_layers > 0, "Number of layers must be greater then zero."

        if isinstance(dimensions, list):
            err_msg = "Number of `dimensions` must be one or equal to the number of layers"
            assert num_layers == len(dimensions), err_msg
        else:
            dimensions = [dimensions] * num_layers

        if use_cudnn_kernel:
            constructor = LSTMStack._build_cudnn_lstm
        else:
            constructor = LSTMStack._build_tensorflow_lstm

        self.lstm_stack = [
            constructor(dimensions[idx], return_sequences=True)
            for idx in range(num_layers - 1)
        ]

        self.lstm_stack.append(constructor(dimensions[num_layers-1]))


    def call(self, inputs, mask=None):
        """Feed the inputs through the stack.

        Args:
            inputs (Tensor): The input tensor. This tensor should represent sequences.
            mask (Tensor9): The mask for input tensor in case the sequences are padded.

        Returns:
            (Tensor) The result of feeding the inputs tensor through all sublayers.
        """
        result = inputs

        for lstm in self.lstm_stack:
            result = lstm(result, mask=mask)

        return result


    def compute_output_shape(self, input_shape):
        """Compute the shape of the output tensor.

        Args:
            input_shape (tuple): The shape of the input tensor.

        Returns.
            (tuple) The shape of the output tensor.
        """
        for lstm in self.lstm_stack:
            input_shape = lstm.compute_output_shape(input_shape)

        return input_shape


    @classmethod
    def _build_cudnn_lstm(cls, units, return_sequences=False):
        return K.layers.LSTM(units, return_sequences=return_sequences)


    @classmethod
    def _build_tensorflow_lstm(cls, units, return_sequences=False):
        return K.layers.RNN(K.layers.LSTMCell(units), return_sequences=return_sequences)




class GRUStack(K.layers.Layer):
    """A stack of GRU layers.

    Args:
        num_layers (int): The number of gru layers.
        dimensions (int or list): The dimensionality of the individual GRU layers.
            If this is an integer every layer will have the same dimensionality.
        use_cudnn_kenel (bool): If True the faster cudnn kernel will be used. However,
            this kernel is not applicable for every input and might raise an error.
    """
    def __init__(self, num_layers, dimensions, use_cudnn_kernel=True, **kwargs):
        super().__init__(**kwargs)

        assert num_layers > 0, "Number of layers must be greater then zero."

        if isinstance(dimensions, list):
            err_msg = "Number of `dimensions` must be one or equal to the number of layers"
            assert num_layers == len(dimensions), err_msg
        else:
            dimensions = [dimensions] * num_layers

        if use_cudnn_kernel:
            constructor = GRUStack._build_cudnn_gru
        else:
            constructor = GRUStack._build_tensorflow_gru

        self.gru_stack = [
            constructor(dimensions[idx], return_sequences=True)
            for idx in range(num_layers - 1)
        ]

        self.gru_stack.append(constructor(dimensions[num_layers-1]))


    def call(self, inputs, mask=None):
        """Feed the inputs through the stack.

        Args:
            inputs (Tensor): The input tensor. This tensor should represent sequences.
            mask (Tensor): The mask for input tensor in case the sequences are padded.

        Returns:
            (Tensor) The result of feeding the inputs tensor through all sublayers.
        """
        result = inputs

        for gru in self.gru_stack:
            result = gru(result, mask=mask)

        return result


    def compute_output_shape(self, input_shape):
        """Compute the shape of the output tensor.

        Args:
            input_shape (tuple): The shape of the input tensor.

        Returns.
            (tuple) The shape of the output tensor.
        """
        for gru in self.gru_stack:
            input_shape = gru.compute_output_shape(input_shape)

        return input_shape


    @classmethod
    def _build_cudnn_gru(cls, units, return_sequences=False):
        return K.layers.GRU(units, return_sequences=return_sequences)


    @classmethod
    def _build_tensorflow_gru(cls, units, return_sequences=False):
        return K.layers.RNN(K.layers.GRUCell(units), return_sequences=return_sequences)




class PathAttention(K.layers.Layer):
    """A variation of attention that is common when processing AST paths.

    The query is an embedded path form the to root to the parent of the node we are
    interest in and the key as well as the value is a set of embedded paths that represent
    the current context. In cases that there is a mismatch between the embedding dimension
    of the query and the key or value the query will be feed through a single dense layer
    to adjust its shape.

    Args:
        activation (activation function): A string or an activation function that will be used
            for the dense layer which adjusts the dimensionality of the query. In case there is
            no mismatch between the dimensionalities this parameter has no effect.
    """
    def __init__(self, activation=None, **kwargs):
        super().__init__(**kwargs)

        self.dimensions_match = True
        self.activation = activation

        self.attention = K.layers.Attention()


    def build(self, input_shape):
        """Maybe build an additional dense layer for the query tensor.

        Args:
            input_shape (tuple): A pair of tensor shapes. The first element specifies the shape
                of the query and second element the shape of key and value.
        """
        query_shape, paths_shape = input_shape

        if query_shape[-1] != paths_shape[-1]:
            self.dimensions_match = False

            self.dense = K.layers.Dense(paths_shape[-1], self.activation)


    def call(self, inputs, mask=None):
        """Feed the paths through the layer.

        Args:
            inputs (tuple): A pair of tensors. The first one beeing the path from the root
                and the second one must represent a context of several paths over which to attend.

        Returns:
            (Tensor) The result of feeding the inputs tensor through all sublayers.
        """
        query, paths = inputs

        if not self.dimensions_match:
            query = self.dense(query)

        return self.attention([query[:,tf.newaxis,:], paths], mask=[None, mask])[:,0,:]


    def compute_output_shape(self, input_shape):
        """Compute the shape of the output tensor.

        Args:
            input_shape (tuple): The shape of the input tensor.

        Returns:
            (tuple) The shape of the output tensor.
        """
        return (input_shape[1][0], input_shape[1][-1])




class CosineSimilarity(K.layers.Layer):
    """Compute the cosine similarity between two tensors along a certain axis.

    Args:
        axis (int): The axis at which to compute the cosine similarity. If axis is None
            all dimensions will be used and a scalar is returned.
        keepdims (bool): If true the axis at which the cosine similarity was computed
            is retained with length 1. If false it will be dropped.
    """
    def __init__(self, axis=-1, keepdims=False, **kwargs):
        super().__init__(**kwargs)

        self._axis = axis
        self._keepdims = keepdims


    def call(self, inputs):
        """Compute the cosine similarity between two tensors x and y along the specified axis.

        Args:
            inputs (tuple): A tuple or list containing the two tensors x and y.

        Returns:
            (Tensor) The tensor that contains the cosine similarity.

        Raises:
            (tf.error.InvalidArgumentError): If the shapes or dtypes of the input tensors x and y
                are not compatible.
        """
        x, y = inputs

        xy_mul = tf.reduce_sum(x * y, axis=self._axis, keepdims=True)

        x_norm = tf.math.sqrt(tf.reduce_sum(x * x, axis=self._axis, keepdims=True))
        y_norm = tf.math.sqrt(tf.reduce_sum(y * y, axis=self._axis, keepdims=True))

        cosine_similarity =  xy_mul / (x_norm * y_norm)

        if not self._keepdims:
            cosine_similarity = tf.squeeze(cosine_similarity, axis=self._axis)

        return cosine_similarity
