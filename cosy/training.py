# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import copy
import itertools as it

import tensorflow as tf
from tensorflow import keras as K
from tensorboard.plugins.hparams import api as hp




def grid_search(hparams):
    """Generate the entire hyperparameter grid.

    In case the HParams are continuous ranges only the min and max will be sampled.

    Args:
        hparams (dict): A mapping from argument names to tensorboard HParams.

    Yields:
        (tuple) A tuple of dicts mapping from parameter names/Hparams to values.
    """
    iterators = []

    for name, param in hparams.items():
        if isinstance(param.domain, hp.Discrete):
            data = param.domain.values
        else:
            data = [param.domain.min_value, param.domain.max_value]

        iterators.append([(name, value) for value in data])

    for data in it.product(*iterators):
        yield dict(data)




class HyperParameterTuning:
    """Optimize the hyper parameters of a given model and training procedure.

    Args:
        model (class or function): This may either be a subclass of tf.keras.Model
            or a function returning a tf.keras.Model.
        log_dir (str): The directory where the hparam logs will be written to.
        compiler (function): A function which takes a model as a first argument alongside
            several (h)params and compiles the model.
        model_generator (generator): Must yield the hyperparameter for the model.
        compile_generator (generator): Must yield the hyperparameter for the compiler.
        run_name (str): The logs of the i´th run will be stored in the folder log_dir/run_name{i}.
    """
    def __init__(self,
                 model,
                 log_dir,
                 compiler=None,
                 model_generator=grid_search,
                 compile_generator=grid_search,
                 run_name="run-"):
        self.model = model
        self.log_dir = log_dir
        self.compiler = compiler
        self.model_generator = model_generator
        self.compile_generator = compile_generator
        self.run_name = run_name

        if self.compiler is None:
            self.compiler = self._default_compiler

        self._model_hparams = {}
        self._model_params = {}

        self._compile_hparams = {}
        self._compile_params = {}


    def add_param(self, name, value, model_param=True):
        """Add a new parameter to the model or the compiler.

        Args:
            name (str): The name of the new parameter
            value (any): The value of the new parameter
            model_param (bool): If True the parameter will be added to the model,
                otherwise it will be added to the compiler.
        """
        if model_param:
            self._model_params[name] = value
        else:
            self._compile_params[name] = value


    def add_hparam(self, name, values, discrete=True, alias=None, model_param=True):
        """Add a new hyperparameter to the model or the compiler.

        Args:
            name (str): The name of the new hyperparameter
            value (any): The value of the new hyperparameter. Should either be a pair indicating
                the start and end of a continuous interval or a container.
            discrete (bool): If the parameter is a discrete. E.g. values is a container.
            alias (str): An alias for the parameter that will be used in the log files, defaults
                to the value of `name`.
            model_param (bool): If True the hyperparameter will be added to the model,
                otherwise it will be added to the compiler.
        """
        if model_param:
            self._model_hparams[name] = self._make_hparam(name, values, discrete, alias)
        else:
            self._compile_hparams[name] = self._make_hparam(name, values, discrete, alias)


    def remove_param(self, name, model_param=True):
        """Remove a parameter from the model or compiler.

        Args:
            name (str): The name of the new parameter
            model_param (bool): If True the parameter will be added to the model,
                otherwise it will be added to the compiler.
        """
        if model_param:
            del self._model_params[name]
        else:
            del self._compile_params[name]


    def remove_hparam(self, name, model_param=True):
        """Remove a hyperparameter from the model or compiler.

        Args:
            name (str): The name of the new hyperparameter
            model_param (bool): If True the hyperparameter will be added to the model,
                otherwise it will be added to the compiler.
        """
        if model_param:
            del self._model_hparams[name]
        else:
            del self._compile_hparams[name]


    def log_config(self):
        """Log the hyperparameter configuration.

        It is general a good practice to call this function once before starting with the
        hyperparameteroptimization. In case several script collaberate to explore the
        hyperparamterspace, only one should call this function.
        """
        # log experiment configuration
        with tf.summary.create_file_writer(self.log_dir).as_default():
            iterator = it.chain(self._model_hparams.values(), self._compile_hparams.values())

            hp.hparams_config(
                hparams=list(iterator),
                metrics=[hp.Metric("accuracy", display_name="Accuracy")]
            )


    def run(self, *args, **kwargs):
        """Run the hyperparameteroptimization.

        This will instantiate and compile a model for every hyperparameter tuple and then train
        it via model.fit(*args, **kwargs). The return values of (e.g the history) of the calls to
        fit will be collected and returned. Additionally the results of every trainings run will
        be logged.

        Args:
            *args (arguments): Will be forwarded to the model.fit function
            **kwargs (keyword arguments): Will be forwarded to the model.fit function.

        Returns.
            (list) The stack of history objects as returned model.fit.
        """
        history_stack = []

        for model_hparams in self.model_generator(self._model_hparams):
            model = self.model(**self._model_params, **model_hparams)

            for compile_hparams in self.compile_generator(self._compile_hparams):
                path = os.path.join(self.log_dir, self.run_name + str(len(history_stack)))
                updated_args, updated_kwargs = self._update_arguments(path, args, kwargs)

                self.compiler(model, **self._compile_params, **compile_hparams)
                history_stack.append(model.fit(*updated_args, **updated_kwargs))

                # log the hyperparameters and accuracy of this run
                with tf.summary.create_file_writer(os.path.join(path, "hparams")).as_default():
                    hp.hparams({
                        **{self._model_hparams[k]: v for k, v in model_hparams.items()},
                        **{self._compile_hparams[k]: v for k, v in compile_hparams.items()}
                    })
                    accuracy = history_stack[-1].history["accuracy"][-1]
                    tf.summary.scalar("accuracy", accuracy, step=1)

        return history_stack


    @classmethod
    def _update_arguments(cls, log_dir, args, kwargs):
        # If it exists the sixth positional argument holds the callbacks
        if len(args) == 6:
            # We can not simply deepcopy everything, because some args may be thread locked.
            res_args = [arg for arg in args[:5]]
            res_args.append(copy.deepcopy(args[5]))
            res_args += [arg for i, arg in args[6:]]

            res_args[5] = [
                c if isinstance(c, K.callbacks.Callback) else c(log_dir)
                for c in args[5]
            ]

            return res_args, kwargs

        if "callbacks" in kwargs:
            # same issue as above
            res_kwargs = {k: v for k, v in kwargs.items() if k != "callbacks"}
            res_kwargs["callbacks"] = copy.deepcopy(kwargs["callbacks"])

            res_kwargs["callbacks"] = [
                c if isinstance(c, K.callbacks.Callback) else c(log_dir)
                for c in kwargs["callbacks"]
            ]

            return args, res_kwargs

        return args, kwargs


    @classmethod
    def _make_hparam(cls, name, values, discrete=True, alias=None):
        assert discrete or (len(values) == 2)

        if alias is None:
            alias = name

        if discrete:
            return hp.HParam(alias, hp.Discrete(values))

        min_val, max_val = values[0], values[1]

        if isinstance(min_val, int) and isinstance(max_val, int):
            return hp.HParam(alias, hp.IntInterval(min_val, max_val))

        return hp.HParam(alias, hp.RealInterval(min_val, max_val))


    @classmethod
    def _default_compiler(cls, model, **kwargs):
        model.compile(**kwargs)




class _MeanReciprocalRankBase(K.metrics.Metric):
    """Base class for (Sparse-)MeanReciprocalRank that contains the shared code.

    Args:
        max_rank (int): The maximum rank that should be considered. For any rank r below
            'max_rank' we set (1/r) to zero.
        name (str): Name of the metric instance.
        dtype (tf.dtypes.Dtype): The data type of the metric result. This must be either
            tf.float16, tf.float32 or tf.float64. The default is tf.float32.
    """
    def __init__(self, max_rank=None, name="sparse_mean_reciprocal_rank", dtype=None):
        super().__init__(name=name, dtype=dtype)

        self._max_rank = max_rank

        self._sample_count = self.add_weight(
            name="first_sample",
            initializer="zeros",
            dtype=self.dtype # This will happen internally anyways. We just make it explicit.
        )

        self._reciprocal_ranks = self.add_weight(
            name="reciprocal_rank",
            initializer="zeros",
            dtype=self.dtype # This will happen internally anyways. We just make it explicit.
        )


    @property
    def max_rank(self):
        """The maximum rank that should be considered."""
        return self._max_rank


    @property
    def total(self):
        """The sum of all reciprocal ranks observed since the last state reset."""
        return self._reciprocal_ranks


    @property
    def count(self):
        """The number of samples observed since the last state reset."""
        return self._sample_count


    def _update_state(self, y_true_prob, y_pred, sample_weight=None):
        """Accumulates metric statistics.

        Args:
            y_true_prob (Tensor): The predicted probability of the ground truth values.
                shape = [batch_size, d0, ..., dN-1]
            y_pred (Tensor): The predicted values. shape = [batch_size, d0, ..., dN]
            sample_weight (Tensor): Coefficient for the metric. Must either be a scalar or
                a tensor with shape = [batch_size, d0, ..., dN-1].

        Returns:
            Update op.
        """
        if len(y_pred.shape) > 1:
            y_true_prob = tf.repeat(y_true_prob[..., tf.newaxis], y_pred.shape[-1], axis=-1)

        y_pred_gt_true = tf.cast(tf.math.greater(y_pred, y_true_prob), dtype=self.dtype)

        y_ranks = tf.math.reduce_sum(y_pred_gt_true, axis=-1) + 1

        if self.max_rank is not None:
            y_ranks *= tf.cast(tf.math.less_equal(y_ranks, self.max_rank), dtype=self.dtype)

        y_reciprocal_ranks = tf.math.divide_no_nan(tf.cast(1, dtype=self.dtype), y_ranks)

        if sample_weight is not None:
            sample_weight = tf.cast(sample_weight, dtype=self.dtype)

            y_reciprocal_ranks *= sample_weight

            # We need to make sure that sample_weights.shape == y_reciprocal_ranks.shape
            sample_counts = tf.ones_like(y_reciprocal_ranks, dtype=self.dtype)
            sample_counts = tf.reduce_sum(sample_counts * sample_weight)
        else:
            sample_counts = tf.cast(tf.size(y_reciprocal_ranks), dtype=self.dtype)

        self._sample_count.assign_add(sample_counts)
        self._reciprocal_ranks.assign_add(tf.math.reduce_sum(y_reciprocal_ranks))


    def result(self):
        """Computes and returns the metric value tensor.

        Result computation is an idempotent operation that simply calculates the metric value
        using the state variables.
        """
        return tf.math.divide_no_nan(self._reciprocal_ranks, self._sample_count)




class MeanReciprocalRank(_MeanReciprocalRankBase):
    """Compute the mean reciprocal rank.

    The ground truth values are expected to be represented as one-hot encoded vectors.

    Args:
        max_rank (int): The maximum rank that should be considered. For any rank r below
            'max_rank' we set (1/r) to zero.
        name (str): Name of the metric instance.
        dtype (tf.dtypes.Dtype): The data type of the metric result. This must be either
            tf.float16, tf.float32 or tf.float64. The default is tf.float32.
    """
    def __init__(self, max_rank=None, name="mean_reciprocal_rank", dtype=None):
        super().__init__(max_rank=max_rank, name=name, dtype=dtype)


    def update_state(self, y_true, y_pred, sample_weight=None):
        """Accumulates metric statistics.

        Args:
            y_true (Tensor): Ground truth values. shape = [batch_size, d0, ..., dN]
            y_pred (Tensor): The predicted values. shape = [batch_size, d0, ..., dN]
            sample_weight (Tensor): Coefficient for the metric. Must either be a scalar or
                a tensor with shape = [batch_size, d0, ..., dN-1].

        Returns:
            Update op.
        """
        y_pred = tf.cast(y_pred, dtype=self.dtype)
        y_true = tf.cast(y_true, dtype=self.dtype)
        y_true_prob = tf.reduce_sum(tf.multiply(y_pred, y_true), axis=-1)

        self._update_state(y_true_prob, y_pred, sample_weight=sample_weight)




class SparseMeanReciprocalRank(_MeanReciprocalRankBase):
    """Compute the mean reciprocal rank.

    The ground truth values are expected to be represented as integers.

    Args:
        max_rank (int): The maximum rank that should be considered. For any rank r below
            'max_rank' we set (1/r) to zero.
        name (str): Name of the metric instance.
        dtype (tf.dtypes.Dtype): The data type of the metric result. This must be either
            tf.float16, tf.float32 or tf.float64. The default is tf.float32.
    """
    def __init__(self, max_rank=None, name="sparse_mean_reciprocal_rank", dtype=None):
        super().__init__(max_rank=max_rank, name=name, dtype=dtype)


    def update_state(self, y_true, y_pred, sample_weight=None):
        """Accumulates metric statistics.

        Args:
            y_true (Tensor): Ground truth values. shape = [batch_size, d0, ..., dN-1]
            y_pred (Tensor): The predicted values. shape = [batch_size, d0, ..., dN]
            sample_weight (Tensor): Coefficient for the metric. Must either be a scalar or
                a tensor with shape = [batch_size, d0, ..., dN-1].

        Returns:
            Update op.
        """
        y_pred = tf.cast(y_pred, dtype=self.dtype)
        y_true = tf.cast(y_true, dtype=tf.int64)
        y_true_prob = tf.gather(y_pred, y_true, axis=-1, batch_dims=len(y_true.shape))

        self._update_state(y_true_prob, y_pred, sample_weight=sample_weight)
