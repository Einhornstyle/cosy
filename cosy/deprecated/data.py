# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import json
import inspect
from abc import ABC, abstractmethod

import igraph as ig
import tensorflow as tf

import cosy.preprocessing as cp
import cosy.utils as cu
import cosy.tree as ct




class DatasetFactoryABC(ABC):
    def __init__(self, prefetch=True, shuffle=True, batch=True):
        self.prefetch = prefetch
        self.shuffle = shuffle
        self.batch = batch


    @abstractmethod
    def make_record(self, *args, **kwargs):
        pass


    @abstractmethod
    def load(self, filenames, buffer_size, batch_size, num_threads, compression):
        pass


    def _load(self,
              filenames,
              buffer_size,
              batch_size,
              batch_shape,
              num_threads,
              compression,
              maps=None):
        dataset = tf.data.TFRecordDataset(filenames, compression, None, num_threads)

        if self.prefetch:
            dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)

        if self.shuffle:
            dataset = dataset.shuffle(buffer_size)

        if maps is not None:
            for map_fn in maps:
                dataset = dataset.map(map_fn, num_threads)

        if self.batch:
            dataset = dataset.padded_batch(batch_size, batch_shape)

        return dataset


    @classmethod
    def _smallest_int_type(cls, value, allow_uint8=True, allow_none=True):
        if allow_none and (value is None):
            return tf.int64

        if tf.int8.min <= value <= tf.int8.max:
            return tf.int8

        if allow_uint8 and (tf.uint8.min <= value <= tf.uint8.max):
            return tf.uint8

        if tf.int16.min <= value <= tf.int16.max:
            return tf.int16

        if tf.int32.min <= value <= tf.int32.max:
            return tf.int32

        if tf.int64.min <= value <= tf.int64.max:
            return tf.int64

        raise ValueError("The provided value is larger then largest possible uint64.")




class DatasetWriter:
    def __init__(self, dataset_factories, filenames, compression=None):
        self._factories = dataset_factories
        self._param_names = [cu.arg_names(f.make_record) for f in self._factories]
        self._writers = []

        if filenames is not None:
            self._open(filenames, compression)


    def broadcast(self, **kwargs):
        for idx, factory in enumerate(self._factories):
            params = {name: kwargs[name] for name in self._param_names[idx]}
            record = factory.make_record(**params)
            self._writers[idx].write(record.SerializeToString())


    def __enter__(self):
        return self


    def __exit__(self, unused_type, unused_value, unused_traceback):
        self.close()


    def flush(self):
        if not self._writers:
            raise ValueError("Writer not initialized or previously closed")

        for writer in self._writers:
            writer.flush()


    def close(self):
        if not self._writers:
            return

        for writer in self._writers:
            writer.close()

        self._writers = []


    def _open(self, filenames, compression=None):
        if not isinstance(compression, list):
            compression = [compression] * len(filenames)

        assert len(filenames) == len(self._factories)
        assert len(compression) == len(self._factories)

        if self._writers:
            self.close()

        for idx, filename in enumerate(filenames):
            self._writers.append(tf.io.TFRecordWriter(filename, compression[idx]))




class ASTPaths(DatasetFactoryABC):
    BATCH_SHAPE = (([None, None], [None, None], [None], [None]), [])

    SEPERATED_BATCH_SHAPE = ([None], [None])

    CONTEXT_DESCRIPTION = {
        "root_indices": tf.io.RaggedFeature(tf.int64),
        "root_symbols": tf.io.RaggedFeature(tf.int64),
        "leaf_symbol": tf.io.FixedLenFeature([], tf.int64),
    }

    SEPERATED_CONTEXT_DESCRIPTION = {
        "terminal_symbols": tf.io.RaggedFeature(tf.int64),
        "terminal_indices": tf.io.RaggedFeature(tf.int64),
    }

    SEQUENCE_DESCRIPTION = {
        "path_symbols": tf.io.RaggedFeature(tf.int64),
        "path_indices": tf.io.RaggedFeature(tf.int64)
    }


    def __init__(self,
                 terminal_vocab_size=None,
                 nonterminal_vocab_size=None,
                 max_sibling_index=None,
                 normalize_datatypes=False,
                 seperate_terminals=True,
                 apply_conversion=False,
                 mask=None,
                 **kwargs):
        super().__init__(**kwargs)

        self.terminal_vocab_size = terminal_vocab_size
        self.nonterminal_vocab_size = nonterminal_vocab_size
        self.max_sibling_index = max_sibling_index

        if normalize_datatypes:
            self.normalize_datatypes()

        self.seperate_terminals = seperate_terminals
        self.apply_conversion = apply_conversion

        self.mask = mask


    @property
    def terminal_vocab_size(self):
        return self._terminal_vocab_size


    @terminal_vocab_size.setter
    def terminal_vocab_size(self, value):
        self._terminal_vocab_size = value
        self._terminal_type = self._smallest_int_type(value)


    @property
    def nonterminal_vocab_size(self):
        return self._nonterminal_vocab_size


    @nonterminal_vocab_size.setter
    def nonterminal_vocab_size(self, value):
        self._nonterminal_vocab_size = value
        self._nonterminal_type = self._smallest_int_type(value)


    @property
    def max_sibling_index(self):
        return self._max_sibling_index


    @max_sibling_index.setter
    def max_sibling_index(self, value):
        self._max_sibling_index = value
        self._index_type = self._smallest_int_type(value)


    @property
    def seperate_terminals(self):
        return self._seperate_terminals


    @seperate_terminals.setter
    def seperate_terminals(self, value):
        self._seperate_terminals = value

        if value:
            self._batch_shape = (
                (*self.SEPERATED_BATCH_SHAPE, *self.BATCH_SHAPE[0]),
                self.BATCH_SHAPE[1]
            )

            self._context_description = {
                **self.SEPERATED_CONTEXT_DESCRIPTION,
                **self.CONTEXT_DESCRIPTION
            }
        else:
            self._batch_shape = self.BATCH_SHAPE
            self._context_description = self.CONTEXT_DESCRIPTION


    def make_record(self, path_symbols, path_indices, root_symbols, root_indices, leaf_symbol):
        context, sequence = self._make_features(
            path_symbols,
            path_indices,
            root_symbols,
            root_indices,
            leaf_symbol
        )

        features = tf.train.Features(feature=context)
        feature_lists = tf.train.FeatureLists(feature_list=sequence)

        return tf.train.SequenceExample(context=features, feature_lists=feature_lists)


    def load(self,
             filenames,
             buffer_size=10000,
             batch_size=64,
             num_threads=None,
             compression="GZIP"):

        maps = [self._parse_batch, self._prepare_features]

        dataset = self._load(
            filenames,
            buffer_size,
            batch_size,
            self._batch_shape,
            num_threads,
            compression,
            maps
        )

        if self.apply_conversion and self.seperate_terminals:
            dataset = dataset.map(self._seperated2joint)

        elif self.apply_conversion and (not self.seperate_terminals):
            dataset = dataset.map(self._joint2seperated)

        dataset = dataset.map(self._cast_features)

        if self.mask is not None:
            dataset = dataset.map(self._apply_mask)

        return dataset


    def normalize_datatypes(self):
        size_type_map = [tf.int8, tf.uint8, tf.int16, tf.int32, tf.int64]
        type_size_map = {dtype: size for size, dtype in enumerate(size_type_map)}

        dtypes = (self._terminal_type, self._nonterminal_type, self._index_type)
        normalized_dtype = size_type_map[max((type_size_map[dtype] for dtype in dtypes))]

        self._terminal_type = normalized_dtype
        self._nonterminal_type = normalized_dtype
        self._index_type = normalized_dtype


    def _apply_mask(self, data, label):
        masked_data = tuple(data[i] for i in range(len(self.mask)) if self.mask[i])
        return masked_data, label


    def _joint2seperated(self, data, label):
        terminals = data[0][:,:,0], data[1][:,:,0]
        nonterminals = data[0][:,:,1:], data[1][:,:,1:]

        return (*terminals, *nonterminals, data[2], data[3], *data[4:]), label


    def _seperated2joint(self, data, label):
        path_symbols = tf.concat([data[0][:,:,tf.newaxis], data[2]], -1)
        path_indices = tf.concat([data[1][:,:,tf.newaxis], data[3]], -1)

        return (path_symbols, path_indices, data[4], data[5], *data[6:]), label


    def _prepare_features(self, context, sequence, shapes=None):
        data = []

        if self._seperate_terminals:
            data.append(context["terminal_symbols"])
            data.append(context["terminal_indices"])

        data.append(sequence["path_symbols"].to_tensor())
        data.append(sequence["path_indices"].to_tensor())
        data.append(context["root_symbols"])
        data.append(context["root_indices"])

        return tuple(data), context["leaf_symbol"]


    def _cast_features(self, data, label):
        cast_data = []

        if len(data) == 6:
            cast_data.append(tf.cast(data[0], self._terminal_type))
            cast_data.append(tf.cast(data[1], self._index_type))
            cast_data.append(tf.cast(data[2], self._nonterminal_type))

        else:
            cast_data.append(tf.cast(data[0], self._terminal_type))

        cast_data.append(tf.cast(data[-3], self._index_type))
        cast_data.append(tf.cast(data[-2], self._nonterminal_type))
        cast_data.append(tf.cast(data[-1], self._index_type))

        return tuple(cast_data), tf.cast(label, self._terminal_type)


    def _parse_batch(self, serialized):
        return tf.io.parse_sequence_example(
            serialized,
            context_features=self._context_description,
            sequence_features=self.SEQUENCE_DESCRIPTION
        )


    def _make_features(self, path_symbols, path_indices, root_symbols, root_indices, leaf_symbol):
        features = self._make_context_features(root_symbols, root_indices, leaf_symbol)

        if self._seperate_terminals:
            terminal_symbols = [path[0] for path in path_symbols]
            terminal_indices = [path[0] for path in path_indices]

            path_symbols = [path[1:] for path in path_symbols]
            path_indices = [path[1:] for path in path_indices]

            features.update(self._make_terminal_features(terminal_symbols, terminal_indices))

        feature_lists = self._make_sequence_features(path_symbols, path_indices)

        return features, feature_lists


    @classmethod
    def _make_context_features(cls, root_symbols, root_indices, leaf_symbol):
        return {
            "root_symbols": cp.int_feature(root_symbols),
            "root_indices": cp.int_feature(root_indices),
            "leaf_symbol": cp.int_feature([leaf_symbol])
        }


    @classmethod
    def _make_terminal_features(cls, terminal_symbols, terminal_indices):
        return {
            "terminal_symbols": cp.int_feature(terminal_symbols),
            "terminal_indices": cp.int_feature(terminal_indices),
        }


    @classmethod
    def _make_sequence_features(cls, path_symbols, path_indices):
        return {
            "path_symbols": cp.int_feature_list(path_symbols),
            "path_indices": cp.int_feature_list(path_indices)
        }




class ASTPathsWithDocs(ASTPaths):
    BATCH_SHAPE = ((*ASTPaths.BATCH_SHAPE[0], [None]), ASTPaths.BATCH_SHAPE[1])

    CONTEXT_DESCRIPTION = {
        **ASTPaths.CONTEXT_DESCRIPTION,
        "docstring": tf.io.RaggedFeature(tf.int64)
    }


    def __init__(self,
                 terminal_vocab_size=None,
                 nonterminal_vocab_size=None,
                 max_sibling_index=None,
                 docstring_vocab_size=None,
                 normalize_datatypes=False,
                 seperate_terminals=True,
                 apply_conversion=False,
                 mask=None,
                 **kwargs):
        super().__init__(
            terminal_vocab_size,
            nonterminal_vocab_size,
            max_sibling_index,
            normalize_datatypes,
            seperate_terminals,
            apply_conversion,
            mask,
            **kwargs
        )

        self.docstring_vocab_size = docstring_vocab_size


    @property
    def docstring_vocab_size(self):
        return self._docstring_vocab_size


    @docstring_vocab_size.setter
    def docstring_vocab_size(self, value):
        self._docstring_vocab_size = value
        self._docstring_type = self._smallest_int_type(value)


    def make_record(self,
                    path_symbols,
                    path_indices,
                    root_symbols,
                    root_indices,
                    docstring,
                    leaf_symbol):
        context, sequence = self._make_features(
            path_symbols,
            path_indices,
            root_symbols,
            root_indices,
            leaf_symbol
        )

        context["docstring"] = cp.int_feature(docstring)

        features = tf.train.Features(feature=context)
        feature_lists = tf.train.FeatureLists(feature_list=sequence)

        return tf.train.SequenceExample(context=features, feature_lists=feature_lists)


    def _prepare_features(self, context, sequence, shapes=None):
        data, label = super()._prepare_features(context, sequence, shapes)
        return (*data, context["docstring"]), label


    def _cast_features(self, data, label):
        cast_data, cast_label = super()._cast_features(data[:-1], label)
        return (*cast_data, tf.cast(data[-1], self._docstring_type)), cast_label
