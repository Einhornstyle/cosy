# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import tensorflow as tf
from tensorflow import keras as K

from cosy.layers import InputSelector, MultiTimeDistributed




class NonterminalPathEmbedding(K.layers.Layer):
    """DEPRECATED: Use NCE layer to embed AST paths."""
    def __init__(self, nonterminal_layer, path_layer, apply_mask=True, **kwargs):
        super().__init__(**kwargs)

        self.apply_mask = apply_mask

        self.nonterminal_layer = nonterminal_layer
        self.path_layer = path_layer


    def call(self, inputs):
        embedding = self.nonterminal_layer(inputs)

        if self.apply_mask:
            mask = self.nonterminal_layer.compute_mask(inputs)
            result = self.path_layer(embedding, mask=mask)

        else:
            result = self.path_layer(embedding)

        return result


    def compute_output_shape(self, input_shape):
        embedding_shape = self.nonterminal_layer.compute_output_shape(input_shape)
        return self.path_layer.compute_output_shape(embedding_shape)




class TerminalPathEmbedding(K.layers.Layer):
    """DEPRECATED: Use NCE layer to embed AST paths."""
    def __init__(self,
                 terminal_layer,
                 nonterminal_layer,
                 path_layer,
                 terminal_first=True,
                 apply_mask=True,
                 **kwargs):
        super().__init__(**kwargs)

        self.apply_mask = apply_mask

        self.terminal_first = terminal_first

        self.terminal_layer = terminal_layer
        self.nonterminal_layer = nonterminal_layer
        self.path_layer = path_layer


    def call(self, inputs):
        terminals, nonterminals = self._prepare_inputs(inputs)

        terminal_embedding = self.terminal_layer(terminals)[:, tf.newaxis, ...]
        nonterminal_embedding = self.nonterminal_layer(nonterminals)
        embedding = tf.concat([terminal_embedding, nonterminal_embedding], axis=1)

        if self.apply_mask:
            mask = self.nonterminal_layer.compute_mask(inputs)
            result = self.path_layer(embedding, mask=mask)

        else:
            result = self.path_layer(embedding)

        return result


    def _prepare_inputs(self, inputs):
        if tf.is_tensor(inputs):
            return self._split_input_tensor(inputs)

        terminal_inputs, nonterminal_inputs = [], []

        for input_tensor in inputs:
            terminals, nonterminals = self._split_input_tensor(input_tensor)
            terminal_inputs.append(terminals)
            nonterminal_inputs.append(nonterminals)

        return terminal_inputs, nonterminal_inputs


    def _split_input_tensor(self, input_tensor):
        if self.terminal_first:
            return input_tensor[:, 0, ...], input_tensor[:, 1:, ...]

        return input_tensor[:, -1, ...], input_tensor[:, :-1, ...]


    def compute_output_shape(self, input_shape):
        embedding_shape = self.nonterminal_layer.compute_output_shape(input_shape)
        return self.path_layer.compute_output_shape(embedding_shape)




class PathEmbedding(K.layers.Layer):
    """DEPRECATED: Use NCE layer to embed AST paths."""
    def __init__(self,
                 terminal_layer,
                 nonterminal_layer,
                 path_layer,
                 last_terminal_layer=None,
                 apply_mask=True,
                 **kwargs):
        super().__init__(**kwargs)

        self.apply_mask = apply_mask

        if last_terminal_layer is None:
            last_terminal_layer = terminal_layer

        self.first_terminal_layer = terminal_layer
        self.last_terminal_layer = last_terminal_layer
        self.nonterminal_layer = nonterminal_layer
        self.path_layer = path_layer


    def call(self, inputs):
        first, nonterminals, last = self._prepare_inputs(inputs)

        first_embedding = self.first_terminal_layer(first)[:, tf.newaxis, ...]
        nonterminal_embedding = self.nonterminal_layer(nonterminals)
        last_embedding = self.last_terminal_layer(last)[:, tf.newaxis, ...]
        embedding = tf.concat([first_embedding, nonterminal_embedding, last_embedding], axis=1)

        if self.apply_mask:
            mask = self.nonterminal_layer.compute_mask(inputs)
            result = self.path_layer(embedding, mask=mask)

        else:
            result = self.path_layer(embedding)

        return result


    def _prepare_inputs(self, inputs):
        if tf.is_tensor(inputs):
            return self._split_input_tensor(inputs)

        first_inputs, nonterminal_inputs, last_inputs = [], [], []

        for input_tensor in inputs:
            first, nonterminals, last = self._split_input_tensor(input_tensor)
            first_inputs.append(first)
            nonterminal_inputs.append(nonterminals)
            last_inputs.append(last)

        return first_inputs, nonterminal_inputs, last_inputs


    def _split_input_tensor(self, input_tensor):
        return input_tensor[:, 0, ...], input_tensor[:, 1:-1, ...], input_tensor[:, -1, ...]


    def compute_output_shape(self, input_shape):
        embedding_shape = self.nonterminal_layer.compute_output_shape(input_shape)
        return self.path_layer.compute_output_shape(embedding_shape)
