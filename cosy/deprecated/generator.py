# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import sys
import os
import math
import random
import threading
import tensorflow as tf
import igraph as ig

from cosy.algorithms import min_depths, max_depths, min_widths, max_widths
from cosy.tree import TreeAttributes




class Generator:
    """A Generator which generates synthetic datasets for code synthesize.

    Currently the programs are generated randomly based on a normal distribution.

    Args:
        bnf (BNF or NumericBNF): The Backus-Naur form of a context-free grammar.
        min_depth (int): The minimum depth of all generated programs.
        max_depth (int): The maximum depth of all generated programs.
        min_width (int): The minimum width/length of all generated programs.
        max_width (int): The maximum width/length of all generated programs.
        return_parse_trees (bool): If True return the parse trees of the progams.
        seed (object): A seed for the random generator, see random.seed for options.
    """
    def __init__(self,
                 bnf,
                 min_depth=0,
                 max_depth=math.inf,
                 min_width=0,
                 max_width=math.inf,
                 return_parse_trees=False,
                 seed=None):
        # establish pre-conditions
        if min_depth > max_depth or min_depth < 0:
            raise ValueError("Invalid depths, it must hold that 0 <= min_depth <= max_depth")

        self._bnf = bnf
        self._cached_grammar = self._bnf.grammar

        self._min_depth = min_depth
        self._max_depth = max_depth

        self._min_width = min_width
        self._max_width = max_width

        self._return_parse_trees = return_parse_trees

        self._expansion_map = None
        self._compute_expansion_map()

        self._seed = seed
        random.seed(seed)


    @property
    def bnf(self):
        """The Backus-Naur form representing the grammar."""
        return self._bnf


    @bnf.setter
    def bnf(self, value):
        self._bnf = value
        self._cached_grammar = self._bnf.grammar

        self._expansion_map = None
        self._compute_expansion_map()


    @property
    def min_depth(self):
        """The minimum depth of all generated programs."""
        return self._min_depth


    @min_depth.setter
    def min_depth(self, value):
        self._min_depth = value


    @property
    def max_depth(self):
        """The maximum depth of all generated programs."""
        return self._max_depth


    @max_depth.setter
    def max_depth(self, value):
        self._max_depth = value


    @property
    def min_width(self):
        """The minimum width of all generated programs."""
        return self._min_width


    @min_width.setter
    def min_width(self, value):
        self._min_width = value


    @property
    def max_width(self):
        """The maximum width of all generated programs."""
        return self._max_width


    @max_width.setter
    def max_width(self, value):
        self._max_width = value


    @property
    def return_parse_trees(self):
        """If True parse trees will be returned, otherwise sentences will be returned."""
        return self._return_parse_trees


    @return_parse_trees.setter
    def return_parse_trees(self, value):
        self._return_parse_trees = value


    @property
    def seed(self):
        """The seed used for the random generator."""
        return self._seed


    @seed.setter
    def seed(self, value):
        self._seed = value
        random.seed(value)


    @property
    def expansion_map(self):
        """The expansion map used for pruning used internally."""
        return self._expansion_map


    def __call__(self, num_programs):
        """Generate a batch of programs.

        A program is represented by a sentence or parse tree.

        Args:
            num_programs (int): The amount of programs to generate.

        Returns:
            (list) A list of sentences or parse trees.
        """
        result = []

        for _ in range(num_programs):
            vertices = []
            edges = []

            self._recursive_generate(
                vertices,
                edges,
                self._bnf.start_symbol,
                0,
                self._min_width,
                self._max_width
            )

            tree = ig.Graph(directed=True)
            tree.add_vertices(len(vertices))
            tree.vs[TreeAttributes.SYMBOL] = vertices
            tree.add_edges(edges)

            if self._return_parse_trees:
                result.append(tree)
            else:
                result.append(self._bnf.make_sentence(tree))

        return result


    def _recursive_generate(self, vertices, edges, nonterminal, depth, min_width, max_width):
        """Helper function for generate. Creates the parse trees recursively.

        Args:
            vertices (list): A list of vertices represented by their associated symbol.
            edges (list): A list of edges represented as pairs of source and tareget vertex id.
            nonterminal (str or int): A nonterminal symbol from the BNF.
            depth (int): The current recursion depth.
            min_width (int): The minimum width/length for the generated subprograms.
            max_width (int): The maximum width/length for the generated subprograms.

        Returns:
            (int) The index of a vertex in the parse tree.
        """
        properties = self._expansion_map[nonterminal]

        min_depth = self._min_depth - depth
        max_depth = self._max_depth - depth

        # prune candidates which have no derivation within the specified ranges
        candidates = [
            idx for idx, stats in enumerate(properties)
            if  (stats["min_depth"] <= max_depth and stats["max_depth"] >= min_depth)
            and (stats["min_width"] <= max_width and stats["max_width"] >= min_width)
        ]

        if not candidates:
            raise ValueError("Unable to find terminating derivation under the given constraints.")

        # pick candidate from an uniform distribution
        chosen_one = candidates[random.randint(0, len(candidates) - 1)]

        min_width_list = properties[chosen_one]["min_width_list"]
        max_width_list = properties[chosen_one]["max_width_list"]
        mask = properties[chosen_one]["mask"]

        vertices.append(nonterminal)
        source = len(vertices) - 1

        # each symbol will be allowed to expand with sufficient min and max width
        for idx, symbol in enumerate(self._cached_grammar[nonterminal][chosen_one]):
            if not mask[idx]: # symbol is a terminal
                vertices.append(symbol)
                target = len(vertices) - 1
                edges.append((source, target))
                min_width -= 1
                max_width -= 1
                continue

            # The samples are not independent (and can not be)
            # TODO: Determine if sampling order creates a bias and if so introduce a random shuffle
            lower = max(min_width_list[idx], min_width - sum(min_width_list[idx+1:]))
            upper = min(max_width_list[idx], max_width - sum(max_width_list[idx+1:]))

            lower_width = random.randint(lower, upper)
            upper_width = random.randint(lower_width, upper)

            min_width -= lower_width
            max_width -= upper_width

            subtree_root = self._recursive_generate(
                vertices,
                edges,
                symbol,
                depth + 1,
                lower_width,
                upper_width
            )

            edges.append((source, subtree_root))

        return source


    def _compute_expansion_map(self):
        """Compute the so called expansion map, which contains the information required for pruning.

        Returns:
            (Generator) self
        """
        apply = lambda fun, container, default: fun(container) if container else default

        nts = self._bnf.nonterminal_symbols

        min_depth_map = min_depths(self._bnf)
        max_depth_map = max_depths(self._bnf)

        min_width_map = min_widths(self._bnf)
        max_width_map = max_widths(self._bnf)

        # Note: The pruning has become quite involved over time and as a result this dict has
        # become rather bloated. Maybe we should move this into a separate class?
        self._expansion_map = {nonterminal: [{
            # The depth range for every derivation
            "min_depth": apply(min, [min_depth_map[s]+1 for s in rule if s in nts], 1),
            "max_depth": apply(max, [max_depth_map[s]+1 for s in rule if s in nts], 1),

            # The width range for every derivation (note that width == number of leafs)
            "min_width": sum([min_width_map[s] if s in nts else 1 for s in rule]),
            "max_width": sum([max_width_map[s] if s in nts else 1 for s in rule]),

            # We need to compute partial sums as well, this happens on the fly though
            "min_width_list": [min_width_map[s] if s in nts else 1 for s in rule],
            "max_width_list": [max_width_map[s] if s in nts else 1 for s in rule],

            # This is a particular common check with O(N) complexity hence we cache it
            "mask": [s in nts for s in rule]
        } for rule in rules] for nonterminal, rules in self._cached_grammar.items()}

        return self


    @classmethod
    def _convert_inf(cls, value):
        return sys.maxsize if value == math.inf else value




def generate(bnf,
             path,
             file_prefix,
             num_files,
             programs_per_file,
             num_threads=1,
             min_depth=0,
             max_depth=math.inf,
             min_width=0,
             max_width=math.inf,
             seed=None):
    """Generate a bunch of programs and save them as TensorFlow records.

    Note:
        It is highly (!) recommend to specify a maximal depth.

    Args:
        bnf (BNF or NumericBNF): The Backus-Naur form of a context-free grammar.
        path (str): The path where the generated records will be saved to.
        file_prefix (str): A common prefix, which will be used for all record files.
        num_files (int): How many record files shall be generated.
        programs_per_file (int): How many programs will be stored per record file.
        num_threads (int): The number of threads used.
        min_depth (int): The minium depth of all generated programs.
        max_depth (int): The maximum depth of all generated programs.
        min_width (int): The minimum width/length of all generated programs.
        max_width (int): The maximum width/length of all generated programs.
        seed (object): A seed for the random generator, see random.seed for options.
    """
    def thread_run(filename):
        for idx in range(num_files):
            with tf.io.TFRecordWriter(filename + "_{}.tfrecord".format(idx)) as record_writer:
                for _ in range(programs_per_file):
                    # TODO: Support storing different record formats (e.g. parse tree embeddings)
                    record_writer.write(bnf.make_record(*generator(1)))

    generator = Generator(bnf, min_depth, max_depth, min_width, max_width, True, seed)
    prefix = os.path.join(os.path.abspath(path), file_prefix)

    threads = [
        threading.Thread(target=thread_run, args=("{}_{}".format(prefix, tid),))
        for tid in range(num_threads)
    ]

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()
