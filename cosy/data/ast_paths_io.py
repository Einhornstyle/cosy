# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import tensorflow as tf

import cosy.preprocessing as cp

from .record_io import RecordWriter, RecordReader




class ASTPathsWriter(RecordWriter):
    """Creates tfrecord files that store AST path features.

    Args:
        separate_terminals (bool): If True the terminal symbols will be stored in a separate
            tensor. Otherwise they will be appended to path tensor.
    """
    def __init__(self, separate_terminals=True, **kwargs):
        super().__init__(**kwargs)

        self._separate_terminals = separate_terminals


    @property
    def separate_terminals(self):
        """Wether or not to separate the path terminals from the paths."""
        return self._separate_terminals


    @separate_terminals.setter
    def separate_terminals(self, value):
        self._separate_terminals = value


    def make_record(self, path_symbols, path_indices, root_symbols, root_indices, leaf_symbol):
        """Convert the AST path features of a single leaf vertex into a tensorflow record.

        Args:
            path_symbols (list): A list of integers that represent the symbols along a path
                through the AST.
            path_indices (list): A list of integers that hold positional information for each
                symbols in the path.
            root_symbols (list): A list of integers that represent the symbols along the path from
                the AST root vertex to the parent of vertex associated with the `leaf_symbol`.
            root_indices (list): A list of integers that hold positional information for each
                symbol in the root path
            leaf_symbols (int): The terminal symbol associated with the leaf vertex of interest.

        Returns:
            (tf.train.Example) The tensorflow record.
        """
        context, sequence = self._make_features(
            path_symbols,
            path_indices,
            root_symbols,
            root_indices,
            leaf_symbol
        )

        features = tf.train.Features(feature=context)
        feature_lists = tf.train.FeatureLists(feature_list=sequence)

        return tf.train.SequenceExample(context=features, feature_lists=feature_lists)


    def _make_features(self, path_symbols, path_indices, root_symbols, root_indices, leaf_symbol):
        features = self._make_context_features(root_symbols, root_indices, leaf_symbol)

        if self._separate_terminals:
            terminal_symbols = [path[0] for path in path_symbols]
            terminal_indices = [path[0] for path in path_indices]

            path_symbols = [path[1:] for path in path_symbols]
            path_indices = [path[1:] for path in path_indices]

            features.update(self._make_terminal_features(terminal_symbols, terminal_indices))

        feature_lists = self._make_sequence_features(path_symbols, path_indices)

        return features, feature_lists


    @classmethod
    def _make_context_features(cls, root_symbols, root_indices, leaf_symbol):
        return {
            "root_symbols": cp.int_feature(root_symbols),
            "root_indices": cp.int_feature(root_indices),
            "leaf_symbol": cp.int_feature([leaf_symbol])
        }


    @classmethod
    def _make_terminal_features(cls, terminal_symbols, terminal_indices):
        return {
            "terminal_symbols": cp.int_feature(terminal_symbols),
            "terminal_indices": cp.int_feature(terminal_indices),
        }


    @classmethod
    def _make_sequence_features(cls, path_symbols, path_indices):
        return {
            "path_symbols": cp.int_feature_list(path_symbols),
            "path_indices": cp.int_feature_list(path_indices)
        }




class ASTPathsReader(RecordReader):
    """Creates a TFRecordDataset from a set of tfrecord files created with the `ASTPathsWriter`.

    Args:
        terminal_vocab_size (int): The size of the terminal symbol vocabulary.
        nonterminal_vocab_size (int): The size of the nonterminal symbol vocabulary.
        max_sibling_index (int): The largest integer used for the positional encoding.
        normalize_datatypes (bool): If true all tensors will have the same data type.
        separate_terminals (bool): If True the terminal symbols will be stored in a separate
            tensor. Otherwise they will be appended to path tensor.
        apply_conversion (bool): This must be set to true if the value of the `separate_terminals`
            argument was different when writting the tfrecord files with the `ASTPathsWriter`.
        mask (list): A list of boolean values that may be used to drop certain tensors from the
            dataset. e.g. [True, True, True, True, False, True] would drop the 5'th tensor.
    """
    BATCH_SHAPE = (([None, None], [None, None], [None], [None]), [])

    SEPARATED_BATCH_SHAPE = ([None], [None])

    CONTEXT_DESCRIPTION = {
        "root_indices": tf.io.RaggedFeature(tf.int64),
        "root_symbols": tf.io.RaggedFeature(tf.int64),
        "leaf_symbol": tf.io.FixedLenFeature([], tf.int64),
    }

    SEPARATED_CONTEXT_DESCRIPTION = {
        "terminal_symbols": tf.io.RaggedFeature(tf.int64),
        "terminal_indices": tf.io.RaggedFeature(tf.int64),
    }

    SEQUENCE_DESCRIPTION = {
        "path_symbols": tf.io.RaggedFeature(tf.int64),
        "path_indices": tf.io.RaggedFeature(tf.int64)
    }


    def __init__(self,
                 terminal_vocab_size=None,
                 nonterminal_vocab_size=None,
                 max_sibling_index=None,
                 normalize_datatypes=False,
                 separate_terminals=True,
                 apply_conversion=False,
                 mask=None,
                 **kwargs):
        super().__init__(**kwargs)

        self.terminal_vocab_size = terminal_vocab_size
        self.nonterminal_vocab_size = nonterminal_vocab_size
        self.max_sibling_index = max_sibling_index

        if normalize_datatypes:
            self.normalize_datatypes()

        self.separate_terminals = separate_terminals
        self.apply_conversion = apply_conversion

        self.mask = mask


    @property
    def terminal_vocab_size(self):
        """The size of the terminal symbol vocabulary."""
        return self._terminal_vocab_size


    @terminal_vocab_size.setter
    def terminal_vocab_size(self, value):
        self._terminal_vocab_size = value
        self._terminal_type = cp.smallest_int_type(value)


    @property
    def nonterminal_vocab_size(self):
        """The size of the nonterminal symbol vocabulary."""
        return self._nonterminal_vocab_size


    @nonterminal_vocab_size.setter
    def nonterminal_vocab_size(self, value):
        self._nonterminal_vocab_size = value
        self._nonterminal_type = cp.smallest_int_type(value)


    @property
    def max_sibling_index(self):
        """The largest integer used for the positional encoding."""
        return self._max_sibling_index


    @max_sibling_index.setter
    def max_sibling_index(self, value):
        self._max_sibling_index = value
        self._index_type = cp.smallest_int_type(value)


    @property
    def separate_terminals(self):
        """Wether or not to separate the path terminals from the paths."""
        return self._separate_terminals


    @separate_terminals.setter
    def separate_terminals(self, value):
        self._separate_terminals = value

        if value:
            self._batch_shape = (
                (*self.SEPARATED_BATCH_SHAPE, *self.BATCH_SHAPE[0]),
                self.BATCH_SHAPE[1]
            )

            self._context_description = {
                **self.SEPARATED_CONTEXT_DESCRIPTION,
                **self.CONTEXT_DESCRIPTION
            }
        else:
            self._batch_shape = self.BATCH_SHAPE
            self._context_description = self.CONTEXT_DESCRIPTION


    def preprocess(self, dataset, num_threads, batch_size=64):
        """Converts the raw records into a 6-tuple of tensors and a label

        Args:
            dataset (TFRecordDataset): The raw (possibly shuffled) dataset.
            num_threads (int): The number of threads that may be used during preprocessing.
            batch_size (int): The batch size of the final tensors.

        Returns:
            (tf.data.TFRecordDataset) The preprocessed dataset.
        """
        dataset = dataset.map(self._parse_batch, num_parallel_calls=num_threads)
        dataset = dataset.map(self._prepare_features, num_parallel_calls=num_threads)
        dataset = dataset.padded_batch(batch_size, self._batch_shape)

        if self.apply_conversion and self.separate_terminals:
            dataset = dataset.map(self._separated2joint, num_parallel_calls=num_threads)

        elif self.apply_conversion and (not self.separate_terminals):
            dataset = dataset.map(self._joint2separated, num_parallel_calls=num_threads)

        dataset = dataset.map(self._cast_features, num_parallel_calls=num_threads)

        if self.mask is not None:
            dataset = dataset.map(self._apply_mask, num_parallel_calls=num_threads)

        return dataset


    def normalize_datatypes(self):
        """Normalizes the datatypes, such that every tensor in the dataset uses the same type."""
        size_type_map = [tf.int8, tf.uint8, tf.int16, tf.int32, tf.int64]
        type_size_map = {dtype: size for size, dtype in enumerate(size_type_map)}

        dtypes = (self._terminal_type, self._nonterminal_type, self._index_type)
        normalized_dtype = size_type_map[max((type_size_map[dtype] for dtype in dtypes))]

        self._terminal_type = normalized_dtype
        self._nonterminal_type = normalized_dtype
        self._index_type = normalized_dtype


    def _apply_mask(self, data, label):
        masked_data = tuple(data[i] for i in range(len(self.mask)) if self.mask[i])
        return masked_data, label


    def _joint2separated(self, data, label):
        terminals = data[0][:,:,0], data[1][:,:,0]
        nonterminals = data[0][:,:,1:], data[1][:,:,1:]

        return (*terminals, *nonterminals, data[2], data[3], *data[4:]), label


    def _separated2joint(self, data, label):
        path_symbols = tf.concat([data[0][:,:,tf.newaxis], data[2]], -1)
        path_indices = tf.concat([data[1][:,:,tf.newaxis], data[3]], -1)

        return (path_symbols, path_indices, data[4], data[5], *data[6:]), label


    def _prepare_features(self, context, sequence, shapes=None):
        data = []

        if self._separate_terminals:
            data.append(context["terminal_symbols"])
            data.append(context["terminal_indices"])

        data.append(sequence["path_symbols"].to_tensor())
        data.append(sequence["path_indices"].to_tensor())
        data.append(context["root_symbols"])
        data.append(context["root_indices"])

        return tuple(data), context["leaf_symbol"]


    def _cast_features(self, data, label):
        cast_data = []

        if len(data) == 6:
            cast_data.append(tf.cast(data[0], self._terminal_type))
            cast_data.append(tf.cast(data[1], self._index_type))
            cast_data.append(tf.cast(data[2], self._nonterminal_type))

        else:
            cast_data.append(tf.cast(data[0], self._terminal_type))

        cast_data.append(tf.cast(data[-3], self._index_type))
        cast_data.append(tf.cast(data[-2], self._nonterminal_type))
        cast_data.append(tf.cast(data[-1], self._index_type))

        return tuple(cast_data), tf.cast(label, self._terminal_type)


    def _parse_batch(self, serialized):
        return tf.io.parse_sequence_example(
            serialized,
            context_features=self._context_description,
            sequence_features=self.SEQUENCE_DESCRIPTION
        )




class ASTPathsDocsWriter(ASTPathsWriter):
    """Creates tfrecord files that store AST path features including the docstring if present.

    Args:
        separate_terminals (bool): If True the terminal symbols will be stored in a separate
            tensor. Otherwise they will be appended to path tensor.
    """
    def __init__(self, separate_terminals, **kwargs):
        super().__init__(**kwargs)

        self._separate_terminals = separate_terminals


    def make_record(self,
                    path_symbols,
                    path_indices,
                    root_symbols,
                    root_indices,
                    docstring,
                    leaf_symbol):
        """Convert the AST path features of a single leaf vertex into a tensorflow record.

        Args:
            path_symbols (list): A list of integers that represent the symbols along a path
                through the AST.
            path_indices (list): A list of integers that hold positional information for each
                symbols in the path.
            root_symbols (list): A list of integers that represent the symbols along the path from
                the AST root vertex to the parent of vertex associated with the `leaf_symbol`.
            root_indices (list): A list of integers that hold positional information for each
                symbol in the root path
            docstring (list): A list of integers that represent the tokens used in the docstring.
            leaf_symbols (int): The terminal symbol associated with the leaf vertex of interest.

        Returns:
            (tf.train.Example) The tensorflow record.
        """
        context, sequence = self._make_features(
            path_symbols,
            path_indices,
            root_symbols,
            root_indices,
            leaf_symbol
        )

        context["docstring"] = cp.int_feature(docstring)

        features = tf.train.Features(feature=context)
        feature_lists = tf.train.FeatureLists(feature_list=sequence)

        return tf.train.SequenceExample(context=features, feature_lists=feature_lists)




class ASTPathsDocsReader(ASTPathsReader):
    """Creates a TFRecordDataset from a set of tfrecord files created with the `ASTPathsWriter`.

    Args:
        terminal_vocab_size (int): The size of the terminal symbol vocabulary.
        nonterminal_vocab_size (int): The size of the nonterminal symbol vocabulary.
        max_sibling_index (int): The largest integer used for the positional encoding.
        docstring_vocab_size (int): The size of the vocabulary used for the docstrings.
        normalize_datatypes (bool): If true all tensors will have the same data type.
        separate_terminals (bool): If True the terminal symbols will be stored in a separate
            tensor. Otherwise they will be appended to path tensor.
        apply_conversion (bool): This must be set to true if the value of the `separate_terminals`
            argument was different when writting the tfrecord files with the `ASTPathsWriter`.
        mask (list): A list of boolean values that may be used to drop certain tensors from the
            dataset. e.g. [True, True, True, True, False, True] would drop the 5'th tensor.
    """
    BATCH_SHAPE = ((*ASTPathsReader.BATCH_SHAPE[0], [None]), ASTPathsReader.BATCH_SHAPE[1])

    CONTEXT_DESCRIPTION = {
        **ASTPathsReader.CONTEXT_DESCRIPTION,
        "docstring": tf.io.RaggedFeature(tf.int64)
    }


    def __init__(self,
                 terminal_vocab_size=None,
                 nonterminal_vocab_size=None,
                 max_sibling_index=None,
                 docstring_vocab_size=None,
                 normalize_datatypes=False,
                 separate_terminals=True,
                 apply_conversion=False,
                 mask=None,
                 **kwargs):
        super().__init__(
            terminal_vocab_size,
            nonterminal_vocab_size,
            max_sibling_index,
            normalize_datatypes,
            separate_terminals,
            apply_conversion,
            mask,
            **kwargs
        )

        self.docstring_vocab_size = docstring_vocab_size


    @property
    def docstring_vocab_size(self):
        """The size of the vocabulary used for the docstrings."""
        return self._docstring_vocab_size


    @docstring_vocab_size.setter
    def docstring_vocab_size(self, value):
        self._docstring_vocab_size = value
        self._docstring_type = cp.smallest_int_type(value)


    def _prepare_features(self, context, sequence, shapes=None):
        data, label = super()._prepare_features(context, sequence, shapes)
        return (*data, context["docstring"]), label


    def _cast_features(self, data, label):
        cast_data, cast_label = super()._cast_features(data[:-1], label)
        return (*cast_data, tf.cast(data[-1], self._docstring_type)), cast_label
