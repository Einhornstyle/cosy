# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from abc import ABC, abstractmethod

import tensorflow as tf

import cosy.preprocessing as cp
import cosy.utils as cu




class RecordWriter(ABC):
    """A record writer creates tfrecord-files from a set of features.

    This is abstract base classes that handles the I/O logic. Derived classes need to overwrite
    the `make_records` method, where the mapping from features to records is encapsulated.

    Args:
        filename (str): The name of the file where the records will be written to.
        compression (str): The compression used for the records. (either: None, 'GZIP' or 'ZLIB')
    """
    def __init__(self, filename=None, compression=None):
        self._writer = None
        self._filename = filename
        self._compression = compression

        self._open()


    @property
    def filename(self):
        """The name of the file where the records will be written to."""
        return self._filename


    @filename.setter
    def filename(self, value):
        if self._filename is not None:
            self.close()

        self._filename = value
        self._open()


    @property
    def compression(self):
        """The compression used for the records. (either: None, 'GZIP' or 'ZLIB')"""
        return self._compression


    @compression.setter
    def compression(self, value):
        self._compression = value
        self._open()


    @abstractmethod
    def make_record(self, **kwargs):
        """Converts a set of features to a tensorflow record.

        This method ist abstract and must be overwritten by derived classes.

        Returns:
            (tf.train.Example) The tensorflow record.
        """
        pass


    def __call__(self, **kwargs):
        """Convert the provided features to a record and write it to disk.

        Args:
            **kwargs (keyword arguments): The features that will be passed to the `make_record`
                method to create the tensorflow record. The keywords must match the arguments of
                `make_record` exactly.
        """
        record = self.make_record(**kwargs)
        self._writer.write(record.SerializeToString())


    def __enter__(self):
        """Support for usage with the `with`-statement"""
        return self


    def __exit__(self, unused_type, unused_value, unused_traceback):
        """Support for usage with the `with`-statement"""
        self.close()


    def flush(self):
        """Flush the writers internal buffer.

        This Happens automatically if the writer is closed.
        """
        if self._writer is None:
            raise ValueError("Writer not initialized or previously closed")

        self._writer.flush()


    def close(self):
        """Close the writer."""
        if self._writer is None:
            return

        self._writer.close()
        self._writer = None


    def _open(self):
        """Create a new TFRecordWriter if `self.filename` is not None"""
        if self._filename is None:
            return

        if self._writer is not None:
            self.close()

        self._writer = tf.io.TFRecordWriter(self._filename, self._compression)




class RecordReader:
    """Reads a set of tfrecord files from disk and creates a TFRecordDataset from them.

    By itself this reader can only shuffle and prefetch the dataset. Additionally preprocessing
    steps may be introduced by derived classes that overwrite the `preprocess` method.

    Args:
        shuffle_buffer_size (int): If provided the dataset will be shuffled using a buffer of
            the specified size.
        prefetch_buffer_size (int): If provided the dataset will be prefetched using a buffer of
            the specified size.
    """
    def __init__(self, shuffle_buffer_size=None, prefetch_buffer_size=None):
        self._shuffle_buffer_size = shuffle_buffer_size
        self._prefetch_buffer_size = prefetch_buffer_size


    @property
    def shuffle_buffer_size(self):
        """If not None the dataset will be shuffled using a buffer of this size."""
        return self._shuffle_buffer_size


    @shuffle_buffer_size.setter
    def shuffle_buffer_size(self, value):
        self._shuffle_buffer_size = value


    @property
    def prefetch_buffer_size(self):
        """If not None the dataset will be prefetched using a buffer of this size."""
        return self._prefetch_buffer_size


    @prefetch_buffer_size.setter
    def prefetch_buffer_size(self, value):
        self._prefetch_buffer_size = value


    def preprocess(self, dataset, num_threads, **kwargs):
        """This is a hook for base classes to introduce additional preprocessing steps.

        Args:
            dataset (TFRecordDataset): The dataset containing the raw records.
                If `shuffle_buffer_size` is not None the dataset will already be shuffled.
            num_threads (int): The number of threads that may be used during preprocessing.
            **kwargs (keyword arguments): Additional arguments may be used in derived classes
                and will be propagated through the `__call__` method.

        Returns:
            (tf.data.TFRecordDataset) The preprocessed dataset.
        """
        return dataset


    def __call__(self,
                 filenames,
                 compression=None,
                 buffer_size=None,
                 num_threads=None,
                 **kwargs):
        """Create a TFRecordDataset from the given files.

        Args:
            filenames (list): A list of filenames corresponding to tfrecord files.
            compression (str): The compression that has been used when creating the records.
                (either: None, 'GZIP' or 'ZLIB')
            buffer_size (int): The number of bytes used for the read buffer of the dataset.
                If not provided a sensible default is chosen.
            num_threads (int): The number of threads that may be used during preprocessing
                and reading of the files.
            **kwargs (keyword arguments): These will be passed to `preprocess` method.
        """
        dataset = tf.data.TFRecordDataset(filenames, compression, buffer_size, num_threads)

        if self._shuffle_buffer_size is not None:
            dataset = dataset.shuffle(self._shuffle_buffer_size)

        dataset = self.preprocess(dataset, num_threads, **kwargs)

        if self._prefetch_buffer_size is not None:
            dataset = dataset.prefetch(self._prefetch_buffer_size)

        return dataset




def broadcast(record_writers, **kwargs):
    """Broadcast a set of features to multiple `RecordWriters`.

    A record writer will receive a feature if the keyword of the feature used in the kwargs
    corresponds to a keyword argument used in the writers `make_record` function. Every writer in
    the provided list must receive a full set of arguments for this function to succeed.

    Args:
        record_writer (list): A list of `RecordWriters`.
        **kwargs (keyword arguments): The features that will be broadcasted.
    """
    for writer in record_writers:
        parameter_names = cu.arg_names(writer.make_record)
        writer(**{name: kwargs[name] for name in parameter_names})




_INT_TYPES = {"uint8", "int8", "int16", "int32", "int64"}

_INT_TYPE_MAP = {
    "bool": tf.bool,
    "uint8": tf.uint8,
    "int8": tf.int8,
    "int16": tf.int16,
    "int32": tf.int32
    # int64 is the default, no need to cast
}

_TYPE_ERR_MSG = "dtype must be one of 'int', 'uint8', 'int8', 'int16', 'int32', 'int64'"\
    ", 'float', 'bytes', 'str' or 'bool'"


def _get_dtype_handler(dtype, is_list=False):
    """Get a preprocessing handler for the specified datatype."""
    if (dtype == "int") or (dtype in _INT_TYPES):
        return cp.int_feature_list if is_list else cp.int_feature

    if dtype == "float":
        return cp.float_feature_list if is_list else cp.float_feature

    if dtype == "bytes":
        return cp.bytes_feature_list if is_list else cp.bytes_feature

    if (dtype == "str") or (dtype == "string"):
        return cp.str_feature_list if is_list else cp.str_feature

    if dtype == "bool":
        return cp.bool_feature_list if is_list else cp.bool_feature

    raise ValueError(_TYPE_ERR_MSG)


def _get_feature_dtype(dtype):
    """Get the appropriate tensorflow record datatype."""
    if (dtype == "int") or (dtype == "bool") or (dtype in _INT_TYPES):
        return tf.dtypes.int64

    if dtype == "float":
        return tf.dtypes.float32

    if (dtype == "bytes") or (dtype == "str") or (dtype == "string"):
        return tf.dtypes.string

    raise ValueError(_TYPE_ERR_MSG)


def _build_dtype_map(dtypes):
    return {
        idx: _INT_TYPE_MAP[t]
        for idx, t in enumerate(dtypes)
        if t in _INT_TYPE_MAP
    }




class SequenceWriter(RecordWriter):
    """Writes one ore more sequences to a tfrecord file.

    The following datatype values are supported::

        'uint8', 'int8', 'int16', 'int32', 'int64', 'int', 'float', 'bytes', 'str' and 'bool'

    Args:
        dtypes (list or str): A list of length `num_sequences` that specifies the datatypes
            of the different sequences as strings. In case all sequences have the same type
            a single string may be passed which is then used as the type for all of them.
        num_sequences (int): The number of sequences.
    """
    def __init__(self, dtypes="int", num_sequences=1, **kwargs):
        super().__init__(**kwargs)

        if not isinstance(dtypes, list):
            dtypes = [dtypes]*num_sequences

        self._dtypes = dtypes
        self._num_sequences = num_sequences

        self._handler = [_get_dtype_handler(t) for t in dtypes]


    @property
    def dtypes(self):
        """A list containing the dtypes of each sequence in order."""
        return self._dtypes


    @property
    def num_sequences(self):
        """The number of sequences."""
        return self._num_sequences


    def make_record(self, sequences):
        """Convert the list of sequences to a tensorflow record.

        In case only one sequence should be written it is not necessary to wrap it into a list.

        Args:
            sequences (list): A list of sequences. Each sequence must be iterable.

        Returns:
            (tf.train.Example) The tensorflow record.
        """
        if (self.num_sequences == 1) and sequences and (not isinstance(sequences[0], list)):
            sequences = [sequences]

        assert len(sequences) == self.num_sequences

        features = tf.train.Features(feature={
            "sequence_{}".format(idx): self._handler[idx](seq)
            for idx, seq in enumerate(sequences)
        })

        return tf.train.Example(features=features)




class SequenceReader(RecordReader):
    """Creates a TFRecordDataset from tfrecord files created with the `SequenceWriter`.

    The following datatype values are supported::

        'uint8', 'int8', 'int16', 'int32', 'int64', 'int', 'float', 'bytes', 'str' and 'bool'

    Args:
        dtypes (list or str): A list of length `num_sequences` that specifies the datatypes
            of the different sequences as strings. In case all sequences have the same type
            a single string may be passed which is then used as the type for all of them.
        num_sequences (int): The number of sequences.
    """
    def __init__(self, dtypes="int", num_sequences=1, **kwargs):
        super().__init__(**kwargs)

        if not isinstance(dtypes, list):
            dtypes = [dtypes]*num_sequences

        self._dtypes = dtypes
        self._num_sequences = num_sequences

        self._features = {
            "sequence_{}".format(idx): tf.io.RaggedFeature(_get_feature_dtype(t))
            for idx, t in enumerate(dtypes)
        }

        self._cast_dtype_map = _build_dtype_map(dtypes)

        self._batch_shape = [None]

        if num_sequences > 1:
            self._batch_shape = tuple(([None] for _ in range(num_sequences)))


    @property
    def dtypes(self):
        """A list containing the dtypes of each sequence in order."""
        return self._dtypes


    @property
    def num_sequences(self):
        """The number of sequences."""
        return self._num_sequences


    def preprocess(self, dataset, num_threads, batch_size=None, drop_remainder=False):
        """Converts the raw records into a tuple that holds sequences represented as tensors.

        Args:
            dataset (TFRecordDataset): The raw (possibly shuffled) dataset.
            num_threads (int): The number of threads that may be used during preprocessing.
            batch_size (int): The batch size of the final tensors. If this is None, the data
                will not be grouped into batches.
            drop_remainder (bool): If True the last batch will be dropped in case it has
                fewer then `batch_size` elements.

        Returns:
            (tf.data.TFRecordDataset) The preprocessed dataset.
        """
        dataset = dataset.map(self._parse_example, num_parallel_calls=num_threads)
        dataset = dataset.map(self._extract_tensors, num_parallel_calls=num_threads)

        if batch_size is not None:
            dataset = dataset.padded_batch(
                batch_size,
                padded_shapes=self._batch_shape,
                drop_remainder=drop_remainder
            )

        return dataset


    def _parse_example(self, serialized):
        return tf.io.parse_example(serialized, self._features)


    def _extract_tensors(self, example):
        tensor_list = []

        for idx in range(self._num_sequences):
            tensor = example["sequence_{}".format(idx)]

            if idx in self._cast_dtype_map:
                tensor = tf.cast(tensor, self._cast_dtype_map[idx])

            tensor_list.append(tensor)

        if len(tensor_list) == 1:
            return tensor_list[0]

        # Tensorflow will convert this to a tuple anyways. We just make this fact explicit.
        return tuple(tensor_list)




class LabeledSequenceWriter(RecordWriter):
    """Writes one ore more sequences together with a label to a tfrecord file.

    The following datatype values are supported::

        'uint8', 'int8', 'int16', 'int32', 'int64', 'int', 'float', 'bytes', 'str' and 'bool'

    Args:
        dtypes (list or str): A list of length `num_sequences` that specifies the datatypes
            of the different sequences as strings. In case all sequences have the same type
            a single string may be passed which is then used as the type for all of them.
        num_sequences (int): The number of sequences.
        label_dtype (str): The datatype of the label. Must be an integer type.
    """
    def __init__(self, dtypes="int", num_sequences=1, label_dtype="int", **kwargs):
        super().__init__(**kwargs)

        if not isinstance(dtypes, list):
            dtypes = [dtypes]*num_sequences

        self._dtypes = dtypes
        self._num_sequences = num_sequences
        self._label_dtype = label_dtype

        self._handler = [_get_dtype_handler(t) for t in dtypes]
        self._label_handler = _get_dtype_handler(label_dtype)


    @property
    def dtypes(self):
        """A list containing the dtypes of each sequence in order."""
        return self._dtypes


    @property
    def num_sequences(self):
        """The number of sequences."""
        return self._num_sequences


    @property
    def label_dtype(self):
        """The dtype of the label."""
        return self._label_dtype


    def make_record(self, sequences, label):
        """Convert the list of sequences and a label to a tensorflow record.

        In case only one sequence should be written it is not necessary to wrap it into a list.

        Args:
            sequences (list): A list of sequences. Each sequence must be iterable.
            label (int): The label of the sequences.

        Returns:
            (tf.train.Example) The tensorflow record.
        """
        if (self.num_sequences == 1) and sequences and (not isinstance(sequences[0], list)):
            sequences = [sequences]

        assert len(sequences) == self.num_sequences

        feature = {
            "sequence_{}".format(idx): self._handler[idx](seq)
            for idx, seq in enumerate(sequences)
        }

        feature["label"] = self._label_handler([label])

        return tf.train.Example(features=tf.train.Features(feature=feature))




class LabeledSequenceReader(RecordReader):
    """Creates a TFRecordDataset from tfrecord files created with the `LabeledSequenceWriter`.

    The following datatype values are supported::

        'uint8', 'int8', 'int16', 'int32', 'int64', 'int', 'float', 'bytes', 'str' and 'bool'

    Args:
        dtypes (list or str): A list of length `num_sequences` that specifies the datatypes
            of the different sequences as strings. In case all sequences have the same type
            a single string may be passed which is then used as the type for all of them.
        num_sequences (int): The number of sequences.
        label_dtype (str): The datatype of the label. Must be an integer type.
    """
    def __init__(self, dtypes="int", num_sequences=1, label_dtype="int", **kwargs):
        super().__init__(**kwargs)

        if not isinstance(dtypes, list):
            dtypes = [dtypes]*num_sequences

        self._dtypes = dtypes
        self._num_sequences = num_sequences
        self._label_dtype = label_dtype

        self._features = {
            "sequence_{}".format(idx): tf.io.RaggedFeature(_get_feature_dtype(t))
            for idx, t in enumerate(dtypes)
        }

        self._features["label"]= tf.io.FixedLenFeature([], _get_feature_dtype(label_dtype))

        self._cast_dtype_map = _build_dtype_map(dtypes)

        cast_label_dtype_map = _build_dtype_map([label_dtype])
        self._cast_label_dtype = cast_label_dtype_map[0] if cast_label_dtype_map else tf.int64

        self._batch_shape = ([None], [])

        if num_sequences > 1:
            self._batch_shape = (tuple(([None] for _ in range(num_sequences))), [])


    @property
    def dtypes(self):
        """A list containing the dtypes of each sequence in order."""
        return self._dtypes


    @property
    def num_sequences(self):
        """The number of sequences."""
        return self._num_sequences


    @property
    def label_dtype(self):
        """The dtype of the label."""
        return self._label_dtype


    def preprocess(self, dataset, num_threads, batch_size=None, drop_remainder=False):
        """Converts the raw records into a tuple that holds sequences represented as tensors.

        Args:
            dataset (TFRecordDataset): The raw (possibly shuffled) dataset.
            num_threads (int): The number of threads that may be used during preprocessing.
            batch_size (int): The batch size of the final tensors. If this is None, the data
                will not be grouped into batches.
            drop_remainder (bool): If True the last batch will be dropped in case it has
                fewer then `batch_size` elements.

        Returns:
            (tf.data.TFRecordDataset) The preprocessed dataset.
        """
        dataset = dataset.map(self._parse_example, num_parallel_calls=num_threads)
        dataset = dataset.map(self._extract_tensors, num_parallel_calls=num_threads)

        if batch_size is not None:
            dataset = dataset.padded_batch(
                batch_size,
                padded_shapes=self._batch_shape,
                drop_remainder=drop_remainder
            )

        return dataset


    def _parse_example(self, serialized):
        return tf.io.parse_example(serialized, self._features)


    def _extract_tensors(self, example):
        tensor_list = []

        for idx in range(self._num_sequences):
            tensor = example["sequence_{}".format(idx)]

            if idx in self._cast_dtype_map:
                tensor = tf.cast(tensor, self._cast_dtype_map[idx])

            tensor_list.append(tensor)

        label_tensor = tf.cast(example["label"], self._cast_label_dtype)

        if len(tensor_list) == 1:
            return (tensor_list[0], label_tensor)

        return (tuple(tensor_list), label_tensor)




class PathWriter(RecordWriter):
    """Writes one ore more paths to a tfrecord file.

    The following datatype values are supported::

        'uint8', 'int8', 'int16', 'int32', 'int64', 'int', 'float', 'bytes', 'str' and 'bool'

    Args:
        dtypes (list or str): A list of length `num_paths` that specifies the datatypes
            of the different paths as strings. In case all paths have the same type
            a single string may be passed which is then used as the type for all of them.
        num_paths (int): The number of paths.
    """
    def __init__(self, dtypes="int", num_paths=1, **kwargs):
        super().__init__(**kwargs)

        if not isinstance(dtypes, list):
            dtypes = [dtypes]*num_paths

        self._dtypes = dtypes
        self._num_paths = num_paths

        self._handler = [_get_dtype_handler(t, is_list=True) for t in dtypes]


    @property
    def dtypes(self):
        """A list containing the dtypes of each path in order."""
        return self._dtypes


    @property
    def num_paths(self):
        """The number of paths."""
        return self._num_paths


    def make_record(self, paths):
        """Convert the list of paths to a tensorflow record.

        In case only one path should be written it is not necessary to wrap it into a list.

        Args:
            paths (list): A list of paths. Each path must be a list of iterables.

        Returns:
            (tf.train.Example) The tensorflow record.
        """
        if (self.num_paths == 1):
            try:
                # If the path is not wrapped in a list this operation will raise an error since
                # paths only have 2 dimensions.
                check = paths[0][0][0]
            except:
                paths = [paths]

        assert len(paths) == self.num_paths

        feature_lists = tf.train.FeatureLists(feature_list={
            "path_{}".format(idx): self._handler[idx](path)
            for idx, path in enumerate(paths)
        })

        return tf.train.SequenceExample(feature_lists=feature_lists)




class PathReader(RecordReader):
    """Creates a TFRecordDataset from tfrecord files created with the `PathWriter`.

    The following datatype values are supported::

        'uint8', 'int8', 'int16', 'int32', 'int64', 'int', 'float', 'bytes', 'str' and 'bool'

    Args:
        dtypes (list or str): A list of length `num_paths` that specifies the datatypes
            of the different paths as strings. In case all paths have the same type
            a single string may be passed which is then used as the type for all of them.
        num_paths (int): The number of paths.
    """
    def __init__(self, dtypes="int", num_paths=1, **kwargs):
        super().__init__(**kwargs)

        if not isinstance(dtypes, list):
            dtypes = [dtypes]*num_paths

        self._dtypes = dtypes
        self._num_paths = num_paths

        self._features = {
            "path_{}".format(idx): tf.io.RaggedFeature(_get_feature_dtype(t))
            for idx, t in enumerate(dtypes)
        }

        self._cast_dtype_map = _build_dtype_map(dtypes)

        self._batch_shape = [None, None]

        if num_paths > 1:
            self._batch_shape = tuple(([None, None] for _ in range(num_paths)))


    @property
    def dtypes(self):
        """A list containing the dtypes of each path in order."""
        return self._dtypes


    @property
    def num_paths(self):
        """The number of paths."""
        return self._num_paths


    def preprocess(self, dataset, num_threads, batch_size=None, drop_remainder=False):
        """Converts the raw records into a tuple that holds paths represented as tensors.

        Args:
            dataset (TFRecordDataset): The raw (possibly shuffled) dataset.
            num_threads (int): The number of threads that may be used during preprocessing.
            batch_size (int): The batch size of the final tensors. If this is None, the data
                will not be grouped into batches.
            drop_remainder (bool): If True the last batch will be dropped in case it has
                fewer then `batch_size` elements.

        Returns:
            (tf.data.TFRecordDataset) The preprocessed dataset.
        """
        dataset = dataset.map(self._parse_example, num_parallel_calls=num_threads)
        dataset = dataset.map(self._extract_tensors, num_parallel_calls=num_threads)

        if batch_size is not None:
            dataset = dataset.padded_batch(
                batch_size,
                padded_shapes=self._batch_shape,
                drop_remainder=drop_remainder
            )

        return dataset


    def _parse_example(self, serialized):
        return tf.io.parse_sequence_example(serialized, sequence_features=self._features)


    def _extract_tensors(self, context, paths, shapes=None):
        tensor_list = []

        for idx in range(self._num_paths):
            tensor = paths["path_{}".format(idx)]

            if idx in self._cast_dtype_map:
                tensor = tf.cast(tensor, self._cast_dtype_map[idx])

            tensor_list.append(tensor.to_tensor())

        if len(tensor_list) == 1:
            return tensor_list[0]

        # Tensorflow will convert this to a tuple anyways. We just make this fact explicit.
        return tuple(tensor_list)




class LabeledPathWriter(RecordWriter):
    """Writes one ore more paths together with a label to a tfrecord file.

    The following datatype values are supported::

        'uint8', 'int8', 'int16', 'int32', 'int64', 'int', 'float', 'bytes', 'str' and 'bool'

    Args:
        dtypes (list or str): A list of length `num_paths` that specifies the datatypes
            of the different paths as strings. In case all paths have the same type
            a single string may be passed which is then used as the type for all of them.
        num_paths (int): The number of paths.
        label_dtype (str): The datatype of the label. Must be an integer type.
    """
    def __init__(self, dtypes="int", num_paths=1, label_dtype="int", **kwargs):
        super().__init__(**kwargs)

        if not isinstance(dtypes, list):
            dtypes = [dtypes]*num_paths

        self._dtypes = dtypes
        self._num_paths = num_paths
        self._label_dtype = label_dtype

        self._handler = [_get_dtype_handler(t, is_list=True) for t in dtypes]
        self._label_handler = _get_dtype_handler(label_dtype)


    @property
    def dtypes(self):
        """A list containing the dtypes of each path in order."""
        return self._dtypes


    @property
    def num_paths(self):
        """The number of path."""
        return self._num_paths


    @property
    def label_dtype(self):
        """The dtype of the label."""
        return self._label_dtype


    def make_record(self, paths, label):
        """Convert the list of paths and a label to a tensorflow record.

        In case only one path should be written it is not necessary to wrap it into a list.

        Args:
            paths (list): A list of paths. Each path must be a list of iterables.
            label (int): The label of the paths.

        Returns:
            (tf.train.Example) The tensorflow record.
        """
        if (self.num_paths == 1):
            try:
                # If the path is not wrapped in a list this operation will raise an error since
                # paths only have 2 dimensions.
                check = paths[0][0][0]
            except:
                paths = [paths]

        assert len(paths) == self.num_paths

        feature_lists = tf.train.FeatureLists(feature_list={
            "path_{}".format(idx): self._handler[idx](path)
            for idx, path in enumerate(paths)
        })

        context = tf.train.Features(feature={"label": self._label_handler([label])})

        return tf.train.SequenceExample(context=context, feature_lists=feature_lists)




class LabeledPathReader(RecordReader):
    """Creates a TFRecordDataset from tfrecord files created with the `LabeledPathWriter`.

    The following datatype values are supported::

        'uint8', 'int8', 'int16', 'int32', 'int64', 'int', 'float', 'bytes', 'str' and 'bool'

    Args:
        dtypes (list or str): A list of length `num_paths` that specifies the datatypes
            of the different paths as strings. In case all paths have the same type
            a single string may be passed which is then used as the type for all of them.
        num_paths (int): The number of paths.
        label_dtype (str): The datatype of the label. Must be an integer type.
    """
    def __init__(self, dtypes="int", num_paths=1, label_dtype="int", **kwargs):
        super().__init__(**kwargs)

        if not isinstance(dtypes, list):
            dtypes = [dtypes]*num_paths

        self._dtypes = dtypes
        self._num_paths = num_paths
        self._label_dtype = label_dtype

        self._features = {
            "path_{}".format(idx): tf.io.RaggedFeature(_get_feature_dtype(t))
            for idx, t in enumerate(dtypes)
        }

        self._label_feature= {
            "label": tf.io.FixedLenFeature([], _get_feature_dtype(label_dtype))
        }

        self._cast_dtype_map = _build_dtype_map(dtypes)

        cast_label_dtype_map = _build_dtype_map([label_dtype])
        self._cast_label_dtype = cast_label_dtype_map[0] if cast_label_dtype_map else tf.int64

        self._batch_shape = ([None, None], [])

        if num_paths > 1:
            self._batch_shape = (tuple(([None, None] for _ in range(num_paths))), [])


    @property
    def dtypes(self):
        """A list containing the dtypes of each path in order."""
        return self._dtypes


    @property
    def num_paths(self):
        """The number of paths."""
        return self._num_paths


    @property
    def label_dtype(self):
        """The dtype of the label."""
        return self._label_dtype


    def preprocess(self, dataset, num_threads, batch_size=None, drop_remainder=False):
        """Converts the raw records into a tuple that holds paths represented as tensors.

        Args:
            dataset (TFRecordDataset): The raw (possibly shuffled) dataset.
            num_threads (int): The number of threads that may be used during preprocessing.
            batch_size (int): The batch size of the final tensors. If this is None, the data
                will not be grouped into batches.
            drop_remainder (bool): If True the last batch will be dropped in case it has
                fewer then `batch_size` elements.

        Returns:
            (tf.data.TFRecordDataset) The preprocessed dataset.
        """
        dataset = dataset.map(self._parse_example, num_parallel_calls=num_threads)
        dataset = dataset.map(self._extract_tensors, num_parallel_calls=num_threads)

        if batch_size is not None:
            dataset = dataset.padded_batch(
                batch_size,
                padded_shapes=self._batch_shape,
                drop_remainder=drop_remainder
            )

        return dataset


    def _parse_example(self, serialized):
        return tf.io.parse_sequence_example(
            serialized,
            context_features=self._label_feature,
            sequence_features=self._features
        )


    def _extract_tensors(self, context, paths, shapes=None):
        tensor_list = []

        for idx in range(self._num_paths):
            tensor = paths["path_{}".format(idx)]

            if idx in self._cast_dtype_map:
                tensor = tf.cast(tensor, self._cast_dtype_map[idx])

            tensor_list.append(tensor.to_tensor())

        label_tensor = tf.cast(context["label"], self._cast_label_dtype)

        if len(tensor_list) == 1:
            return (tensor_list[0], label_tensor)

        return (tuple(tensor_list), label_tensor)
