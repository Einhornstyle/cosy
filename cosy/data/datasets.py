# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import shutil
import json
from pathlib import Path

import requests
import igraph as ig

import cosy.tree as ct




def _download(url, path=None, verbose=False, tmp_file="_tmp.tar.gz"):
    """Download an archive and decompress to the specified path.

    Args:
        url (str): The url from where to download the archive.
        path (str): The path where the content will be stored. It will be created if it does
            not exist.
        verbose (bool): If True progress messages will be printed to stdout.
        tmp_file (str): The name to be used for the temporary downloaded archive.
    """
    if path is not None:
        Path(path).mkdir(parents=True, exist_ok=True)
        tmp_file = os.path.join(path, tmp_file)

    try:
        if verbose:
            print("Downloading ...")

        req = requests.get(url)

        with open(tmp_file, "wb") as f:
            f.write(req.content)

        if verbose:
            print("Decompressing ...")

        shutil.unpack_archive(tmp_file, path)
    finally:
        os.remove(tmp_file)

    if verbose:
        print("Done!")




class Dataset:
    """Wraps around a file that holds raw data points.

    This class implements the iterator interface and may be used in a `with`-statement.

    Args:
        filename (str): The name of the file that holds the data.
        encoding (str): The encoding used to decode the file.
    """
    def __init__(self, filename, encoding="utf-8"):
        self._filename = filename
        self._encoding = encoding
        self._file = open(filename, "r", encoding=encoding)


    @property
    def filename(self):
        """The name of the file that holds the data."""
        return self._filename

    @property
    def encoding(self):
        """The encoding used to decode the file."""
        return self._encoding


    def __enter__(self):
        """Support for usage with the `with`-statement"""
        return self


    def __exit__(self, unused_type, unused_value, unused_traceback):
        """Support for usage with the `with`-statement"""
        self.close()


    def open(self, filename=None, encoding="utf-8"):
        """Open a new file or reopen the current one.

        Args:
            filename (str): The name of the new datafile. If None the old file will be reopened.
            encoding (str): The encoding used to decode the file.
        """
        self.close()

        if filename is not None:
            self._filename = filename
        self._encoding = encoding

        self._file = open(filename, "r", encoding=encoding)


    def close(self):
        """Close the file."""
        self._file.close()


    def __iter__(self):
        """Returns an iterator over the lines in the file."""
        self._reset_iter()
        return self


    def __next__(self):
        """Get the next line in the file."""
        return next(self._file)


    def _reset_iter(self):
        self._file.seek(0)




class _Base150kDataset(Dataset):
    """Base class for Python150k and JavaScript150k datasets.

    This class implements the iterator interface and may be used in a `with`-statement.

    Args:
        filename (str): The name of the file that holds the data.
        expand_values (bool): If true the value attributes are expanded into a separate vertex
            as the left most child. This will convert the ASTs into pseudo concrete syntax trees.
    """

    TYPE = "type"
    VALUE = "value"
    CHILDREN = "children"
    SYMBOL = ct.TreeAttributes.SYMBOL


    def __init__(self, filename, expand_values=False):
        super().__init__(filename)
        self.expand_values = expand_values


    def __iter__(self):
        """Returns an iterator over the ASTs contained in the dataset."""
        super()._reset_iter()
        return self


    def __next__(self):
        """Get the next AST."""
        vertex_list = json.loads(super().__next__())

        if self.expand_values:
            return self._build_expanded_ast(vertex_list)

        return self._build_default_ast(vertex_list)


    def _build_default_ast(self, vertex_list):
        tree = ig.Graph(len(vertex_list), directed=True)

        all_edges = []
        for index, vertex in enumerate(vertex_list):
            tree.vs[index][self.SYMBOL] = vertex[self.TYPE]

            if self.VALUE in vertex:
                tree.vs[index][self.VALUE] = vertex[self.VALUE]

            if self.CHILDREN in vertex:
                for child_index in vertex[self.CHILDREN]:
                    all_edges.append((index, child_index))

        tree.add_edges(all_edges)
        return tree


    def _build_expanded_ast(self, vertex_list):
        num_expansions = sum(self.VALUE in vertex for vertex in vertex_list)

        tree = ig.Graph(len(vertex_list) + num_expansions, directed=True)

        expansion_index = 0

        all_edges = []
        for index, vertex in enumerate(vertex_list):
            index += num_expansions

            tree.vs[index][self.SYMBOL] = vertex[self.TYPE]

            if self.VALUE in vertex:
                tree.vs[expansion_index][self.SYMBOL] = vertex[self.VALUE]
                all_edges.append((index, expansion_index))
                expansion_index += 1

            if self.CHILDREN in vertex:
                for child_index in vertex[self.CHILDREN]:
                    all_edges.append((index, child_index + num_expansions))

        tree.add_edges(all_edges)
        return tree




class Python150kDataset(_Base150kDataset):
    """Provides the AST contained in the Python150k dataset as igraph objects.

    This class implements the iterator interface and may be used in a `with`-statement.

    Args:
        filename (str): The name of the file that holds the data.
        expand_values (bool): If true the value attributes are expanded into a separate vertex
            as the left most child. This will convert the ASTs into pseudo concrete syntax trees.
    """

    TMP_FILE = "tmp_py150.tar.gz"
    URL = "http://files.srl.inf.ethz.ch/data/py150.tar.gz"

    @classmethod
    def download(cls, path=None, verbose=False):
        """Download the dataset to the specified path.

        This will create 3 files at the target location. One for the trainings and evaluation
        datasets respectively and a third containing the code used to create them.

        Args:
            path (str): The path where the dataset will be stored. It will be created if it does
                not exist.
            verbose (bool): If True progress messages will be printed to stdout.
        """
        _download(url=cls.URL, path=path, verbose=verbose, tmp_file=cls.TMP_FILE)




class JavaScript150kDataset(_Base150kDataset):
    """Provides the AST contained in the JavaScript150k dataset as igraph objects.

    This class implements the iterator interface and may be used in a `with`-statement.

    Args:
        filename (str): The name of the file that holds the data.
        expand_values (bool): If true the value attributes are expanded into a separate vertex
            as the left most child. This will convert the ASTs into pseudo concrete syntax trees.
    """

    TMP_FILE = "tmp_js_dataset.tar.gz"
    URL = "http://files.srl.inf.ethz.ch/data/js_dataset.tar.gz"


    def __next__(self):
        """Get the next AST."""
        # Remove Null-termination
        vertex_list = json.loads(Dataset.__next__(self))[:-1]

        if self.expand_values:
            return self._build_expanded_ast(vertex_list)

        return self._build_default_ast(vertex_list)


    @classmethod
    def download(cls, path=None, verbose=False):
        """Download the dataset to the specified path.

        This will create 6 files at the target location. One for the trainings and evaluation
        datasets respectively, an archive containing the original source code for all ASTs, two
        files detailing which files were used to create the training and test datasets and a readme.

        Args:
            path (str): The path where the dataset will be stored. It will be created if it does
                not exist.
            verbose (bool): If True progress messages will be printed to stdout.
        """
        _download(url=cls.URL, path=path, verbose=verbose, tmp_file=cls.TMP_FILE)



