# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import numpy as np
import tensorflow as tf
from tensorflow import keras as K
import cosy




class SequenceTransformer(K.Model):
    """Reconstruct the input sequence with a transformer encoder.

    Args:
        vocab_size (int): Size of the input/output vocabulary.
        embedding_dim (int): Size of the embedding dimension.
        transformer_dim (int): Size of the internal transformer dimension.
        num_layers (int): Number of layers used in the transformer encoder.
        num_heads (int): Number of attention heads used in the transformer encoder.
        dropout (float): The dropout rate used by the transformer encoder.
        return_logits (bool): If True the raw logits will be returned instead of their softmax.
    """
    def __init__(self,
                 vocab_size,
                 embedding_dim=300,
                 transformer_dim=300,
                 num_layers=6,
                 num_heads=6,
                 dropout=0.1,
                 return_logits=False,
                 **kwargs):
        super().__init__(**kwargs)

        self._return_logits = return_logits

        self.embedding = K.layers.Embedding(
            input_dim=vocab_size,
            output_dim=embedding_dim,
            mask_zero=True
        )

        self.encoder = cosy.layers.TransformerEncoder(
            num_layers=num_layers,
            dimension=transformer_dim,
            num_heads=num_heads,
            hidden_units=transformer_dim,
            dropout_rate=dropout,
            prevent_look_ahead=True
        )

        self.linear = K.layers.Dense(units=vocab_size)
        self.softmax = K.layers.Softmax()


    def call(self, inputs, training=None):
        """Predict the tokens (t_1, t_2, ..., t_n+1) given (t_0, t_1, ... , t_n).

        Args:
            inputs (Tensor): A tensor that contains a token sequence.
            training (bool): Whether or not the model gets currently trained.

        Returns:
            (Tensor): The predicted token sequence
        """
        output = self.embedding(inputs)
        mask = self.embedding.compute_mask(inputs)

        output = self.encoder(output, training=training, mask=mask)

        output = self.linear(output)

        if not self._return_logits:
            output = self.softmax(output)

        return output




class PathTransformer(K.Model):
    """Compute the next terminal for every path in the input with a transformer encoder.

    Args:
        vocab_size (int): Size of the input/output vocabulary.
        embedding_dim (int): Size of the embedding dimension.
        transformer_dim (int): Size of the internal transformer dimension.
        num_layers (int): Number of layers used in the transformer encoder.
        num_heads (int): Number of attention heads used in the transformer encoder.
        transformer_dropout (float): The dropout rate used by the transformer encoder.
        rnn_dropout (float): The dropout rate used by the path encoder lstm.
        terminals (str): The position of the terminal symbols within the path. Must be either
            'first', 'last', 'both' or None. Indicating that either first, last or both (the
            first and the last symbol) are terminals. In case the path does not contain any
            terminals this parameter should be None.
        return_logits (bool): If True the raw logits will be returned instead of their softmax.
    """

    _FIRST = "first"
    _LAST = "last"
    _BOTH = "both"
    _NONE = None


    def __init__(self,
                 vocab_size,
                 embedding_dim=300,
                 transformer_dim=300,
                 num_layers=6,
                 num_heads=6,
                 transformer_dropout=0.1,
                 rnn_dropout=0.0,
                 terminals=None,
                 return_logits=False,
                 **kwargs):
        super().__init__(**kwargs)

        self._return_logits = return_logits

        self.embedding = K.layers.Embedding(
            input_dim=vocab_size,
            output_dim=embedding_dim,
            mask_zero=True
        )

        self.path_lstm = cosy.layers.PathRNN(
            embedding_dim,
            rnn_type="lstm",
            dropout=rnn_dropout,
            recurrent_dropout=rnn_dropout,
            allow_cudnn_kernel=False
        )

        self.encoder = cosy.layers.TransformerEncoder(
            num_layers=num_layers,
            dimension=transformer_dim,
            num_heads=num_heads,
            hidden_units=transformer_dim,
            dropout_rate=transformer_dropout,
            prevent_look_ahead=True
        )

        self.linear = K.layers.Dense(units=vocab_size)
        self.softmax = K.layers.Softmax()

        if terminals == self._NONE:
            self._path_encoder = self._none_path

        elif terminals == self._FIRST:
            self._path_encoder = self._first_path

        elif terminals == self._LAST:
            self._path_encoder = self._last_path

        elif terminals == self._BOTH:
            self._path_encoder = self._both_path

        else:
            raise ValueError("'terminals' must be one of 'first', 'last', 'both' or None.")


    def call(self, inputs, training=None):
        """Predict the tokens (t_1, t_2, ..., t_n+1) given (path_0, t_1, ... , path_n).

        Args:
            inputs (Tensor): A tensor that contains the sequence of paths.
            training (bool): Whether or not the model gets currently trained.

        Returns:
            (Tensor): The predicted token sequence
        """
        output = self.embedding(inputs)
        mask = self.embedding.compute_mask(inputs)

        output, mask = self._path_encoder(output, training, mask)

        output = self.encoder(output, training=training, mask=mask)

        output = self.linear(output)

        if not self._return_logits:
            output = self.softmax(output)

        return output


    def _none_path(self, inputs, training, mask):
        paths = self.path_lstm(inputs, training=training, mask=mask)
        path_mask = self.path_lstm.compute_mask(inputs, mask=mask)

        return paths, path_mask


    def _first_path(self, inputs, training, mask):
        terminals = inputs[..., 0, :]
        terminals_mask = mask[..., 0]

        nonterminals = inputs[..., 1:, :]
        nonterminal_mask = mask[..., 1:]

        paths, path_mask = self._none_path(nonterminals, training, nonterminal_mask)

        output = terminals + paths
        output_mask = tf.math.logical_or(terminals_mask, path_mask)

        return output, output_mask


    def _last_path(self, inputs, training, mask):
        terminals = inputs[..., -1, :]
        terminals_mask = mask[..., -1]

        nonterminals = inputs[..., :-1, :]
        nonterminal_mask = mask[..., :-1]

        paths, path_mask = self._none_path(nonterminals, training, nonterminal_mask)

        output = paths + terminals
        output_mask = tf.math.logical_or(path_mask, terminals_mask)

        return output, output_mask


    def _both_path(self, inputs, training, mask):
        first_terminals = inputs[..., 0, :]
        first_terminals_mask = mask[..., 0]

        last_terminals = inputs[..., -1, :]
        last_terminals_mask = mask[..., -1]

        nonterminals = inputs[..., 1:-1, :]
        nonterminal_mask = mask[..., 1:-1]

        paths, path_mask = self._none_path(nonterminals, training, nonterminal_mask)

        output = first_terminals + paths + last_terminals
        output_mask = tf.math.logical_or(first_terminals_mask, last_terminals_mask)
        output_mask = tf.math.logical_or(path_mask, output_mask)

        return output, output_mask




class JNESP0(K.Model):
    """Experimental code completion model.

    This model uses a joint node context embedding with pooling.
    Positional information is only used for by nonterminal embedding.

    Args:
        terminal_vocab_size (int): Size of the terminal symbol vocabulary.
        nonterminal_vocab_size (int): Size of the nonterminal symbol vocabulary.
        max_nonterminal_idx (int): Largest integer used to encode positional information for
            nonterminal symbols.
        terminal_dimension (int): Dimensionality of the terminal embedding.
        nonterminal_dimension (int): Dimensionality of the nonterminal embedding.
        nonterminal_index_dimension (int): Dimensionality of the nonterminal positional embedding.
        mask_paths (bool): If this is true the paths will be masked
        pooling_type (str): Must be either 'average' or 'max'.
    """
    def __init__(self,
                 terminal_vocab_size,
                 nonterminal_vocab_size,
                 max_nonterminal_index,
                 terminal_dimension=512,
                 nonterminal_dimension=256,
                 nonterminal_index_dimension=256,
                 mask_paths=False,
                 pooling_type="average",
                 **kwargs):
        super().__init__(**kwargs)

        # This condition must hold for any joint model
        assert (nonterminal_index_dimension + nonterminal_dimension) == terminal_dimension

        terminal_embedding = K.layers.Embedding(
            terminal_vocab_size,
            terminal_dimension,
            mask_zero=True
        )

        nonterminal_embedding = cosy.layers.SymbolEmbedding(
            nonterminal_vocab_size,
            max_nonterminal_index,
            vocab_dimension=nonterminal_dimension,
            index_dimension=nonterminal_index_dimension
        )

        self.nce = cosy.layers.PoolingNCE(
            terminal_embedding,
            nonterminal_embedding,
            use_joint_embedding=True,
            mask_paths=mask_paths,
            pooling_type=pooling_type
        )

        self.attention = K.layers.Attention()

        self.linear = K.layers.Dense(terminal_vocab_size)

        self.softmax = K.layers.Softmax()


    def call(self, inputs):
        """Predict the terminal symbol that should be appended to the root path.

        The tuple of input tensors must have the following order:

        1. terminal symbol tensor, shape = (..., num_paths)
        2. nonterminal leaf path symbol tensor, shape = (..., num_paths, path_length)
        3. nonterminal leaf path index tensor, shape = (..., num_paths, path_length)
        4. nonterminal root path symbol tensor, shape = (..., path_length)
        5. nonterminal root path index tensor, shape = (..., path_length)

        Args:
            inputs (tuple): A 5-tuple of tensors.

        Retruns:
            (Tensor) Probability distribution over the set of terminal symbols.
        """
        leaf_path_set, root_path = self.nce((inputs[0], inputs[1:3], inputs[3:5]))

        # query = root_path, values = keys = leaf_path_list
        output = self.attention([root_path[:,tf.newaxis,:], leaf_path_set])[:,0,:]
        output = tf.concat([output, root_path], axis=-1)

        # predict next nonterminal
        output = self.linear(output)
        output = self.softmax(output)

        return output




class SNESP0(K.Model):
    """Experimental code completion model.

    This model uses a separated node context embedding with pooling.
    Positional information is only used for by nonterminal embedding.

    Args:
        terminal_vocab_size (int): Size of the terminal symbol vocabulary.
        nonterminal_vocab_size (int): Size of the nonterminal symbol vocabulary.
        max_nonterminal_idx (int): Largest integer used to encode positional information for
            nonterminal symbols.
        terminal_dimension (int): Dimensionality of the terminal embedding.
        nonterminal_dimension (int): Dimensionality of the nonterminal embedding.
        nonterminal_index_dimension (int): Dimensionality of the nonterminal positional embedding.
        mask_paths (bool): If this is true the paths will be masked
        pooling_type (str): Must be either 'average' or 'max'.
        activation (activation function): A string or an activation function that will be used
            for the dense layer which adjusts the dimensionality of the root path before attending
            of the path context.
    """
    def __init__(self,
                 terminal_vocab_size,
                 nonterminal_vocab_size,
                 max_nonterminal_index,
                 terminal_dimension=512,
                 nonterminal_dimension=256,
                 nonterminal_index_dimension=256,
                 mask_paths=False,
                 pooling_type="average",
                 activation=None,
                 **kwargs):
        super().__init__(**kwargs)

        dim = nonterminal_dimension + nonterminal_index_dimension

        terminal_embedding = K.layers.Embedding(
            terminal_vocab_size,
            terminal_dimension,
            mask_zero=True
        )

        nonterminal_embedding = cosy.layers.SymbolEmbedding(
            nonterminal_vocab_size,
            max_nonterminal_index,
            vocab_dimension=nonterminal_dimension,
            index_dimension=nonterminal_index_dimension
        )

        self.nce = cosy.layers.PoolingNCE(
            terminal_embedding,
            nonterminal_embedding,
            use_joint_embedding=False,
            mask_paths=mask_paths,
            pooling_type=pooling_type
        )

        self.dense = K.layers.Dense(dim, activation=activation)

        self.attention = K.layers.Attention()

        self.linear = K.layers.Dense(terminal_vocab_size)

        self.softmax = K.layers.Softmax()


    def call(self, inputs):
        """Predict the terminal symbol that should be appended to the root path.

        The tuple of input tensors must have the following order:

        1. terminal symbol tensor, shape = (..., num_paths)
        2. nonterminal leaf path symbol tensor, shape = (..., num_paths, path_length)
        3. nonterminal leaf path index tensor, shape = (..., num_paths, path_length)
        4. nonterminal root path symbol tensor, shape = (..., path_length)
        5. nonterminal root path index tensor, shape = (..., path_length)

        Args:
            inputs (tuple): A 5-tuple of tensors.

        Retruns:
            (Tensor) Probability distribution over the set of terminal symbols.
        """
        leaf_path_set, root_path = self.nce((inputs[0], inputs[1:3], inputs[3:5]))

        leaf_path_set = self.dense(leaf_path_set)

        # query = root_path, values = keys = leaf_path_list
        output = self.attention([root_path[:,tf.newaxis,:], leaf_path_set])[:,0,:]
        output = tf.concat([output, root_path], axis=-1)

        # predict next nonterminal
        output = self.linear(output)
        output = self.softmax(output)

        return output




class CodeCompletionXL(K.Model):
    """Experimental code completion model.

    This model uses a joint node context embedding with a lstm.
    Positional information is only used for by nonterminal embedding.
    The embedded leaf paths are feed through a transformer encoder and two additional
    feed forward layers are used afterwards.

    Args:
        terminal_vocab_size (int): Size of the terminal symbol vocabulary.
        nonterminal_vocab_size (int): Size of the nonterminal symbol vocabulary.
        max_sibling_idx (int): Largest integer used to encode positional information for
            nonterminal symbols.
        embedding_dimension (int): Dimensionality of the symbol embedding.
        num_lstms (int): The number of lstm layers.
        lstm_dimensions (int or list): The dimensionality of the individual lstm layers.
            If this is an integer every layer will have the same dimensionality.
        num_transformer_layers (int): The number of `EncoderLayers` that are used.
        transformer_dimension (int): The output dimension of the multi-head attention and
            point wise feed forward network used inside the `EncoderLayer`
        num_heads (int): The number of heads used in the multi-head attention layer.
        hidden_units (int): The dimension of the hidden layer used by the `EncoderLayers`.
        dropout_rate (float): The dropout rate to be used. Default is 0.3.
    """
    def __init__(self,
                 terminal_vocab_size,
                 nonterminal_vocab_size,
                 max_sibling_index,
                 embedding_dimension=512,
                 num_lstms=2,
                 lstm_dimension=256,
                 num_transformer_layers=4,
                 transformer_dimension=256,
                 num_heads=8,
                 hidden_units=512,
                 dropout_rate=0.3,
                 **kwargs):
        super().__init__(**kwargs)

        assert embedding_dimension % 2 == 0, "`embedding_dimension` must be a power of 2."

        terminal_embedding = K.layers.Embedding(
            terminal_vocab_size,
            embedding_dimension,
            mask_zero=True
        )

        nonterminal_embedding = cosy.layers.SymbolEmbedding(
            nonterminal_vocab_size,
            max_sibling_index,
            vocab_dimension=embedding_dimension//2,
            index_dimension=embedding_dimension//2
        )

        self.nce = cosy.layers.RecurrentNCE(
            terminal_embedding,
            nonterminal_embedding,
            use_joint_embedding=True,
            rnn_type="lstm",
            num_layers=num_lstms,
            dimensions=lstm_dimension
        )


        self.transformer_encoder = cosy.layers.TransformerEncoder(
            num_transformer_layers,
            transformer_dimension,
            num_heads,
            hidden_units,
            dropout_rate
        )

        self.relu1 = K.layers.Dense(lstm_dimension, activation="relu")
        self.linear1 = K.layers.Dense(lstm_dimension)

        self.attention = K.layers.Attention()

        self.relu2 = K.layers.Dense(
            embedding_dimension,
            input_shape=(2*lstm_dimension,),
            activation="relu"
        )
        self.linear2 = K.layers.Dense(terminal_vocab_size)

        self.softmax = K.layers.Softmax()


    def call(self, inputs, training=None):
        """Predict the terminal symbol that should be appended to the root path.

        The tuple of input tensors must have the following order:

        1. terminal symbol tensor, shape = (..., num_paths)
        2. nonterminal leaf path symbol tensor, shape = (..., num_paths, path_length)
        3. nonterminal leaf path index tensor, shape = (..., num_paths, path_length)
        4. nonterminal root path symbol tensor, shape = (..., path_length)
        5. nonterminal root path index tensor, shape = (..., path_length)

        Args:
            inputs (tuple): A 5-tuple of tensors.
            training (bool): Indicates whether the call is meant for training or inference.

        Retruns:
            (Tensor) Probability distribution over the set of terminal symbols.
        """
        leaf_path_set, root_path = self.nce((inputs[0], inputs[1:3], inputs[3:5]))

        leaf_path_list = self.transformer_encoder(leaf_path_set, training)

        root_path = self.relu1(root_path)
        root_path = self.linear1(root_path)

        # query = root_path, values = keys = leaf_path_list
        output = self.attention([root_path[:,tf.newaxis,:], leaf_path_list])[:,0,:]
        output = self.relu2(tf.concat([output, root_path], axis=-1))

        # predict next nonterminal
        output = self.linear2(output)
        output = self.softmax(output)

        return output




class CodeCompletionDocsXL(K.Model):
    """Experimental code completion model.

    This model uses a joint node context embedding with a lstm.
    Positional information is only used for by nonterminal embedding.
    The embedded leaf paths are feed through a transformer encoder and two additional
    feed forward layers are used afterwards.
    This also used docstrings.

    Args:
        terminal_vocab_size (int): Size of the terminal symbol vocabulary.
        nonterminal_vocab_size (int): Size of the nonterminal symbol vocabulary.
        max_sibling_idx (int): Largest integer used to encode positional information for
            nonterminal symbols.
        docstring_vocab_size (int): Size of the vocabulary used in docstrings.
        embedding_dimension (int): Dimensionality of the symbol embedding.
        num_lstms (int): The number of lstm layers.
        lstm_dimensions (int or list): The dimensionality of the individual lstm layers.
            If this is an integer every layer will have the same dimensionality.
        num_transformer_layers (int): The number of `EncoderLayers` that are used.
        transformer_dimension (int): The output dimension of the multi-head attention and
            point wise feed forward network used inside the `EncoderLayer`
        num_heads (int): The number of heads used in the multi-head attention layer.
        hidden_units (int): The dimension of the hidden layer used by the `EncoderLayers`.
        dropout_rate (float): The dropout rate to be used. Default is 0.3.
    """
    def __init__(self,
                 terminal_vocab_size,
                 nonterminal_vocab_size,
                 max_sibling_index,
                 docstring_vocab_size,
                 embedding_dimension=512,
                 num_lstms=2,
                 lstm_dimension=256,
                 num_transformer_layers=4,
                 transformer_dimension=256,
                 num_heads=8,
                 hidden_units=512,
                 dropout_rate=0.3,
                 **kwargs):
        super().__init__(**kwargs)

        assert embedding_dimension % 2 == 0, "`embedding_dimension` must be a power of 2."

        terminal_embedding = K.layers.Embedding(
            terminal_vocab_size,
            embedding_dimension,
            mask_zero=True
        )

        nonterminal_embedding = cosy.layers.SymbolEmbedding(
            nonterminal_vocab_size,
            max_sibling_index,
            vocab_dimension=embedding_dimension//2,
            index_dimension=embedding_dimension//2
        )

        self.docstring_embedding = K.layers.Embedding(
            docstring_vocab_size,
            embedding_dimension//2,
            mask_zero=True,
        )

        self.nce = cosy.layers.RecurrentNCE(
            terminal_embedding,
            nonterminal_embedding,
            use_joint_embedding=True,
            rnn_type="lstm",
            num_layers=num_lstms,
            dimensions=lstm_dimension
        )

        self.transformer_encoder = cosy.layers.TransformerEncoder(
            num_transformer_layers,
            transformer_dimension,
            num_heads,
            hidden_units,
            dropout_rate
        )

        self.relu1 = K.layers.Dense(lstm_dimension, activation="relu")
        self.linear1 = K.layers.Dense(lstm_dimension)

        self.attention = K.layers.Attention()

        self.relu2 = K.layers.Dense(
            embedding_dimension,
            input_shape=(2*lstm_dimension,),
            activation="relu"
        )
        self.linear2 = K.layers.Dense(terminal_vocab_size)

        self.softmax = K.layers.Softmax()


    def call(self, inputs, training=None):
        """Predict the terminal symbol that should be appended to the root path.

        The tuple of input tensors must have the following order:

        1. terminal symbol tensor, shape = (..., num_paths)
        2. nonterminal leaf path symbol tensor, shape = (..., num_paths, path_length)
        3. nonterminal leaf path index tensor, shape = (..., num_paths, path_length)
        4. nonterminal root path symbol tensor, shape = (..., path_length)
        5. nonterminal root path index tensor, shape = (..., path_length)
        6. docstring tensor, shape = (..., docstring_length)

        Args:
            inputs (tuple): A 6-tuple of tensors.
            training (bool): Indicates whether the call is meant for training or inference.

        Retruns:
            (Tensor) Probability distribution over the set of terminal symbols.
        """
        leaf_path_set, root_path = self.nce((inputs[0], inputs[1:3], inputs[3:5]))

        leaf_path_set = self.transformer_encoder(leaf_path_set, training)

        root_path = self.relu1(root_path)
        root_path = self.linear1(root_path)

        # query = root_path, values = keys = leaf_path_list
        code = self.attention([root_path[:,tf.newaxis,:], leaf_path_set])[:,0,:]

        docs = self.docstring_embedding(inputs[5])
        docs = self.attention([root_path[:,tf.newaxis,:], docs])[:,0,:]

        output = self.relu2(tf.concat([docs, code, root_path], axis=-1))

        output = self.linear2(output)
        output = self.softmax(output)

        return output




class CodeCompletionXS(K.Model):
    """Experimental code completion model.

    This model uses a joint node context embedding with a lstm.
    Positional information is only used for by nonterminal embedding.

    Args:
        terminal_vocab_size (int): Size of the terminal symbol vocabulary.
        nonterminal_vocab_size (int): Size of the nonterminal symbol vocabulary.
        max_sibling_idx (int): Largest integer used to encode positional information for
            nonterminal symbols.
        embedding_dimension (int): Dimensionality of the symbol embedding.
    """
    def __init__(self,
                 terminal_vocab_size,
                 nonterminal_vocab_size,
                 max_sibling_index,
                 embedding_dimension=512,
                 **kwargs):
        super().__init__(**kwargs)

        assert embedding_dimension % 2 == 0, "`embedding_dimension` must be a power of 2."

        terminal_embedding = K.layers.Embedding(
            terminal_vocab_size,
            embedding_dimension,
            mask_zero=True
        )

        nonterminal_embedding = cosy.layers.SymbolEmbedding(
            nonterminal_vocab_size,
            max_sibling_index,
            vocab_dimension=embedding_dimension//2,
            index_dimension=embedding_dimension//2
        )

        self.nce = cosy.layers.PoolingNCE(
            terminal_embedding,
            nonterminal_embedding,
            use_joint_embedding=True,
            pooling_type="average"
        )

        self.attention = K.layers.Attention()

        self.linear = K.layers.Dense(terminal_vocab_size)

        self.softmax = K.layers.Softmax()


    def call(self, inputs):
        """Predict the terminal symbol that should be appended to the root path.

        The tuple of input tensors must have the following order:

        1. terminal symbol tensor, shape = (..., num_paths)
        2. nonterminal leaf path symbol tensor, shape = (..., num_paths, path_length)
        3. nonterminal leaf path index tensor, shape = (..., num_paths, path_length)
        4. nonterminal root path symbol tensor, shape = (..., path_length)
        5. nonterminal root path index tensor, shape = (..., path_length)

        Args:
            inputs (tuple): A 5-tuple of tensors.
            training (bool): Indicates whether the call is meant for training or inference.

        Retruns:
            (Tensor) Probability distribution over the set of terminal symbols.
        """
        leaf_path_set, root_path = self.nce((inputs[0], inputs[1:3], inputs[3:5]))

        # query = root_path, values = keys = leaf_path_list
        output = self.attention([root_path[:,tf.newaxis,:], leaf_path_set])[:,0,:]
        output = tf.concat([output, root_path], axis=-1)

        # predict next nonterminal
        output = self.linear(output)
        output = self.softmax(output)

        return output




class CodeCompletionDocsXS(K.Model):
    """Experimental code completion model.

    This model uses a joint node context embedding with a lstm.
    Positional information is only used for by nonterminal embedding.
    This also used docstrings.

    Args:
        terminal_vocab_size (int): Size of the terminal symbol vocabulary.
        nonterminal_vocab_size (int): Size of the nonterminal symbol vocabulary.
        max_sibling_idx (int): Largest integer used to encode positional information for
            nonterminal symbols.
        docstring_vocab_size (int): Size of the vocabulary used in docstrings.
        embedding_dimension (int): Dimensionality of the symbol embedding.
    """
    def __init__(self,
                 terminal_vocab_size,
                 nonterminal_vocab_size,
                 max_sibling_index,
                 docstring_vocab_size,
                 embedding_dimension=512,
                 **kwargs):
        super().__init__(**kwargs)

        assert embedding_dimension % 2 == 0, "`embedding_dimension` must be a power of 2."

        terminal_embedding = K.layers.Embedding(
            terminal_vocab_size,
            embedding_dimension,
            mask_zero=True
        )

        nonterminal_embedding = cosy.layers.SymbolEmbedding(
            nonterminal_vocab_size,
            max_sibling_index,
            vocab_dimension=embedding_dimension//2,
            index_dimension=embedding_dimension//2
        )

        self.docstring_embedding = K.layers.Embedding(
            docstring_vocab_size,
            embedding_dimension,
            mask_zero=True,
        )

        self.nce = cosy.layers.PoolingNCE(
            terminal_embedding,
            nonterminal_embedding,
            use_joint_embedding=True,
            pooling_type="average"
        )

        self.attention = K.layers.Attention()

        self.relu = K.layers.Dense(embedding_dimension, activation="relu")
        self.linear = K.layers.Dense(terminal_vocab_size)

        self.softmax = K.layers.Softmax()


    def call(self, inputs):
        """Predict the terminal symbol that should be appended to the root path.

        The tuple of input tensors must have the following order:

        1. terminal symbol tensor, shape = (..., num_paths)
        2. nonterminal leaf path symbol tensor, shape = (..., num_paths, path_length)
        3. nonterminal leaf path index tensor, shape = (..., num_paths, path_length)
        4. nonterminal root path symbol tensor, shape = (..., path_length)
        5. nonterminal root path index tensor, shape = (..., path_length)
        6. docstring tensor, shape = (..., docstring_length)

        Args:
            inputs (tuple): A 6-tuple of tensors.
            training (bool): Indicates whether the call is meant for training or inference.

        Retruns:
            (Tensor) Probability distribution over the set of terminal symbols.
        """
        leaf_path_set, root_path = self.nce((inputs[0], inputs[1:3], inputs[3:5]))
        code = self.attention([root_path[:,tf.newaxis,:], leaf_path_set])[:,0,:]

        docs = self.docstring_embedding(inputs[5])
        docs = self.attention([root_path[:,tf.newaxis,:], docs])[:,0,:]

        output = self.relu(tf.concat([docs, code, root_path], axis=-1))

        output = self.linear(output)
        output = self.softmax(output)

        return output




class Transformer(K.Model):
    """The original transformer model.

    The models architecture is described in this paper and all credit goes to the authors::

        `Attention Is All You Need https://arxiv.org/abs/1706.03762`_

    Args:

    Args:
        num_layers (int): The number of `EncoderLayers` and `DecoderLayers` that are used.
        dimension (int): The output dimension of the multi-head attention and point wise feed
            forward network.
        num_heads (int): The number of heads used in the multi-head attention layer.
        hidden_units (int): The dimension of the hidden layer inside the point wise feed forward
            network.
        vocab_size_input (int): Size of the input vocabulary
        vocab_size_target (int): Size of the target vocabulary
        max_position_input (int): Max sentence length in input.
        max_position_target (int): Max sentence length in output
        dropout_rate (float): The dropout rate to be used. Default is 0.1.
    """
    def __init__(self,
                 num_layers,
                 dimension,
                 num_heads,
                 hidden_units,
                 vocab_size_input,
                 vocab_size_target,
                 max_position_input,
                 max_position_target,
                 dropout_rate=0.1,
                 **kwargs):
        super().__init__(**kwargs)

        self.encoder_embedding = cosy.layers.TransformerEmbedding(
            dimension,
            vocab_size_input,
            max_position_input
        )

        self.encoder = cosy.layers.TransformerEncoder(
            num_layers,
            dimension,
            num_heads,
            hidden_units,
            dropout_rate
        )

        self.decoder_embedding = cosy.layers.TransformerEmbedding(
            dimension,
            vocab_size_target,
            max_position_target
        )

        self.decoder = cosy.layers.TransformerDecoder(
            num_layers,
            dimension,
            num_heads,
            hidden_units,
            dropout_rate
        )

        self.dense = K.layers.Dense(vocab_size_target)


    def call(self,
             inputs,
             target,
             training,
             encoder_padding_mask=None,
             look_ahead_mask=None,
             decoder_padding_mask=None):
        """Feed the inputs through the transformer.

        Args:
            inputs (Tensor): A tensor containing sequences of input token embeddings
                of shape = (..., seq_len, embedding_dim)
            target (Tensor): A tensor containing sequences of output token embeddings
                of shape = (..., seq_len, embedding_dim)
            training (bool): Indicates whether the call is meant for training or inference.
            encoder_padding_mask (Tensor): A mask indicating the padding in the `inputs` tensor.
            look_ahead_mask (Tensor): A mask restricting the part of the `target` that will
                be read by the model.
            decoder_padding_mask (Tensor): A mask indicating the padding in the `target` tensor.
        Returns:
            (Tensor) The result of feeding the inputs tensor through all sublayers.
        """
        inputs_embedding = self.encoder_embedding(inputs)
        target_embedding = self.decoder_embedding(target)

        # (..., input_seq_len, dimension)
        encoder_output = self.encoder(inputs_embedding, training, encoder_padding_mask)

        # (..., target_seq_len, dimension)
        decoder_output, attention_weights = self.decoder(
            (target_embedding, encoder_output),
            training=training,
            mask=(decoder_padding_mask, look_ahead_mask)
        )

        output = self.dense(decoder_output)

        return output, attention_weights




class KarelModel(K.Model):
    """A tensorflow model which generates recommendations for the Karel programming language.

    The models architecture is described in this paper and all credit goes to the authors::

        `Leveraging Grammar and Reinforcement Learning for Neural Program Synthesis
        https://arxiv.org/abs/1805.04276`_

    Args:
        num_tokens (int): The maximal number of tokens in per input sentence.
        num_examples (int): The number of I/O examples per sentence.
        **kwargs: Keyword arguments passed to the parent class tf.keras.Model.
    """
    def __init__(self, num_tokens, num_examples, **kwargs):
        super().__init__(**kwargs)

        self.num_tokens = num_tokens
        self.num_examples = num_examples

        self.encoder = cosy.layers.KarelEncoder()
        self.decoder = cosy.layers.KarelDecoder(num_tokens, num_examples)


    def call(self, inputs, reshape_inputs=True, mask=None):
        """Forward pass of the model on the given inputs.

        The `inputs` argument must be a list or tuple with 3 tensors with the folowing shapes

        * input examples: inputs[0].shape = [batch_size, num_examples, width, height, 16]
        * output examples: inputs[1].shape = [batch_size, num_examples, width, height, 16]
        * programs:  inputs[2].shape = [batch_size, num_tokens]

        Args:
            inputs (list or tuple): A list of Tensors (see above).
            reshape_inputs (bool): If True the reshapes I/O tensors to (batch_size x num_examples).
            mask (Tensor): A mask specifing the padding of the program Tensor (optional)

        Returns:
            (Tensor): Probabilities each grammar token with shape [batch_size, 52].
        """
        idata, odata, programs = inputs

        result = self.encoder((idata, odata), reshape_inputs)
        result = self.decoder((result, programs), mask)

        return result
