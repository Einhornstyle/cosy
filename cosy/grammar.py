# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import re
import json
import itertools as it
import numpy as np

import tensorflow as tf

from .preprocessing import DynamicSymbolEncoder
from .tree import TreeAttributes, EdgeSort, leafs




class NumericBNF:
    """An numeric encoding of a context-free grammar in Backus-Naur form.

    The grammar is stored in a single dict of the form::

        grammar[nonterminal] = [[terminal | nonterminal, ...], ...]

    The sets of terminal and nonterminal symbols are defined implicitly.
    Every symbol for which no derivation rule exists is considerd to be a terminal symbol.

    A NumericBNF is considered to be 'normalized' if and only if

    1. The symbols are consecutive unsigned integers starting at 1.
    2. max(terminal symbols) == min(nonterminal symbols) - 1

    Sentences of a normalized grammmar are particular easy to embed into a vector space.
    Hence, it is almost always advisable to normalize a grammar befor using it.

    Args:
        start_symbol (int): The start symbol for every derivation.
        grammar (dict): A mapping from nonterminals to possible derivations.

    Raises:
        ValueError: If the start symbol is specified and is not a nonterminal symbol.
    """
    def __init__(self, start_symbol=None, grammar=None):
        self._start_symbol = None
        self._grammar = {} if grammar is None else grammar

        if start_symbol is not None:
            self.start_symbol = start_symbol

        self._cached_terminal_symbols = None


    @property
    def grammar(self):
        """A copy (!) of the grammar rules"""
        return self._grammar.copy()


    @property
    def terminal_symbols(self):
        """A frozenset containing all terminal symbols."""
        if self._cached_terminal_symbols is None:
            self._compute_terminal_symbols()

        return self._cached_terminal_symbols


    @property
    def nonterminal_symbols(self):
        """A frozenset containing all nonterminal symbols."""
        return frozenset(self._grammar)


    @property
    def start_symbol(self):
        """The first symbol in every derivation. Must be an existing nonterminal symbol."""
        return self._start_symbol


    @start_symbol.setter
    def start_symbol(self, value):
        if value not in self._grammar:
            raise ValueError("The start symbol must be an existing nonterminal symbol.")

        self._start_symbol = value


    def __iter__(self):
        """Iterator over the nonterminal symbols."""
        return iter(self._grammar)


    def __getitem__(self, nonterminal):
        """Get a list of the possible derivations for a certain nonterminal symbol.

        Args:
            nonterminal (inst): A nonterminal symbol.

        Returns:
            (list) A list of lists that contain symbols. Each sub-list corresponds to the result
                of a derivation for the given nonterminal symbol.

        Raises:
            KeyError: If the nonterminal symbol is not contained in the grammar.
        """
        return self._grammar[nonterminal]


    def add(self, lhs, rhs):
        """Add a new rule to the grammar.

        Args:
            lhs (int): A nonterminal symbol.
            rhs (list): Possible derivations for `lhs`, represented as a list of integer lists.

        Returns:
            (NumericBNF) self
        """
        self._cached_terminal_symbols = None

        if lhs not in self._grammar:
            self._grammar[lhs] = []

        self._grammar[lhs] += [r for r in rhs if r not in self._grammar[lhs]]

        return self


    def remove(self, lhs, rhs):
        """Remove a rule from the grammar.

        Args:
            lhs (int): A nonterminal symbol.
            rhs (list): Possible derivations for `lhs`, represented as a list of integer lists.

        Returns:
            (NumericBNF) self

        Raises:
            KeyError: If lhs is not a nonterminal.
            ValueError: If the rule is not in the grammar.
        """
        self._cached_terminal_symbols = None

        for rule in rhs:
            self._grammar[lhs].remove(rule)

        if not self._grammar[lhs]:
            del self._grammar[lhs]

        return self


    def pop(self, lhs, rhs):
        """Remove a rule from the grammar and return it.

        Args:
            lhs (int): A nonterminal symbol.
            rhs (list): Possible derivations for `lhs`, represented as a list of integer lists.

        Returns:
            (pair): lhs, rhs of the removed rule. lhs may be None

        Raises:
            KeyError: If lhs is not a nonterminal.
            ValueError: If the rule is not in the grammar.
        """
        self._cached_terminal_symbols = None

        for rule in rhs:
            self._grammar[lhs].remove(rule)

        if not self._grammar[lhs]:
            del self._grammar[lhs]
            return lhs, rhs

        return None, rhs


    def save(self, path):
        """Save the BNF as a json file.

        Note:
            If the filename does not end with the suffix '.json' it will be appended.

        Args:
            path (str): path (including filename) where the grammar will be saved to.

        Returns:
            (NumericBNF) self
        """
        path = os.path.abspath(path)
        path += "" if path.endswith(".json") else ".json"

        data = self.get_config()

        with open(path, "w") as save_file:
            json.dump(data, save_file)

        return self


    @classmethod
    def load(cls, path):
        """Load a NumericBNF from a json file.

        Note:
            If the filename does not end with the suffix '.json' it will be appended.

        Args:
            path (str): path (including filename) where the grammar will be loaded from.

        Returns:
            (NumericBNF) A new instance of a NumericBNF

        Raises:
            ValueError: If the start symbol is not a nonterminal symbol.
        """
        path = os.path.abspath(path)
        path += "" if path.endswith(".json") else ".json"

        with open(path, "r") as save_file:
            data = json.load(save_file)

        bnf = cls()

        bnf._grammar = {int(k): v for k, v in data["grammar"].items()}
        bnf.start_symbol = data["start_symbol"]

        return bnf


    @classmethod
    def make_sentence(cls, tree):
        """Construct a sentence from a parse tree.

        Args:
            tree (igraph.Graph): A directed tree.

        Returns:
            (numpy.array) A vector representing a sentence.
        """
        MAX_CHILD_INDEX, SYMBOL = TreeAttributes.MAX_CHILD_INDEX, TreeAttributes.SYMBOL

        key = "child_index" if MAX_CHILD_INDEX in tree.attributes() else "global_index"

        return np.array(
            [leaf[SYMBOL] for leaf in leafs(tree, sort=EdgeSort(key)) if leaf[SYMBOL]],
            dtype=np.int32
        )


    @classmethod
    def make_record(cls, tree):
        """Construct a sentence suitable to be used as a TensorFlow record.

        Args:
            tree (igraph.Graph): A directed tree.

        Returns:
            (str) A serialized representation of the sentence.
        """
        MAX_CHILD_INDEX, SYMBOL = TreeAttributes.MAX_CHILD_INDEX, TreeAttributes.SYMBOL

        key = "child_index" if MAX_CHILD_INDEX in tree.attributes() else "global_index"

        int_list = tf.train.Int64List(
            value=[leaf[SYMBOL] for leaf in leafs(tree, sort=EdgeSort(key)) if leaf[SYMBOL]]
        )

        feature = tf.train.Features(feature={"program": tf.train.Feature(int64_list=int_list)})
        return tf.train.Example(features=feature).SerializeToString()


    def is_normalized(self):
        """Check if the numeric BNF is normalized.

        See help(NumericBNF) for the definition of 'normalized NumericBNF'.

        Returns:
            (bool) True if the numeric BNF is normalized, False otherwise.
        """
        symbols = sorted(self.terminal_symbols) + sorted(self.nonterminal_symbols)
        for idx, symbol in enumerate(symbols):
            if symbol != idx + 1:
                return False

        return True


    def normalize(self):
        """Normalize the BNF.

        A NumericBNF is considered to be 'normalized' if and only if

        1. The symbols are consecutive unsigned integers starting at 1.
        2. max(terminal symbols) == min(nonterminal symbols) - 1

        Returns:
            (dict) The mapping from old symbols to new symbols.
        """
        symbols = sorted(self.terminal_symbols) + sorted(self.nonterminal_symbols)
        symbol_map = {symbol: idx + 1 for idx, symbol in enumerate(symbols)}

        grammar = {
            symbol_map[lhs]: [[symbol_map[symbol] for symbol in rule] for rule in rhs]
            for lhs, rhs in self._grammar.items()
        }

        self._cached_terminal_symbols = None

        self._grammar = grammar

        if self.start_symbol is not None:
            self.start_symbol = symbol_map[self.start_symbol]

        return symbol_map


    def get_config(self):
        """The configuration of this numeric BNF instance.

        Returns:
            (dict) Containing the start_symbol and grammar.
        """
        return {
            "start_symbol": self._start_symbol,
            "grammar": self._grammar
        }


    def _compute_terminal_symbols(self):
        """Compute the list of terminal symbols and store them in a cache.

        Returns:
            (NumericBNF) self
        """
        self._cached_terminal_symbols = frozenset(
            symbol
            for rhs in self._grammar.values()
            for rule in rhs
            for symbol in rule
            if symbol not in self._grammar
        )
        return self




class BNF:
    """A context-free grammar in Backus-Naur form.

    This class is a high level API, which wraps around a NumericBNF.
    The BNF represents the grammar symbols as strings and use a SymbolEncoder internally.

    The grammar is stored in a single dict of the form::

        grammar[nonterminal] = [[terminal | nonterminal, ...], ...]

    The sets of terminal and nonterminal symbols are defined implicitly.
    Every symbol for which no derivation rule exists is considerd to be a terminal symbol.

    A BNF is considered to be 'normalized' if and only if its internal NumericBNF is 'normalized'.
    After constructionen a BNF will be normalized and every method which might change this will
    explictly say so.

    Args:
        start_symbol (int): The start symbol for every derivation.
        grammar (dict): A mapping from nonterminals to possible derivations.

    Raises:
        ValueError: If the start symbol is specified and is not a nonterminal symbol.
    """
    _ASSIGNMENT = "::="
    _SEPERATOR = "|"
    _NONTERMINAL_REGEX = re.compile(r"<[^\s]*>")
    _TERMINAL_REGEX = re.compile(r"'.*'")
    _COMMENT_PREFIX = "#"


    def __init__(self, start_symbol=None, grammar=None):
        self._numeric_bnf = NumericBNF()
        self._symbol_encoder = DynamicSymbolEncoder()

        self._cached_grammar = None
        self._cached_terminal_symbols = None
        self._cached_nonterminal_symbols = None

        if grammar is not None:
            self.parse(grammar)

        if start_symbol is not None:
            self.start_symbol = start_symbol

        if not self.is_normalized():
            self.normalize()


    @classmethod
    def from_file(cls, filepath):
        """Create a BNF from a file that contains a grammar description.

        The left hand side of the first grammar rule present in the file is assumed
        to be the start symbol.

        Args:
            filepath (str): The path to the file that contains the grammar description.

        Returns:
            (BNF): A new instance of a BNF.

        Raises:
            (ValueError): On formatting errors.
        """
        bnf = cls()

        bnf.parse(filepath)

        # add the start symbol
        with open(filepath, "r") as grammar_file:
            for line in grammar_file:
                if (not line) or line.lstrip().startswith(cls._COMMENT_PREFIX):
                    continue

                lhs = line.split(cls._ASSIGNMENT)[0].strip()

                if not cls._NONTERMINAL_REGEX.match(lhs):
                    raise ValueError("{} is not a valid nonterminal symbol.".format(lhs))

                bnf.start_symbol = lhs[1:-1]
                break

        return bnf


    @property
    def numeric(self):
        """Provides access to the internal NumericBNF. Usefull when calling algorithms."""
        return self._numeric_bnf

    @property
    def symbol_encoder(self):
        """The SymbolEncoder used to interface with the internal NumericBNF."""
        return self._symbol_encoder


    @property
    def grammar(self):
        """A copy (!) of the grammar rules"""
        if self._cached_grammar is None:
            self._compute_grammar()

        # there is no frozen dict, hence this is the save alternative
        return self._cached_grammar.copy()


    @property
    def terminal_symbols(self):
        """A frozenset containing all terminal symbols."""
        if self._cached_terminal_symbols is None:
            self._compute_terminal_symbols()

        return self._cached_terminal_symbols


    @property
    def nonterminal_symbols(self):
        """A frozenset containing all nonterminal symbols."""
        if self._cached_nonterminal_symbols is None:
            self._compute_nonterminal_symbols()

        return self._cached_nonterminal_symbols


    @property
    def start_symbol(self):
        """The first symbol in every derivation. Must be an existing nonterminal symbol."""
        return self._symbol_encoder.decode(self._numeric_bnf._start_symbol)


    @start_symbol.setter
    def start_symbol(self, value):
        self._numeric_bnf.start_symbol = self._symbol_encoder.encode(value)


    @property
    def assignment(self):
        """The assignment operator."""
        return self._ASSIGNMENT


    @property
    def seperator(self):
        """The seperator (or operator) of derivations."""
        return self._SEPERATOR


    @property
    def comment_prefix(self):
        """The prefix of comment lines, which will be ignored by the parser."""
        return self._COMMENT_PREFIX


    @property
    def nonterminal_regex(self):
        """A regex specifying valid nonterminal symbols."""
        return self._NONTERMINAL_REGEX


    @property
    def terminal_regex(self):
        """A regex specifying valid terminal symbols."""
        return self._TERMINAL_REGEX


    def __iter__(self):
        """Iterator over the nonterminal symbols."""
        return iter(self.nonterminal_symbols)


    def __getitem__(self, nonterminal):
        """Get a list of the possible derivations for a certain nonterminal symbol.

        Args:
            nonterminal (str): A nonterminal symbol

        Returns:
            (list) A list of lists that contain symbols. Each sub-list corresponds to the result
                of a derivation for the given nonterminal symbol.

        Raises:
            KeyError: If the nonterminal symbol is not contained in the grammar.
        """
        if self._cached_grammar is None:
            self._compute_grammar()

        return self._cached_grammar[nonterminal]


    def add(self, lhs, rhs):
        """Add a new rule to the grammar.

        Note:
            The resulting grammar will in general not be normalized.

        Args:
            lhs (str): A nonterminal symbol.
            rhs (list): Possible derivations for `lhs`, represented as a list of str lists.

        Returns:
            (BNF) self
        """
        self._clear_cache()
        return self._numeric_bnf.add(*self._encode(lhs, rhs))


    def remove(self, lhs, rhs):
        """Remove a rule from the grammar.

        Note:
            The resulting grammar will in general not be normalized.

        Args:
            lhs (str): A nonterminal symbol.
            rhs (list): Possible derivations for `lhs`, represented as a list of str lists.

        Returns:
            (BNF) self

        Raises:
            KeyError: If lhs is not a nonterminal.
            ValueError: If the rule is not in the grammar.
        """
        self._clear_cache()

        numeric_lhs, numeric_rhs = self._encode(lhs, rhs)
        poped_lhs, poped_rhs = self._numeric_bnf.pop(numeric_lhs, numeric_rhs)

        self._remove_unused_symbols(poped_lhs, poped_rhs)

        return self


    def pop(self, lhs, rhs):
        """Add a new rule to the grammar and return it

        Note:
            The resulting grammar will in general not be normalized.

        Args:
            lhs (str): A nonterminal symbol.
            rhs (list): Possible derivations for `lhs`, represented as a list of str lists.

        Returns:
            (pair): lhs, rhs of the removed rule. lhs may be None

        Raises:
            KeyError: If lhs is not a nonterminal.
            ValueError: If the rule is not in the grammar.
        """
        self._clear_cache()

        numeric_lhs, numeric_rhs = self._encode(lhs, rhs)
        poped_lhs, poped_rhs = self._numeric_bnf.pop(numeric_lhs, numeric_rhs)

        ret_lhs, ret_rhs = self._decode(poped_lhs, poped_rhs)
        self._remove_unused_symbols(poped_lhs, poped_rhs)

        return ret_lhs, ret_rhs


    def save(self, path):
        """Save the BNF to a json file.

        Note:
            If the filename does not end with the suffix '.json' it will be appended.

        Args:
            path (str): path (including filename) where the grammar will be loaded from.

        Returns:
            (BNF) self
        """
        path = os.path.abspath(path)
        path += "" if path.endswith(".json") else ".json"

        data = {
            "start_symbol": self.start_symbol,
            "grammar": self.grammar
        }

        data.update(self._symbol_encoder.get_config())

        with open(path, "w") as save_file:
            json.dump(data, save_file)

        return self


    @classmethod
    def load(cls, path, with_encoding=True):
        """Load a BNF from a json file.

        Note:
            If the filename does not end with the suffix '.json' it will be appended.

        Note:
            If `with_encoding` is True, the grammar might not be normalized anymore.

        Args:
            path (str): path (including filename) where the grammar will be loaded from.
            with_encoding (str): If True the encoding will be loaded as well

        Returns:
            (BNF) A new instance of a BNF.

        Raises:
            ValueError: If the start symbol is not a nonterminal symbol.
        """
        path = os.path.abspath(path)
        path += "" if path.endswith(".json") else ".json"

        with open(path, "r") as save_file:
            data = json.load(save_file)

        bnf = cls()

        bnf._symbol_encoder = DynamicSymbolEncoder()

        if with_encoding:
            bnf._symbol_encoder._str2int = data["encoding"]
            bnf._symbol_encoder._int2str = data["decoding"]

        for lhs, rhs in data["grammar"].items():
            bnf.add(lhs, rhs)

        bnf.start_symbol = data["start_symbol"]

        if not with_encoding:
            bnf.normalize()

        return bnf


    def parse(self, value):
        """Parse a grammar description.

        A grammar descriptions is a sting of the following form::

            nonterminal ::= (terminal or nonterminal)... | (terminal or nonterminal)... | ...

        Args:
            value (str or list) A filepath, a grammar description string or a list of the later.

        Returns:
            (BNF) self

        Raises:
            (ValueError): On formatting errors.
        """
        if isinstance(value, str):
            if os.path.isfile(value):
                with open(value, "r") as grammar_file:
                    value = [line.rstrip() for line in grammar_file]
            else:
                value = value.split("\n")

        rules = []

        for line in value:
            if not line or line.lstrip().startswith(self.comment_prefix):
                continue

            splitted = line.split(self.assignment)

            if len(splitted) == 2:
                rules.append(splitted)
            elif len(splitted) == 1:
                rules[-1][1] += splitted[0]
            else:
                raise ValueError("Only one assignment operator is allow per line")

        for raw_lhs, raw_rhs in rules:
            lhs = self._extract(raw_lhs, nonterminal_only=True)
            rhs = [[self._extract(s) for s in r.split()] for r in raw_rhs.split(self.seperator)]
            self.add(lhs, rhs)

        return self


    @classmethod
    def make_sentence(cls, tree):
        """Construct a sentence from a parse tree.

        Args:
            tree (igraph.Graph): A directed tree.

        Returns:
            (list) A list representing a sentence.
        """
        MAX_CHILD_INDEX, SYMBOL = TreeAttributes.MAX_CHILD_INDEX, TreeAttributes.SYMBOL

        key = "child_index" if MAX_CHILD_INDEX in tree.attributes() else "global_index"

        return [leaf[SYMBOL] for leaf in leafs(tree, sort=EdgeSort(key)) if leaf[SYMBOL]]


    @classmethod
    def make_record(cls, tree):
        """Construct a sentence suitable to be used as a TensorFlow record.

        Args:
            tree (igraph.Graph): A directed tree.

        Returns:
            (str) A serialized representation of the sentence.
        """
        MAX_CHILD_INDEX, SYMBOL = TreeAttributes.MAX_CHILD_INDEX, TreeAttributes.SYMBOL

        key = "child_index" if MAX_CHILD_INDEX in tree.attributes() else "global_index"

        value = [
            bytes(leaf[SYMBOL], "utf-8")
            for leaf in leafs(tree, sort=EdgeSort(key)) if leaf[SYMBOL]
        ]

        prog = tf.train.Feature(bytes_list=tf.train.BytesList(value=value))

        feature = tf.train.Features(feature={"program": prog})
        return tf.train.Example(features=feature).SerializeToString()


    def is_normalized(self):
        """Check if the grammar (more precisely: its numeric representation) is normalized.

        Returns:
            (bool) True if the grammar is normalized, false otherwise.
        """
        return self._numeric_bnf.is_normalized()


    def normalize(self):
        """Normalize the grammar.

        Returns:
            (BNF) self
        """
        mapping = self._numeric_bnf.normalize()
        self._symbol_encoder.apply_mapping(mapping)
        return self


    def get_config(self):
        """The configuration of this BNF instance.

        Returns:
            (dict) mapping to the start symbol, grammar and encoding.
        """
        return {
            "start_symbol": self.start_symbol,
            "grammar": self.grammar,
            **self._symbol_encoder.get_config()
        }


    def _extract(self, symbol, nonterminal_only=False):
        """
        Args:
            symbol (str): The symbol to be extracted
            nonterminal_only (bool): If true only nonterminals will be extracted.

        Raises:
            ValueError: If symbol is not a valid (nonterminal) symbol.
        """
        symbol = symbol.strip()

        is_nonterminal = self.nonterminal_regex.match(symbol)
        is_terminal = self.terminal_regex.match(symbol)

        if nonterminal_only and not is_nonterminal:
            raise ValueError("{} is not a valid nonterminal symbol.".format(symbol))

        elif not (is_nonterminal or is_terminal):
            raise ValueError("{} is not a valid symbol.".format(symbol))

        return symbol[1:-1]


    def _encode(self, lhs, rhs):
        """Encode every symbol in lhs and rhs.

        Returns:
            (pair) The encoded lhs and rhs.
        """
        converted_lhs = None if lhs is None else self._symbol_encoder.encode(lhs)
        converted_rhs = [[self._symbol_encoder.encode(s) for s in rule] for rule in rhs]
        return converted_lhs, converted_rhs


    def _decode(self, lhs, rhs):
        """Decode every symbol in lhs and rhs.

        Returns:
            (pair) The decoded lhs anr rhs.

        Raises:
            IndexError: If any symbol in lhs or rhs has never been seen before.
            ValueError: If any symbol in lhs or rhs has been removed.
        """
        converted_lhs = None if lhs is None else self._symbol_encoder.decode(lhs)
        converted_rhs = [[self._symbol_encoder.decode(s) for s in rule] for rule in rhs]
        return converted_lhs, converted_rhs


    def _remove_unused_symbols(self, lhs, rhs):
        """Remove every unused symbol contained in lhs and rhs from the encoder.

        Returns:
            (BNF) self
        """
        nonterminal_symbols = self._numeric_bnf.nonterminal_symbols
        terminal_symbols = self._numeric_bnf.terminal_symbols

        if (lhs is not None) and (lhs not in nonterminal_symbols):
            self._symbol_encoder.remove(lhs)

        for s in it.chain.from_iterable(rhs):
            if (s not in terminal_symbols) and (s not in nonterminal_symbols):
                self._symbol_encoder.remove(s)

        return self


    def _clear_cache(self):
        """Clear the symbol and grammar caches.

        Returns:
            (BNF) self
        """
        self._cached_grammar = None
        self._cached_terminal_symbols = None
        self._cached_nonterminal_symbols = None


    def _compute_grammar(self):
        """Compute and cache a non numeric representation of the grammar.

        Returns:
            (BNF) self
        """
        self._cached_grammar = {
            self._symbol_encoder.decode(lhs): [[self._symbol_encoder.decode(symbol)
            for symbol in rule]
            for rule in rhs]
            for lhs, rhs in self._numeric_bnf.grammar.items()
        }
        return self


    def _compute_terminal_symbols(self):
        """Compute and cache a non numeric representation of the terminal symbols.

        Returns:
            (BNF) self
        """
        self._cached_terminal_symbols = frozenset(
            self._symbol_encoder.decode(s) for s in self._numeric_bnf.terminal_symbols
        )
        return self


    def _compute_nonterminal_symbols(self):
        """Compute and cache a non numeric representation of the nonterminal symbols.

        Returns:
            (BNF) self
        """
        self._cached_nonterminal_symbols = frozenset(
            self._symbol_encoder.decode(s) for s in self._numeric_bnf.nonterminal_symbols
        )
        return self
