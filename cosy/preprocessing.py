# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import json
import math
import numpy as np
import tensorflow as tf
import tokenizers




def build_bpe_vocab(symbol_iterator, vocab_size, verbose=False):
    """Build a BPE vocabulary of the specified size from a sequence of symbols.

    Args:
        symbol_iterator (iterator): An iterator that returns symbols represented as strings.
        vocab_size (int): The size of the vocabulary
        verbose (bool): If True, progress updates will be displayed.

    Returns:
        (list) The vocabulary of our BPE.
    """
    tokenizer = tokenizers.Tokenizer(tokenizers.models.BPE())
    trainer = tokenizers.trainers.BpeTrainer(vocab_size=vocab_size, show_progress=verbose)
    tokenizer.train_from_iterator(symbol_iterator, trainer)
    return list(tokenizer.get_vocab())




class DynamicSymbolEncoder:
    """Stores a numeric encoding for symbols represented as strings.

    If the dynamic symbol encoder observes are oov word it will automatically
    add it to its encoding. Additionally it may drop the encoding of certain symbols.

    Args:
        symbols (list): A list of strings to be encoded
        return_list (bool): If True, the `encode` method will insert the encoded symbols
            into a list before returning them.
    """
    def __init__(self, symbols=None, return_list=False):
        self._str2int = {}
        self._int2str = []

        self._return_list = return_list

        if symbols is not None:
            for symbol in symbols:
                self.encode(symbol)


    @property
    def encoding(self):
        """The map used to encode the symbols."""
        return self._str2int


    @property
    def decoding(self):
        """The map used to decode the symbols."""
        return self._int2str


    @property
    def vocab_size(self):
        """The size of the vocabulary."""
        return len(self._str2int)


    @property
    def return_list(self):
        """If True, the `encode` method will insert the codes into a list before returning them."""
        return self._return_list


    @return_list.setter
    def return_list(self, value):
        self._return_list = value


    def encode(self, symbol):
        """Encode a symbol.

        Args:
            symbol (str): The symbol to be encoded.

        Returns:
            (int) The code for this symbol.
        """
        if symbol not in self._str2int:
            self._int2str.append(symbol)
            self._str2int[symbol] = len(self._int2str) - 1

        if self._return_list:
            return [self._str2int[symbol]]

        return self._str2int[symbol]


    def decode(self, number):
        """Decode a number.

        Args:
            number (int): The number to be decoded.

        Returns:
            (str) The decoded string.

        Raises:
            AssertionError: If `number` is negative.
            IndexError: If no symbol has ever been associated with this number.
            ValueError: If the symbol associated with this number has been removed.
        """
        assert number >= 0, "Decodes symbols must be positive integers."

        symbol = self._int2str[number]

        if symbol is None:
            raise ValueError("This symbol has been removed from the encoding.")

        return symbol


    def has_encoding(self, symbol):
        """Check if an encoding for the given symbol exists.

        Args:
            symbol (str): The symbol to be checked.

        Returns:
            (bool) True if the symbol has an encoding, false otherwise.
        """
        return symbol in self._str2int


    def is_encoding(self, number):
        """Check if number encodes a symbol.

        Args:
            number (int): The number to be checked.

        Returns:
            (bool) True if the number is an encoding, false otherwise.
        """
        if number < 0:
            return False

        try:
            return self._int2str[number] is not None
        except IndexError:
            return False


    def remove(self, value):
        """Remove a symbol-number pair from the encoding.

        Args:
            value (str or int): Either the str representation or the code.

        Returns:
            (SymbolEncoder) self
        """
        symbol = self._int2str[value] if isinstance(value, int) else value

        if symbol in self._str2int:
            self._int2str[self._str2int[symbol]] = None
            del self._str2int[symbol]

        return self


    def apply_mapping(self, mapping):
        """Changed the encoding according to some mapping.

        Args:
            mapping (dict): A map from codes to different ones.

        Returns:
            (SymbolEncoder) self

        Raises:
            IndexError: If the mapping refers to non existing symbol encodings.
            KeyError: If the mapping tries to reassign removed symbols.
        """
        int2str = [None] * (max(mapping.values()) + 1)
        str2int = {}

        for old, new in mapping.items():
            symbol = self._int2str[old]
            if symbol is None:
                raise KeyError("Attempting to map a removed symbol.")

            int2str[new] = symbol
            str2int[symbol] = new

        self._int2str = int2str
        self._str2int = str2int

        return self


    def get_config(self):
        """The the configuration of this symbol encoder instance.

        Returns:
            (dict): The symbol to code and code to symbol mappings.
        """
        return {
            "encoding": self._str2int,
            "decoding": self._int2str
        }


    def save(self, filename):
        """Save the encoding to disk.

        Args:
            filename (str): The file where the encoding will be saved to.
        """
        with open(filename, "w") as f:
            json.dump(self.get_config(), f, indent=4)


    @classmethod
    def load(cls, filename):
        """Load the encoding from disk.

        Args:
            filename (str): The file that contains the saved encoding.
        """
        with open(filename, "r") as f:
            data = json.load(f)

            encoder = DynamicSymbolEncoder()

            encoder._str2int = data["encoding"]
            encoder._int2str = data["decoding"]

        return encoder




class StaticSymbolEncoder:
    """Stores a numeric encoding for symbols represented as strings.

    The encoding of the static symbol encoder may not be changed after construction.
    The code 0 will always be used to representing padding and the code (vocabulary
    size + 1) is used for oov symbols.

    Args:
        symbols (list): A list of strings to be encoded
        oov_symbol (str): The symbol used for oov encodings.
        padding_symbol (str): The symbol associated with the encoding 0.
        return_list (bool): If True, the `encode` method will insert the encoded symbols
            into a list before returning them.
    """
    def __init__(self,
                 symbols,
                 oov_symbol="<UNKNOWN>",
                 padding_symbol="<PADDING>",
                 return_list=False):
        self._str2int = {padding_symbol: 0}
        self._int2str = [padding_symbol]

        for code, symbol in enumerate(symbols):
            self._str2int[symbol] = code + 1
            self._int2str.append(symbol)

        self._vocab_size = len(self._str2int) + 1
        self._oov_symbol = oov_symbol
        self._padding_symbol = padding_symbol
        self._return_list = return_list


    @property
    def encoding(self):
        """The map used to encode the symbols."""
        return self._str2int


    @property
    def decoding(self):
        """The map used to decode the symbols."""
        return self._int2str


    @property
    def vocab_size(self):
        """The size of the vocabulary including padding and oov symbol."""
        return self._vocab_size


    @property
    def oov_symbol(self):
        """The symbol used for encodings which are oov."""
        return self._oov_symbol


    @property
    def padding_symbol(self):
        """The symbol used for padding."""
        return self._padding_symbol


    @property
    def return_list(self):
        """If True, the `encode` method will insert the codes into a list before returning them."""
        return self._return_list


    @return_list.setter
    def return_list(self, value):
        self._return_list = value


    def encode(self, symbol):
        """Encode a symbol.

        Args:
            symbol (str): The symbol to be encoded.

        Returns:
            (int) The code for this symbol.
        """
        try:
            code = self._str2int[symbol]
        except KeyError:
            code = self.vocab_size - 1 # oov code

        if self._return_list:
            return [code]

        return code


    def decode(self, number):
        """Decode a number.

        Args:
            number (int): The number to be decoded.

        Returns:
            (str) The decoded string.

        Raises:
            AssertionError: If `number` is negative.
        """
        assert number >= 0, "Decodes symbols must be positive integers."

        try:
            return self._int2str[number]
        except IndexError:
            return self._oov_symbol


    def has_encoding(self, symbol):
        """Check if an encoding for the given symbol exists.

        Args:
            symbol (str): The symbol to be checked.

        Returns:
            (bool) True if the symbol has an encoding, false otherwise.
        """
        if symbol == self.oov_symbol:
            return False

        return symbol in self._str2int


    def is_encoding(self, number):
        """Check if number encodes a symbol.

        Args:
            number (int): The number to be checked.

        Returns:
            (bool) True if the number is an encoding, false otherwise.
        """
        return 0 <= number < self.vocab_size - 1


    def get_config(self):
        """The the configuration of this symbol encoder instance.

        Returns:
            (dict): The symbol to code and code to symbol mappings.
        """
        return {
            "encoding": self._str2int,
            "decoding": self._int2str,
            "oov_symbol": self._oov_symbol,
            "padding_symbol": self._padding_symbol
        }


    def save(self, filename):
        """Save the encoding to disk.

        Args:
            filename (str): The file where the encoding will be saved to.
        """
        with open(filename, "w") as f:
            json.dump(self.get_config(), f, indent=4)


    @classmethod
    def load(cls, filename):
        """Load the encoding from disk.

        Args:
            filename (str): The file that contains the saved encoding.
        """
        with open(filename, "r") as f:
            data = json.load(f)

            encoder = StaticSymbolEncoder([])

            encoder._str2int = data["encoding"]
            encoder._int2str = data["decoding"]
            encoder._oov_symbol = data["oov_symbol"]
            encoder._padding_symbol = data["padding_symbol"]
            encoder._vocab_size = len(encoder._str2int) + 1

        return encoder




class SubTokenEncoder:
    """Stores a numeric encoding for the tokens that comprise the symbols.

    Similar to StaticSymbolEncoder the encoding may not be changed after constrution.
    The encoding of a given symbol is performed greedily from the left to the right.

    Note:
        The special tokens `end_token`, `oov_token` and `padding_token` must not occur in symbols
        provided as input to the SubTokenEncoder.encode and SubTokenEncoder.has_encoding methods.

    Args:
        sub_tokens (iterable): An iterable that yields the sub tokens that comprise the encoding.
        end_token (str): The token used to indicate the end of the token sequence. If this is None
            no end token will be appended to a token sequence during encoding.
        oov_token (str): The token used to indicate elements of a symbol that or OOV. If this is
            None an error will be raised if symbol can not be completed encoded.
        padding_token (str): The token used to indicate padding.
    """
    def __init__(self,
                 sub_tokens,
                 end_token=None,
                 oov_token=None,
                 padding_token="<PADDING>"):
        self._str2int = {}
        self._int2str = []

        self._min_token_length = math.inf
        self._max_token_length = 0

        for code, token in enumerate(sub_tokens):
            token_length = len(token)

            if token_length < self._min_token_length:
                self._min_token_length = token_length

            if token_length > self._max_token_length:
                self._max_token_length = token_length

            self._str2int[token] = code + 1
            # note that int2str maps: token -> code_of(token) - 1
            self._int2str.append(token)

        self._end_token = end_token
        self._end_code = None if end_token is None else len(self._int2str) + 1

        self._oov_token = oov_token
        offset = 1 if end_token is None else 2
        self._oov_code = None if oov_token is None else len(self._int2str) + offset

        self._padding_token = padding_token
        self._padding_code = 0


    @property
    def vocab_size(self):
        """The size of the vocabulary including padding and potentially the end and oov token."""
        num_codes = len(self._int2str) + 1 # for padding

        if self._end_token is not None:
            num_codes += 1

        if self._oov_token is not None:
            num_codes += 1

        return num_codes


    @property
    def end_token(self):
        """The string that represents the end of sub token sequence. May be None."""
        return self._end_token


    @property
    def end_code(self):
        """The code associated with the end token. May be None"""
        return self._end_code


    @property
    def oov_token(self):
        """The string that represents OOV sub tokens. May be None"""
        return self._oov_token


    @property
    def oov_code(self):
        """The code associated with the OOV token. May be None"""
        return self._oov_code


    @property
    def padding_token(self):
        """The token used for padding."""
        return self._padding_token


    @property
    def padding_code(self):
        """The code associated with the padding token."""
        return self._padding_code


    def encode(self, symbol):
        """Encode a symbol.

        Args:
            symbol (str): The symbol to be encoded.

        Returns:
            (list) The encoded sub tokens that comprise this symbol.
        """
        encoded_sub_tokens = []
        start = 0

        while (start + self._min_token_length) < (len(symbol) + 1):
            sub_token_size = 0

            # constrain search range based on the lengths of sub tokens in our encoding
            min_stop = start + self._min_token_length
            max_stop = min(len(symbol), start + self._max_token_length)

            # search for the longest possible sub token within the (start, stop) boundaries
            for stop in range(max_stop, min_stop - 1, -1):
                if symbol[start:stop] in self._str2int:
                    encoded_sub_tokens.append(self._str2int[symbol[start:stop]])
                    sub_token_size = stop - start
                    break

            # no subtoken was found => add OOV token and continue or raise an error if OOV
            # tokens are disabled
            if sub_token_size == 0:
                if self._oov_token is None:
                    raise ValueError("Unable to encode the symbol: {}".format(symbol))

                encoded_sub_tokens.append(self._oov_code)
                sub_token_size += 1

            start += sub_token_size

        # add the end token if this feature is enabled
        if self._end_token is not None:
            encoded_sub_tokens.append(self._end_code)

        return encoded_sub_tokens


    def decode(self, number):
        """Decode a number that represents a sub token.

        Args:
            number (int): The number to be decoded.

        Returns:
            (str) The string value of the sub token.

        Raises:
            AssertionError: If `number` is negative.
            ValueError: If OVV tokens are disabled and number does not represent an encoded token.
        """
        assert number >= 0, "Decodes symbols must be positive integers."

        if number == 0:
            return self._padding_token

        try:
            return self._int2str[number - 1]

        except IndexError:
            if (self._end_token is not None) and (number == self._end_code):
                return self._end_token

            if self._oov_token is not None:
                return self._oov_token

            # in this case oov tokens are disabled
            raise ValueError("Unable to decode the integer {}".format(number))


    def has_encoding(self, symbol, sub_token_only=False):
        """Check if it is possible to encode the given symbols without using the OOV token.

        Args:
            symbol (str): The symbol to be checked.
            sub_token_only (bool): If True, the symbol must be a proper sub token contained in the
                encoding, otherwise False will be returned.

        Returns:
            (bool) True if such an encoding exists, False otherwise.
        """
        if sub_token_only:
            return symbol in self._str2int

        try:
            encoded_subtokens = self.encode(symbol)

            if self._oov_token is None:
                return True
            else:
                return all(code != self._oov_code for code in encoded_subtokens)
        except:
            return False


    def is_encoding(self, number):
        """Check if number encodes a sub token.

        Args:
            number (int): The number to be checked.

        Returns:
            (bool) True if the number is an encoding, false otherwise.
        """
        if 0 <= number <= len(self._int2str):
            return True

        if (self._end_token is not None) and (number == self._end_code):
            return True

        return False


    def get_config(self):
        """The the configuration of this sub token encoder instance.

        Returns:
            (dict): Holds the internal state of the encoder.
        """
        return {
            "encoding": self._str2int,
            "decoding": self._int2str,
            "end_token": self._end_token,
            "end_code": self._end_code,
            "oov_token": self._oov_token,
            "oov_code": self._oov_code,
            "min_token_length": self._min_token_length,
            "max_token_length": self._max_token_length,
            "padding_token": self._padding_token,
            "padding_code": self._padding_code,
        }


    def save(self, filename):
        """Save the encoding to disk.

        Args:
            filename (str): The file where the encoding will be saved to.
        """
        with open(filename, "w") as f:
            json.dump(self.get_config(), f, indent=4)


    @classmethod
    def load(cls, filename):
        """Load the encoding from disk.

        Args:
            filename (str): The file that contains the saved encoding.
        """
        with open(filename, "r") as f:
            data = json.load(f)

            encoder = SubTokenEncoder([])

            encoder._str2int = data["encoding"]
            encoder._int2str = data["decoding"]
            encoder._end_token = data["end_token"]
            encoder._end_code = data["end_code"]
            encoder._oov_token = data["oov_token"]
            encoder._oov_code = data["oov_code"]
            encoder._min_token_length = data["min_token_length"]
            encoder._max_token_length = data["max_token_length"]
            encoder._padding_token = data["padding_token"]
            encoder._padding_code = data["padding_code"]

        return encoder




def int_feature(int_list):
    """Convert a list of integers into a tensorflow feature.

    Args:
        int_list (list): A list of integers.

    Returns:
        (tf.train.Feature) The new tensorflow feature.
    """
    return tf.train.Feature(int64_list=tf.train.Int64List(value=int_list))


def float_feature(float_list):
    """Convert a list of floats into a tensorflow feature.

    Args:
        float_list (list): A list of floats.

    Returns:
        (tf.train.Feature) The new tensorflow feature.
    """
    return tf.train.Feature(float_list=tf.train.FloatList(value=float_list))


def bytes_feature(bytes_list):
    """Convert a list of byte strings into a tensorflow feature.

    Args:
        byte_list (list): A list of byte strings.

    Returns:
        (tf.train.Feature) The new tensorflow feature.
    """
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=bytes_list))


def str_feature(str_list):
    """Convert a list of strings into a tensorflow feature.

    Args:
        str_list (list): A list of strings.

    Returns:
        (tf.train.Feature) The new tensorflow feature.
    """
    return bytes_feature([bytes(s, "utf-8") for s in str_list])


def bool_feature(bool_list):
    """Convert a list of boolean values into a tensorflow feature.

    Args:
        bool_list (list): A list of boolean values.

    Returns:
        (tf.train.Feature) The new tensorflow feature.
    """
    return int_feature([int(b) for b in bool_list])


def int_feature_list(iterable):
    """Convert an iterable containing lists of integers into a tensorflow feature list.

    Args:
        iterable (iterable): Any iterable object containing lists of integers.

    Returns:
        (tf.train.FeatureList) The new tensorflow feature list.
    """
    return tf.train.FeatureList(feature=[int_feature(l) for l in iterable])


def float_feature_list(iterable):
    """Convert an iterable containing lists of floats into a tensorflow feature list.

    Args:
        iterable (iterable): Any iterable object containing lists of floats.

    Returns:
        (tf.train.FeatureList) The new tensorflow feature list.
    """
    return tf.train.FeatureList(feature=[float_feature(l) for l in iterable])


def bytes_feature_list(iterable):
    """Convert an iterable containing lists of byte strings into a tensorflow feature list.

    Args:
        iterable (iterable): Any iterable object containing lists of byte strings.

    Returns:
        (tf.train.FeatureList) The new tensorflow feature list.
    """
    return tf.train.FeatureList(feature=[bytes_feature(l) for l in iterable])


def str_feature_list(iterable):
    """Convert an iterable containing lists of strings into a tensorflow feature list.

    Args:
        iterable (iterable): Any iterable object containing lists of strings.

    Returns:
        (tf.train.FeatureList) The new tensorflow feature list.
    """
    return tf.train.FeatureList(feature=[str_feature(l) for l in iterable])


def bool_feature_list(iterable):
    """Convert an iterable containing lists of boolean values into a tensorflow feature list.

    Args:
        iterable (iterable): Any iterable object containing lists of boolean values.

    Returns:
        (tf.train.FeatureList) The new tensorflow feature list.
    """
    return tf.train.FeatureList(feature=[bool_feature(l) for l in iterable])




def smallest_int_type(value, allow_uint8=True, allow_none=True):
    """Compute the smallest tensorflow integer type capable of holding the provided value.

    Args:
        value (int): The value for which to compute the type.
        allow_uint8 (bool): If True tf.uint8 is a possible result, otherwise it will be ignored.
        allow_none (bool): If True no error will be raised if the `value` is None.
            Instead tf.int64 is returned.

    Returns:
        (type) One of the following tensorflow types: uint8, int8, int16, int32 and int64

    Raises:
        (ValuerError): If no tensorflow type exists that can hold the value.
    """
    if allow_none and (value is None):
        return tf.int64

    if tf.int8.min <= value <= tf.int8.max:
        return tf.int8

    if allow_uint8 and (tf.uint8.min <= value <= tf.uint8.max):
        return tf.uint8

    if tf.int16.min <= value <= tf.int16.max:
        return tf.int16

    if tf.int32.min <= value <= tf.int32.max:
        return tf.int32

    if tf.int64.min <= value <= tf.int64.max:
        return tf.int64

    raise ValueError("The provided value is larger then largest possible uint64.")




def window_iter(array_like, window_size, step_size):
    """Slide a window over an array like structure and yield the slices one at a time.

    The returned slice and the associated mask are of the same structure
    (e.g. list, np.ndarray, tf.Tensor) as the `array_like` input.
    The data type of the mask is always either bool, np.bool or tf.bool.

    Args:
        array_like (list, ndarray, Tensor): A list, 1D numpy array or rank 1 tensor.
        window_size (int): Size of the sliding window.
        step_size (int): Step size of the sliding window.

    Yields:
        (tuple) A pair containing the window together with a mask. mask[i] is True if the symbol at
            position i in the window is not already part of a previous window and False otherwise.
    """
    num_processed, offset = 0, 0

    while num_processed < len(array_like):
        rel_num_processed = num_processed - offset

        window = array_like[offset:(offset + window_size)]
        mask = [False] * rel_num_processed + [True] * (len(window) - rel_num_processed)

        if isinstance(array_like, tf.Tensor):
            mask = tf.constant(mask, dtype=tf.bool)

        elif isinstance(array_like, np.ndarray):
            mask = np.array(mask, dtype=np.bool)

        num_processed = offset + len(window)
        offset += step_size

        yield window, mask




def add_shifted_labels(*tensors, offset=1):
    """Labels each input tensor with a version of itself where the innermost dimension is shifted.

    For example assume the input tensor x = [1, 2, 3, 4, 5, 6, 7] is given. We then have::

        add_shifted_labels(x, offset=1) == ([1, 2, 3, 4, 5, 6], [2, 3, 4, 5, 6, 7])
        add_shifted_labels(x, offset=3) == ([2, 3, 4, 5], [4, 5, 6, 7])

    In case we have multiple tensors, e.g. x = [1, 2, 3] and y = [4, 5, 6] we get::

        add_shifted_labels(x, y, offset=1) == (([1, 2], [4, 5]), ([2, 3], [5, 6]))

    In case the inputs have multiple dimensions, only the innermost one will be shifted.

    Args:
        *tensors (Tensor(s)): One or more tensors.
        offset (int): The offest by which the labels will be shifted. Must be a positive integer.

    Returns:
        (tuple) A pair where the first element is the truncated input and the second one its
            shifted counterpart.
    """
    if len(tensors) > 1:
        return (
            tuple((x[..., :-offset] for x in tensors)),
            tuple((x[..., offset:] for x in tensors))
        )

    return (tensors[0][..., :-offset], tensors[0][..., offset:])




def _sliding_window_helper(tensor, window_size, step_size, padding_value):
    windows = []
    offset = 0

    for idx in range(0, tensor.shape[-1] - window_size + 1, step_size):
        windows.append(tensor[..., idx:(idx + window_size)])
        offset += step_size

    # Sliding window does not align with tensor shape.
    # Hence, we need to add padding to the last window due to shape constraints on output.
    if (offset - step_size) + window_size != tensor.shape[-1]:
        padding = [[0, 0]] * (len(tensor.shape) - 1)
        padding.append([0, (offset + window_size) - tensor.shape[-1]])

        windows.append(tf.pad(tensor[..., offset:], padding, constant_values=padding_value))

    return windows


def sliding_window(*tensors, window_size, step_size, padding_value=0):
    """Creates a dataset from the input tensors via a sliding window.

    In case multiple input tensors are provided all of them must have the same shape and type.
    The last window will be padded if necessary, such that all output tensor have the same shape.
    The last dimension of the input tensors must not be None.

    Args:
        *tensors (Tensor(s)): One or more tensors.
        windows_size (int): The size of the sliding window. Must be a positive integer.
        step_size (int): The step size by which the window will moved. Must be a positive integer.
        padding_value (Any): The value to be used for padding. Must of the same type as the input
            tensors.

    Returns:
        (tf.data.Dataset) A dataset containing the sections of the input tensors.
    """
    if len(tensors) == 1:
        windows = _sliding_window_helper(tensors[0], window_size, step_size, padding_value)
    else:
        windows = tuple(
            _sliding_window_helper(t, window_size, step_size, padding_value)
            for t in tensors
        )

    return tf.data.Dataset.from_tensor_slices(windows)




def mask_sample(y_true, sample_weight=None, mask_values=0, mask_dtype=tf.int64):
    """Mask certain values in y_true.

    Args:
        y_true (Tensor): The ground truth values.
        sample_weight (Tensor): A weight tensor that determines the impact each element
            in the sample has on the accuracy.
        mask_values (list or Any): The values to be masked. Must be a list or a single value.
        mask_dtype (tf.dtypes.DType): The datatype used for the mask tensor.

    Returns:
        (Tensor) The mask of 'y_true', multiplied with 'sample_weight' if it is not None.
    """
    if isinstance(mask_values, list):
        mask_list = [tf.math.equal(y_true, v)[tf.newaxis, ...] for v in mask_values]
        mask = tf.math.reduce_any(tf.stack(mask_list, axis=0), axis=0)[0, ...]
    else:
        mask = tf.math.equal(y_true, mask_values)

    mask = tf.math.logical_not(mask)
    mask = tf.cast(mask, dtype=mask_dtype)

    if sample_weight is not None:
        mask *= sample_weight

    return mask
