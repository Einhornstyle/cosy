#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import os

import numpy as np
import tensorflow as tf

import cosy.preprocessing as cp
import cosy.deprecated.data as cds




# Trivial Factories usefull for testing specific features
class SingleFactory(cds.DatasetFactoryABC):
    def make_record(self, alpha):
        features = tf.train.Features(feature={"alpha": cp.int_feature(alpha)})
        return tf.train.Example(features=features)

    def load(self, filenames):
        dataset = tf.data.TFRecordDataset(filenames)
        dataset = dataset.map(self._parse)
        return dataset

    def _parse(self, serialized):
        feature = tf.io.parse_single_example(
            serialized,
            features={"alpha": tf.io.FixedLenFeature([], tf.int64)}
        )

        return feature["alpha"]


class DoubleFactory(cds.DatasetFactoryABC):
    def make_record(self, alpha, beta):
        features = tf.train.Features(feature={
            "alpha": cp.int_feature(alpha),
            "beta": cp.str_feature(beta)
        })

        return tf.train.Example(features=features)

    def load(self, filenames):
        dataset = tf.data.TFRecordDataset(filenames)
        dataset = dataset.map(self._parse)
        return dataset

    def _parse(self, serialized):
        feature = tf.io.parse_single_example(
            serialized,
            features={
                "alpha": tf.io.FixedLenFeature([], tf.int64),
                "beta": tf.io.FixedLenFeature([], tf.string)
            }
        )

        return feature["alpha"], feature["beta"]


class SequenceFactory(cds.DatasetFactoryABC):
    def make_record(self, sequence, beta):
        features = tf.train.Features(feature={"beta": cp.str_feature(beta)})

        feature_lists = tf.train.FeatureLists(
            feature_list={"sequence": cp.float_feature_list(sequence)}
        )

        return tf.train.SequenceExample(context=features, feature_lists=feature_lists)

    def load(self, filenames):
        dataset = tf.data.TFRecordDataset(filenames)
        dataset = dataset.map(self._parse)
        return dataset

    def _parse(self, serialized):
        context, sequence = tf.io.parse_single_sequence_example(
            serialized,
            context_features={"beta": tf.io.FixedLenFeature([], tf.string)},
            sequence_features={"sequence": tf.io.RaggedFeature(tf.float32)}
        )

        return context["beta"], sequence["sequence"]




class TestDatasetFactoryABC(unittest.TestCase):
    def test_init_failure(self):
        self.assertRaises(TypeError, cds.DatasetFactoryABC.__init__)


    def test_smallest_int8_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(2**7-1), tf.int8)


    def test_smallest_int16_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(2**10 + 12), tf.int16)


    def test_smallest_int32_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(2**30), tf.int32)


    def test_smallest_int64_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(2**55), tf.int64)


    def test_smallest_negative_int8_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(-1*(2**7-1)), tf.int8)


    def test_smallest_negative_int16_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(-1*(2**10 + 12)), tf.int16)


    def test_smallest_negative_int32_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(-1*(2**30)), tf.int32)


    def test_smallest_negative_int64_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(-1*(2**55)), tf.int64)


    def test_smallest_int8_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(2**7-1), tf.int8)


    def test_smallest_int16_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(2**10 + 12), tf.int16)


    def test_smallest_int32_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(2**30), tf.int32)


    def test_smallest_int64_type(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(2**55), tf.int64)


    def test_smallest_int_type_allow_none(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(None), tf.int64)


    def test_smallest_uint_type_disallow_none(self):
        self.assertRaises(TypeError, cds.DatasetFactoryABC._smallest_int_type, None, True, False)


    def test_smallest_uint8_type_allowed(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(2**7), tf.uint8)


    def test_smallest_uint8_type_disallowed(self):
        self.assertEqual(cds.DatasetFactoryABC._smallest_int_type(2**7, False), tf.int16)




class TestDatasetWriter(unittest.TestCase):
    def setUp(self):
        self.filenames = [
            os.path.join("tests", "data", "facotry_file_{}.tfrecord".format(idx))
            for idx in range(3)
        ]

        self.factories = [SingleFactory(), DoubleFactory(), SequenceFactory()]


    def test_open_no_compression(self):
        try:
            writer = cds.DatasetWriter(self.factories, self.filenames)

            for writer in writer._writers:
                self.assertIsInstance(writer, tf.io.TFRecordWriter)

            writer.close()
        finally:
            for filename in self.filenames:
                os.remove(filename)


    def test_open_uniform_compression(self):
        try:
            writer = cds.DatasetWriter(self.factories, self.filenames, "GZIP")

            for writer in writer._writers:
                self.assertIsInstance(writer, tf.io.TFRecordWriter)

            writer.close()
        finally:
            for filename in self.filenames:
                os.remove(filename)


    def test_open_mixed_compression(self):
        try:
            writer = cds.DatasetWriter(self.factories, self.filenames, ["", "GZIP", "ZLIB"])

            for writer in writer._writers:
                self.assertIsInstance(writer, tf.io.TFRecordWriter)

            writer.close()
        finally:
            for filename in self.filenames:
                os.remove(filename)


    def test_close(self):
        try:
            writer = cds.DatasetWriter(self.factories, self.filenames)
            self.assertNotEqual(writer._writers, [])

            writer.close()
            self.assertEqual(writer._writers, [])
        finally:
            for filename in self.filenames:
                os.remove(filename)


    def test_close_twice(self):
        try:
            writer = cds.DatasetWriter(self.factories, self.filenames)
            self.assertNotEqual(writer._writers, [])

            writer.close()
            self.assertEqual(writer._writers, [])

            writer.close()
            self.assertEqual(writer._writers, [])
        finally:
            for filename in self.filenames:
                os.remove(filename)


    def test_flush_success(self):
        try:
            writer = cds.DatasetWriter(self.factories, self.filenames)
            writer.flush()
            writer.close()
        finally:
            for filename in self.filenames:
                os.remove(filename)


    def test_flush_failure(self):
        try:
            writer = cds.DatasetWriter(self.factories, self.filenames)
            writer.close()
            self.assertRaises(ValueError, writer.flush)
        finally:
            for filename in self.filenames:
                os.remove(filename)


    def test_with_block(self):
        try:
            with cds.DatasetWriter(self.factories, self.filenames) as writer:
                writer.flush()
        finally:
            for filename in self.filenames:
                os.remove(filename)


    def disabled_test_broadcast(self):
        data = {
            "alpha": [42],
            "beta": ["foobar"],
            "sequence": [[1.2, 1.3, 1], [], [7.2, 5.1]]
        }

        try:
            with cds.DatasetWriter(self.factories, self.filenames) as writer:
                writer.broadcast(**data)

                single = next(iter(self.factories[0].load(self.filenames[0])))
                self.assertEqual(single.numpy(), 42)

                double = next(iter(self.factories[1].load(self.filenames[1])))
                self.assertEqual(double[0].numpy(), 42)
                self.assertEqual(double[1].numpy(), b"foobar")

                sequence = next(iter(self.factories[2].load(self.filenames[2])))
                self.assertEqual(sequence[0].numpy(), b"foobar")

                seq_list = sequence[1].to_list()
                for idx in range(3):
                    np.testing.assert_almost_equal(seq_list[idx], data["sequence"][idx], 1)
        finally:
            for filename in self.filenames:
                os.remove(filename)




if __name__ == "__main__":
    unittest.main(verbosity=2)
