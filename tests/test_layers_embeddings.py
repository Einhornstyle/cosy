#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import numpy as np
import tensorflow as tf
from cosy import layers
from cosy.deprecated import layers as l




class TestPathEmbeddings(unittest.TestCase):
    def test_single_symbol_embedding_forward_pass(self):
        symbol = tf.random.uniform((64,), dtype=np.int64, minval=0, maxval=300)
        index = tf.random.uniform((64,), dtype=np.int64, minval=0, maxval=200)

        embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        output = embedding((symbol, index))

        self.assertEqual(output.shape, (64, 256 + 128))


    def test_multi_symbol_embedding_forward_pass(self):
        symbols = tf.random.uniform((64, 17), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 17), dtype=np.int64, minval=0, maxval=200)

        embedding = layers.SymbolEmbedding(300, 200, 64, 512)
        output = embedding((symbols, indices))

        self.assertEqual(output.shape, (64, 17, 64 + 512))


    def test_single_symbol_embedding_compute_mask(self):
        symbol = tf.random.uniform((62,), dtype=np.int64, minval=0, maxval=300)
        index = tf.random.uniform((62,), dtype=np.int64, minval=0, maxval=200)

        symbol = tf.concat([symbol, tf.zeros((2,), dtype=np.int64)], axis=0)
        indix = tf.concat([index, tf.zeros((2,), dtype=np.int64)], axis=0)

        embedding = layers.SymbolEmbedding(300, 200, 256, 128)

        mask = embedding.compute_mask((symbol, index))
        target = tf.not_equal(symbol, 0)

        self.assertTrue(tf.reduce_all(tf.equal(mask, target)))


    def test_multi_symbol_embedding_compute_mask(self):
        symbols = tf.random.uniform((63, 17), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((63, 17), dtype=np.int64, minval=0, maxval=200)

        symbols = tf.concat([symbols, tf.zeros((1, 17), dtype=np.int64)], axis=0)
        indices = tf.concat([indices, tf.zeros((1, 17), dtype=np.int64)], axis=0)

        embedding = layers.SymbolEmbedding(300, 200, 64, 512)

        mask = embedding.compute_mask((symbols, indices))
        target = tf.not_equal(symbols, 0)

        self.assertTrue(tf.reduce_all(tf.equal(mask, target)))


    def test_single_symbol_embedding_compute_output_shape(self):
        symbol = tf.random.uniform((64,), dtype=np.int64, minval=0, maxval=300)
        index = tf.random.uniform((64,), dtype=np.int64, minval=0, maxval=200)

        embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        output = embedding((symbol, index))
        shape = embedding.compute_output_shape((symbol.shape, index.shape))

        self.assertEqual(output.shape, shape)


    def test_multi_symbol_embedding_compute_output_shape(self):
        symbols = tf.random.uniform((64, 17), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 17), dtype=np.int64, minval=0, maxval=200)

        embedding = layers.SymbolEmbedding(300, 200, 64, 512)
        output = embedding((symbols, indices))
        shape = embedding.compute_output_shape((symbols.shape, indices.shape))

        self.assertEqual(output.shape, shape)


    def test_nonterminal_path_embedding_forward_pass(self):
        symbols = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=200)

        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.NonterminalPathEmbedding(nonterminal_embedding, path_embedding)
        output = embedding([symbols, indices])

        self.assertEqual(output.shape, (64, 256))


    def test_nonterminal_path_embedding_no_mask_forward_pass(self):
        symbols = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=200)

        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.NonterminalPathEmbedding(nonterminal_embedding, path_embedding, False)
        output = embedding([symbols, indices])

        self.assertEqual(output.shape, (64, 256))


    def test_nonterminal_path_embedding_compute_output_shape(self):
        symbols = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=200)

        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.NonterminalPathEmbedding(nonterminal_embedding, path_embedding)
        output = embedding([symbols, indices])
        shape = embedding.compute_output_shape((symbols.shape, indices.shape))

        self.assertEqual(output.shape, (64, 256))


    def test_terminal_path_embedding_forward_pass(self):
        leaf = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        symbols = tf.random.uniform((64, 14), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 15), dtype=np.int64, minval=0, maxval=200)

        terminal_embedding = layers.SymbolEmbedding(1000, 200, 256, 128)
        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.TerminalPathEmbedding(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding
        )

        output = embedding([tf.concat([leaf, symbols], -1), indices])

        self.assertEqual(output.shape, (64, 256))


    def test_terminal_path_embedding_no_mask_forward_pass(self):
        leaf = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        symbols = tf.random.uniform((64, 14), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 15), dtype=np.int64, minval=0, maxval=200)

        terminal_embedding = layers.SymbolEmbedding(1000, 200, 256, 128)
        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.TerminalPathEmbedding(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            apply_mask=False
        )

        output = embedding([tf.concat([leaf, symbols], -1), indices])

        self.assertEqual(output.shape, (64, 256))


    def test_terminal_path_embedding_compute_output_shape(self):
        leaf = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        symbols = tf.random.uniform((64, 14), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 15), dtype=np.int64, minval=0, maxval=200)

        terminal_embedding = layers.SymbolEmbedding(1000, 200, 256, 128)
        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.TerminalPathEmbedding(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding
        )

        output = embedding([tf.concat([leaf, symbols], -1), indices])
        shape = embedding.compute_output_shape(((64, 15), (64, 15)))

        self.assertEqual(output.shape, shape)


    def test_uniform_path_embedding_forward_pass(self):
        first = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        last = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        symbols = tf.random.uniform((64, 14), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 16), dtype=np.int64, minval=0, maxval=200)

        terminal_embedding = layers.SymbolEmbedding(1000, 200, 256, 128)
        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.PathEmbedding(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding
        )

        output = embedding([tf.concat([first, symbols, last], -1), indices])

        self.assertEqual(output.shape, (64, 256))


    def test_uniform_path_embedding_no_mask_forward_pass(self):
        first = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        last = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        symbols = tf.random.uniform((64, 14), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 16), dtype=np.int64, minval=0, maxval=200)

        terminal_embedding = layers.SymbolEmbedding(1000, 200, 256, 128)
        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.PathEmbedding(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            apply_mask=False
        )

        output = embedding([tf.concat([first, symbols, last], -1), indices])

        self.assertEqual(output.shape, (64, 256))



    def test_mixed_path_embedding_forward_pass(self):
        first = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        last = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        symbols = tf.random.uniform((64, 14), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 16), dtype=np.int64, minval=0, maxval=200)

        first_terminal_embedding = layers.SymbolEmbedding(1000, 200, 256, 128)
        last_terminal_embedding = layers.SymbolEmbedding(1000, 200, 256, 128)
        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.PathEmbedding(
            first_terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            last_terminal_embedding
        )

        output = embedding([tf.concat([first, symbols, last], -1), indices])

        self.assertEqual(output.shape, (64, 256))


    def test_mixed_path_embedding_no_mask_forward_pass(self):
        first = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        last = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        symbols = tf.random.uniform((64, 14), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 16), dtype=np.int64, minval=0, maxval=200)

        first_terminal_embedding = layers.SymbolEmbedding(1000, 200, 256, 128)
        last_terminal_embedding = layers.SymbolEmbedding(1000, 200, 256, 128)
        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.PathEmbedding(
            first_terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            last_terminal_embedding,
            apply_mask=False
        )

        output = embedding([tf.concat([first, symbols, last], -1), indices])

        self.assertEqual(output.shape, (64, 256))


    def test_path_embedding_compute_output_shape(self):
        first = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        last = tf.random.uniform((64, 1), dtype=np.int64, minval=0, maxval=1000)
        symbols = tf.random.uniform((64, 14), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 16), dtype=np.int64, minval=0, maxval=200)

        terminal_embedding = layers.SymbolEmbedding(1000, 200, 256, 128)
        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 128)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.PathEmbedding(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding
        )

        output = embedding([tf.concat([first, symbols, last], -1), indices])
        shape = embedding.compute_output_shape(((64, 16), (64, 16)))

        self.assertEqual(output.shape, shape)




class TestPathEncoderBase(unittest.TestCase):
    def setUp(self):
        self.input_shape = (16, 5, 10, 64)
        self.output_shape = (16, 5, 32)
        self.orig_output_shape = (16, 5, 64)

        self.inputs = tf.random.uniform(self.input_shape, dtype=tf.float32, minval=0, maxval=100)

        mask = tf.ones([16, 4, 7], dtype=tf.bool)
        mask = tf.concat([mask, tf.zeros([16, 1, 7], dtype=tf.bool)], axis=1)
        mask = tf.concat([mask, tf.zeros([16, 5, 3], dtype=tf.bool)], axis=2)
        self.mask = mask

        mask = tf.ones([16, 5, 7], dtype=tf.bool)
        mask = tf.concat([mask, tf.zeros([16, 5, 3], dtype=tf.bool)], axis=2)
        self.cudnn_mask = mask




class TestPathLSTM(TestPathEncoderBase):
    def _get_rnn_type(self):
        return "lstm"


    def test_forward_pass(self):
        path_rnn = layers.PathRNN(self.output_shape[-1], rnn_type=self._get_rnn_type())
        output = path_rnn(self.inputs)

        self.assertEqual(output.shape, self.output_shape)


    def test_forward_pass_masked(self):
        path_rnn = layers.PathRNN(
            self.output_shape[-1],
            rnn_type=self._get_rnn_type(),
            allow_cudnn_kernel=False
        )

        output = path_rnn(self.inputs, mask=self.mask)

        self.assertEqual(output.shape, self.output_shape)


    def test_forward_pass_masked_cudnn(self):
        path_rnn = layers.PathRNN(self.output_shape[-1], rnn_type=self._get_rnn_type())
        output = path_rnn(self.inputs, mask=self.cudnn_mask)

        self.assertEqual(output.shape, self.output_shape)


    def disabled_test_forward_pass_masked_raises(self):
        path_rnn = layers.PathRNN(self.output_shape[-1], rnn_type=self._get_rnn_type())

        self.assertRaisesRegex(
            ValueError,
            layers.PathRNN._CUDNN_ERROR_MSG,
            path_rnn,
            self.inputs,
            mask=self.mask
        )


    def test_compute_output_shape(self):
        path_rnn = layers.PathRNN(self.output_shape[-1], rnn_type=self._get_rnn_type())
        output_shape = path_rnn.compute_output_shape(self.input_shape)

        self.assertEqual(output_shape, self.output_shape)


    def test_compute_mask(self):
        path_rnn = layers.PathRNN(self.output_shape[-1], rnn_type=self._get_rnn_type())
        output_mask = path_rnn.compute_mask(self.inputs)

        self.assertEqual(output_mask, None)


    def test_compute_mask_masked(self):
        path_rnn = layers.PathRNN(
            self.output_shape[-1],
            rnn_type=self._get_rnn_type(),
            allow_cudnn_kernel=False
        )

        output_mask = path_rnn.compute_mask(self.inputs, mask=self.mask)
        target_mask = np.any(self.mask.numpy(), axis=-1)

        self.assertTrue(np.all(output_mask.numpy() == target_mask))


    def test_compute_mask_masked_cudnn(self):
        path_rnn = layers.PathRNN(self.output_shape[-1], rnn_type=self._get_rnn_type())

        output_mask = path_rnn.compute_mask(self.inputs, mask=self.cudnn_mask)
        target_mask = np.any(self.cudnn_mask.numpy(), axis=-1)

        self.assertTrue(np.all(output_mask.numpy() == target_mask))




class TestPathGRU(TestPathLSTM):
    def _get_rnn_type(self):
        return "gru"




class TestPathMaxPooling(TestPathEncoderBase):
    def _get_pooling_type(self):
        return "max"


    def test_forward_pass(self):
        path_pooling = layers.PathPooling(pooling_type=self._get_pooling_type())

        output = path_pooling(self.inputs)

        self.assertEqual(output.shape, self.orig_output_shape)


    def test_forward_pass_masked(self):
        path_pooling = layers.PathPooling(pooling_type=self._get_pooling_type())

        output = path_pooling(self.inputs, mask=self.mask)

        self.assertEqual(output.shape, self.orig_output_shape)


    def test_compute_output_shape(self):
        path_pooling = layers.PathPooling(pooling_type=self._get_pooling_type())
        output_shape = path_pooling.compute_output_shape(self.input_shape)

        self.assertEqual(output_shape, self.orig_output_shape)


    def test_compute_mask(self):
        path_pooling = layers.PathPooling(pooling_type=self._get_pooling_type())

        output_mask = path_pooling.compute_mask(self.inputs)

        self.assertEqual(output_mask, None)


    def test_compute_mask_masked(self):
        path_pooling = layers.PathPooling(pooling_type=self._get_pooling_type())

        output_mask = path_pooling.compute_mask(self.inputs, mask=self.mask)
        target_mask = np.any(self.mask.numpy(), axis=-1)

        self.assertTrue(np.all(output_mask.numpy() == target_mask))




class TestPathAveragePooling(TestPathMaxPooling):
    def _get_pooling_type(self):
        return "average"




if __name__ == "__main__":
    unittest.main(verbosity=2)
