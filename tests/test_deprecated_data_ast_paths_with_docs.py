#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import os

import numpy as np
import tensorflow as tf

import cosy.preprocessing as cp
import cosy.deprecated.data as cds




class TestASTPathsWithDocs(unittest.TestCase):
    def setUp(self):
        self.padded_path_symbols = [
            list(range(1, 5)),
            list(range(1, 3)) + [0, 0],
            list(range(3, 7))
        ]

        self.padded_path_indices = [
            list(range(4, 8)),
            list(range(1, 3)) + [0, 0],
            list(range(2, 6))
        ]

        self.path_symbols = [
            list(range(1, 5)),
            list(range(1, 3)),
            list(range(3, 7))
        ]

        self.path_indices = [
            list(range(4, 8)),
            list(range(1, 3)),
            list(range(2, 6))
        ]

        self.padded_seperated_path_symbols = [
            list(range(2, 5)),
            list(range(2, 3)) + [0, 0],
            list(range(4, 7))
        ]

        self.padded_seperated_path_indices = [
            list(range(5, 8)),
            list(range(2, 3)) + [0, 0],
            list(range(3, 6))
        ]

        self.seperated_path_symbols = [
            list(range(2, 5)),
            list(range(2, 3)),
            list(range(4, 7))
        ]

        self.seperated_path_indices = [
            list(range(5, 8)),
            list(range(2, 3)),
            list(range(3, 6))
        ]

        self.terminal_symbols = [p[0] for p in self.path_symbols]
        self.terminal_indices = [p[0] for p in self.path_indices]

        self.root_symbols = list(range(3, 6))
        self.root_indices = list(range(2, 5))
        self.docstring_tokens = list(range(13, 35))

        self.leaf_symbol = 7

        self.terminal_vocab_size = 7
        self.nonterminal_vocab_size = 6
        self.max_sibling_index = 8
        self.docstring_vocab_size = 35


    def test_set_docstring_vocab_size(self):
        dsf = cds.ASTPathsWithDocs()
        self.assertEqual(dsf.docstring_vocab_size, None)
        self.assertEqual(dsf._docstring_type, tf.int64)

        dsf.docstring_vocab_size = 24
        self.assertEqual(dsf.docstring_vocab_size, 24)
        self.assertEqual(dsf._docstring_type, tf.int8)


    def test_record_roundtrip(self):
        record = cds.ASTPathsWithDocs(seperate_terminals=False).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.docstring_tokens,
            self.leaf_symbol
        )

        context, sequence = tf.io.parse_single_sequence_example(
            record.SerializeToString(),
            context_features=cds.ASTPathsWithDocs.CONTEXT_DESCRIPTION,
            sequence_features=cds.ASTPathsWithDocs.SEQUENCE_DESCRIPTION
        )

        self.assertEqual(sequence["path_symbols"].to_list(), self.path_symbols)
        self.assertEqual(sequence["path_indices"].to_list(), self.path_indices)

        self.assertTrue(np.all(context["root_symbols"].numpy() == self.root_symbols))
        self.assertTrue(np.all(context["root_indices"].numpy() == self.root_indices))

        self.assertTrue(np.all(context["docstring"].numpy() == self.docstring_tokens))

        self.assertEqual(context["leaf_symbol"].numpy(), self.leaf_symbol)


    def test_prepare_features(self):
        context = {
            "root_symbols": tf.constant(self.root_symbols),
            "root_indices": tf.constant(self.root_indices),
            "docstring": tf.constant(self.docstring_tokens),
            "leaf_symbol": tf.constant(self.leaf_symbol)
        }

        sequence = {
            "path_symbols": tf.ragged.constant(self.path_symbols),
            "path_indices": tf.ragged.constant(self.path_indices)
        }

        dataset = tf.data.TFRecordDataset.from_tensors((context, sequence))
        dataset = dataset.map(cds.ASTPathsWithDocs(seperate_terminals=False)._prepare_features)

        for idx, (data, label) in enumerate(dataset):
            self.assertEqual(idx, 0) # The dataset should only contain one element

            self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))
            self.assertTrue(np.all(data[1].numpy() == self.padded_path_indices))

            self.assertTrue(np.all(data[2].numpy() == self.root_symbols))
            self.assertTrue(np.all(data[3].numpy() == self.root_indices))

            self.assertTrue(np.all(data[4].numpy() == self.docstring_tokens))

            self.assertEqual(label.numpy(), self.leaf_symbol)


    def test_load(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_load.tfrecord")

        record = cds.ASTPathsWithDocs(seperate_terminals=False).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.docstring_tokens,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsWithDocs(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                self.docstring_vocab_size,
                seperate_terminals=False
            ).load([filename])

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 5)

                self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.padded_path_indices))

                self.assertTrue(np.all(data[2].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.root_indices))

                self.assertTrue(np.all(data[4].numpy() == self.docstring_tokens))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)
                self.assertEqual(data[1].dtype, tf.int8)

                self.assertEqual(data[2].dtype, tf.int8)
                self.assertEqual(data[3].dtype, tf.int8)

                self.assertEqual(data[4].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)


    def test_load_seperated(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_seperated_test_load.tfrecord")

        record = cds.ASTPathsWithDocs(seperate_terminals=True).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.docstring_tokens,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsWithDocs(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                self.docstring_vocab_size,
                seperate_terminals=True
            ).load([filename])

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 7)

                self.assertTrue(np.all(data[0].numpy() == self.terminal_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.terminal_indices))

                self.assertTrue(np.all(data[2].numpy() == self.padded_seperated_path_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.padded_seperated_path_indices))

                self.assertTrue(np.all(data[4].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[5].numpy() == self.root_indices))

                self.assertTrue(np.all(data[6].numpy() == self.docstring_tokens))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)
                self.assertEqual(data[1].dtype, tf.int8)

                self.assertEqual(data[2].dtype, tf.int8)
                self.assertEqual(data[3].dtype, tf.int8)

                self.assertEqual(data[4].dtype, tf.int8)
                self.assertEqual(data[5].dtype, tf.int8)

                self.assertEqual(data[6].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)



    def test_load_default_types(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_load.tfrecord")

        record = cds.ASTPathsWithDocs(seperate_terminals=False).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.docstring_tokens,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsWithDocs(seperate_terminals=False).load([filename])

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 5)

                self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.padded_path_indices))

                self.assertTrue(np.all(data[2].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.root_indices))

                self.assertTrue(np.all(data[4].numpy() == self.docstring_tokens))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int64)
                self.assertEqual(data[1].dtype, tf.int64)

                self.assertEqual(data[2].dtype, tf.int64)
                self.assertEqual(data[3].dtype, tf.int64)

                self.assertEqual(data[4].dtype, tf.int64)

                self.assertEqual(label.dtype, tf.int64)
        finally:
            os.remove(filename)


    def test_load_seperated_default_types(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_seperated_test_load.tfrecord")

        record = cds.ASTPathsWithDocs(seperate_terminals=True).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.docstring_tokens,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsWithDocs(seperate_terminals=True).load([filename])

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 7)

                self.assertTrue(np.all(data[0].numpy() == self.terminal_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.terminal_indices))

                self.assertTrue(np.all(data[2].numpy() == self.padded_seperated_path_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.padded_seperated_path_indices))

                self.assertTrue(np.all(data[4].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[5].numpy() == self.root_indices))

                self.assertTrue(np.all(data[6].numpy() == self.docstring_tokens))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int64)
                self.assertEqual(data[1].dtype, tf.int64)

                self.assertEqual(data[2].dtype, tf.int64)
                self.assertEqual(data[3].dtype, tf.int64)

                self.assertEqual(data[4].dtype, tf.int64)
                self.assertEqual(data[5].dtype, tf.int64)

                self.assertEqual(data[6].dtype, tf.int64)

                self.assertEqual(label.dtype, tf.int64)
        finally:
            os.remove(filename)


    def test_load_joint_to_seperated(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_load.tfrecord")

        record = cds.ASTPathsWithDocs(seperate_terminals=False).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.docstring_tokens,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsWithDocs(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                self.docstring_vocab_size,
                seperate_terminals=False,
                apply_conversion=True
            ).load([filename])

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 7)

                self.assertTrue(np.all(data[0].numpy() == self.terminal_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.terminal_indices))

                self.assertTrue(np.all(data[2].numpy() == self.padded_seperated_path_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.padded_seperated_path_indices))

                self.assertTrue(np.all(data[4].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[5].numpy() == self.root_indices))

                self.assertTrue(np.all(data[6].numpy() == self.docstring_tokens))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)
                self.assertEqual(data[1].dtype, tf.int8)

                self.assertEqual(data[2].dtype, tf.int8)
                self.assertEqual(data[3].dtype, tf.int8)

                self.assertEqual(data[4].dtype, tf.int8)
                self.assertEqual(data[5].dtype, tf.int8)

                self.assertEqual(data[6].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)


    def test_load_seperated_to_joint(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_seperated_load.tfrecord")

        record = cds.ASTPathsWithDocs(seperate_terminals=True).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.docstring_tokens,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsWithDocs(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                self.docstring_vocab_size,
                seperate_terminals=True,
                apply_conversion=True
            ).load([filename])

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 5)

                self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.padded_path_indices))

                self.assertTrue(np.all(data[2].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.root_indices))

                self.assertTrue(np.all(data[4].numpy() == self.docstring_tokens))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)
                self.assertEqual(data[1].dtype, tf.int8)

                self.assertEqual(data[2].dtype, tf.int8)
                self.assertEqual(data[3].dtype, tf.int8)

                self.assertEqual(data[4].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)




    def test_load_masked(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_load.tfrecord")

        record = cds.ASTPathsWithDocs(seperate_terminals=False).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.docstring_tokens,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsWithDocs(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                self.docstring_vocab_size,
                seperate_terminals=False,
                mask=[True, False, True, False, False]
            ).load([filename])

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 2)

                self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))

                self.assertTrue(np.all(data[1].numpy() == self.root_symbols))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)

                self.assertEqual(data[1].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)


    def test_load_seperated_masked(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_seperated_test_load.tfrecord")

        record = cds.ASTPathsWithDocs(seperate_terminals=True).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.docstring_tokens,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsWithDocs(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                self.docstring_vocab_size,
                seperate_terminals=True,
                mask=[True, False] + [True]*5
            ).load([filename])

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 6)

                self.assertTrue(np.all(data[0].numpy() == self.terminal_symbols))

                self.assertTrue(np.all(data[1].numpy() == self.padded_seperated_path_symbols))
                self.assertTrue(np.all(data[2].numpy() == self.padded_seperated_path_indices))

                self.assertTrue(np.all(data[3].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[4].numpy() == self.root_indices))

                self.assertTrue(np.all(data[5].numpy() == self.docstring_tokens))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)

                self.assertEqual(data[1].dtype, tf.int8)
                self.assertEqual(data[2].dtype, tf.int8)

                self.assertEqual(data[3].dtype, tf.int8)
                self.assertEqual(data[4].dtype, tf.int8)

                self.assertEqual(data[5].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)





if __name__ == "__main__":
    unittest.main(verbosity=2)
