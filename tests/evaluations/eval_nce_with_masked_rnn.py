#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest

import tensorflow as tf
from tensorflow import keras as K

import cosy

from .utils import TestModelQuality




class JNESR0(K.Model):
    def __init__(self,
                 terminal_vocab_size,
                 nonterminal_vocab_size,
                 max_nonterminal_index,
                 terminal_dimension=512,
                 nonterminal_dimension=256,
                 nonterminal_index_dimension=256,
                 mask_paths=False,
                 rnn_type="lstm",
                 rnn_layers=1,
                 rnn_dimensions=256,
                 **kwargs):
        super().__init__(**kwargs)

        # This condition must hold for any joint model
        assert (nonterminal_index_dimension + nonterminal_dimension) == terminal_dimension

        terminal_embedding = K.layers.Embedding(
            terminal_vocab_size,
            terminal_dimension,
            mask_zero=True
        )

        nonterminal_embedding = cosy.layers.SymbolEmbedding(
            nonterminal_vocab_size,
            max_nonterminal_index,
            vocab_dimension=nonterminal_dimension,
            index_dimension=nonterminal_index_dimension
        )

        self.nce = cosy.layers.RecurrentNCE(
            terminal_embedding,
            nonterminal_embedding,
            use_joint_embedding=True,
            mask_paths=mask_paths,
            rnn_type=rnn_type,
            num_layers=rnn_layers,
            dimensions=rnn_dimensions,
        )

        self.attention = K.layers.Attention()

        self.linear = K.layers.Dense(terminal_vocab_size)

        self.softmax = K.layers.Softmax()


    def call(self, inputs):
        leaf_path_set, root_path = self.nce((inputs[0], inputs[1:3], inputs[3:5]))

        # query = root_path, values = keys = leaf_path_list
        output = self.attention([root_path[:,tf.newaxis,:], leaf_path_set])[:,0,:]
        output = tf.concat([output, root_path], axis=-1)

        # predict next nonterminal
        output = self.linear(output)
        output = self.softmax(output)

        return output




class TestMaskedRecurrentNCE(TestModelQuality):
    def setUp(self):
        self._build_dataset("mini_10k")


    def test_jnesr0_no_mask(self):
        TRAIN_THRESHOLD = 0.57
        EVAL_THRESHOLD = 0.63

        dataset_factory = self._make_factory()

        model = JNESR0(
            self.params["terminal_vocab_size"],
            self.params["nonterminal_vocab_size"],
            self.params["max_sibling_index"],
            mask_paths=False,
            rnn_type="lstm",
            rnn_layers=1,
            rnn_dimensions=256,
        )

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_jnesr0_with_mask(self):
        TRAIN_THRESHOLD = 0.57
        EVAL_THRESHOLD = 0.63

        dataset_factory = self._make_factory()

        model = JNESR0(
            self.params["terminal_vocab_size"],
            self.params["nonterminal_vocab_size"],
            self.params["max_sibling_index"],
            mask_paths=True,
            rnn_type="lstm",
            rnn_layers=1,
            rnn_dimensions=256,
        )

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_jnesr0_no_mask_two_rnn_layers(self):
        TRAIN_THRESHOLD = 0.57
        EVAL_THRESHOLD = 0.63

        dataset_factory = self._make_factory()

        model = JNESR0(
            self.params["terminal_vocab_size"],
            self.params["nonterminal_vocab_size"],
            self.params["max_sibling_index"],
            mask_paths=False,
            rnn_type="lstm",
            rnn_layers=2,
            rnn_dimensions=256,
        )

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_jnesr0_with_mask_two_rnn_layers(self):
        TRAIN_THRESHOLD = 0.57
        EVAL_THRESHOLD = 0.63

        dataset_factory = self._make_factory()

        model = JNESR0(
            self.params["terminal_vocab_size"],
            self.params["nonterminal_vocab_size"],
            self.params["max_sibling_index"],
            mask_paths=True,
            rnn_type="lstm",
            rnn_layers=2,
            rnn_dimensions=256,
        )

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)




if __name__ == "__main__":
    unittest.main(verbosity=2)

