#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import os
import glob
import json
import shutil
import unittest

import tensorflow as tf
from tensorflow import keras as K

import cosy




class TestModelQuality(unittest.TestCase):
    def _build_dataset(self, folder, with_docs=False):
        data_folder = os.path.join("tests", "data", folder)

        self.train_files = glob.glob(os.path.join(data_folder, "train", "*.tfrecord"))
        self.eval_files = glob.glob(os.path.join(data_folder, "eval", "*.tfrecord"))

        with open(os.path.join(data_folder, "encoding.json"), "r") as encoding_json:
            data = json.load(encoding_json)

            self.params = {
                "terminal_vocab_size": len(data["terminal_encoding"]) + 2,
                "nonterminal_vocab_size": len(data["nonterminal_encoding"]) + 2,
                "max_sibling_index": data["max_child_count"] + 1
            }

            if with_docs:
                self.params["docstring_vocab_size"] = len(data["docstring_vocabulary"]) + 2

        self.log_dir = os.path.join("tests", "data", "eval_logs")

        self.batch_size = 64
        self.num_threads = 64
        self.num_epochs = 1


    def _run(self, dataset_factory, model, train_threshold, eval_threshold):
        dataset = dataset_factory(
            self.train_files,
            batch_size=self.batch_size,
            num_threads=self.num_threads,
            compression="GZIP"
        )

        eval_dataset = dataset_factory(
            self.eval_files,
            batch_size=self.batch_size,
            num_threads=self.num_threads,
            compression="GZIP"
        )

        model.compile(
            optimizer="adam",
            loss="sparse_categorical_crossentropy",
            metrics=["accuracy"]
        )

        print()

        history = model.fit(
            dataset,
            epochs=self.num_epochs,
            verbose=1
        )

        self.assertTrue(history.history["accuracy"][-1] >= train_threshold)

        result = model.evaluate(eval_dataset, verbose=1)

        self.assertTrue(result[1] >= eval_threshold)

        print()


    def _tuning(self, dataset_factory, hparam_tuning, train_thresholds, eval_thresholds):
        for name, param in self.params.items():
            hparam_tuning.add_param(name, param)

        hparam_tuning.add_param("optimizer", "adam", model_param=False)
        hparam_tuning.add_param("loss", "sparse_categorical_crossentropy", model_param=False)
        hparam_tuning.add_param("metrics", ["accuracy"], model_param=False)

        dataset = dataset_factory(
            self.train_files,
            batch_size=self.batch_size,
            num_threads=self.num_threads,
            compression="GZIP"
        )

        eval_dataset = dataset_factory(
            self.eval_files,
            batch_size=self.batch_size,
            num_threads=self.num_threads,
            compression="GZIP"
        )

        print()

        try:
            history_stack = hparam_tuning.run(
                dataset,
                epochs=self.num_epochs,
                verbose=1,
                validation_data=eval_dataset
            )
        finally:
            shutil.rmtree(self.log_dir)

        self.assertEqual(len(history_stack), len(train_thresholds))
        self.assertEqual(len(history_stack), len(eval_thresholds))

        for i, history in enumerate(history_stack):
            self.assertTrue(history.history["accuracy"][-1] >= train_thresholds[i])
            self.assertTrue(history.history["val_accuracy"][-1] >= eval_thresholds[i])


    def _make_factory(self, with_docs=False):
        if with_docs:
            return cosy.data.ASTPathsDocsReader(
                **self.params,
                normalize_datatypes=True,
                separate_terminals=False,
                apply_conversion=True,
                mask=[True, False] + [True] * 5
            )

        return cosy.data.ASTPathsReader(
            **self.params,
            normalize_datatypes=True,
            separate_terminals=False,
            apply_conversion=True,
            mask=[True, False] + [True] * 4
        )




if __name__ == "__main__":
    unittest.main(verbosity=2)
