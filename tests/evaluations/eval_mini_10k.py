#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest

import cosy

from .utils import TestModelQuality




class TestMini10k(TestModelQuality):
    def setUp(self):
        self._build_dataset("mini_10k")


    def test_code_completion_model_xl(self):
        TRAIN_THRESHOLD = 0.54
        EVAL_THRESHOLD = 0.55

        dataset_factory = self._make_factory()
        model = cosy.models.CodeCompletionXL(**self.params)

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_code_completion_model_xs(self):
        TRAIN_THRESHOLD = 0.57
        EVAL_THRESHOLD = 0.63

        dataset_factory = self._make_factory()
        model = cosy.models.CodeCompletionXS(**self.params)

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)




if __name__ == "__main__":
    unittest.main(verbosity=2)
