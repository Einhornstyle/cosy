#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest

import cosy

from .utils import TestModelQuality




class TestTinyWithDocs(TestModelQuality):
    def test_code_completion_model_xl(self):
        self._build_dataset("tiny_with_docs")

        TRAIN_THRESHOLD = 0.28
        EVAL_THRESHOLD = 0.33

        dataset_factory = self._make_factory()
        model = cosy.models.CodeCompletionXL(**self.params)

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_code_completion_model_xs(self):
        self._build_dataset("tiny_with_docs")

        TRAIN_THRESHOLD = 0.28
        EVAL_THRESHOLD = 0.37

        dataset_factory = self._make_factory()
        model = cosy.models.CodeCompletionXS(**self.params)

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_code_completion_model_docs_xl(self):
        self._build_dataset("tiny_with_docs", with_docs=True)

        TRAIN_THRESHOLD = 0.28
        EVAL_THRESHOLD = 0.33

        dataset_factory = self._make_factory(with_docs=True)
        model = cosy.models.CodeCompletionDocsXL(**self.params)

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_code_completion_model_docs_xs(self):
        self._build_dataset("tiny_with_docs", with_docs=True)

        TRAIN_THRESHOLD = 0.28
        EVAL_THRESHOLD = 0.37

        dataset_factory = self._make_factory(with_docs=True)
        model = cosy.models.CodeCompletionDocsXS(**self.params)

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_jnesp0_average(self):
        self._build_dataset("tiny_with_docs")

        TRAIN_THRESHOLD = 0.28
        EVAL_THRESHOLD = 0.37

        dataset_factory = self._make_factory()

        model = cosy.models.JNESP0(
            terminal_vocab_size=self.params["terminal_vocab_size"],
            nonterminal_vocab_size=self.params["nonterminal_vocab_size"],
            max_nonterminal_index=self.params["max_sibling_index"],
            terminal_dimension=512,
            nonterminal_dimension=256,
            nonterminal_index_dimension=256,
            pooling_type="average"
        )

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_jnesp0_max(self):
        self._build_dataset("tiny_with_docs")

        TRAIN_THRESHOLD = 0.28
        EVAL_THRESHOLD = 0.36

        dataset_factory = self._make_factory()

        model = cosy.models.JNESP0(
            terminal_vocab_size=self.params["terminal_vocab_size"],
            nonterminal_vocab_size=self.params["nonterminal_vocab_size"],
            max_nonterminal_index=self.params["max_sibling_index"],
            terminal_dimension=512,
            nonterminal_dimension=256,
            nonterminal_index_dimension=256,
            pooling_type="max"
        )

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_snesp0_average_linear(self):
        self._build_dataset("tiny_with_docs")

        TRAIN_THRESHOLD = 0.3
        EVAL_THRESHOLD = 0.4

        dataset_factory = self._make_factory()

        model = cosy.models.SNESP0(
            terminal_vocab_size=self.params["terminal_vocab_size"],
            nonterminal_vocab_size=self.params["nonterminal_vocab_size"],
            max_nonterminal_index=self.params["max_sibling_index"],
            terminal_dimension=512,
            nonterminal_dimension=256,
            nonterminal_index_dimension=256,
            pooling_type="average"
        )

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_snesp0_max_linear(self):
        self._build_dataset("tiny_with_docs")

        TRAIN_THRESHOLD = 0.3
        EVAL_THRESHOLD = 0.4

        dataset_factory = self._make_factory()

        model = cosy.models.SNESP0(
            terminal_vocab_size=self.params["terminal_vocab_size"],
            nonterminal_vocab_size=self.params["nonterminal_vocab_size"],
            max_nonterminal_index=self.params["max_sibling_index"],
            terminal_dimension=512,
            nonterminal_dimension=256,
            nonterminal_index_dimension=256,
            pooling_type="max"
        )

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_snesp0_average_tanh(self):
        self._build_dataset("tiny_with_docs")

        TRAIN_THRESHOLD = 0.3
        EVAL_THRESHOLD = 0.4

        dataset_factory = self._make_factory()

        model = cosy.models.SNESP0(
            terminal_vocab_size=self.params["terminal_vocab_size"],
            nonterminal_vocab_size=self.params["nonterminal_vocab_size"],
            max_nonterminal_index=self.params["max_sibling_index"],
            terminal_dimension=512,
            nonterminal_dimension=256,
            nonterminal_index_dimension=256,
            pooling_type="average",
            activation="tanh"
        )

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)


    def test_snesp0_max_tanh(self):
        self._build_dataset("tiny_with_docs")

        TRAIN_THRESHOLD = 0.3
        EVAL_THRESHOLD = 0.4

        dataset_factory = self._make_factory()

        model = cosy.models.SNESP0(
            terminal_vocab_size=self.params["terminal_vocab_size"],
            nonterminal_vocab_size=self.params["nonterminal_vocab_size"],
            max_nonterminal_index=self.params["max_sibling_index"],
            terminal_dimension=512,
            nonterminal_dimension=256,
            nonterminal_index_dimension=256,
            pooling_type="max",
            activation="tanh"
        )

        self._run(dataset_factory, model, TRAIN_THRESHOLD, EVAL_THRESHOLD)




if __name__ == "__main__":
    unittest.main(verbosity=2)
