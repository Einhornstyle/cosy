#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest

import cosy

from .utils import TestModelQuality




class TestTinyWithDocs(TestModelQuality):
    def test_code_completion_model_xs(self):
        self._build_dataset("tiny_with_docs")

        TRAIN_THRESHOLDS = [0.26, 0.27, 0.28]
        EVAL_THRESHOLDS = [0.29, 0.33, 0.37]

        dataset_factory = self._make_factory()
        hpt = cosy.training.HyperParameterTuning(cosy.models.CodeCompletionXS, self.log_dir)
        hpt.add_hparam("embedding_dimension", [128, 256, 512])

        self._tuning(dataset_factory, hpt, TRAIN_THRESHOLDS, EVAL_THRESHOLDS)


    def test_code_completion_model_docs_xs(self):
        self._build_dataset("tiny_with_docs", with_docs=True)

        TRAIN_THRESHOLDS = [0.26, 0.27, 0.28]
        EVAL_THRESHOLDS = [0.29, 0.33, 0.34]

        dataset_factory = self._make_factory(with_docs=True)
        hpt = cosy.training.HyperParameterTuning(cosy.models.CodeCompletionDocsXS, self.log_dir)
        hpt.add_hparam("embedding_dimension", [128, 256, 512])

        self._tuning(dataset_factory, hpt, TRAIN_THRESHOLDS, EVAL_THRESHOLDS)


    def test_jnesp0(self):
        self._build_dataset("tiny_with_docs")

        TRAIN_THRESHOLDS = [0.28, 0.28]
        EVAL_THRESHOLDS = [0.37, 0.36]

        dataset_factory = self._make_factory()
        hpt = cosy.training.HyperParameterTuning(cosy.models.JNESP0, self.log_dir)
        hpt.add_hparam("pooling_type", ["average", "max"])

        self.params["max_nonterminal_index"] = self.params["max_sibling_index"]
        del self.params["max_sibling_index"]

        self._tuning(dataset_factory, hpt, TRAIN_THRESHOLDS, EVAL_THRESHOLDS)




if __name__ == "__main__":
    unittest.main(verbosity=2)
