#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import glob
import os

import tensorflow as tf
from tensorflow import keras as K

from cosy.data import SequenceReader
from cosy.preprocessing import add_shifted_labels, mask_sample
from cosy.training import SparseMeanReciprocalRank
from cosy.models import SequenceTransformer




class LRSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
  def __init__(self, d_model, warmup_steps):
    super().__init__()

    self.d_model = d_model
    self.d_model = tf.cast(self.d_model, tf.float32)

    self.warmup_steps = warmup_steps

  def __call__(self, step):
    arg1 = tf.math.rsqrt(step)
    arg2 = step * (self.warmup_steps ** -1.5)

    return tf.math.rsqrt(self.d_model) * tf.math.minimum(arg1, arg2)




class TestSequenceTransformer(unittest.TestCase):
    def setUp(self):
        self.VOCAB_SIZE = 10000


    def _make_dataset(self, folder):
        filenames = glob.glob(os.path.join("tests", "data", folder, "*.tfrecord"))

        dataset_config = {"compression": "GZIP", "num_threads": 2, "batch_size": 2}
        mask_config = {"mask_values": [0, self.VOCAB_SIZE-1], "mask_dtype": tf.int32}

        dataset = SequenceReader("int32")(filenames, **dataset_config)
        dataset = dataset.map(add_shifted_labels)
        dataset = dataset.map(lambda t, l: (t, l, mask_sample(l, **mask_config)))

        return dataset


    def _run(self, folder, train_threshold, eval_threshold, metric, metric_name):
        print()

        train_dataset = self._make_dataset(os.path.join(folder, "train"))
        eval_dataset = self._make_dataset(os.path.join(folder, "eval"))

        model = SequenceTransformer(vocab_size=self.VOCAB_SIZE)

        learning_rate = LRSchedule(300, 1000)
        optimizer = K.optimizers.Adam(learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)

        model.compile(
            optimizer=optimizer,
            loss="sparse_categorical_crossentropy",
            metrics=[metric]
        )

        history = model.fit(train_dataset, verbose=1)

        self.assertTrue(history.history[metric_name][-1] >= train_threshold)

        result = model.evaluate(eval_dataset, verbose=1)

        self.assertTrue(result[1] >= eval_threshold)


    def test_leafs(self):
        self._run("leafs_seq", 0.04, 0.04, "accuracy", "accuracy")


    def test_dfs(self):
        self._run("dfs_seq", 0.13, 0.01, "accuracy", "accuracy")


    def test_leafs_mrr(self):
        metric = SparseMeanReciprocalRank(name="mrr")
        self._run("leafs_seq", 0.07, 0.07, metric, "mrr")


    def test_dfs_mrr(self):
        metric = SparseMeanReciprocalRank(name="mrr")
        self._run("dfs_seq", 0.20, 0.06, metric, "mrr")




if __name__ == "__main__":
    unittest.main(verbosity=2)
