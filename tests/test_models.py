# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import warnings
import unittest
import numpy as np
import tensorflow as tf
from tensorflow import keras as K
from cosy import models

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)




class TestKarelModel(unittest.TestCase):
    def setUp(self):
        self.dataset = np.load("tests/data/tensor/karel_train.npz", allow_pickle=True)
        self.inputs = self.dataset["inputs"].astype(np.float32)
        self.outputs = self.dataset["outputs"].astype(np.float32)
        self.programs = self.dataset["codes"]
        self.program_lengths = self.dataset["code_lengths"]


    def test_forward_pass(self):
        inputs = self.inputs[:10]
        outputs = self.outputs[:10]

        codes = np.zeros((10, np.max(self.program_lengths)))
        for i, program in enumerate(self.programs[:10]):
            for j, token in enumerate(program):
                codes[i, j] = token

        model = models.KarelModel(52, self.inputs.shape[1])
        self.assertEqual(model((inputs, outputs, codes)).shape, [10, 52])


    def test_explicit_masking(self):
        inputs = self.inputs[:10]
        outputs = self.outputs[:10]

        codes = np.zeros((10, np.max(self.program_lengths)))
        for i, program in enumerate(self.programs[:10]):
            for j, token in enumerate(program):
                codes[i, j] = token

        mask = tf.sequence_mask(self.program_lengths[:10], maxlen=28)

        model = models.KarelModel(52, self.inputs.shape[1])
        self.assertEqual(model((inputs, outputs, codes), mask=mask).shape, [10, 52])




class TestTransformer(unittest.TestCase):
    def test_forward_pass(self):
        transformer = models.Transformer(
            num_layers=2,
            dimension=512,
            num_heads=8,
            hidden_units=2048,
            vocab_size_input=8500,
            vocab_size_target=8000,
            max_position_input=10000,
            max_position_target=6000
        )

        inputs = tf.random.uniform((64, 36), dtype=np.int64, minval=0, maxval=200)
        target = tf.random.uniform((64, 36), dtype=np.int64, minval=0, maxval=200)

        output, _ = transformer(inputs, target, False)

        self.assertEqual(output.shape, (64, 36, 8000))




class TestCodeCompletionXL(unittest.TestCase):
    def _build_inputs(self, with_docs=False):
        self.terminal_symbols = tf.random.uniform((16, 123), dtype=np.int64, minval=0, maxval=300)
        self.terminal_indices = tf.random.uniform((16, 123), dtype=np.int64, minval=0, maxval=200)

        self.nonterminal_symbols = tf.random.uniform(
            (16, 123, 15),
            dtype=np.int64,
            minval=0,
            maxval=300
        )

        self.nonterminal_indices = tf.random.uniform(
            (16, 123, 15),
            dtype=np.int64,
            minval=0,
            maxval=200
        )

        self.nonterminal_inputs = (self.nonterminal_symbols, self.nonterminal_indices)

        self.root_symbols = tf.random.uniform((16, 28), dtype=np.int64, minval=0, maxval=300)
        self.root_indices = tf.random.uniform((16, 28), dtype=np.int64, minval=0, maxval=200)
        self.root_inputs = (self.root_symbols, self.root_indices)

        if with_docs:
            self.docstrings = tf.random.uniform((16, 120), dtype=np.int64, minval=0, maxval=500)

        self.labels = tf.random.uniform((16,), dtype=np.int64, minval=0, maxval=300)

        self.inputs = [self.terminal_symbols, *self.nonterminal_inputs, *self.root_inputs]

        if with_docs:
            self.docstrings = tf.random.uniform((16, 120), dtype=np.int64, minval=0, maxval=500)
            self.inputs.append(self.docstrings)

        self.dataset = tf.data.Dataset.from_tensors((tuple(self.inputs), self.labels))


    def setUp(self):
        self._build_inputs()
        self.model = models.CodeCompletionXL(300, 300, 200)


    def test_forward_pass(self):
        output = self.model(self.inputs)

        self.assertEqual(output.shape, (16, 300))


    def test_dataset_forward_pass(self):
        for record, label in self.dataset:
            output = self.model(record)

        self.assertEqual(output.shape, (16, 300))


    def test_compiled_dataset_forward_pass(self):
        self.model.compile(
            optimizer="adam",
            loss="sparse_categorical_crossentropy",
            metrics=["accuracy"]
        )

        for record, label in self.dataset:
            output = self.model(record)

        self.assertEqual(output.shape, (16, 300))


    def test_fit(self):
        self.model.compile(
            optimizer="adam",
            loss="sparse_categorical_crossentropy",
            metrics=["accuracy"]
        )

        self.model.fit(self.dataset, epochs=1, verbose=0)


    def test_evaluate(self):
        self.model.compile(
            optimizer="adam",
            loss="sparse_categorical_crossentropy",
            metrics=["accuracy"]
        )

        self.model.evaluate(self.dataset, verbose=0)




class TestCodeCompletionDocsXL(TestCodeCompletionXL):
    def setUp(self):
        self._build_inputs(with_docs=True)
        self.model = models.CodeCompletionDocsXL(300, 300, 200, 500)




class TestCodeCompletionXS(TestCodeCompletionXL):
    def setUp(self):
        self._build_inputs(with_docs=False)
        self.model = models.CodeCompletionXS(300, 300, 200)




class TestCodeCompletionDocsXS(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=True)
        self.model = models.CodeCompletionDocsXS(300, 300, 200, 500)




class TestJNESP0Average(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)
        self.model = models.JNESP0(300, 300, 200, 512, (256 + 128), 128, "average")




class TestJNESP0Max(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)
        self.model = models.JNESP0(300, 300, 200, 512, (256 + 128), 128, "max")




class TestJNESP0Average(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)

        self.model = models.JNESP0(
            300,
            300,
            200,
            512,
            (256 + 128),
            128,
            mask_paths=False,
            pooling_type="average"
        )




class TestJNESP0Max(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)

        self.model = models.JNESP0(
            300,
            300,
            200,
            512,
            (256 + 128),
            128,
            mask_paths=False,
            pooling_type="max"
        )




class TestJNESP0AverageWithMasking(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)

        self.model = models.JNESP0(
            300,
            300,
            200,
            512,
            (256 + 128),
            128,
            mask_paths=True,
            pooling_type="average"
        )




class TestJNESP0MaxWithMasking(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)

        self.model = models.JNESP0(
            300,
            300,
            200,
            512,
            (256 + 128),
            128,
            mask_paths=True,
            pooling_type="max"
        )




class TestSNESP0Average(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)

        self.model = models.SNESP0(
            300,
            300,
            200,
            512,
            (256 + 128),
            128,
            mask_paths=False,
            pooling_type="average"
        )




class TestSNESP0Max(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)

        self.model = models.SNESP0(
            300,
            300,
            200,
            512,
            (256 + 128),
            128,
            mask_paths=False,
            pooling_type="max"
        )




class TestSNESP0AverageWithMasking(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)

        self.model = models.SNESP0(
            300,
            300,
            200,
            512,
            (256 + 128),
            128,
            mask_paths=True,
            pooling_type="average"
        )




class TestSNESP0MaxWithMasking(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)

        self.model = models.SNESP0(
            300,
            300,
            200,
            512,
            (256 + 128),
            128,
            mask_paths=True,
            pooling_type="max"
        )




class TestSNESP0MaxTanh(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)

        self.model = models.SNESP0(
            300,
            300,
            200,
            512,
            (256 + 128),
            128,
            mask_paths=False,
            pooling_type="max",
            activation="tanh"
        )




class TestSNESP0AverageWithMaskingTanh(TestCodeCompletionDocsXL):
    def setUp(self):
        self._build_inputs(with_docs=False)

        self.model = models.SNESP0(
            300,
            300,
            200,
            512,
            (256 + 128),
            128,
            mask_paths=True,
            pooling_type="average",
            activation="tanh"
        )




class TestSequenceTransformer(unittest.TestCase):
    def _build_inputs(self, with_docs=False):
        self.input_shape = (8, 100)
        self.output_shape = (8, 100, 500)
        self.inputs = tf.random.uniform(self.input_shape, dtype=tf.int32, minval=1, maxval=500)
        self.labels = tf.random.uniform(self.input_shape, dtype=tf.int32, minval=1, maxval=500)
        self.dataset = tf.data.Dataset.from_tensors((self.inputs, self.labels))


    def setUp(self):
        self._build_inputs()
        self.model = models.SequenceTransformer(vocab_size=500)


    def test_forward_pass(self):
        output = self.model(self.inputs, training=False)
        self.assertEqual(output.shape, self.output_shape)


    def test_forward_pass_logits(self):
        output = self.model(self.inputs, training=False)

        self.model._return_logits = True

        logits = self.model(self.inputs, training=False)
        pred = K.layers.Softmax()(logits)

        self.assertEqual(logits.shape, self.output_shape)
        self.assertTrue(np.all(output == pred))


    def test_dataset_forward_pass(self):
        for inputs, label in self.dataset:
            output = self.model(inputs, training=True)

        self.assertEqual(output.shape, self.output_shape)


    def test_compiled_dataset_forward_pass(self):
        self.model.compile(
            optimizer="adam",
            loss="sparse_categorical_crossentropy",
            metrics=["accuracy"]
        )

        for inputs, label in self.dataset:
            output = self.model(inputs, training=True)

        self.assertEqual(output.shape, self.output_shape)


    def test_fit(self):
        self.model.compile(
            optimizer="adam",
            loss="sparse_categorical_crossentropy",
            metrics=["accuracy"]
        )

        self.model.fit(self.dataset, epochs=1, verbose=0)


    def test_evaluate(self):
        self.model.compile(
            optimizer="adam",
            loss="sparse_categorical_crossentropy",
            metrics=["accuracy"]
        )

        self.model.evaluate(self.dataset, verbose=0)




class TestPathTransformerNoneTerminals(TestSequenceTransformer):
    def _build_inputs(self, with_docs=False):
        self.input_shape =  (8, 5, 10)
        self.output_shape =  (8, 5, 500)
        self.inputs = tf.random.uniform(self.input_shape, dtype=tf.int32, minval=1, maxval=500)
        self.labels = tf.random.uniform((8, 5), dtype=tf.int32, minval=1, maxval=500)
        self.dataset = tf.data.Dataset.from_tensors((self.inputs, self.labels))


    def setUp(self):
        self._build_inputs()
        self.model = models.PathTransformer(vocab_size=500, terminals=None)




class TestPathTransformerFirstTerminals(TestPathTransformerNoneTerminals):
    def setUp(self):
        self._build_inputs()
        self.model = models.PathTransformer(vocab_size=500, terminals="first")




class TestPathTransformerLastTerminals(TestPathTransformerNoneTerminals):
    def setUp(self):
        self._build_inputs()
        self.model = models.PathTransformer(vocab_size=500, terminals="last")




class TestPathTransformerBothTerminals(TestPathTransformerNoneTerminals):
    def setUp(self):
        self._build_inputs()
        self.model = models.PathTransformer(vocab_size=500, terminals="both")




if __name__ == "__main__":
    unittest.main(verbosity=2)
