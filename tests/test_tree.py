# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from collections import defaultdict
import unittest
import cosy.tree as ct
import igraph as ig




class TestEdgeSort(unittest.TestCase):
    def setUp(self):
        self.tree = ig.Graph(directed=True)
        self.tree.add_vertices(13)
        self.tree.add_edges([
            (0, 1), (0, 3), (0, 12),
            (1, 6), (1, 2),
            (2, 7), (2, 8),
            (3, 4),
            (4, 5), (4, 11),
            (5, 9), (5, 10)
        ])

        self.tree.es[0]["child_index"] = 0
        self.tree.es[1]["child_index"] = 1
        self.tree.es[2]["child_index"] = 2

        self.tree.es[3]["child_index"] = 0
        self.tree.es[4]["child_index"] = 1

        self.tree.es[5]["child_index"] = 0
        self.tree.es[6]["child_index"] = 1

        self.tree.es[7]["child_index"] = 0

        self.tree.es[8]["child_index"] = 0
        self.tree.es[9]["child_index"] = 1

        self.tree.es[10]["child_index"] = 0
        self.tree.es[11]["child_index"] = 1


    def test_global_index_key(self):
        sort = ct.EdgeSort(key="global_index", reverse=False)
        edges = [self.tree.es[3], self.tree.es[2], self.tree.es[11], self.tree.es[9]]
        self.assertEqual([e.index for e in sort(edges)], [2, 3, 9, 11])


    def test_global_index_key_reversed(self):
        sort = ct.EdgeSort(key="global_index", reverse=True)
        edges = [self.tree.es[3], self.tree.es[2], self.tree.es[11], self.tree.es[9]]
        self.assertEqual([e.index for e in sort(edges)], [11, 9, 3, 2])


    def test_child_index_key(self):
        sort = ct.EdgeSort(key="child_index", reverse=False)
        edges = [self.tree.es[1], self.tree.es[2], self.tree.es[0]]
        self.assertEqual([e.index for e in sort(edges)], [0, 1, 2])


    def test_child_index_key_reversed(self):
        sort = ct.EdgeSort(key="global_index", reverse=True)
        edges = [self.tree.es[1], self.tree.es[2], self.tree.es[0]]
        self.assertEqual([e.index for e in sort(edges)], [2, 1, 0])


    def test_custom_key(self):
        sort = ct.EdgeSort(key=lambda e: e.source + e.target, reverse=False)
        edges = [self.tree.es[3], self.tree.es[2], self.tree.es[11], self.tree.es[8]]
        self.assertEqual([e.index for e in sort(edges)], [3, 8, 2, 11])


    def test_custom_key_reversed(self):
        sort = ct.EdgeSort(key=lambda e: e.source + e.target, reverse=True)
        edges = [self.tree.es[3], self.tree.es[2], self.tree.es[11], self.tree.es[8]]
        self.assertEqual([e.index for e in sort(edges)], [11, 2, 8, 3])




class TestTree(unittest.TestCase):
    def setUp(self):
        self.tree = ig.Graph(directed=True)
        self.tree.add_vertices(13)
        self.tree.add_edges([
            (0, 1), (0, 3), (0, 12),
            (1, 6), (1, 2),
            (2, 7), (2, 8),
            (3, 4),
            (4, 5), (4, 11),
            (5, 9), (5, 10)
        ])


    def test_is_root(self):
        self.assertTrue(ct.is_root(self.tree.vs[0]))
        self.assertFalse(any([ct.is_root(v) for v in self.tree.vs[1:]]))


    def test_is_leaf(self):
        self.assertFalse(any([ct.is_leaf(v) for v in self.tree.vs[:6]]))
        self.assertTrue(all([ct.is_leaf(v) for v in self.tree.vs[6:]]))


    def test_parent(self):
        self.assertEqual(ct.parent(self.tree.vs[5]).index, 4)


    def test_children(self):
        self.assertEqual([c.index for c in ct.children(self.tree.vs[1])], [6, 2])


    def test_no_children(self):
        self.assertEqual(ct.children(self.tree.vs[9]), [])


    def test_root(self):
        self.assertEqual(ct.root(self.tree).index, 0)


    def test_leafs_sort_global(self):
        target = [6, 7, 8, 9, 10, 11, 12]
        leafs = ct.leafs(self.tree, ct.EdgeSort())

        for idx, leaf in zip(target, leafs):
            self.assertEqual(leaf.index, idx)


    def test_leafs_sort_global_reverse(self):
        target = reversed([6, 7, 8, 9, 10, 11, 12])
        leafs = ct.leafs(self.tree, ct.EdgeSort(reverse=True))

        for idx, leaf in zip(target, leafs):
            self.assertEqual(leaf.index, idx)


    def test_subtree_leafs_sort_global(self):
        target = [6, 7, 8]
        leafs = ct.leafs(self.tree.vs[1], ct.EdgeSort())

        self.assertEqual(len(leafs), 3)

        for idx, leaf in zip(target, leafs):
            self.assertEqual(leaf.index, idx)


    def test_subtree_leafs_sort_global_reverse(self):
        target = reversed([6, 7, 8])
        leafs = ct.leafs(self.tree.vs[1], ct.EdgeSort(reverse=True))

        self.assertEqual(len(leafs), 3)

        for idx, leaf in zip(target, leafs):
            self.assertEqual(leaf.index, idx)


    def test_add_child_index(self):
        ct.add_child_index(self.tree)

        zero_idx = [0, 3, 5, 7, 8, 10]
        one_idx = [1, 4, 6, 9, 11]
        two_idx = [2]

        self.assertTrue(all(self.tree.es[i]["child_index"] == 0 for i in zero_idx))
        self.assertTrue(all(self.tree.es[i]["child_index"] == 1 for i in one_idx))
        self.assertTrue(all(self.tree.es[i]["child_index"] == 2 for i in two_idx))


    def test_add_child_index_reverse(self):
        ct.add_child_index(self.tree, ct.EdgeSort(reverse=True))

        zero_idx = [2, 4, 6, 7, 9, 11]
        one_idx = [1, 3, 5, 8, 10]
        two_idx = [0]

        self.assertTrue(all(self.tree.es[i]["child_index"] == 0 for i in zero_idx))
        self.assertTrue(all(self.tree.es[i]["child_index"] == 1 for i in one_idx))
        self.assertTrue(all(self.tree.es[i]["child_index"] == 2 for i in two_idx))




class TestPaths(unittest.TestCase):
    def setUp(self):
        self.tree = ig.Graph(directed=True)
        self.tree.add_vertices(13)
        self.tree.add_edges([
            (0, 1), (0, 3), (0, 12),
            (1, 6), (1, 2),
            (2, 7), (2, 8),
            (3, 4),
            (4, 5), (4, 11),
            (5, 9), (5, 10)
        ])

        self.leaf_paths = {
            6: [
                [7, 2, 1],
                [8, 2, 1],
                [9, 5, 4, 3, 0, 1],
                [10, 5, 4, 3, 0, 1],
                [11, 4, 3, 0, 1],
                [12, 0, 1]
            ],
            7: [
                [6, 1, 2],
                [8, 2],
                [9, 5, 4, 3, 0, 1, 2],
                [10, 5, 4, 3, 0, 1, 2],
                [11, 4, 3, 0, 1, 2],
                [12, 0, 1, 2]
            ],
            8: [
                [6, 1, 2],
                [7, 2],
                [9, 5, 4, 3, 0, 1, 2],
                [10, 5, 4, 3, 0, 1, 2],
                [11, 4, 3, 0, 1, 2],
                [12, 0, 1, 2]
            ],
            9: [
                [6, 1, 0, 3, 4, 5],
                [7, 2, 1, 0, 3, 4, 5],
                [8, 2, 1, 0, 3, 4, 5],
                [10, 5],
                [11, 4, 5],
                [12, 0, 3, 4, 5],
            ],
            10: [
                [6, 1, 0, 3, 4, 5],
                [7, 2, 1, 0, 3, 4, 5],
                [8, 2, 1, 0, 3, 4, 5],
                [9, 5],
                [11, 4, 5],
                [12, 0, 3, 4, 5],
            ],
            11: [
                [6, 1, 0, 3, 4],
                [7, 2, 1, 0, 3, 4],
                [8, 2, 1, 0, 3, 4],
                [9, 5, 4],
                [10, 5, 4],
                [12, 0, 3, 4]
            ],
            12 : [
                [6, 1, 0],
                [7, 2, 1, 0],
                [8, 2, 1, 0],
                [9, 5, 4, 3, 0],
                [10, 5, 4, 3, 0],
                [11, 4, 3, 0]
            ],
        }


    def test_root_path(self):
        path_indices = [v.index for v in ct.root_path(self.tree.vs[9])]
        self.assertEqual(path_indices, [9, 5, 4, 3, 0])


    def test_root_path_reverse(self):
        path_indices = [v.index for v in ct.root_path(self.tree.vs[9], True)]
        self.assertEqual(path_indices, [0, 3, 4, 5, 9])


    def test_leaf_path(self):
        path_indices = [v.index for v in ct.leaf_path(self.tree.vs[8], self.tree.vs[9])]
        self.assertEqual(path_indices, [8, 2, 1, 0, 3, 4, 5, 9])


    def test_leaf_path_equal_vertices(self):
        path_indices = [v.index for v in ct.leaf_path(self.tree.vs[8], self.tree.vs[8])]
        self.assertEqual(path_indices, [8])


    def test_raises_leaf_path_no_leaf(self):
        self.assertRaises(AssertionError, ct.leaf_path, self.tree.vs[3], self.tree.vs[8])


    def test_raises_leaf_path_unequal_graphs(self):
        tree_copy = self.tree.copy()
        self.assertRaises(AssertionError, ct.leaf_path, self.tree.vs[8], tree_copy.vs[9])


    def test_raises_leaf_path_missing_edge(self):
        self.tree.delete_edges(4)
        self.assertRaises(AssertionError, ct.leaf_path, self.tree.vs[8], self.tree.vs[9])


    def test_all_root_paths(self):
        paths = ct.all_root_paths(self.tree)

        self.assertEqual(len(paths), 7)
        self.assertEqual([v.index for v in paths[0]], [6, 1, 0])
        self.assertEqual([v.index for v in paths[1]], [7, 2, 1, 0])
        self.assertEqual([v.index for v in paths[2]], [8, 2, 1, 0])
        self.assertEqual([v.index for v in paths[3]], [9, 5, 4, 3, 0])
        self.assertEqual([v.index for v in paths[4]], [10, 5, 4, 3, 0])
        self.assertEqual([v.index for v in paths[5]], [11, 4, 3, 0])
        self.assertEqual([v.index for v in paths[6]], [12, 0])


    def test_all_root_paths_reverse(self):
        paths = ct.all_root_paths(self.tree, reverse=True)

        self.assertEqual(len(paths), 7)
        self.assertEqual([v.index for v in paths[0]], list(reversed([6, 1, 0])))
        self.assertEqual([v.index for v in paths[1]], list(reversed([7, 2, 1, 0])))
        self.assertEqual([v.index for v in paths[2]], list(reversed([8, 2, 1, 0])))
        self.assertEqual([v.index for v in paths[3]], list(reversed([9, 5, 4, 3, 0])))
        self.assertEqual([v.index for v in paths[4]], list(reversed([10, 5, 4, 3, 0])))
        self.assertEqual([v.index for v in paths[5]], list(reversed([11, 4, 3, 0])))
        self.assertEqual([v.index for v in paths[6]], list(reversed([12, 0])))


    def test_all_root_paths_empty(self):
        paths = ct.all_root_paths(ig.Graph(directed=True))
        self.assertEqual(paths, [])


    def test_all_leaf_paths(self):
        result = ct.all_leaf_paths(self.tree)
        target = self.leaf_paths

        for leaf, paths in result.items():
            for idx, path in enumerate(paths):
                self.assertEqual([v.index for v in path], target[leaf][idx])


    def test_all_leaf_paths_reverse(self):
        result = ct.all_leaf_paths(self.tree, reverse=True)

        target = {l: [list(reversed(p)) for p in paths] for l, paths in self.leaf_paths.items()}

        for leaf, paths in result.items():
            for idx, path in enumerate(paths):
                self.assertEqual([v.index for v in path], target[leaf][idx])


    def test_all_leaf_paths_full_path(self):
        result = ct.all_leaf_paths(self.tree, full_path=True)
        target = self.leaf_paths

        target = {l: [p + [l] for p in paths] for l, paths in self.leaf_paths.items()}

        for leaf, paths in result.items():
            for idx, path in enumerate(paths):
                self.assertEqual([v.index for v in path], target[leaf][idx])


    def test_all_leaf_paths_reverse_full_path(self):
        result = ct.all_leaf_paths(self.tree, reverse=True, full_path=True)

        target = {
            l: [[l] + list(reversed(p)) for p in paths]
            for l, paths in self.leaf_paths.items()
        }

        for leaf, paths in result.items():
            for idx, path in enumerate(paths):
                self.assertEqual([v.index for v in path], target[leaf][idx])


    def test_all_leaf_paths_empty(self):
        paths = ct.all_leaf_paths(ig.Graph(directed=True))
        self.assertEqual(paths, defaultdict(list))




class TestAlgorithm(unittest.TestCase):
    def setUp(self):
        self.tree = ig.Graph(directed=True)
        self.tree.add_vertices(13)
        self.tree.add_edges([
            (0, 1), (0, 3), (0, 12),
            (1, 6), (1, 2),
            (2, 7), (2, 8),
            (3, 4),
            (4, 5), (4, 11),
            (5, 9), (5, 10)
        ])


    def test_dfs(self):
        vertex_list = list(ct.dfs(self.tree))

        target_list = [
            0, 1, 6, 2, 7, 8, 3, 4, 5, 9, 10, 11, 12
        ]

        self.assertEqual([v.index for v in vertex_list], target_list)


    def test_dfs_custom_sort(self):
        vertex_list = list(ct.dfs(self.tree, ct.EdgeSort(lambda e: e.target)))

        target_list = [
            0, 1, 2, 7, 8, 6, 3, 4, 5, 9, 10, 11, 12
        ]

        self.assertEqual([v.index for v in vertex_list], target_list)


    def test_dfs_leaf_order(self):
        vertex_list = list(ct.dfs(self.tree))

        target_list = [v.index for v in ct.leafs(self.tree)]
        target_list_2 = [6, 7, 8, 9, 10, 11, 12] # sanity check

        self.assertEqual([v.index for v in vertex_list if ct.is_leaf(v)], target_list)
        self.assertEqual([v.index for v in vertex_list if ct.is_leaf(v)], target_list_2)


    def test_dfs_leaf_order_custom_sort(self):
        vertex_list = list(ct.dfs(self.tree, ct.EdgeSort(lambda e: e.target)))

        target_list = [v.index for v in ct.leafs(self.tree, ct.EdgeSort(lambda e: e.target))]
        target_list_2 = [7, 8, 6, 9, 10, 11, 12] # sanity check

        self.assertEqual([v.index for v in vertex_list if ct.is_leaf(v)], target_list)
        self.assertEqual([v.index for v in vertex_list if ct.is_leaf(v)], target_list_2)


    def test_subtree_dfs(self):
        vertex_list = list(ct.dfs(self.tree.vs[3]))

        target_list = [3, 4, 5, 9, 10, 11]

        self.assertEqual(len(vertex_list), len(target_list))
        self.assertEqual([v.index for v in vertex_list], target_list)


    def test_subtree_dfs_custom_sort(self):
        vertex_list = list(ct.dfs(self.tree.vs[1], ct.EdgeSort(lambda e: e.target)))

        target_list = [1, 2, 7, 8, 6]

        self.assertEqual(len(vertex_list), len(target_list))
        self.assertEqual([v.index for v in vertex_list], target_list)


    def test_subtree_dfs_leaf_order(self):
        vertex_list = list(ct.dfs(self.tree.vs[1]))

        target_list = [v.index for v in ct.leafs(self.tree.vs[1])]
        target_list_2 = [6, 7, 8] # sanity check

        self.assertEqual([v.index for v in vertex_list if ct.is_leaf(v)], target_list)
        self.assertEqual([v.index for v in vertex_list if ct.is_leaf(v)], target_list_2)


    def test_subtree_dfs_leaf_order_custom_sort(self):
        vertex_list = list(ct.dfs(self.tree.vs[3], ct.EdgeSort(lambda e: e.target)))

        target_list = [v.index for v in ct.leafs(self.tree.vs[3], ct.EdgeSort(lambda e: e.target))]
        target_list_2 = [9, 10, 11] # sanity check

        self.assertEqual([v.index for v in vertex_list if ct.is_leaf(v)], target_list)
        self.assertEqual([v.index for v in vertex_list if ct.is_leaf(v)], target_list_2)




if __name__ == "__main__":
    unittest.main(verbosity=2)
