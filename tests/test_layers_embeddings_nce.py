#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import numpy as np
import tensorflow as tf
from cosy import layers




class TestNCENoMask(unittest.TestCase):
    def setUp(self):
        self.terminal_symbols = tf.random.uniform((64, 7), dtype=np.int64, minval=0, maxval=1000)
        self.terminal_indices = tf.random.uniform((64, 7), dtype=np.int64, minval=0, maxval=200)
        self.terminal_inputs = (self.terminal_symbols, self.terminal_indices)
        self.terminal_shapes = (self.terminal_symbols.shape, self.terminal_indices.shape)

        self.nonterminal_symbols = tf.random.uniform((64, 7, 14), dtype=np.int64, minval=0, maxval=300)
        self.nonterminal_indices = tf.random.uniform((64, 7, 14), dtype=np.int64, minval=0, maxval=200)
        self.nonterminal_inputs = (self.nonterminal_symbols, self.nonterminal_indices)
        self.nonterminal_shapes = (self.nonterminal_symbols.shape, self.nonterminal_indices.shape)

        self.root_symbols = tf.random.uniform((64, 5), dtype=np.int64, minval=0, maxval=300)
        self.root_indices = tf.random.uniform((64, 5), dtype=np.int64, minval=0, maxval=200)
        self.root_inputs = (self.root_symbols, self.root_indices)
        self.root_shapes = (self.root_symbols.shape, self.root_indices.shape)

        self.inputs = (self.terminal_inputs, self.nonterminal_inputs, self.root_inputs)
        self.inputs_shape = (self.terminal_shapes, self.nonterminal_shapes, self.root_shapes)

        self.input_symbols = (self.terminal_symbols, self.nonterminal_symbols, self.root_symbols)
        self.input_symbols_shape = (
            self.terminal_shapes[0],
            self.nonterminal_shapes[0],
            self.root_shapes[0]
        )

        self.terminal_vocab_size = 1000
        self.nonterminal_vocab_size = 300

        self.max_terminal_index = 200
        self.max_nonterminal_index = 200

        self.terminal_vocab_dimension = 256
        self.nonterminal_vocab_dimension = 256

        self.terminal_index_dimension = 128
        self.nonterminal_index_dimension = 128

        self.use_joint_embedding = False
        self.apply_leaf_mask = False
        self.apply_root_mask = False


    def test_sym_sym_foward_pass(self):
        terminal_embedding = layers.SymbolEmbedding(
            self.terminal_vocab_size,
            self.max_terminal_index,
            self.terminal_vocab_dimension,
            self.terminal_index_dimension,
        )

        nonterminal_embedding = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
        )

        leaf_path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        root_path_embedding = layers.LSTMStack(1, 256, use_cudnn_kernel=False)

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            leaf_path_embedding,
            root_path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce(self.inputs)

        self.assertEqual(output[0].shape, (64, 7, 2 * (256 + 128)))
        self.assertEqual(output[1].shape, (64, 256))


    def test_sym_sym_compute_output_shape(self):
        terminal_embedding = layers.SymbolEmbedding(
            self.terminal_vocab_size,
            self.max_terminal_index,
            self.terminal_vocab_dimension,
            self.terminal_index_dimension,
        )

        nonterminal_embedding = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
        )

        leaf_path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        root_path_embedding = layers.LSTMStack(1, 256, use_cudnn_kernel=False)

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            leaf_path_embedding,
            root_path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce(self.inputs)
        shape = nce.compute_output_shape(self.inputs_shape)

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_sym_sym_shared_foward_pass(self):
        terminal_embedding = layers.SymbolEmbedding(
            self.terminal_vocab_size,
            self.max_terminal_index,
            self.terminal_vocab_dimension,
            self.terminal_index_dimension,
        )

        nonterminal_embedding = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
        )

        path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce(self.inputs)

        self.assertEqual(output[0].shape, (64, 7, 2 * (256 + 128)))
        self.assertEqual(output[1].shape, (64, 256 + 128))


    def test_sym_sym_shared_compute_output_shape(self):
        terminal_embedding = layers.SymbolEmbedding(
            self.terminal_vocab_size,
            self.max_terminal_index,
            self.terminal_vocab_dimension,
            self.terminal_index_dimension,
        )

        nonterminal_embedding = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
        )

        path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce(self.inputs)
        shape = nce.compute_output_shape(self.inputs_shape)

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_emb_sym_foward_pass(self):
        terminal_embedding = tf.keras.layers.Embedding(
            self.terminal_vocab_size,
            self.terminal_vocab_dimension + self.terminal_index_dimension,
            mask_zero=True
        )

        nonterminal_embedding = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
        )

        leaf_path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        root_path_embedding = layers.LSTMStack(1, 256, use_cudnn_kernel=False)

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            leaf_path_embedding,
            root_path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_symbols, self.nonterminal_inputs, self.root_inputs))

        self.assertEqual(output[0].shape, (64, 7, 2 * (256 + 128)))
        self.assertEqual(output[1].shape, (64, 256))


    def test_emb_sym_compute_output_shape(self):
        terminal_embedding = tf.keras.layers.Embedding(
            self.terminal_vocab_size,
            self.terminal_vocab_dimension + self.terminal_index_dimension,
            mask_zero=True
        )

        nonterminal_embedding = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
        )

        leaf_path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        root_path_embedding = layers.LSTMStack(1, 256, use_cudnn_kernel=True)

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            leaf_path_embedding,
            root_path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_symbols, self.nonterminal_inputs, self.root_inputs))

        shape = nce.compute_output_shape(
            (self.terminal_shapes[0], self.nonterminal_shapes, self.root_shapes)
        )

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_emb_sym_shared_foward_pass(self):
        terminal_embedding = tf.keras.layers.Embedding(
            self.terminal_vocab_size,
            self.terminal_vocab_dimension + self.terminal_index_dimension,
            mask_zero=True
        )

        nonterminal_embedding = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
        )

        path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_symbols, self.nonterminal_inputs, self.root_inputs))

        self.assertEqual(output[0].shape, (64, 7, 2 * (256 + 128)))
        self.assertEqual(output[1].shape, (64, 256 + 128))


    def test_emb_sym_shared_compute_output_shape(self):
        terminal_embedding = tf.keras.layers.Embedding(
            self.terminal_vocab_size,
            self.terminal_vocab_dimension + self.terminal_index_dimension,
            mask_zero=True
        )

        nonterminal_embedding = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
        )

        path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_symbols, self.nonterminal_inputs, self.root_inputs))

        shape = nce.compute_output_shape(
            (self.terminal_shapes[0], self.nonterminal_shapes, self.root_shapes)
        )

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_sym_emb_foward_pass(self):
        terminal_embedding = layers.SymbolEmbedding(
            self.terminal_vocab_size,
            self.max_terminal_index,
            self.terminal_vocab_dimension,
            self.terminal_index_dimension,
        )

        nonterminal_embedding = tf.keras.layers.Embedding(
            self.nonterminal_vocab_size,
            self.nonterminal_vocab_dimension + self.nonterminal_index_dimension,
            mask_zero=True
        )

        leaf_path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        root_path_embedding = layers.LSTMStack(1, 256, use_cudnn_kernel=False)

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            leaf_path_embedding,
            root_path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_inputs, self.nonterminal_symbols, self.root_symbols))

        self.assertEqual(output[0].shape, (64, 7, 2 * (256 + 128)))
        self.assertEqual(output[1].shape, (64, 256))


    def test_sym_emb_compute_output_shape(self):
        terminal_embedding = layers.SymbolEmbedding(
            self.terminal_vocab_size,
            self.max_terminal_index,
            self.terminal_vocab_dimension,
            self.terminal_index_dimension,
        )

        nonterminal_embedding = tf.keras.layers.Embedding(
            self.nonterminal_vocab_size,
            self.nonterminal_vocab_dimension + self.nonterminal_index_dimension,
            mask_zero=True
        )

        leaf_path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        root_path_embedding = layers.LSTMStack(1, 256, use_cudnn_kernel=False)

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            leaf_path_embedding,
            root_path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_inputs, self.nonterminal_symbols, self.root_symbols))

        shape = nce.compute_output_shape(
            (self.terminal_shapes, self.nonterminal_shapes[0], self.root_shapes[0])
        )

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_sym_emb_shared_foward_pass(self):
        terminal_embedding = layers.SymbolEmbedding(
            self.terminal_vocab_size,
            self.max_terminal_index,
            self.terminal_vocab_dimension,
            self.terminal_index_dimension,
        )

        nonterminal_embedding = tf.keras.layers.Embedding(
            self.nonterminal_vocab_size,
            self.nonterminal_vocab_dimension + self.nonterminal_index_dimension,
            mask_zero=True
        )

        path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_inputs, self.nonterminal_symbols, self.root_symbols))

        self.assertEqual(output[0].shape, (64, 7, 2 * (256 + 128)))
        self.assertEqual(output[1].shape, (64, 256 + 128))


    def test_sym_emb_shared_compute_output_shape(self):
        terminal_embedding = layers.SymbolEmbedding(
            self.terminal_vocab_size,
            self.max_terminal_index,
            self.terminal_vocab_dimension,
            self.terminal_index_dimension,
        )

        nonterminal_embedding = tf.keras.layers.Embedding(
            self.nonterminal_vocab_size,
            self.nonterminal_vocab_dimension + self.nonterminal_index_dimension,
            mask_zero=True
        )

        path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_inputs, self.nonterminal_symbols, self.root_symbols))

        shape = nce.compute_output_shape(
            (self.terminal_shapes, self.nonterminal_shapes[0], self.root_shapes[0])
        )

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_emb_emb_foward_pass(self):
        terminal_embedding = tf.keras.layers.Embedding(
            self.terminal_vocab_size,
            self.terminal_vocab_dimension + self.terminal_index_dimension,
            mask_zero=True
        )

        nonterminal_embedding = tf.keras.layers.Embedding(
            self.nonterminal_vocab_size,
            self.nonterminal_vocab_dimension + self.nonterminal_index_dimension,
            mask_zero=True
        )

        leaf_path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        root_path_embedding = layers.LSTMStack(1, 256, use_cudnn_kernel=False)

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            leaf_path_embedding,
            root_path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_symbols, self.nonterminal_symbols, self.root_symbols))

        self.assertEqual(output[0].shape, (64, 7, 2 * (256 + 128)))
        self.assertEqual(output[1].shape, (64, 256))


    def test_emb_emb_compute_output_shape(self):
        terminal_embedding = tf.keras.layers.Embedding(
            self.terminal_vocab_size,
            self.terminal_vocab_dimension + self.terminal_index_dimension,
            mask_zero=True
        )

        nonterminal_embedding = tf.keras.layers.Embedding(
            self.nonterminal_vocab_size,
            self.nonterminal_vocab_dimension + self.nonterminal_index_dimension,
            mask_zero=True
        )

        leaf_path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        root_path_embedding = layers.LSTMStack(1, 256, use_cudnn_kernel=False)

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            leaf_path_embedding,
            root_path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_symbols, self.nonterminal_symbols, self.root_symbols))

        shape = nce.compute_output_shape(
            (self.terminal_shapes[0], self.nonterminal_shapes[0], self.root_shapes[0])
        )

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_emb_emb_shared_foward_pass(self):
        terminal_embedding = tf.keras.layers.Embedding(
            self.terminal_vocab_size,
            self.terminal_vocab_dimension + self.terminal_index_dimension,
            mask_zero=True
        )

        nonterminal_embedding = tf.keras.layers.Embedding(
            self.nonterminal_vocab_size,
            self.nonterminal_vocab_dimension + self.nonterminal_index_dimension,
            mask_zero=True
        )

        path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_symbols, self.nonterminal_symbols, self.root_symbols))

        self.assertEqual(output[0].shape, (64, 7, 2 * (256 + 128)))
        self.assertEqual(output[1].shape, (64, 256 + 128))


    def test_emb_emb_shared_compute_output_shape(self):
        terminal_embedding = tf.keras.layers.Embedding(
            self.terminal_vocab_size,
            self.terminal_vocab_dimension + self.terminal_index_dimension,
            mask_zero=True
        )

        nonterminal_embedding = tf.keras.layers.Embedding(
            self.nonterminal_vocab_size,
            self.nonterminal_vocab_dimension + self.nonterminal_index_dimension,
            mask_zero=True
        )

        path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        nce = layers.NCE(
            terminal_embedding,
            nonterminal_embedding,
            path_embedding,
            path_embedding,
            apply_leaf_mask=self.apply_leaf_mask,
            apply_root_mask=self.apply_root_mask,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce((self.terminal_symbols, self.nonterminal_symbols, self.root_symbols))

        shape = nce.compute_output_shape(
            (self.terminal_shapes[0], self.nonterminal_shapes[0], self.root_shapes[0])
        )

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])




class TestNCELeafMask(TestNCENoMask):
    def setUp(self):
        super().setUp()
        self.apply_leaf_mask = True




class TestNCERootMask(TestNCENoMask):
    def setUp(self):
        super().setUp()
        self.apply_root_mask = True




class TestNCEBothMasks(TestNCENoMask):
    def setUp(self):
        super().setUp()
        self.apply_leaf_mask = True
        self.apply_root_mask = True




class TestDerivedNCENoMask(unittest.TestCase):
    def setUp(self):
        self.terminal_symbols = tf.random.uniform((64, 7), dtype=np.int64, minval=0, maxval=1000)
        self.terminal_indices = tf.random.uniform((64, 7), dtype=np.int64, minval=0, maxval=200)
        self.terminal_inputs = (self.terminal_symbols, self.terminal_indices)
        self.terminal_shapes = (self.terminal_symbols.shape, self.terminal_indices.shape)

        self.nonterminal_symbols = tf.random.uniform((64, 7, 14), dtype=np.int64, minval=0, maxval=300)
        self.nonterminal_indices = tf.random.uniform((64, 7, 14), dtype=np.int64, minval=0, maxval=200)
        self.nonterminal_inputs = (self.nonterminal_symbols, self.nonterminal_indices)
        self.nonterminal_shapes = (self.nonterminal_symbols.shape, self.nonterminal_indices.shape)

        self.root_symbols = tf.random.uniform((64, 5), dtype=np.int64, minval=0, maxval=300)
        self.root_indices = tf.random.uniform((64, 5), dtype=np.int64, minval=0, maxval=200)
        self.root_inputs = (self.root_symbols, self.root_indices)
        self.root_shapes = (self.root_symbols.shape, self.root_indices.shape)

        self.inputs = (self.terminal_inputs, self.nonterminal_inputs, self.root_inputs)
        self.inputs_shape = (self.terminal_shapes, self.nonterminal_shapes, self.root_shapes)

        self.terminal_vocab_size = 1000
        self.nonterminal_vocab_size = 300

        self.max_terminal_index = 200
        self.max_nonterminal_index = 200

        self.terminal_vocab_dimension = 256
        self.nonterminal_vocab_dimension = 256

        self.terminal_index_dimension = 128
        self.nonterminal_index_dimension = 128

        self.use_joint_embedding = False
        self.apply_leaf_mask = False
        self.apply_root_mask = False

        self.terminal_embedding = layers.SymbolEmbedding(
            self.terminal_vocab_size,
            self.max_terminal_index,
            self.terminal_vocab_dimension,
            self.terminal_index_dimension,
        )

        self.nonterminal_embedding = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
        )


    def test_average_pooling_forward_pass(self):
        nce = layers.PoolingNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            pooling_type="average"
        )

        output = nce(self.inputs)

        self.assertEqual(output[0].shape, (64, 7, 2 * (256 + 128)))
        self.assertEqual(output[1].shape, (64, 256 + 128))


    def test_average_pooling_compute_output_shape(self):
        nce = layers.PoolingNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=True,
            pooling_type="average"
        )

        output = nce(self.inputs)
        shape = nce.compute_output_shape(self.inputs_shape)

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_max_pooling_forward_pass(self):
        nce = layers.PoolingNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=self.apply_leaf_mask,
            pooling_type="max"
        )

        output = nce(self.inputs)

        self.assertEqual(output[0].shape, (64, 7, 2 * (256 + 128)))
        self.assertEqual(output[1].shape, (64, 256 + 128))


    def test_max_pooling_compute_output_shape(self):
        nce = layers.PoolingNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=self.apply_leaf_mask,
            pooling_type="max"
        )

        output = nce(self.inputs)
        shape = nce.compute_output_shape(self.inputs_shape)

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_lstm_forward_pass(self):
        nce = layers.RecurrentNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=self.apply_leaf_mask,
            rnn_type="lstm",
            num_layers=1,
            dimensions=256
        )

        output = nce(self.inputs)

        self.assertEqual(output[0].shape, (64, 7, 640))
        self.assertEqual(output[1].shape, (64, 256))


    def test_lstm_compute_output_shape(self):
        nce = layers.RecurrentNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=self.apply_leaf_mask,
            rnn_type="lstm",
            num_layers=1,
            dimensions=256
        )

        output = nce(self.inputs)
        shape = nce.compute_output_shape(self.inputs_shape)

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_multi_lstm_forward_pass(self):
        nce = layers.RecurrentNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=self.apply_leaf_mask,
            rnn_type="lstm",
            num_layers=2,
            dimensions=[128, 256]
        )

        output = nce(self.inputs)

        self.assertEqual(output[0].shape, (64, 7, 640))
        self.assertEqual(output[1].shape, (64, 256))


    def test_multi_lstm_compute_output_shape(self):
        nce = layers.RecurrentNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=self.apply_leaf_mask,
            rnn_type="lstm",
            num_layers=2,
            dimensions=[128, 256]
        )

        output = nce(self.inputs)
        shape = nce.compute_output_shape(self.inputs_shape)

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_gru_forward_pass(self):
        nce = layers.RecurrentNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=self.apply_leaf_mask,
            rnn_type="gru",
            num_layers=1,
            dimensions=256
        )

        output = nce(self.inputs)

        self.assertEqual(output[0].shape, (64, 7, 640))
        self.assertEqual(output[1].shape, (64, 256))


    def test_gru_compute_output_shape(self):
        nce = layers.RecurrentNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=self.apply_leaf_mask,
            rnn_type="gru",
            num_layers=1,
            dimensions=256
        )

        output = nce(self.inputs)
        shape = nce.compute_output_shape(self.inputs_shape)

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])


    def test_multi_gru_forward_pass(self):
        nce = layers.RecurrentNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=self.apply_leaf_mask,
            rnn_type="gru",
            num_layers=2,
            dimensions=[128, 256]
        )

        output = nce(self.inputs)

        self.assertEqual(output[0].shape, (64, 7, 640))
        self.assertEqual(output[1].shape, (64, 256))


    def test_multi_gru_compute_output_shape(self):
        nce = layers.RecurrentNCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            use_joint_embedding=self.use_joint_embedding,
            mask_paths=self.apply_leaf_mask,
            rnn_type="gru",
            num_layers=2,
            dimensions=[128, 256]
        )

        output = nce(self.inputs)
        shape = nce.compute_output_shape(self.inputs_shape)

        self.assertEqual(output[0].shape, shape[0])
        self.assertEqual(output[1].shape, shape[1])




class TestDerivedNCELeafMask(TestDerivedNCENoMask):
    def setUp(self):
        super().setUp()
        self.apply_leaf_mask = True




if __name__ == "__main__":
    unittest.main(verbosity=2)
