#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import math
import os

import igraph as ig

from cosy.grammar import BNF
from cosy.tree import TreeAttributes, children, root
from cosy.deprecated.generator import Generator, generate




class TestGeneratorExpansionMap(unittest.TestCase):
    def setUp(self):
        self.bnf = BNF()
        self.bnf.add("S", [["A"], ["B"]])
        self.bnf.add("A", [["A"], ["a"]])
        self.bnf.add("B", [["C"]])
        self.bnf.add("C", [["c"]])


    def test_compute_expansion_map_S(self):
        gen = Generator(self.bnf, 1, 1)

        self.assertEqual(gen.expansion_map["S"][0]["min_depth"], 2)
        self.assertEqual(gen.expansion_map["S"][0]["max_depth"], math.inf)
        self.assertEqual(gen.expansion_map["S"][0]["mask"], [True])

        self.assertEqual(gen.expansion_map["S"][1]["min_depth"], 3)
        self.assertEqual(gen.expansion_map["S"][1]["max_depth"], 3)
        self.assertEqual(gen.expansion_map["S"][1]["mask"], [True])


    def test_compute_expansion_map_A(self):
        gen = Generator(self.bnf, 1, 1)

        self.assertEqual(gen.expansion_map["A"][0]["min_depth"], 2)
        self.assertEqual(gen.expansion_map["A"][0]["max_depth"], math.inf)
        self.assertEqual(gen.expansion_map["A"][0]["mask"], [True])

        self.assertEqual(gen.expansion_map["A"][1]["min_depth"], 1)
        self.assertEqual(gen.expansion_map["A"][1]["max_depth"], 1)
        self.assertEqual(gen.expansion_map["A"][1]["mask"], [False])


    def test_compute_expansion_map_B(self):
        gen = Generator(self.bnf, 1, 1)

        self.assertEqual(gen.expansion_map["B"][0]["min_depth"], 2)
        self.assertEqual(gen.expansion_map["B"][0]["max_depth"], 2)
        self.assertEqual(gen.expansion_map["B"][0]["mask"], [True])


    def test_compute_expansion_map_C(self):
        gen = Generator(self.bnf, 1, 1)

        self.assertEqual(gen.expansion_map["C"][0]["min_depth"], 1)
        self.assertEqual(gen.expansion_map["C"][0]["max_depth"], 1)
        self.assertEqual(gen.expansion_map["C"][0]["mask"], [False])


    def test_numeric_compute_expansion_map_S(self):
        gen = Generator(self.bnf.numeric, 1, 1)

        self.assertEqual(gen.expansion_map[0][0]["min_depth"], 2)
        self.assertEqual(gen.expansion_map[0][0]["max_depth"], math.inf)
        self.assertEqual(gen.expansion_map[0][0]["mask"], [True])

        self.assertEqual(gen.expansion_map[0][1]["min_depth"], 3)
        self.assertEqual(gen.expansion_map[0][1]["max_depth"], 3)
        self.assertEqual(gen.expansion_map[0][1]["mask"], [True])


    def test_numeric_compute_expansion_map_A(self):
        gen = Generator(self.bnf.numeric, 1, 1)

        self.assertEqual(gen.expansion_map[1][0]["min_depth"], 2)
        self.assertEqual(gen.expansion_map[1][0]["max_depth"], math.inf)
        self.assertEqual(gen.expansion_map[1][0]["mask"], [True])

        self.assertEqual(gen.expansion_map[1][1]["min_depth"], 1)
        self.assertEqual(gen.expansion_map[1][1]["max_depth"], 1)
        self.assertEqual(gen.expansion_map[1][1]["mask"], [False])


    def test_numeric_compute_expansion_map_B(self):
        gen = Generator(self.bnf.numeric, 1, 1)

        self.assertEqual(gen.expansion_map[2][0]["min_depth"], 2)
        self.assertEqual(gen.expansion_map[2][0]["max_depth"], 2)
        self.assertEqual(gen.expansion_map[2][0]["mask"], [True])


    def test_numeric_compute_expansion_map_C(self):
        gen = Generator(self.bnf.numeric, 1, 1)

        self.assertEqual(gen.expansion_map[4][0]["min_depth"], 1)
        self.assertEqual(gen.expansion_map[4][0]["max_depth"], 1)
        self.assertEqual(gen.expansion_map[4][0]["mask"], [False])




class TestGeneratorExpansionMapWidths(unittest.TestCase):
    def setUp(self):
        self.bnf = BNF()
        self.bnf.add("S", [["A"], ["B"]])
        self.bnf.add("A", [["A"], ["a", "a"]])
        self.bnf.add("B", [["C"], ["B", "b"]])
        self.bnf.add("C", [["c"]])


    def test_compute_expansion_map_S(self):
        gen = Generator(self.bnf)

        self.assertEqual(gen.expansion_map["S"][0]["min_width"], 2)
        self.assertEqual(gen.expansion_map["S"][0]["max_width"], 2)
        self.assertEqual(gen.expansion_map["S"][0]["min_width_list"], [2])
        self.assertEqual(gen.expansion_map["S"][0]["max_width_list"], [2])

        self.assertEqual(gen.expansion_map["S"][1]["min_width"], 1)
        self.assertEqual(gen.expansion_map["S"][1]["max_width"], math.inf)
        self.assertEqual(gen.expansion_map["S"][1]["min_width_list"], [1])
        self.assertEqual(gen.expansion_map["S"][1]["max_width_list"], [math.inf])


    def test_compute_expansion_map_A(self):
        gen = Generator(self.bnf)

        self.assertEqual(gen.expansion_map["A"][0]["min_width"], 2)
        self.assertEqual(gen.expansion_map["A"][0]["max_width"], 2)
        self.assertEqual(gen.expansion_map["A"][0]["min_width_list"], [2])
        self.assertEqual(gen.expansion_map["A"][0]["max_width_list"], [2])

        self.assertEqual(gen.expansion_map["A"][1]["min_width"], 2)
        self.assertEqual(gen.expansion_map["A"][1]["max_width"], 2)
        self.assertEqual(gen.expansion_map["A"][1]["min_width_list"], [1, 1])
        self.assertEqual(gen.expansion_map["A"][1]["max_width_list"], [1, 1])


    def test_compute_expansion_map_B(self):
        gen = Generator(self.bnf)

        self.assertEqual(gen.expansion_map["B"][0]["min_width"], 1)
        self.assertEqual(gen.expansion_map["B"][0]["max_width"], 1)
        self.assertEqual(gen.expansion_map["B"][0]["min_width_list"], [1])
        self.assertEqual(gen.expansion_map["B"][0]["max_width_list"], [1])

        self.assertEqual(gen.expansion_map["B"][1]["min_width"], 2)
        self.assertEqual(gen.expansion_map["B"][1]["max_width"], math.inf)
        self.assertEqual(gen.expansion_map["B"][1]["min_width_list"], [1, 1])
        self.assertEqual(gen.expansion_map["B"][1]["max_width_list"], [math.inf, 1])


    def test_compute_expansion_map_C(self):
        gen = Generator(self.bnf)

        self.assertEqual(gen.expansion_map["C"][0]["min_width"], 1)
        self.assertEqual(gen.expansion_map["C"][0]["max_width"], 1)
        self.assertEqual(gen.expansion_map["C"][0]["min_width_list"], [1])
        self.assertEqual(gen.expansion_map["C"][0]["max_width_list"], [1])


    def test_numeric_compute_expansion_map_S(self):
        gen = Generator(self.bnf.numeric)

        self.assertEqual(gen.expansion_map[0][0]["min_width"], 2)
        self.assertEqual(gen.expansion_map[0][0]["max_width"], 2)
        self.assertEqual(gen.expansion_map[0][0]["min_width_list"], [2])
        self.assertEqual(gen.expansion_map[0][0]["max_width_list"], [2])

        self.assertEqual(gen.expansion_map[0][1]["min_width"], 1)
        self.assertEqual(gen.expansion_map[0][1]["max_width"], math.inf)
        self.assertEqual(gen.expansion_map[0][1]["min_width_list"], [1])
        self.assertEqual(gen.expansion_map[0][1]["max_width_list"], [math.inf])


    def test_numeric_compute_expansion_map_A(self):
        gen = Generator(self.bnf.numeric)

        self.assertEqual(gen.expansion_map[1][0]["min_width"], 2)
        self.assertEqual(gen.expansion_map[1][0]["max_width"], 2)
        self.assertEqual(gen.expansion_map[1][0]["min_width_list"], [2])
        self.assertEqual(gen.expansion_map[1][0]["max_width_list"], [2])

        self.assertEqual(gen.expansion_map[1][1]["min_width"], 2)
        self.assertEqual(gen.expansion_map[1][1]["max_width"], 2)
        self.assertEqual(gen.expansion_map[1][1]["min_width_list"], [1, 1])
        self.assertEqual(gen.expansion_map[1][1]["max_width_list"], [1, 1])


    def test_numeric_compute_expansion_map_B(self):
        gen = Generator(self.bnf.numeric)

        self.assertEqual(gen.expansion_map[2][0]["min_width"], 1)
        self.assertEqual(gen.expansion_map[2][0]["max_width"], 1)
        self.assertEqual(gen.expansion_map[2][0]["min_width_list"], [1])
        self.assertEqual(gen.expansion_map[2][0]["max_width_list"], [1])

        self.assertEqual(gen.expansion_map[2][1]["min_width"], 2)
        self.assertEqual(gen.expansion_map[2][1]["max_width"], math.inf)
        self.assertEqual(gen.expansion_map[2][1]["min_width_list"], [1, 1])
        self.assertEqual(gen.expansion_map[2][1]["max_width_list"], [math.inf, 1])


    def test_numeric_compute_expansion_map_C(self):
        gen = Generator(self.bnf.numeric)

        self.assertEqual(gen.expansion_map[4][0]["min_width"], 1)
        self.assertEqual(gen.expansion_map[4][0]["max_width"], 1)
        self.assertEqual(gen.expansion_map[4][0]["min_width_list"], [1])
        self.assertEqual(gen.expansion_map[4][0]["max_width_list"], [1])




class TestGeneratorGenerate(unittest.TestCase):
    def setUp(self):
        self.bnf = BNF()
        self.bnf.add("S", [["A"], ["B"]])
        self.bnf.add("A", [["A"], ["a"]])
        self.bnf.add("B", [["C"]])
        self.bnf.add("C", [["c"]])
        self.bnf.start_symbol = "S"


    def test_recursive_generate(self):
        vertices, edges = [], []
        gen = Generator(self.bnf, 3, 3, return_parse_trees=True)
        idx = gen._recursive_generate(vertices, edges, "A", 2, 0, math.inf)

        tree = ig.Graph(directed=True)
        tree.add_vertices(len(vertices))
        tree.vs[TreeAttributes.SYMBOL] = vertices
        tree.add_edges(edges)

        vertex = tree.vs[idx]

        self.assertEqual(vertex[TreeAttributes.SYMBOL], "A")
        self.assertEqual([child[TreeAttributes.SYMBOL] for child in children(vertex)], ["a"])
        self.assertEqual([children(child) for child in children(vertex)], [[]])


    def test_recursive_generate_width(self):
        vertices, edges = [], []
        self.bnf.add("B", [["B", "b"]])
        Generator(self.bnf)._recursive_generate(vertices, edges, "S", 0, 2, 2)

        tree = ig.Graph(directed=True)
        tree.add_vertices(len(vertices))
        tree.vs[TreeAttributes.SYMBOL] = vertices
        tree.add_edges(edges)

        self.assertEqual(self.bnf.make_sentence(tree), ["c", "b"])


    def test_generate(self):
        gen = Generator(self.bnf, 3, 3, return_parse_trees=True)
        data = gen(num_programs=100)

        data_a = [root(v) for v in data if children(root(v))[0][TreeAttributes.SYMBOL] == "A"]
        data_b = [root(v) for v in data if children(root(v))[0][TreeAttributes.SYMBOL] == "B"]

        def verify_tree(tree, val_one, val_two, val_three):
            if len(children(tree)) != 1:
                return False
            if len(children(children(tree)[0])) != 1:
                return False
            if len(children(children(children(tree)[0])[0])) != 1:
                return False
            if len(children(children(children(children(tree)[0])[0])[0])) != 0:
                return False

            if children(tree)[0][TreeAttributes.SYMBOL] != val_one:
                return False
            if children(children(tree)[0])[0][TreeAttributes.SYMBOL] != val_two:
                return False
            if children(children(children(tree)[0])[0])[0][TreeAttributes.SYMBOL] != val_three:
                return False

            return True

        self.assertEqual(len(data_a) + len(data_b), len(data))
        self.assertTrue(all([verify_tree(tree, "A", "A", "a") for tree in data_a]))
        self.assertTrue(all([verify_tree(tree, "B", "C", "c") for tree in data_b]))


    def test_numeric_recursive_generate(self):
        vertices, edges = [], []
        gen = Generator(self.bnf.numeric, 3, 3, return_parse_trees=True)
        idx = gen._recursive_generate(vertices, edges, 1, 2, 0, math.inf)

        tree = ig.Graph(directed=True)
        tree.add_vertices(len(vertices))
        tree.vs[TreeAttributes.SYMBOL] = vertices
        tree.add_edges(edges)

        vertex = tree.vs[idx]

        self.assertEqual(vertex[TreeAttributes.SYMBOL], 1)
        self.assertEqual([child[TreeAttributes.SYMBOL] for child in children(vertex)], [3])
        self.assertEqual([children(child) for child in children(vertex)], [[]])


    def test_numeric_recursive_generate_width(self):
        vertices, edges = [], []
        self.bnf.add("B", [["B", "b"]])
        res = Generator(self.bnf.numeric)._recursive_generate(vertices, edges, 0, 0, 2, 2)

        tree = ig.Graph(directed=True)
        tree.add_vertices(len(vertices))
        tree.vs[TreeAttributes.SYMBOL] = vertices
        tree.add_edges(edges)

        self.assertTrue((self.bnf.numeric.make_sentence(tree) == [5, 6]).all())


    def test_numeric_generate(self):
        gen = Generator(self.bnf.numeric, 3, 3, return_parse_trees=True)
        data = gen(num_programs=100)

        data_a = [root(v) for v in data if children(root(v))[0][TreeAttributes.SYMBOL] == 1]
        data_b = [root(v) for v in data if children(root(v))[0][TreeAttributes.SYMBOL] == 2]

        def verify_tree(tree, val_one, val_two, val_three):
            if len(children(tree)) != 1:
                return False
            if len(children(children(tree)[0])) != 1:
                return False
            if len(children(children(children(tree)[0])[0])) != 1:
                return False
            if len(children(children(children(children(tree)[0])[0])[0])) != 0:
                return False

            if children(tree)[0][TreeAttributes.SYMBOL] != val_one:
                return False
            if children(children(tree)[0])[0][TreeAttributes.SYMBOL] != val_two:
                return False
            if children(children(children(tree)[0])[0])[0][TreeAttributes.SYMBOL] != val_three:
                return False

            return True

        self.assertEqual(len(data_a) + len(data_b), len(data))
        self.assertTrue(all([verify_tree(tree, 1, 1, 3) for tree in data_a]))
        self.assertTrue(all([verify_tree(tree, 2, 4, 5) for tree in data_b]))




class TestGenerate(unittest.TestCase):
    def setUp(self):
        self.bnf = BNF()
        self.bnf.add("S", [["A"], ["B"]])
        self.bnf.add("A", [["A"], ["a"]])
        self.bnf.add("B", [["C"]])
        self.bnf.add("C", [["c"]])
        self.bnf.start_symbol = "S"


    def test_run_1_file_1_thread(self):
        path = os.path.join("tests", "data")
        filename = os.path.join(path, "gentest_0_0.tfrecord")

        try:
            generate(self.bnf, path, "gentest", num_files=1, programs_per_file=10)
            self.assertTrue(os.path.isfile(filename))
        finally:
            os.remove(filename)


    def test_run_n_files_1_thread(self):
        path = os.path.join("tests", "data")
        filename = os.path.join(path, "gentest_0_{}.tfrecord")

        try:
            generate(self.bnf, path, "gentest", num_files=10, programs_per_file=10)
            for idx in range(10):
                self.assertTrue(os.path.isfile(filename.format(idx)))
        finally:
            for idx in range(10):
                os.remove(filename.format(idx))


    def test_run_1_file_m_threads(self):
        path = os.path.join("tests", "data")
        filename = os.path.join(path, "gentest_{}_0.tfrecord")

        try:
            generate(self.bnf, path, "gentest", num_files=1, programs_per_file=10, num_threads=7)
            for idx in range(7):
                self.assertTrue(os.path.isfile(filename.format(idx)))
        finally:
            for idx in range(7):
                os.remove(filename.format(idx))


    def test_run_n_files_m_threads(self):
        path = os.path.join("tests", "data")
        filename = os.path.join(path, "gentest_{}_{}.tfrecord")

        try:
            generate(self.bnf, path, "gentest", num_files=10, programs_per_file=10, num_threads=7)
            for tid in range(7):
                for fid in range(10):
                    self.assertTrue(os.path.isfile(filename.format(tid, fid)))
        finally:
            for tid in range(7):
                for fid in range(10):
                    os.remove(filename.format(tid, fid))




if __name__ == "__main__":
    unittest.main(verbosity=2)
