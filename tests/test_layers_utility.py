#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import numpy as np
import tensorflow as tf
from tensorflow import keras as K
from cosy import layers
from cosy.deprecated import layers as l




class TestUtilityLayers(unittest.TestCase):
    def test_input_selector_correctness(self):
        symbols = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=200)

        embedding = tf.keras.layers.Embedding(300, 256)
        wrapped = layers.InputSelector(embedding, 0)

        output = wrapped((symbols, indices))
        target = embedding(symbols)

        self.assertTrue(tf.reduce_all(tf.equal(output, target)))


    def test_input_selector_compute_output_shape(self):
        symbols = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 12, 3), dtype=np.int64, minval=0, maxval=200)

        embedding = tf.keras.layers.Embedding(300, 256)
        wrapped = layers.InputSelector(embedding, 0)

        shape = wrapped.compute_output_shape(((64, 28), (64, 12, 3)))
        output = wrapped((symbols, indices))

        self.assertEqual(shape, output.shape)


    def test_input_selector_compute_mask(self):
        symbols = tf.random.uniform((63, 17), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((63, 17), dtype=np.int64, minval=0, maxval=200)

        symbols = tf.concat([symbols, tf.zeros((1, 17), dtype=np.int64)], axis=0)
        indices = tf.concat([indices, tf.zeros((1, 17), dtype=np.int64)], axis=0)

        embedding = tf.keras.layers.Embedding(300, 256, mask_zero=True)
        wrapped = layers.InputSelector(embedding, 0)

        output = wrapped.compute_mask((symbols, indices))
        target = embedding.compute_mask(symbols)

        self.assertTrue(tf.reduce_all(tf.equal(output, target)))


    def test_input_sperator_correctness(self):
        symbols = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=200)

        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 256)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.NonterminalPathEmbedding(nonterminal_embedding, path_embedding)
        wrapped = layers.InputSeperator(embedding)

        output = wrapped(tf.concat([symbols[..., tf.newaxis], indices[..., tf.newaxis]], -1))
        target = embedding([symbols, indices])

        self.assertTrue(tf.reduce_all(tf.equal(output, target)))


    def test_input_seperator_compute_output_shape(self):
        symbols = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, 28), dtype=np.int64, minval=0, maxval=200)

        embedding = layers.SymbolEmbedding(300, 200, 256, 256)
        wrapped = layers.InputSeperator(embedding)

        inputs = tf.concat([symbols[..., tf.newaxis], indices[..., tf.newaxis]], -1)

        shape = wrapped.compute_output_shape(inputs.shape)
        output = wrapped(inputs)

        self.assertEqual(shape, output.shape)


    def test_input_seperator_compute_mask(self):
        symbols = tf.random.uniform((63, 17), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((63, 17), dtype=np.int64, minval=0, maxval=200)

        symbols = tf.concat([symbols, tf.zeros((1, 17), dtype=np.int64)], axis=0)
        indices = tf.concat([indices, tf.zeros((1, 17), dtype=np.int64)], axis=0)

        embedding = layers.SymbolEmbedding(300, 200, 256, 256)
        wrapped = layers.InputSeperator(embedding)

        inputs = tf.concat([symbols[..., tf.newaxis], indices[..., tf.newaxis]], -1)

        output = wrapped.compute_mask(inputs)
        target = embedding.compute_mask((symbols, indices))

        self.assertTrue(tf.reduce_all(tf.equal(output, target)))


    def test_multi_time_distributed_correctness(self):
        time_steps = 7

        symbols = tf.random.uniform((64, time_steps, 28), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, time_steps, 28), dtype=np.int64, minval=0, maxval=200)

        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 256)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.NonterminalPathEmbedding(nonterminal_embedding, path_embedding)
        wrapped = layers.MultiTimeDistributed(embedding)

        output = wrapped((symbols, indices))

        results = [
            embedding((symbols[:,i,:], indices[:,i,:]))[:,tf.newaxis,:]
            for i in range(time_steps)
        ]

        target = tf.concat(results, axis=1)

        np.testing.assert_almost_equal(output.numpy(), target.numpy())


    def test_multi_time_distributed_compute_output_shape(self):
        time_steps = 7

        symbols = tf.random.uniform((64, time_steps, 28), dtype=np.int64, minval=0, maxval=300)
        indices = tf.random.uniform((64, time_steps, 28), dtype=np.int64, minval=0, maxval=200)

        nonterminal_embedding = layers.SymbolEmbedding(300, 200, 256, 256)
        path_embedding = tf.keras.layers.LSTM(256)

        embedding = l.NonterminalPathEmbedding(nonterminal_embedding, path_embedding)
        wrapped = layers.MultiTimeDistributed(embedding)

        output = wrapped((symbols, indices))
        shape = wrapped.compute_output_shape((symbols.shape, indices.shape))

        self.assertEqual(shape, output.shape)


    def test_single_lstm_stack_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        layer = layers.LSTMStack(num_layers=1, dimensions=256)
        self.assertEqual(layer(inputs).shape, (64, 256))


    def test_multi_lstm_stack_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        layer = layers.LSTMStack(num_layers=3, dimensions=256)
        self.assertEqual(layer(inputs).shape, (64, 256))


    def test_null_lstm_stack_failure(self):
        self.assertRaises(AssertionError, layers.LSTMStack, 0, 128)


    def test_lstm_stack_compute_output_shape(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        layer = layers.LSTMStack(num_layers=2, dimensions=256)
        self.assertEqual(layer(inputs).shape, layer.compute_output_shape((64, 28, 512)))


    def test_lstm_stack_masked_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        ones, zeros = tf.ones((64, 21), dtype=tf.bool), tf.zeros((64, 7), dtype=tf.bool)
        mask = tf.concat([ones, zeros], axis=1)

        layer = layers.LSTMStack(num_layers=2, dimensions=256)

        self.assertEqual(layer(inputs, mask=mask).shape, (64, 256))


    def test_no_cudnn_lstm_stack_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        layer = layers.LSTMStack(num_layers=2, dimensions=256, use_cudnn_kernel=False)
        self.assertEqual(layer(inputs).shape, (64, 256))


    def test_no_cudnn_lstm_stack_masked_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        ones, zeros = tf.ones((60, 28), dtype=tf.bool), tf.zeros((4, 28), dtype=tf.bool)
        mask = tf.concat([ones, zeros], axis=0)

        layer = layers.LSTMStack(num_layers=2, dimensions=256, use_cudnn_kernel=False)

        self.assertEqual(layer(inputs, mask=mask).shape, (64, 256))


    def test_lstm_stack_mask_impact(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        ones, zeros = tf.ones((64, 12), dtype=tf.bool), tf.zeros((64, 16), dtype=tf.bool)
        mask = tf.concat([ones, zeros], axis=1)

        layer = layers.LSTMStack(num_layers=2, dimensions=256)

        out = layer(inputs)
        masked_out = layer(inputs, mask=mask)

        self.assertTrue(np.any(out.numpy() != masked_out.numpy()))


    def test_no_cudnn_lstm_stack_mask_impact(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        ones, zeros = tf.ones((64, 12), dtype=tf.bool), tf.zeros((64, 16), dtype=tf.bool)
        mask = tf.concat([ones, zeros], axis=1)

        layer = layers.LSTMStack(num_layers=2, dimensions=256, use_cudnn_kernel=False)

        out = layer(inputs)
        masked_out = layer(inputs, mask=mask)

        self.assertTrue(np.any(out.numpy() != masked_out.numpy()))


    def test_multi_dimensions_lstm_stack_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        layer = layers.LSTMStack(num_layers=3, dimensions=[256, 512, 128])
        self.assertEqual(layer(inputs).shape, (64, 128))


    def test_multi_dimensions_lstm_stack_raises(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        self.assertRaises(AssertionError, layers.LSTMStack, 3, [128, 128])


    def test_single_gru_stack_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        layer = layers.GRUStack(num_layers=1, dimensions=256)
        self.assertEqual(layer(inputs).shape, (64, 256))


    def test_multi_gru_stack_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        layer = layers.GRUStack(num_layers=3, dimensions=256)
        self.assertEqual(layer(inputs).shape, (64, 256))


    def test_null_gru_stack_failure(self):
        self.assertRaises(AssertionError, layers.GRUStack, 0, 128)


    def test_gru_stack_compute_output_shape(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        layer = layers.GRUStack(num_layers=2, dimensions=256)
        self.assertEqual(layer(inputs).shape, layer.compute_output_shape((64, 28, 512)))


    def test_gru_stack_masked_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        ones, zeros = tf.ones((64, 21), dtype=tf.bool), tf.zeros((64, 7), dtype=tf.bool)
        mask = tf.concat([ones, zeros], axis=1)

        layer = layers.GRUStack(num_layers=2, dimensions=256)

        self.assertEqual(layer(inputs, mask=mask).shape, (64, 256))


    def test_no_cudnn_gru_stack_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        layer = layers.GRUStack(num_layers=2, dimensions=256, use_cudnn_kernel=False)
        self.assertEqual(layer(inputs).shape, (64, 256))


    def test_no_cudnn_gru_stack_masked_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        ones, zeros = tf.ones((60, 28), dtype=tf.bool), tf.zeros((4, 28), dtype=tf.bool)
        mask = tf.concat([ones, zeros], axis=0)

        layer = layers.GRUStack(num_layers=2, dimensions=256, use_cudnn_kernel=False)

        self.assertEqual(layer(inputs, mask=mask).shape, (64, 256))


    def test_gru_stack_mask_impact(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        ones, zeros = tf.ones((64, 12), dtype=tf.bool), tf.zeros((64, 16), dtype=tf.bool)
        mask = tf.concat([ones, zeros], axis=1)

        layer = layers.GRUStack(num_layers=2, dimensions=256)

        out = layer(inputs)
        masked_out = layer(inputs, mask=mask)

        self.assertTrue(np.any(out.numpy() != masked_out.numpy()))


    def test_no_cudnn_gru_stack_mask_impact(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        ones, zeros = tf.ones((64, 12), dtype=tf.bool), tf.zeros((64, 16), dtype=tf.bool)
        mask = tf.concat([ones, zeros], axis=1)

        layer = layers.GRUStack(num_layers=2, dimensions=256, use_cudnn_kernel=False)

        out = layer(inputs)
        masked_out = layer(inputs, mask=mask)

        self.assertTrue(np.any(out.numpy() != masked_out.numpy()))


    def test_multi_dimensions_gru_stack_forward_pass(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        layer = layers.GRUStack(num_layers=3, dimensions=[256, 512, 128])
        self.assertEqual(layer(inputs).shape, (64, 128))


    def test_multi_dimensions_gru_stack_raises(self):
        inputs = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        self.assertRaises(AssertionError, layers.GRUStack, 3, [128, 128])


    def test_path_attention_build_forward_pass(self):
        query = tf.random.uniform((64, 256), dtype=np.float32, minval=0.0, maxval=300.0)
        paths = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)

        layer = layers.PathAttention()

        self.assertEqual(layer((query, paths)).shape, (64, 512))


    def test_path_attention_no_build_forward_pass(self):
        query = tf.random.uniform((64, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        paths = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)

        layer = layers.PathAttention()

        self.assertEqual(layer((query, paths)).shape, (64, 512))


    def test_path_attention_build_num_params(self):
        query = tf.random.uniform((64, 256), dtype=np.float32, minval=0.0, maxval=300.0)
        paths = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)

        layer = layers.PathAttention()
        layer((query, paths))

        self.assertEqual(layer.count_params(), (256 + 1) * 512)


    def test_path_attention_no_build_num_params(self):
        query = tf.random.uniform((64, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        paths = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)

        layer = layers.PathAttention()
        layer((query, paths))

        self.assertEqual(layer.count_params(), 0)


    def test_path_attention_build_compute_output_shape(self):
        query = tf.random.uniform((64, 256), dtype=np.float32, minval=0.0, maxval=300.0)
        paths = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)

        layer = layers.PathAttention()

        output = layer((query, paths))
        shape = layer.compute_output_shape((query.shape, paths.shape))

        self.assertEqual(output.shape, shape)


    def test_path_attention_no_build_output_shape(self):
        query = tf.random.uniform((64, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        paths = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)

        layer = layers.PathAttention()

        output = layer((query, paths))
        shape = layer.compute_output_shape((query.shape, paths.shape))

        self.assertEqual(output.shape, shape)


    def test_path_attention_build_masked_forward_pass(self):
        query = tf.random.uniform((64, 256), dtype=np.float32, minval=0.0, maxval=300.0)
        paths = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        mask = tf.concat([tf.ones((64, 21), dtype=tf.bool), tf.zeros((64, 7), dtype=tf.bool)], -1)

        layer = layers.PathAttention()

        self.assertEqual(layer((query, paths), mask=mask).shape, (64, 512))


    def test_path_attention_no_build_masked_forward_pass(self):
        query = tf.random.uniform((64, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        paths = tf.random.uniform((64, 28, 512), dtype=np.float32, minval=0.0, maxval=300.0)
        mask = tf.concat([tf.ones((64, 21), dtype=tf.bool), tf.zeros((64, 7), dtype=tf.bool)], -1)

        layer = layers.PathAttention()

        self.assertEqual(layer((query, paths), mask=mask).shape, (64, 512))


    def test_cos_sim_forward_pass_1D(self):
        layer = layers.CosineSimilarity(axis=-1, keepdims=False)
        metric = K.metrics.CosineSimilarity()

        x = tf.constant([1, 2, 3], dtype=tf.float32)
        y = tf.constant([4, 5, 3], dtype=tf.float32)

        s = layer((x, y))
        m = metric(x, y)

        self.assertTrue(s.shape == [])
        self.assertTrue(np.allclose(s, m))


    def test_cos_sim_forward_pass_2D_last(self):
        layer = layers.CosineSimilarity(axis=-1, keepdims=False)
        metric = K.metrics.CosineSimilarity(axis=-1)

        x = tf.constant([[1, 2, 3], [2, 3, 4]], dtype=tf.float32)
        y = tf.constant([[4, 5, 3], [2, 6, 7]], dtype=tf.float32)

        s = layer((x, y))

        self.assertTrue(s.shape == [2])
        self.assertTrue(np.allclose(s[0], metric(x[0,:], y[0,:])))
        metric.reset_states()
        self.assertTrue(np.allclose(s[1], metric(x[1,:], y[1,:])))


    def test_cos_sim_forward_pass_2D_all(self):
        layer = layers.CosineSimilarity(axis=None, keepdims=False)
        metric = K.metrics.CosineSimilarity(axis=None)

        x = tf.constant([[1, 2, 3], [2, 3, 4]], dtype=tf.float32)
        y = tf.constant([[4, 5, 3], [2, 6, 7]], dtype=tf.float32)

        s = layer((x, y))
        m = metric(x, y)

        self.assertTrue(s.shape == [])
        self.assertTrue(np.allclose(s, m))


    def test_cos_sim_forward_pass_1D_keepdims(self):
        layer = layers.CosineSimilarity(axis=-1, keepdims=True)
        metric = K.metrics.CosineSimilarity()

        x = tf.constant([1, 2, 3], dtype=tf.float32)
        y = tf.constant([4, 5, 3], dtype=tf.float32)

        s = layer((x, y))
        m = metric(x, y)

        self.assertTrue(s.shape == [1])
        self.assertTrue(np.allclose(s, m))


    def test_cos_sim_forward_pass_2D_last_keepdims(self):
        layer = layers.CosineSimilarity(axis=-1, keepdims=True)
        metric = K.metrics.CosineSimilarity(axis=-1)

        x = tf.constant([[1, 2, 3], [2, 3, 4]], dtype=tf.float32)
        y = tf.constant([[4, 5, 3], [2, 6, 7]], dtype=tf.float32)

        s = layer((x, y))

        self.assertTrue(s.shape == [2, 1])
        self.assertTrue(np.allclose(s[0], metric(x[0,:], y[0,:])))
        metric.reset_states()
        self.assertTrue(np.allclose(s[1], metric(x[1,:], y[1,:])))


    def test_cos_sim_forward_pass_2D_all_keepdims(self):
        layer = layers.CosineSimilarity(axis=None, keepdims=True)
        metric = K.metrics.CosineSimilarity(axis=None)

        x = tf.constant([[1, 2, 3], [2, 3, 4]], dtype=tf.float32)
        y = tf.constant([[4, 5, 3], [2, 6, 7]], dtype=tf.float32)

        s = layer((x, y))
        m = metric(x, y)

        self.assertTrue(s.shape == [1, 1])
        self.assertTrue(np.allclose(s, m))


    def test_cos_sim_compute_output_shape_1D(self):
        layer = layers.CosineSimilarity(axis=-1, keepdims=False)

        x = tf.constant([1, 2, 3], dtype=tf.float32)
        y = tf.constant([4, 5, 3], dtype=tf.float32)

        s = layer.compute_output_shape((x.shape, y.shape))
        self.assertTrue(s == [])


    def test_cos_sim_compute_output_shape_2D_last(self):
        layer = layers.CosineSimilarity(axis=-1, keepdims=False)

        x = tf.constant([[1, 2, 3], [2, 3, 4]], dtype=tf.float32)
        y = tf.constant([[4, 5, 3], [2, 6, 7]], dtype=tf.float32)

        s = layer.compute_output_shape((x.shape, y.shape))
        self.assertTrue(s == [2])


    def test_cos_sim_compute_output_shape_2D_all(self):
        layer = layers.CosineSimilarity(axis=None, keepdims=False)

        x = tf.constant([[1, 2, 3], [2, 3, 4]], dtype=tf.float32)
        y = tf.constant([[4, 5, 3], [2, 6, 7]], dtype=tf.float32)

        s = layer((x, y))
        self.assertTrue(s.shape == [])


    def test_cos_sim_compute_output_shape_1D_keepdims(self):
        layer = layers.CosineSimilarity(axis=-1, keepdims=True)

        x = tf.constant([1, 2, 3], dtype=tf.float32)
        y = tf.constant([4, 5, 3], dtype=tf.float32)

        s = layer.compute_output_shape((x.shape, y.shape))

        self.assertTrue(s == [1])


    def test_cos_sim_compute_output_shape_2D_last_keepdims(self):
        layer = layers.CosineSimilarity(axis=-1, keepdims=True)

        x = tf.constant([[1, 2, 3], [2, 3, 4]], dtype=tf.float32)
        y = tf.constant([[4, 5, 3], [2, 6, 7]], dtype=tf.float32)

        s = layer.compute_output_shape((x.shape, y.shape))
        self.assertTrue(s == [2, 1])


    def test_cos_sim_compute_output_shape_2D_all_keepdims(self):
        layer = layers.CosineSimilarity(axis=None, keepdims=True)

        x = tf.constant([[1, 2, 3], [2, 3, 4]], dtype=tf.float32)
        y = tf.constant([[4, 5, 3], [2, 6, 7]], dtype=tf.float32)

        s = layer((x, y))
        self.assertTrue(s.shape == [1, 1])


    def test_cos_sim_raises_shapes_1D(self):
        layer = layers.CosineSimilarity(axis=None, keepdims=True)

        x = tf.constant([1, 2, 3], dtype=tf.float32)
        y = tf.constant([4, 5], dtype=tf.float32)

        self.assertRaises(
            tf.errors.InvalidArgumentError,
            layer,
            (x, y)
        )


    def test_cos_sim_raises_shapes_2D(self):
        layer = layers.CosineSimilarity(axis=None, keepdims=True)

        x = tf.constant([[1, 2, 3], [2, 3, 4]], dtype=tf.float32)
        y = tf.constant([[4, 5], [2, 6]], dtype=tf.float32)

        self.assertRaises(
            tf.errors.InvalidArgumentError,
            layer,
            (x, y)
        )


    def test_cos_sim_raises_dtypes_1D(self):
        layer = layers.CosineSimilarity(axis=None, keepdims=True)

        x = tf.constant([1, 2, 3], dtype=tf.float32)
        y = tf.constant([4, 5, 3], dtype=tf.int64)

        self.assertRaises(
            tf.errors.InvalidArgumentError,
            layer,
            (x, y)
        )


    def test_cos_sim_raises_dtypes_2D(self):
        layer = layers.CosineSimilarity(axis=None, keepdims=True)

        x = tf.constant([[1, 2, 3], [2, 3, 4]], dtype=tf.float32)
        y = tf.constant([[4, 5, 3], [2, 6, 3]], dtype=tf.int64)

        self.assertRaises(
            tf.errors.InvalidArgumentError,
            layer,
            (x, y)
        )




if __name__ == "__main__":
    unittest.main(verbosity=2)
