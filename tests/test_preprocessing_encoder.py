# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import unittest

from cosy.preprocessing import (
    DynamicSymbolEncoder,
    StaticSymbolEncoder,
    SubTokenEncoder,
    build_bpe_vocab
)




class TestBuildVocab(unittest.TestCase):
    def test_build_bpe_vocab(self):
        words = ["abcc", "bbcc", "dadc", "bbad", "a"]
        vocab = build_bpe_vocab(words, vocab_size=7)
        self.assertEqual(set(vocab), {"a", "b", "c", "d", "ad", "bb", "cc"})


    def test_build_bpe_vocab_list(self):
        words = [list("abcc"), list("bbcc"), list("dadc"), list("bbad"), "a"]
        vocab = build_bpe_vocab(words, vocab_size=7)
        self.assertEqual(set(vocab), {"a", "b", "c", "d"})


    def test_build_bpe_vocab_too_smaller(self):
        words = ["abcc", "bbcc", "dadc", "bbad", "a"]
        vocab = build_bpe_vocab(words, vocab_size=2)
        # all letters in alphabet should be used, even if vocab size is to small
        self.assertEqual(set(vocab), {"a", "b", "c", "d"})


    def test_build_bpe_vocab_wrong_datatype(self):
        self.assertRaises(TypeError, build_bpe_vocab, range(10), 5)


    def test_build_bpe_vocab_wrong_list_datatype(self):
        self.assertRaises(TypeError, build_bpe_vocab, [range(10)]*3, 5)




class TestDynamicSymbolEncoder(unittest.TestCase):
    def setUp(self):
        self.encoder = DynamicSymbolEncoder(["a", "b", "c"])

    def test_init(self):
        self.assertEqual(self.encoder._int2str, ["a", "b", "c"])
        self.assertEqual(self.encoder._str2int, {"a": 0, "b": 1, "c": 2})


    def test_encode(self):
        self.assertEqual(self.encoder.encode("b"), 1)


    def test_encode_return_list(self):
        encoder = DynamicSymbolEncoder(["a", "b", "c"], return_list=True)
        self.assertEqual(encoder.encode("b"), [1])


    def test_set_return_list(self):
        self.assertEqual(self.encoder.encode("b"), 1)
        self.encoder.return_list = True
        self.assertEqual(self.encoder.encode("b"), [1])


    def test_encode_missing(self):
        encoder = DynamicSymbolEncoder(["a", "c"])
        self.assertEqual(encoder.encode("b"), 2)
        self.assertEqual(encoder._int2str, ["a", "c", "b"])
        self.assertEqual(encoder._str2int, {"a": 0, "c": 1, "b": 2})


    def test_decode(self):
        self.assertEqual(self.encoder.decode(2), "c")


    def test_raises_decode_negative(self):
        self.assertRaises(AssertionError, self.encoder.decode, -1)
        self.assertRaises(AssertionError, self.encoder.decode, -7)
        self.assertRaises(AssertionError, self.encoder.decode, -12)


    def test_raises_decode_missing(self):
        self.assertRaises(IndexError, self.encoder.decode, 3)


    def test_raises_decode_removed(self):
        self.encoder._int2str[0] = None
        del self.encoder._str2int["a"]
        self.assertRaises(ValueError, self.encoder.decode, 0)


    def test_remove_from_str(self):
        self.encoder.remove("b")
        self.assertEqual(self.encoder._int2str, ["a", None, "c"])
        self.assertEqual(self.encoder._str2int, {"a": 0, "c": 2})


    def test_remove_from_int(self):
        self.encoder.remove(1)
        self.assertEqual(self.encoder._int2str, ["a", None, "c"])
        self.assertEqual(self.encoder._str2int, {"a": 0, "c": 2})


    def test_has_encoding(self):
        self.assertTrue(self.encoder.has_encoding("c"))


    def test_has_no_encoding(self):
        self.assertFalse(self.encoder.has_encoding("d"))


    def test_is_encoding(self):
        self.assertTrue(self.encoder.is_encoding(2))


    def test_is_no_encoding_negative(self):
        self.assertFalse(self.encoder.is_encoding(-1))
        self.assertFalse(self.encoder.is_encoding(-7))
        self.assertFalse(self.encoder.is_encoding(-12))


    def test_is_no_encoding_none(self):
        self.encoder._int2str[1] = None
        self.assertFalse(self.encoder.is_encoding(1))


    def test_is_no_encoding_index(self):
        self.assertFalse(self.encoder.is_encoding(3))


    def test_apply_mapping(self):
        mapping = {0: 4, 1: 1, 2: 0}
        self.encoder.apply_mapping(mapping)
        self.assertEqual(self.encoder._int2str, ["c", "b", None, None, "a"])
        self.assertEqual(self.encoder._str2int, {"a": 4, "b": 1, "c": 0})


    def test_raises_apply_mapping_noexist(self):
        mapping = {0: 4, 1: 1, 3: 0}
        self.assertRaises(IndexError, self.encoder.apply_mapping, mapping)


    def test_raises_apply_mapping_removed(self):
        self.encoder._int2str[0] = None
        del self.encoder._str2int["a"]

        mapping = {0: 4, 1: 1, 2: 0}
        self.assertRaises(KeyError, self.encoder.apply_mapping, mapping)


    def test_get_config(self):
        config = self.encoder.get_config()

        self.assertEqual(config["decoding"], ["a", "b", "c"])
        self.assertEqual(config["encoding"], {"a": 0, "b": 1, "c": 2})


    def test_save_load_roundtrip(self):
        filename = os.path.join("tests", "data", "dynamic_symbol_encoder_roundtrip.json")

        try:
            self.encoder.save(filename)

            new_encoder = DynamicSymbolEncoder.load(filename)

            self.assertEqual(new_encoder._int2str, self.encoder._int2str)
            self.assertEqual(new_encoder._str2int, self.encoder._str2int)
            self.assertEqual(new_encoder.vocab_size, self.encoder.vocab_size)

        finally:
            os.remove(filename)




class TestStaticSymbolEncoder(unittest.TestCase):
    def setUp(self):
        self.symbols = ["a", "b", "c"]
        self.decoder_map = ["<PADDING>", "a", "b", "c"]
        self.encoder_map = {val: idx for idx, val in enumerate(self.decoder_map)}
        self.encoder = StaticSymbolEncoder(self.symbols)


    def test_init(self):
        self.assertEqual(self.encoder._int2str, self.decoder_map)
        self.assertEqual(self.encoder._str2int, self.encoder_map)


    def test_encode(self):
        self.assertEqual(self.encoder.encode("b"), 2)


    def test_encode_return_list(self):
        encoder = StaticSymbolEncoder(self.symbols, return_list=True)
        self.assertEqual(encoder.encode("b"), [2])


    def test_set_return_list(self):
        self.assertEqual(self.encoder.encode("b"), 2)
        self.encoder.return_list = True
        self.assertEqual(self.encoder.encode("b"), [2])


    def test_encode_oov(self):
        self.assertEqual(self.encoder.encode("foobar"), 4)


    def test_encode_padding(self):
        self.assertEqual(self.encoder.encode("<PADDING>"), 0)


    def test_decode(self):
        self.assertEqual(self.encoder.decode(1), "a")


    def test_decode_oov(self):
        self.assertEqual(self.encoder.decode(4), "<UNKNOWN>")


    def test_decode_padding(self):
        self.assertEqual(self.encoder.decode(0), "<PADDING>")


    def test_raises_decode_negative(self):
        self.assertRaises(AssertionError, self.encoder.decode, -1)
        self.assertRaises(AssertionError, self.encoder.decode, -7)
        self.assertRaises(AssertionError, self.encoder.decode, -12)


    def test_has_encoding(self):
        self.assertTrue(self.encoder.has_encoding("c"))


    def test_has_no_encoding(self):
        self.assertFalse(self.encoder.has_encoding("d"))


    def test_has_encoding_padding(self):
        self.assertTrue(self.encoder.has_encoding("<PADDING>"))


    def test_has_encoding_oov(self):
        self.assertFalse(self.encoder.has_encoding("<UNKNOWN>"))


    def test_is_encoding(self):
        self.assertTrue(self.encoder.is_encoding(2))


    def test_is_no_encoding_negative(self):
        self.assertFalse(self.encoder.is_encoding(-1))
        self.assertFalse(self.encoder.is_encoding(-7))
        self.assertFalse(self.encoder.is_encoding(-12))


    def test_is_no_encoding(self):
        self.assertFalse(self.encoder.is_encoding(5))


    def test_is_encoding_padding(self):
        self.assertTrue(self.encoder.is_encoding(0))


    def test_is_encoding_oov(self):
        self.assertFalse(self.encoder.is_encoding(4))


    def test_get_config(self):
        config = self.encoder.get_config()

        self.assertEqual(config["decoding"], self.decoder_map)
        self.assertEqual(config["encoding"], self.encoder_map)
        self.assertEqual(config["oov_symbol"], "<UNKNOWN>")
        self.assertEqual(config["padding_symbol"], "<PADDING>")


    def test_save_load_roundtrip(self):
        filename = os.path.join("tests", "data", "static_symbol_encoder_roundtrip.json")

        try:
            self.encoder.save(filename)

            new_encoder = StaticSymbolEncoder.load(filename)

            self.assertEqual(new_encoder._int2str, self.encoder._int2str)
            self.assertEqual(new_encoder._str2int, self.encoder._str2int)
            self.assertEqual(new_encoder._oov_symbol, self.encoder._oov_symbol)
            self.assertEqual(new_encoder._padding_symbol, self.encoder._padding_symbol)
            self.assertEqual(new_encoder._vocab_size, self.encoder._vocab_size)

        finally:
            os.remove(filename)




class TestSubTokenEncoder(unittest.TestCase):
    def setUp(self):
        self.symbols = ["foobar", "hello_world", "mondo loops"]

        self.sub_tokens_short = [c for sym in self.symbols for c in sym]
        self.sub_tokens_short += ["oo", "he", "ps"]

        self.sub_tokens_default = [
            "foo",
            "xx", #noise
            "bar", "he", "ll",
            "xxx", #noise
            "o_w",
            "xxxx", #noise
            "orld", "mon",
            "yy", #noise
            "do", " lo",
            "yyy", #noise
            "ops",
            "yyyy", #noise
        ]

        self.sub_tokens_oov = [
            "foo",
            "xx", #noise
            "bar", "ll",
            "xxx", #noise
            "orld", "mon",
            "yy", #noise
            "do",
            "yyy", #noise
            "ops",
            "yyyy", #noise
        ]

        self.padding_token = "<PADDING>"


    def test_init_default(self):
        encoder = SubTokenEncoder(self.sub_tokens_default, end_token="<END>")

        decoder_map = self.sub_tokens_default
        encoder_map = {s: i + 1 for i, s in enumerate(decoder_map)}

        self.assertEqual(encoder._int2str, decoder_map)
        self.assertEqual(encoder._str2int, encoder_map)
        self.assertEqual(encoder._min_token_length, 2)
        self.assertEqual(encoder._max_token_length, 4)
        self.assertEqual(encoder.end_code, len(decoder_map) + 1)
        self.assertEqual(encoder.oov_code, None)
        self.assertEqual(encoder.vocab_size, len(decoder_map) + 2)


    def test_init_short(self):
        encoder = SubTokenEncoder(self.sub_tokens_short, oov_token="<OOV>")

        decoder_map = self.sub_tokens_short
        encoder_map = {s: i + 1 for i, s in enumerate(decoder_map)}

        self.assertEqual(encoder._int2str, decoder_map)
        self.assertEqual(encoder._str2int, encoder_map)
        self.assertEqual(encoder._min_token_length, 1)
        self.assertEqual(encoder._max_token_length, 2)
        self.assertEqual(encoder.end_code, None)
        self.assertEqual(encoder.oov_code, len(decoder_map) + 1)
        self.assertEqual(encoder.vocab_size, len(decoder_map) + 2)


    def test_init_oov(self):
        encoder = SubTokenEncoder(self.sub_tokens_oov, end_token="<END>", oov_token="<OOV>")

        decoder_map = self.sub_tokens_oov
        encoder_map = {s: i + 1 for i, s in enumerate(decoder_map)}

        self.assertEqual(encoder._int2str, decoder_map)
        self.assertEqual(encoder._str2int, encoder_map)
        self.assertEqual(encoder._min_token_length, 2)
        self.assertEqual(encoder._max_token_length, 4)
        self.assertEqual(encoder.end_code, len(decoder_map) + 1)
        self.assertEqual(encoder.oov_code, len(decoder_map) + 2)
        self.assertEqual(encoder.vocab_size, len(decoder_map) + 3)


    def test_encode_default(self):
        encoder = SubTokenEncoder(self.sub_tokens_default)

        self.assertEqual(encoder.encode(self.symbols[0]), [1, 3])
        self.assertEqual(encoder.encode(self.symbols[1]), [4, 5, 7, 9])
        self.assertEqual(encoder.encode(self.symbols[2]), [10, 12, 13, 15])


    def test_encode_default_with_end_token(self):
        encoder = SubTokenEncoder(self.sub_tokens_default, end_token="<END>")
        end = encoder._end_code

        self.assertEqual(encoder.encode(self.symbols[0]), [1, 3, end])
        self.assertEqual(encoder.encode(self.symbols[1]), [4, 5, 7, 9, end])
        self.assertEqual(encoder.encode(self.symbols[2]), [10, 12, 13, 15, end])


    def test_encode_short(self):
        encoder = SubTokenEncoder(self.sub_tokens_short)

        for i in range(3):
            res = "".join((encoder._int2str[c-1] for c in encoder.encode(self.symbols[i])))
            self.assertEqual(res, self.symbols[i])


    def test_encode_oov(self):
        encoder = SubTokenEncoder(self.sub_tokens_oov, oov_token="<OOV>")
        oov = encoder.oov_code

        self.assertEqual(encoder.encode(self.symbols[0]), [1, 3])
        self.assertEqual(encoder.encode(self.symbols[1]), [oov, oov, 4, oov, oov, oov, 6])
        self.assertEqual(encoder.encode(self.symbols[2]), [7, 9, oov, oov, oov, 11])


    def test_encode_oov_raises(self):
        encoder = SubTokenEncoder(self.sub_tokens_oov)
        self.assertRaises(ValueError, encoder.encode, self.symbols[1])


    def test_decode_default(self):
        encoder = SubTokenEncoder(self.sub_tokens_default)

        codes = [4, 5, 7, 9]
        self.assertEqual([encoder.decode(c) for c in codes], ["he", "ll", "o_w", "orld"])

        codes = [1, 3]
        self.assertEqual([encoder.decode(c) for c in codes], ["foo", "bar"])

        codes = [10, 12, 13, 15]
        self.assertEqual([encoder.decode(c) for c in codes], ["mon", "do", " lo", "ops"])


    def test_decode_default_with_end_token(self):
        encoder = SubTokenEncoder(self.sub_tokens_default, end_token="<END>")
        end = encoder._end_code

        codes = [4, 5, 7, 9, end]
        self.assertEqual([encoder.decode(c) for c in codes], ["he", "ll", "o_w", "orld", "<END>"])

        codes = [1, 3, end]
        self.assertEqual([encoder.decode(c) for c in codes], ["foo", "bar", "<END>"])

        codes = [10, 12, 13, 15, end]
        self.assertEqual([encoder.decode(c) for c in codes], ["mon", "do", " lo", "ops", "<END>"])


    def test_decode_default_with_padding(self):
        encoder = SubTokenEncoder(self.sub_tokens_default)

        codes = [4, 5, 7, 9, 0, 0]
        tokens = ["he", "ll", "o_w", "orld", "<PADDING>", "<PADDING>"]
        self.assertEqual([encoder.decode(c) for c in codes], tokens)

        codes = [1, 3, 0]
        tokens = ["foo", "bar", "<PADDING>"]
        self.assertEqual([encoder.decode(c) for c in codes], tokens)

        codes = [10, 12, 0, 13, 15]
        tokens = ["mon", "do", "<PADDING>", " lo", "ops"]
        self.assertEqual([encoder.decode(c) for c in codes], tokens)


    def test_decode_short(self):
        encoder = SubTokenEncoder(self.sub_tokens_short)

        for i in range(3):
            tokens = [encoder.decode(t) for t in encoder.encode(self.symbols[i])]
            self.assertEqual("".join(tokens), self.symbols[i])


    def test_decode_oov(self):
        encoder = SubTokenEncoder(self.sub_tokens_oov, oov_token="<OOV>")
        oov = encoder.oov_code

        codes = [1, 3]
        self.assertEqual([encoder.decode(c) for c in codes], ["foo", "bar"])

        codes = [oov, oov, 4, oov, oov, oov, 6]
        tokens = ["<OOV>","<OOV>", "ll", "<OOV>","<OOV>","<OOV>", "orld"]
        self.assertEqual([encoder.decode(c) for c in codes], tokens)

        codes = [7, 9, oov, oov, oov, 11]
        tokens = ["mon", "do", "<OOV>","<OOV>","<OOV>", "ops"]
        self.assertEqual([encoder.decode(c) for c in codes], tokens)


    def test_decode_raises(self):
        encoder = SubTokenEncoder(self.sub_tokens_default)
        self.assertRaises(AssertionError, encoder.decode, -1)


    def test_decode_oov_raises(self):
        encoder = SubTokenEncoder(self.sub_tokens_oov)
        self.assertRaises(ValueError, encoder.decode, 77)


    def test_has_encoding_sub_token(self):
        encoder = SubTokenEncoder(self.sub_tokens_default)

        self.assertTrue(encoder.has_encoding("foo", sub_token_only=True))
        self.assertTrue(encoder.has_encoding("bar", sub_token_only=True))
        self.assertFalse(encoder.has_encoding("foobar", sub_token_only=True))


    def test_has_encoding_symbol_default(self):
        encoder = SubTokenEncoder(self.sub_tokens_default)

        for symbol in self.symbols:
            self.assertTrue(encoder.has_encoding(symbol))


    def test_has_no_encoding_symbol_default(self):
        encoder = SubTokenEncoder(self.sub_tokens_default)

        self.assertFalse(encoder.has_encoding("schlong"))


    def test_has_encoding_symbol_oov(self):
        encoder = SubTokenEncoder(self.sub_tokens_oov)

        self.assertTrue(encoder.has_encoding(self.symbols[0]))
        self.assertFalse(encoder.has_encoding(self.symbols[1]))


    def test_has_encoding_symbol_with_end_symbol(self):
        encoder = SubTokenEncoder(self.sub_tokens_default, end_token="<END>")

        for symbol in self.symbols:
            self.assertTrue(encoder.has_encoding(symbol))


    def test_is_encoding_default(self):
        encoder = SubTokenEncoder(self.sub_tokens_default)

        turning_point = len(self.sub_tokens_default) + 1

        for code in range(turning_point):
            self.assertTrue(encoder.is_encoding(code))

        for code in range(turning_point, 2 * turning_point):
            self.assertFalse(encoder.is_encoding(code))


    def test_is_no_encoding_negative_code(self):
        encoder = SubTokenEncoder(self.sub_tokens_default)

        self.assertFalse(encoder.is_encoding(-1))
        self.assertFalse(encoder.is_encoding(-7))
        self.assertFalse(encoder.is_encoding(-12))


    def test_is_encoding_padding(self):
        encoder = SubTokenEncoder(self.sub_tokens_default)
        self.assertTrue(encoder.is_encoding(encoder.padding_code))


    def test_is_no_encoding_oov(self):
        encoder = SubTokenEncoder(self.sub_tokens_default, oov_token="<OOV>")
        self.assertFalse(encoder.is_encoding(encoder.oov_code))


    def test_is_encoding_end(self):
        encoder = SubTokenEncoder(self.sub_tokens_default, end_token="<END>")
        self.assertTrue(encoder.is_encoding(encoder.end_code))


    def test_get_config(self):
        encoder = SubTokenEncoder(
            self.sub_tokens_default,
            end_token="<END>",
            oov_token="<OOV>",
            padding_token="<PAD>"
        )

        decoder_map = self.sub_tokens_default
        encoder_map = {s: i + 1 for i, s in enumerate(decoder_map)}
        config = encoder.get_config()

        self.assertEqual(config["encoding"], encoder_map)
        self.assertEqual(config["decoding"], decoder_map)
        self.assertEqual(config["end_token"], "<END>")
        self.assertEqual(config["end_code"], len(decoder_map) + 1)
        self.assertEqual(config["oov_token"], "<OOV>")
        self.assertEqual(config["oov_code"], len(decoder_map) + 2)
        self.assertEqual(config["min_token_length"], 2)
        self.assertEqual(config["max_token_length"], 4)
        self.assertEqual(config["padding_token"], "<PAD>")
        self.assertEqual(config["padding_code"], 0)


    def test_save_load_roundtrip(self):
        filename = os.path.join("tests", "data", "sub_token_encoder_roundtrip.json")

        encoder = SubTokenEncoder(
            self.sub_tokens_default,
            end_token="<END>",
            oov_token="<OOV>",
            padding_token="<PAD>"
        )

        try:
            encoder.save(filename)

            new_encoder = SubTokenEncoder.load(filename)

            self.assertEqual(new_encoder._str2int, encoder._str2int)
            self.assertEqual(new_encoder._int2str, encoder._int2str)
            self.assertEqual(new_encoder._end_token, encoder._end_token)
            self.assertEqual(new_encoder._end_code, encoder._end_code)
            self.assertEqual(new_encoder._oov_token, encoder._oov_token)
            self.assertEqual(new_encoder._oov_code, encoder._oov_code)
            self.assertEqual(new_encoder._min_token_length, encoder._min_token_length)
            self.assertEqual(new_encoder._max_token_length, encoder._max_token_length)
            self.assertEqual(new_encoder._padding_token, encoder._padding_token)
            self.assertEqual(new_encoder._padding_code, encoder._padding_code)
        finally:
            os.remove(filename)




if __name__ == "__main__":
    unittest.main(verbosity="b")
