#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import os
import json

import cosy.data as cd




def compare_vertices(a, b):
    return (a.index == b.index) and (a.attributes() == b.attributes())


def compare_edges(a, b):
    if not compare_vertices(a.source_vertex, b.source_vertex):
        return False

    if not compare_vertices(a.target_vertex, b.target_vertex):
        return False

    return (a.index == b.index) and (a.attributes() == b.attributes())




class TestDataset(unittest.TestCase):
    def setUp(self):
        self.filename = os.path.join("tests", "data", "generic", "digits.txt")
        self.dataset = cd.Dataset(self.filename)

    def test_dataset_size(self):
        self.assertEqual(sum(1 for line in self.dataset), 10)

    def test_iter_reset(self):
        iter_a = iter(self.dataset)
        next(iter_a)
        next(iter_a)

        self.assertEqual(next(iter_a), '2\n')

        iter_b = iter(self.dataset)
        next(iter_b)
        next(iter_b)
        next(iter_b)

        self.assertEqual(next(iter_b), '3\n')

    def tearDown(self):
        self.dataset.close()




class TestBase150k(TestDataset):
    def setUp(self):
        self.filename = os.path.join("tests", "data", "python150k", "python_10_asts.txt")
        self.dataset = cd.datasets._Base150kDataset(self.filename)


    def test_default_ast_vertex_count(self):
        vertex_counts = []

        raw_dataset = cd.Dataset(self.filename)
        for line in raw_dataset:
            data = json.loads(line)
            vertex_counts.append(len(data))
            vertex_counts[-1] -= 1 if 0 in data else 0

        raw_dataset.close()

        for ast, vertex_count in zip(self.dataset, vertex_counts):
            self.assertEqual(ast.vcount(), vertex_count)


    def test_default_ast_edge_count(self):
        edge_counts = []

        raw_dataset = cd.Dataset(self.filename)
        for line in raw_dataset:
            data = json.loads(line)
            edge_counts.append(sum(len(v["children"]) for v in data if v != 0 and "children" in v))

        raw_dataset.close()

        for ast, edge_count in zip(self.dataset, edge_counts):
            self.assertEqual(ast.ecount(), edge_count)


    def test_expanded_ast_vertex_count(self):
        vertex_counts = []

        raw_dataset = cd.Dataset(self.filename)
        for line in raw_dataset:
            data = json.loads(line)
            vertex_counts.append(len(data))
            vertex_counts[-1] += sum(1 for v in data if v != 0 and "value" in v)
            vertex_counts[-1] -= 1 if 0 in data else 0

        raw_dataset.close()

        self.dataset.expand_values = True
        for ast, vertex_count in zip(self.dataset, vertex_counts):
            self.assertEqual(ast.vcount(), vertex_count)


    def test_expanded_ast_edge_count(self):
        edge_counts = []

        raw_dataset = cd.Dataset(self.filename)
        for line in raw_dataset:
            data = json.loads(line)
            edge_counts.append(sum(len(v["children"]) for v in data if v != 0 and "children" in v))
            edge_counts[-1] += sum(1 for v in data if v != 0 and "value" in v)

        raw_dataset.close()

        self.dataset.expand_values = True
        for ast, edge_count in zip(self.dataset, edge_counts):
            self.assertEqual(ast.ecount(), edge_count)


    def test_iter_reset(self):
        self.dataset.expand_values = True

        iter_a = iter(self.dataset)
        next(iter_a)
        next(iter_a)
        ast_a = next(iter_a)

        iter_b = iter(self.dataset)
        next(iter_b)
        next(iter_b)
        ast_b = next(iter_b)

        self.assertTrue(len(ast_a.vs) == len(ast_b.vs))
        self.assertTrue(len(ast_a.es) == len(ast_b.es))

        self.assertTrue(all((compare_vertices(a, b) for a, b in zip(ast_a.vs, ast_b.vs))))
        self.assertTrue(all((compare_edges(a, b) for a, b in zip(ast_a.es, ast_b.es))))





class TestPython150k(TestBase150k):
    def setUp(self):
        self.filename = os.path.join("tests", "data", "python150k", "python_10_asts.txt")
        self.dataset = cd.Python150kDataset(self.filename)




class TestJavaScript150k(TestBase150k):
    def setUp(self):
        self.filename = os.path.join("tests", "data", "javascript150k", "javascript_10_asts.txt")
        self.dataset = cd.JavaScript150kDataset(self.filename)




if __name__ == "__main__":
    unittest.main(verbosity=2)
