#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import numpy as np
import tensorflow as tf
from tensorflow import keras as K
from cosy import layers

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)




class CustomAttention(K.layers.Layer):
    def __init__(self, scaling, **kwargs):
        super().__init__(**kwargs)

        self.scaling = scaling
        self.attention = layers.ScaledDotProductAttention()


    def call(self, query, key, value, mask=None, constant=None):
        output, weights = self.attention(query, key, value, mask)

        output *= self.scaling
        output = tf.math.add(output, constant)

        return output, weights




class TestTransformer(unittest.TestCase):
    def test_scaled_dot_product_attention_forward_pass(self):
        query = tf.constant([
            [0, 10, 0],
            [0, 0, 10],
            [10, 10, 0]
        ], dtype=tf.float32)

        key = tf.constant([
            [10,0,0],
            [0,10,0],
            [0,0,10],
            [0,0,10]
        ], dtype=tf.float32)

        value = tf.constant([
            [   1,0],
            [  10,0],
            [ 100,5],
            [1000,6]
        ], dtype=tf.float32)

        output, attention = layers.ScaledDotProductAttention()(query, key, value)

        # some degree of numerical instability is to be expected
        self.assertTrue(np.allclose(output.numpy(), [
            [10.0, 0.0 ],
            [550.0, 5.5],
            [5.5, 0.0]
        ]))

        self.assertTrue(np.allclose(attention.numpy(), [
            [0.0, 1.0, 0.0, 0.0],
            [0.0, 0.0, 0.5, 0.5],
            [0.5, 0.5, 0.0, 0.0]
        ]))


    def test_multi_head_attention_forward_pass(self):
        tmp_mha = layers.MultiHeadAttention(512, 8)
        y = tf.random.uniform((1, 60, 512))
        output, attention = tmp_mha(y, key=y, value=y, mask=None)

        self.assertEqual(output.shape, (1, 60, 512))
        self.assertEqual(attention.shape, (1, 8, 60, 60))


    def test_multi_head_attention_forward_pass_padding_masked(self):
        tmp_mha = layers.MultiHeadAttention(512, 8)
        y = tf.random.uniform((1, 60, 512))
        mask = tf.ones((1, 60), dtype=tf.bool)[:, tf.newaxis, tf.newaxis, :]
        output, attention = tmp_mha(y, key=y, value=y, mask=mask)

        self.assertEqual(output.shape, (1, 60, 512))
        self.assertEqual(attention.shape, (1, 8, 60, 60))


    def test_multi_head_attention_forward_pass_look_ahead_masked(self):
        tmp_mha = layers.MultiHeadAttention(512, 8)
        y = tf.random.uniform((1, 60, 512))
        mask = tf.cast(1 - tf.linalg.band_part(tf.ones((60, 60)), -1, 0), dtype=tf.bool)
        output, attention = tmp_mha(y, key=y, value=y, mask=mask)

        self.assertEqual(output.shape, (1, 60, 512))
        self.assertEqual(attention.shape, (1, 8, 60, 60))


    def test_point_wise_feed_forward_layer_forward_pass(self):
        sample_ffn = layers.PointWiseFeedForward(2048, 512)
        output = sample_ffn(tf.random.uniform((64, 50, 512)))

        self.assertEqual(output.shape, (64, 50, 2048))


    def test_transformer_embedding_forward_pass(self):
        embedding = layers.TransformerEmbedding(512, 8500, 10000)
        inputs = tf.random.uniform((64, 62), dtype=np.int64, minval=0, maxval=200)
        output = embedding(inputs)

        self.assertEqual(output.shape, (64, 62, 512))


    def test_transformer_encoder_layer_forward_pass(self):
        encoder_layer = layers.TransformerEncoderLayer(512, 8, 2048)
        output = encoder_layer(tf.random.uniform((64, 50, 512)), False)

        self.assertEqual(output.shape, (64, 50, 512))


    def test_transformer_encoder_layer_forward_pass_masked(self):
        encoder_layer = layers.TransformerEncoderLayer(512, 8, 2048)
        mask = tf.ones((64, 50), dtype=tf.bool)
        output = encoder_layer(tf.random.uniform((64, 50, 512)), False, mask=mask)

        self.assertEqual(output.shape, (64, 50, 512))


    def test_transformer_encoder_layer_compute_mask(self):
        encoder_layer = layers.TransformerEncoderLayer(512, 8, 2048)

        inputs = tf.random.uniform((64, 50, 512))
        mask = tf.ones((64, 50), dtype=tf.bool)

        output = encoder_layer.compute_mask(inputs, mask)

        self.assertTrue(np.all(output == mask))


    def test_transformer_encoder_layer_forward_pass_no_look_ahead(self):
        encoder_layer = layers.TransformerEncoderLayer(512, 8, 2048)
        encoder_layer_p = layers.TransformerEncoderLayer(512, 8, 2048, prevent_look_ahead=True)

        output = encoder_layer(tf.random.uniform((64, 50, 512)), False)
        output_p = encoder_layer_p(tf.random.uniform((64, 50, 512)), False)

        self.assertEqual(output.shape, (64, 50, 512))
        self.assertEqual(output_p.shape, (64, 50, 512))
        self.assertFalse(np.all(np.isclose(output, output_p)))


    def test_transformer_encoder_layer_forward_pass_masked_no_look_ahead(self):
        encoder_layer = layers.TransformerEncoderLayer(512, 8, 2048)
        encoder_layer_p = layers.TransformerEncoderLayer(512, 8, 2048, prevent_look_ahead=True)

        mask = tf.ones((64, 50), dtype=tf.bool)
        output = encoder_layer(tf.random.uniform((64, 50, 512)), False, mask=mask)
        output_p = encoder_layer_p(tf.random.uniform((64, 50, 512)), False, mask=mask)

        self.assertEqual(output.shape, (64, 50, 512))
        self.assertEqual(output_p.shape, (64, 50, 512))
        self.assertFalse(np.all(np.isclose(output, output_p)))


    def test_transformer_decoder_layer_forward_pass(self):
        # we need the encoder output
        encoder_layer = layers.TransformerEncoderLayer(512, 8, 2048)
        encoder_output = encoder_layer(tf.random.uniform((64, 50, 512)), False)

        decoder_layer = layers.TransformerDecoderLayer(512, 8, 2048)
        output, _, _ = decoder_layer((tf.random.uniform((64, 50, 512)), encoder_output), False)

        self.assertEqual(output.shape, (64, 50, 512))


    def test_transformer_decoder_layer_forward_pass_masked(self):
        # we need the encoder output
        encoder_layer = layers.TransformerEncoderLayer(512, 8, 2048)
        encoder_output = encoder_layer(tf.random.uniform((64, 50, 512)), False)

        mask = (
            tf.ones((64, 50), dtype=tf.bool),
            tf.cast(1 - tf.linalg.band_part(tf.ones((50, 50)), -1, 0), dtype=tf.bool)
        )

        decoder_layer = layers.TransformerDecoderLayer(512, 8, 2048)
        output, _, _ = decoder_layer(
            (tf.random.uniform((64, 50, 512)), encoder_output),
            False,
            mask=mask
        )

        self.assertEqual(output.shape, (64, 50, 512))


    def test_transformer_decoder_layer_compute_mask(self):
        # we need the encoder output
        encoder_layer = layers.TransformerEncoderLayer(512, 8, 2048)
        encoder_output = encoder_layer(tf.random.uniform((64, 50, 512)), False)

        inputs = (tf.random.uniform((64, 50, 512)), encoder_output)
        mask = (
            tf.ones((64, 50), dtype=tf.bool),
            tf.cast(1 - tf.linalg.band_part(tf.ones((50, 50)), -1, 0), dtype=tf.bool)
        )

        decoder_layer = layers.TransformerDecoderLayer(512, 8, 2048)
        output = decoder_layer.compute_mask(inputs, mask)

        self.assertEqual(len(output), 3)
        self.assertTrue(np.all(output[0] == mask[0]))
        self.assertEqual(output[1], None)
        self.assertEqual(output[2], None)


    def test_transformer_encoder_forward_pass(self):
        sample_encoder = layers.TransformerEncoder(2, 512, 8, 2048)

        inputs = tf.random.uniform((64, 62, 512), dtype=np.float32, minval=0, maxval=200)

        output = sample_encoder(inputs, training=False, mask=None)

        self.assertEqual(output.shape, (64, 62, 512))


    def test_transformer_encoder_forward_pass_masked(self):
        sample_encoder = layers.TransformerEncoder(2, 512, 8, 2048)

        inputs = tf.random.uniform((64, 62, 512), dtype=np.float32, minval=0, maxval=200)
        mask = tf.ones((64, 62), dtype=tf.bool)

        output = sample_encoder(inputs, training=False, mask=mask)

        self.assertEqual(output.shape, (64, 62, 512))


    def test_transformer_encoder_compute_mask(self):
        sample_encoder = layers.TransformerEncoder(2, 512, 8, 2048)

        inputs = tf.random.uniform((64, 62, 512), dtype=np.float32, minval=0, maxval=200)
        mask = tf.ones((64, 62), dtype=tf.bool)

        output = sample_encoder.compute_mask(inputs, mask)

        self.assertTrue(np.all(output == mask))


    def test_transformer_encoder_forward_pass_no_look_ahead(self):
        sample_encoder = layers.TransformerEncoder(2, 512, 8, 2048)
        sample_encoder_p = layers.TransformerEncoder(2, 512, 8, 2048, prevent_look_ahead=True)

        inputs = tf.random.uniform((64, 62, 512), dtype=np.float32, minval=0, maxval=200)

        output = sample_encoder(inputs, training=False)
        output_p = sample_encoder_p(inputs, training=False)

        self.assertEqual(output.shape, (64, 62, 512))
        self.assertEqual(output_p.shape, (64, 62, 512))
        self.assertFalse(np.all(np.isclose(output, output_p)))


    def test_transformer_encoder_forward_pass_masked_no_look_ahead(self):
        sample_encoder = layers.TransformerEncoder(2, 512, 8, 2048)
        sample_encoder_p = layers.TransformerEncoder(2, 512, 8, 2048, prevent_look_ahead=True)

        inputs = tf.random.uniform((64, 62, 512), dtype=np.float32, minval=0, maxval=200)
        mask = tf.ones((64, 62), dtype=tf.bool)

        output = sample_encoder(inputs, training=False, mask=mask)
        output_p = sample_encoder_p(inputs, training=False, mask=mask)

        self.assertEqual(output.shape, (64, 62, 512))
        self.assertEqual(output_p.shape, (64, 62, 512))
        self.assertFalse(np.all(np.isclose(output, output_p)))


    def test_transformer_decoder_forward_pass(self):
        # we need the encoder output
        sample_encoder = layers.TransformerEncoder(2, 512, 8, 2048)
        inputs = tf.random.uniform((64, 62, 512), dtype=np.float32, minval=0, maxval=200)
        encoder_output = sample_encoder(inputs, training=False, mask=None)

        sample_encoder = layers.TransformerDecoder(2, 512, 8, 2048)
        output, attention = sample_encoder((inputs, encoder_output), training=False)

        self.assertEqual(output.shape, (64, 62, 512))
        self.assertEqual(attention["decoder_layer_1_block_1"].shape, (64, 8, 62, 62))
        self.assertEqual(attention["decoder_layer_1_block_2"].shape, (64, 8, 62, 62))
        self.assertEqual(attention["decoder_layer_2_block_1"].shape, (64, 8, 62, 62))
        self.assertEqual(attention["decoder_layer_2_block_2"].shape, (64, 8, 62, 62))


    def test_transformer_decoder_forward_pass_masked(self):
        # we need the encoder output
        sample_encoder = layers.TransformerEncoder(2, 512, 8, 2048)
        inputs = tf.random.uniform((64, 62, 512), dtype=np.float32, minval=0, maxval=200)
        encoder_output = sample_encoder(inputs, training=False, mask=None)

        mask = (
            tf.ones((64, 62), dtype=tf.bool),
            tf.cast(1 - tf.linalg.band_part(tf.ones((62, 62)), -1, 0), dtype=tf.bool)
        )

        sample_encoder = layers.TransformerDecoder(2, 512, 8, 2048)
        output, attention = sample_encoder((inputs, encoder_output), training=False, mask=mask)

        self.assertEqual(output.shape, (64, 62, 512))
        self.assertEqual(attention["decoder_layer_1_block_1"].shape, (64, 8, 62, 62))
        self.assertEqual(attention["decoder_layer_1_block_2"].shape, (64, 8, 62, 62))
        self.assertEqual(attention["decoder_layer_2_block_1"].shape, (64, 8, 62, 62))
        self.assertEqual(attention["decoder_layer_2_block_2"].shape, (64, 8, 62, 62))


    def test_transformer_decoder_compute_mask(self):
        # we need the encoder output
        sample_encoder = layers.TransformerEncoder(2, 512, 8, 2048)
        inputs = tf.random.uniform((64, 62, 512), dtype=np.float32, minval=0, maxval=200)
        encoder_output = sample_encoder(inputs, training=False, mask=None)

        mask = (
            tf.ones((64, 62), dtype=tf.bool),
            tf.cast(1 - tf.linalg.band_part(tf.ones((62, 62)), -1, 0), dtype=tf.bool)
        )

        sample_encoder = layers.TransformerDecoder(2, 512, 8, 2048)
        output = sample_encoder.compute_mask((inputs, encoder_output), mask=mask)

        self.assertEqual(len(output), 2)
        self.assertTrue(np.all(output[0] == mask[0]))
        self.assertEqual(output[1], None)




class TestCustomTransformer(unittest.TestCase):
    def setUp(self):
        self.attention = CustomAttention(1.7)


    def test_multi_head_attention_forward_pass(self):
        tmp_mha = layers.MultiHeadAttention(512, 8, self.attention)
        y = tf.random.uniform((1, 60, 512))
        attn_kwargs = {"constant": tf.constant(2.0, dtype=tf.float32, shape=(1, 8, 60, 64))}

        output, attention = tmp_mha(y, key=y, value=y, mask=None, attention_kwargs=attn_kwargs)

        self.assertEqual(output.shape, (1, 60, 512))
        self.assertEqual(attention.shape, (1, 8, 60, 60))


    def test_transformer_encoder_layer_forward_pass(self):
        encoder_layer = layers.TransformerEncoderLayer(512, 8, 2048, attention=self.attention)
        y = tf.random.uniform((64, 50, 512))
        attn_kwargs = {"constant": tf.constant(2.0, dtype=tf.float32, shape=(64, 8, 50, 64))}

        output = encoder_layer(y, False, attention_kwargs=attn_kwargs)

        self.assertEqual(output.shape, (64, 50, 512))


    def test_transformer_decoder_layer_forward_pass(self):
        # we need the encoder output
        encoder_layer = layers.TransformerEncoderLayer(512, 8, 2048)
        encoder_output = encoder_layer(tf.random.uniform((64, 50, 512)), False)

        decoder_layer = layers.TransformerDecoderLayer(512, 8, 2048, attention=self.attention)
        y = tf.random.uniform((64, 50, 512))
        attn_kwargs = {"constant": tf.constant(2.0, dtype=tf.float32, shape=(64, 8, 50, 64))}

        output, _, _ = decoder_layer((y, encoder_output), False, attention_kwargs=attn_kwargs)

        self.assertEqual(output.shape, (64, 50, 512))


    def test_transformer_encoder_forward_pass(self):
        sample_encoder = layers.TransformerEncoder(2, 512, 8, 2048, attention=self.attention)
        y = tf.random.uniform((64, 62, 512), dtype=np.float32, minval=0, maxval=200)
        attn_kwargs = {"constant": tf.constant(2.0, dtype=tf.float32, shape=(64, 8, 62, 64))}

        output = sample_encoder(y, training=False, mask=None, attention_kwargs=attn_kwargs)

        self.assertEqual(output.shape, (64, 62, 512))


    def test_transformer_decoder_forward_pass(self):
        # we need the encoder output
        sample_encoder = layers.TransformerEncoder(2, 512, 8, 2048)
        y = tf.random.uniform((64, 62, 512), dtype=np.float32, minval=0, maxval=200)
        attn_kwargs = {"constant": tf.constant(2.0, dtype=tf.float32, shape=(64, 8, 62, 64))}
        encoder_output = sample_encoder(y, training=False, mask=None)

        sample_encoder = layers.TransformerDecoder(2, 512, 8, 2048, attention=self.attention)
        output, attention = sample_encoder(
            (y, encoder_output),
            training=False,
            attention_kwargs=attn_kwargs
        )

        self.assertEqual(output.shape, (64, 62, 512))
        self.assertEqual(attention["decoder_layer_1_block_1"].shape, (64, 8, 62, 62))
        self.assertEqual(attention["decoder_layer_1_block_2"].shape, (64, 8, 62, 62))
        self.assertEqual(attention["decoder_layer_2_block_1"].shape, (64, 8, 62, 62))
        self.assertEqual(attention["decoder_layer_2_block_2"].shape, (64, 8, 62, 62))




if __name__ == "__main__":
    unittest.main(verbosity=2)
