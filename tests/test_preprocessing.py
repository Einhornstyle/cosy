# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest
import numpy as np
import tensorflow as tf
import cosy.preprocessing as cp




class TestFeatures(unittest.TestCase):
    def test_int_feature_success(self):
        data = list(range(10))
        self.assertEqual(cp.int_feature(data).int64_list.value, data)


    def test_int_feature_failure(self):
        data = [0.1 * i for i in range(10)]
        self.assertRaises(TypeError, cp.int_feature, data)


    def test_float_feature_success(self):
        data = [0.1 * i for i in range(10)]

        feature = cp.float_feature(data).float_list.value

        for i in range(len(data)):
            self.assertAlmostEqual(feature[i], data[i])


    def test_float_feature_failure(self):
        data = [str(i) for i in range(10)]
        self.assertRaises(TypeError, cp.float_feature, data)


    def test_bytes_feature_success(self):
        data = [bytes(str(i), "utf-8") for i in range(10)]
        self.assertEqual(cp.bytes_feature(data).bytes_list.value, data)


    def test_bytes_feature_failure(self):
        data = list(range(10))
        self.assertRaises(TypeError, cp.bytes_feature, data)


    def test_str_feature_success(self):
        data = [str(i) for i in range(10)]
        target = [bytes(str(i), "utf-8") for i in range(10)]
        self.assertEqual(cp.str_feature(data).bytes_list.value, target)


    def test_str_feature_failure(self):
        data = list(range(10))
        self.assertRaises(TypeError, cp.str_feature, data)


    def test_bool_feature_success(self):
        data = [i % 2 == 0 for i in range(10)]
        self.assertEqual(cp.bool_feature(data).int64_list.value, data)


    def test_bool_feature_failure(self):
        data = [str(i) + "foobar" for i in range(10)]
        self.assertRaises(ValueError, cp.bool_feature, data)




class TestFeatureLists(unittest.TestCase):
    def test_int_feature_list_success(self):
        data = [list(range(10)), list(range(0, 15)), list(range(10, 20))]

        feature = cp.int_feature_list(data).feature

        for i in range(len(data)):
            self.assertEqual(feature[i].int64_list.value, data[i])


    def test_int_feature_list_failure(self):
        data = [list(range(10)), [0.1 * i for i in range(10)], list(range(10, 20))]
        self.assertRaises(TypeError, cp.int_feature_list, data)


    def test_float_feature_list_success(self):
        data = [[0.01 * i for i in range(10)], [0.03 * i for i in range(0, 15)]]

        feature = cp.float_feature_list(data).feature

        for i in range(len(data)):
            for j in range(len(data[i])):
                self.assertAlmostEqual(feature[i].float_list.value[j], data[i][j])


    def test_float_feature_list_failure(self):
        data = [[7.2 * i for i in range(10)], [str(i) for i in range(10)]]
        self.assertRaises(TypeError, cp.float_feature_list, data)


    def test_bytes_feature_list_success(self):
        data = [
            [bytes(str(i), "utf-8") for i in range(10)],
            [bytes(str(i) + "foobar", "utf-8") for i in range(10, 13)]
        ]

        feature = cp.bytes_feature_list(data).feature

        for i in range(len(data)):
            self.assertEqual(feature[i].bytes_list.value, data[i])


    def test_bytes_feature_list_failure(self):
        data = [[7.2 * i for i in range(10)], list(range(10))]
        self.assertRaises(TypeError, cp.bytes_feature_list, data)


    def test_str_feature_list_success(self):
        data = [[str(i) for i in range(10)], [str(i) + "foobar" for i in range(10, 13)]]

        target = [
            [bytes(str(i), "utf-8") for i in range(10)],
            [bytes(str(i) + "foobar", "utf-8") for i in range(10, 13)]
        ]

        feature = cp.str_feature_list(data).feature

        for i in range(len(data)):
            self.assertEqual(feature[i].bytes_list.value, target[i])


    def test_str_feature_list_failure(self):
        data = [[7.2 * i for i in range(10)], list(range(10))]
        self.assertRaises(TypeError, cp.str_feature, data)


    def test_bool_feature_list_success(self):
        data = [[i % 2 == 0 for i in range(10)], [True, False, True, True]]

        feature = cp.bool_feature_list(data).feature

        for i in range(len(data)):
            self.assertEqual(feature[i].int64_list.value, data[i])


    def test_bool_feature_list_failure(self):
        data = [[str(i) + "foobar" for i in range(10)], [True, False, True, True]]
        self.assertRaises(ValueError, cp.bool_feature_list, data)




class TestSmallestIntType(unittest.TestCase):
    def test_smallest_int8_type(self):
        self.assertEqual(cp.smallest_int_type(2**7-1), tf.int8)


    def test_smallest_int16_type(self):
        self.assertEqual(cp.smallest_int_type(2**10 + 12), tf.int16)


    def test_smallest_int32_type(self):
        self.assertEqual(cp.smallest_int_type(2**30), tf.int32)


    def test_smallest_int64_type(self):
        self.assertEqual(cp.smallest_int_type(2**55), tf.int64)


    def test_smallest_negative_int8_type(self):
        self.assertEqual(cp.smallest_int_type(-1*(2**7-1)), tf.int8)


    def test_smallest_negative_int16_type(self):
        self.assertEqual(cp.smallest_int_type(-1*(2**10 + 12)), tf.int16)


    def test_smallest_negative_int32_type(self):
        self.assertEqual(cp.smallest_int_type(-1*(2**30)), tf.int32)


    def test_smallest_negative_int64_type(self):
        self.assertEqual(cp.smallest_int_type(-1*(2**55)), tf.int64)


    def test_smallest_int8_type(self):
        self.assertEqual(cp.smallest_int_type(2**7-1), tf.int8)


    def test_smallest_int16_type(self):
        self.assertEqual(cp.smallest_int_type(2**10 + 12), tf.int16)


    def test_smallest_int32_type(self):
        self.assertEqual(cp.smallest_int_type(2**30), tf.int32)


    def test_smallest_int64_type(self):
        self.assertEqual(cp.smallest_int_type(2**55), tf.int64)


    def test_smallest_int_type_allow_none(self):
        self.assertEqual(cp.smallest_int_type(None), tf.int64)


    def test_smallest_uint_type_disallow_none(self):
        self.assertRaises(TypeError, cp.smallest_int_type, None, True, False)


    def test_smallest_uint8_type_allowed(self):
        self.assertEqual(cp.smallest_int_type(2**7), tf.uint8)


    def test_smallest_uint8_type_disallowed(self):
        self.assertEqual(cp.smallest_int_type(2**7, False), tf.int16)




class TestWindowIterList(unittest.TestCase):
    def convert(self, input_list):
        return input_list


    def test_aligned(self):
        array_like = self.convert(list(range(12)))

        w_iter =  cp.window_iter(array_like, 6, 3)

        w, m = next(w_iter)
        self.assertTrue(np.all(w == self.convert(list(range(6)))))
        self.assertTrue(np.all(m == self.convert([True] * 6)))

        w, m = next(w_iter)
        self.assertTrue(np.all(w == self.convert(list(range(3, 9)))))
        self.assertTrue(np.all(m == self.convert([False] * 3 + [True] * 3)))

        w, m = next(w_iter)
        self.assertTrue(np.all(w == self.convert(list(range(6, 12)))))
        self.assertTrue(np.all(m == self.convert([False] * 3 + [True] * 3)))


    def test_misaligned(self):
        array_like = self.convert(list(range(13)))

        w_iter =  cp.window_iter(array_like, 5, 3)

        w, m = next(w_iter)
        self.assertTrue(np.all(w == self.convert(list(range(5)))))
        self.assertTrue(np.all(m == self.convert([True] * 5)))

        w, m = next(w_iter)
        self.assertTrue(np.all(w == self.convert(list(range(3, 8)))))
        self.assertTrue(np.all(m == self.convert([False] * 2 + [True] * 3)))

        w, m = next(w_iter)
        self.assertTrue(np.all(w == self.convert(list(range(6, 11)))))
        self.assertTrue(np.all(m == self.convert([False] * 2 + [True] * 3)))

        w, m = next(w_iter)
        self.assertTrue(np.all(w == self.convert(list(range(9, 13)))))
        self.assertTrue(np.all(m == self.convert([False] * 2 + [True] * 2)))


    def test_empty(self):
        array_like = self.convert([])

        w_iter = cp.window_iter(array_like, 4, 2)

        self.assertRaises(StopIteration, next, w_iter)




class TestWindowIterIntArray(TestWindowIterList):
    def convert(self, input_list):
        if input_list and isinstance(input_list[0], bool):
            return np.array(input_list, dtype=np.bool)

        return np.array(input_list, dtype=np.int32)




class TestWindowIterFloatArray(TestWindowIterList):
    def convert(self, input_list):
        if input_list and isinstance(input_list[0], bool):
            return np.array(input_list, dtype=np.bool)

        return np.array(input_list, dtype=np.float32)





class TestWindowIterIntTensor(TestWindowIterList):
    def convert(self, input_list):
        if input_list and isinstance(input_list[0], bool):
            return tf.constant(input_list, dtype=np.bool)

        return tf.constant(input_list, dtype=np.int32)




class TestWindowIterFloatTensor(TestWindowIterList):
    def convert(self, input_list):
        if input_list and isinstance(input_list[0], bool):
            return tf.constant(input_list, dtype=np.bool)

        return tf.constant(input_list, dtype=np.float32)




class TestShiftLabels(unittest.TestCase):
    def test_add_shifted_labels_1D(self):
        inputs = tf.constant([1, 2, 3, 4, 5, 6, 7])
        output = cp.add_shifted_labels(inputs)

        self.assertTrue(all(output[0].numpy() == [1, 2, 3, 4, 5, 6]))
        self.assertTrue(all(output[1].numpy() == [2, 3, 4, 5, 6, 7]))


    def test_add_shifted_labels_1D_offset(self):
        inputs = tf.constant([1, 2, 3, 4, 5, 6, 7])
        output = cp.add_shifted_labels(inputs, offset=3)

        self.assertTrue(all(output[0].numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(output[1].numpy() == [4, 5, 6, 7]))


    def test_add_shifted_labels_1D_multi(self):
        inputs = (
            tf.constant([1, 2, 3, 4, 5, 6, 7]),
            tf.constant([11, 22, 33, 44, 55, 66, 77])
        )

        output = cp.add_shifted_labels(*inputs)

        self.assertTrue(all(output[0][0].numpy() == [1, 2, 3, 4, 5, 6]))
        self.assertTrue(all(output[0][1].numpy() == [11, 22, 33, 44, 55, 66]))
        self.assertTrue(all(output[1][0].numpy() == [2, 3, 4, 5, 6, 7]))
        self.assertTrue(all(output[1][1].numpy() == [22, 33, 44, 55, 66, 77]))


    def test_add_shifted_labels_2D(self):
        inputs = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [11, 12, 13, 14, 15, 16, 17]
        ])

        output = cp.add_shifted_labels(inputs)

        self.assertTrue(all(output[0][0,:].numpy() == [1, 2, 3, 4, 5, 6]))
        self.assertTrue(all(output[0][1,:].numpy() == [11, 12, 13, 14, 15, 16]))
        self.assertTrue(all(output[1][0,:].numpy() == [2, 3, 4, 5, 6, 7]))
        self.assertTrue(all(output[1][1,:].numpy() == [12, 13, 14, 15, 16, 17]))


    def test_add_shifted_labels_2D_offset(self):
        inputs = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [11, 12, 13, 14, 15, 16, 17]
        ])

        output = cp.add_shifted_labels(inputs, offset=2)

        self.assertTrue(all(output[0][0,:].numpy() == [1, 2, 3, 4, 5]))
        self.assertTrue(all(output[0][1,:].numpy() == [11, 12, 13, 14, 15]))
        self.assertTrue(all(output[1][0,:].numpy() == [3, 4, 5, 6, 7]))
        self.assertTrue(all(output[1][1,:].numpy() == [13, 14, 15, 16, 17]))


    def test_add_shifted_labels_2D_multi(self):
        inputs = (
            tf.constant([
                [1, 2, 3, 4, 5, 6, 7],
                [11, 22, 33, 44, 55, 66, 77],
            ]),
            tf.constant([
                [10, 20, 30, 40, 50, 60, 70],
                [110, 220, 330, 440, 550, 660, 770],
            ])
        )

        output = cp.add_shifted_labels(*inputs)

        self.assertTrue(all(output[0][0][0,:].numpy() == [1, 2, 3, 4, 5, 6]))
        self.assertTrue(all(output[0][0][1,:].numpy() == [11, 22, 33, 44, 55, 66]))
        self.assertTrue(all(output[0][1][0,:].numpy() == [10, 20, 30, 40, 50, 60]))
        self.assertTrue(all(output[0][1][1,:].numpy() == [110, 220, 330, 440, 550, 660]))

        self.assertTrue(all(output[1][0][0,:].numpy() == [2, 3, 4, 5, 6, 7]))
        self.assertTrue(all(output[1][0][1,:].numpy() == [22, 33, 44, 55, 66, 77]))
        self.assertTrue(all(output[1][1][0,:].numpy() == [20, 30, 40, 50, 60, 70]))
        self.assertTrue(all(output[1][1][1,:].numpy() == [220, 330, 440, 550, 660, 770]))


    def test_add_shifted_labels_1D_dataset(self):
        inputs = tf.constant([1, 2, 3, 4, 5, 6, 7])

        dataset = tf.data.Dataset.from_tensors(inputs)
        dataset = dataset.map(cp.add_shifted_labels)
        output = next(iter(dataset))

        self.assertTrue(all(output[0].numpy() == [1, 2, 3, 4, 5, 6]))
        self.assertTrue(all(output[1].numpy() == [2, 3, 4, 5, 6, 7]))


    def test_add_shifted_labels_1D_offset_dataset(self):
        inputs = tf.constant([1, 2, 3, 4, 5, 6, 7])

        dataset = tf.data.Dataset.from_tensors(inputs)
        dataset = dataset.map(lambda x: cp.add_shifted_labels(x, offset=3))
        output = next(iter(dataset))

        self.assertTrue(all(output[0].numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(output[1].numpy() == [4, 5, 6, 7]))


    def test_add_shifted_labels_1D_multi_dataset(self):
        inputs = (
            tf.constant([1, 2, 3, 4, 5, 6, 7]),
            tf.constant([11, 22, 33, 44, 55, 66, 77])
        )

        dataset = tf.data.Dataset.from_tensors(inputs)
        dataset = dataset.map(cp.add_shifted_labels)
        output = next(iter(dataset))

        self.assertTrue(all(output[0][0].numpy() == [1, 2, 3, 4, 5, 6]))
        self.assertTrue(all(output[0][1].numpy() == [11, 22, 33, 44, 55, 66]))
        self.assertTrue(all(output[1][0].numpy() == [2, 3, 4, 5, 6, 7]))
        self.assertTrue(all(output[1][1].numpy() == [22, 33, 44, 55, 66, 77]))


    def test_add_shifted_labels_2D_dataset(self):
        inputs = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [11, 12, 13, 14, 15, 16, 17]
        ])

        dataset = tf.data.Dataset.from_tensors(inputs)
        dataset = dataset.map(cp.add_shifted_labels)
        output = next(iter(dataset))

        self.assertTrue(all(output[0][0,:].numpy() == [1, 2, 3, 4, 5, 6]))
        self.assertTrue(all(output[0][1,:].numpy() == [11, 12, 13, 14, 15, 16]))
        self.assertTrue(all(output[1][0,:].numpy() == [2, 3, 4, 5, 6, 7]))
        self.assertTrue(all(output[1][1,:].numpy() == [12, 13, 14, 15, 16, 17]))


    def test_add_shifted_labels_2D_offset_dataset(self):
        inputs = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [11, 12, 13, 14, 15, 16, 17]
        ])

        dataset = tf.data.Dataset.from_tensors(inputs)
        dataset = dataset.map(lambda x: cp.add_shifted_labels(x, offset=2))
        output = next(iter(dataset))

        self.assertTrue(all(output[0][0,:].numpy() == [1, 2, 3, 4, 5]))
        self.assertTrue(all(output[0][1,:].numpy() == [11, 12, 13, 14, 15]))
        self.assertTrue(all(output[1][0,:].numpy() == [3, 4, 5, 6, 7]))
        self.assertTrue(all(output[1][1,:].numpy() == [13, 14, 15, 16, 17]))


    def test_add_shifted_labels_2D_multi_dataset(self):
        inputs = (
            tf.constant([
                [1, 2, 3, 4, 5, 6, 7],
                [11, 22, 33, 44, 55, 66, 77],
            ]),
            tf.constant([
                [10, 20, 30, 40, 50, 60, 70],
                [110, 220, 330, 440, 550, 660, 770],
            ])
        )

        dataset = tf.data.Dataset.from_tensors(inputs)
        dataset = dataset.map(cp.add_shifted_labels)
        output = next(iter(dataset))

        self.assertTrue(all(output[0][0][0,:].numpy() == [1, 2, 3, 4, 5, 6]))
        self.assertTrue(all(output[0][0][1,:].numpy() == [11, 22, 33, 44, 55, 66]))
        self.assertTrue(all(output[0][1][0,:].numpy() == [10, 20, 30, 40, 50, 60]))
        self.assertTrue(all(output[0][1][1,:].numpy() == [110, 220, 330, 440, 550, 660]))

        self.assertTrue(all(output[1][0][0,:].numpy() == [2, 3, 4, 5, 6, 7]))
        self.assertTrue(all(output[1][0][1,:].numpy() == [22, 33, 44, 55, 66, 77]))
        self.assertTrue(all(output[1][1][0,:].numpy() == [20, 30, 40, 50, 60, 70]))
        self.assertTrue(all(output[1][1][1,:].numpy() == [220, 330, 440, 550, 660, 770]))




class TestSlidingWindow(unittest.TestCase):
    def test_sliding_window_1D(self):
        inputs = tf.constant([1, 2, 3, 4, 5, 6, 7])

        output = cp.sliding_window(inputs, window_size=4, step_size=3)
        res_iter = iter(output)

        self.assertTrue(all(next(res_iter).numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(next(res_iter).numpy() == [4, 5, 6, 7]))
        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_1D_partial(self):
        inputs = tf.constant([1, 2, 3, 4, 5, 6, 7])

        output = cp.sliding_window(inputs, window_size=4, step_size=2)
        res_iter = iter(output)

        self.assertTrue(all(next(res_iter).numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(next(res_iter).numpy() == [3, 4, 5, 6]))
        self.assertTrue(all(next(res_iter).numpy() == [5, 6, 7, 0]))
        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_1D_short(self):
        inputs = tf.constant([1, 2, 3, 4, 5, 6, 7])

        output = cp.sliding_window(inputs, window_size=8, step_size=3)
        res_iter = iter(output)

        self.assertTrue(all(next(res_iter).numpy() == [1, 2, 3, 4, 5, 6, 7, 0]))
        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_1D_exact(self):
        inputs = tf.constant([1, 2, 3, 4])

        output = cp.sliding_window(inputs, window_size=4, step_size=10)
        res_iter = iter(output)

        self.assertTrue(all(next(res_iter).numpy() == [1, 2, 3, 4]))
        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_1D_multi(self):
        inputs = (
            tf.constant([1, 2, 3, 4, 5, 6, 7]),
            tf.constant([11, 22, 33, 44, 55, 66, 77])
        )

        output = cp.sliding_window(*inputs, window_size=4, step_size=2)
        res_iter = iter(output)

        res = next(res_iter)
        self.assertTrue(all(res[0].numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(res[1].numpy() == [11, 22, 33, 44]))

        res = next(res_iter)
        self.assertTrue(all(res[0].numpy() == [3, 4, 5, 6]))
        self.assertTrue(all(res[1].numpy() == [33, 44, 55, 66]))

        res = next(res_iter)
        self.assertTrue(all(res[0].numpy() == [5, 6, 7, 0]))
        self.assertTrue(all(res[1].numpy() == [55, 66, 77, 0]))

        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_1D_padding_value(self):
        inputs = tf.constant([1, 2, 3, 4, 5, 6, 7])

        output = cp.sliding_window(inputs, window_size=4, step_size=2, padding_value=12)
        res_iter = iter(output)

        self.assertTrue(all(next(res_iter).numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(next(res_iter).numpy() == [3, 4, 5, 6]))
        self.assertTrue(all(next(res_iter).numpy() == [5, 6, 7, 12]))
        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_2D(self):
        inputs = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [11, 12, 13, 14, 15, 16, 17]
        ])

        output = cp.sliding_window(inputs, window_size=4, step_size=3)
        res_iter = iter(output)

        res = next(res_iter)
        self.assertTrue(all(res[0, ...].numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(res[1, ...].numpy() == [11, 12, 13, 14]))

        res = next(res_iter)
        self.assertTrue(all(res[0, ...].numpy() == [4, 5, 6, 7]))
        self.assertTrue(all(res[1, ...].numpy() == [14, 15, 16, 17]))

        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_2D_partial(self):
        inputs = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [11, 12, 13, 14, 15, 16, 17]
        ])

        output = cp.sliding_window(inputs, window_size=4, step_size=2)
        res_iter = iter(output)

        res = next(res_iter)
        self.assertTrue(all(res[0, ...].numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(res[1, ...].numpy() == [11, 12, 13, 14]))

        res = next(res_iter)
        self.assertTrue(all(res[0, ...].numpy() == [3, 4, 5, 6]))
        self.assertTrue(all(res[1, ...].numpy() == [13, 14, 15, 16]))

        res = next(res_iter)
        self.assertTrue(all(res[0, ...].numpy() == [5, 6, 7, 0]))
        self.assertTrue(all(res[1, ...].numpy() == [15, 16, 17, 0]))

        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_2D_short(self):
        inputs = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [11, 12, 13, 14, 15, 16, 17]
        ])

        output = cp.sliding_window(inputs, window_size=8, step_size=3)
        res_iter = iter(output)

        res = next(res_iter)
        self.assertTrue(all(res[0, ...].numpy() == [1, 2, 3, 4, 5, 6, 7, 0]))
        self.assertTrue(all(res[1, ...].numpy() == [11, 12, 13, 14, 15, 16, 17, 0]))

        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_2D_exact(self):
        inputs = tf.constant([
            [1, 2, 3, 4],
            [11, 22, 33, 44]
        ])

        output = cp.sliding_window(inputs, window_size=4, step_size=10)
        res_iter = iter(output)

        res = next(res_iter)
        self.assertTrue(all(res[0, ...].numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(res[1, ...].numpy() == [11, 22, 33, 44]))

        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_2D_multi(self):
        inputs = (
            tf.constant([
                [1, 2, 3, 4, 5, 6, 7],
                [11, 12, 13, 14, 15, 16, 17]
            ]),
            tf.constant([
                [11, 22, 33, 44, 55, 66, 77],
                [110, 220, 330, 440, 550, 660, 770]
            ])
        )

        output = cp.sliding_window(*inputs, window_size=4, step_size=2)
        res_iter = iter(output)

        res = next(res_iter)
        self.assertTrue(all(res[0][0, ...].numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(res[0][1, ...].numpy() == [11, 12, 13, 14]))
        self.assertTrue(all(res[1][0, ...].numpy() == [11, 22, 33, 44]))
        self.assertTrue(all(res[1][1, ...].numpy() == [110, 220, 330, 440]))

        res = next(res_iter)
        self.assertTrue(all(res[0][0, ...].numpy() == [3, 4, 5, 6]))
        self.assertTrue(all(res[0][1, ...].numpy() == [13, 14, 15, 16]))
        self.assertTrue(all(res[1][0, ...].numpy() == [33, 44, 55, 66]))
        self.assertTrue(all(res[1][1, ...].numpy() == [330, 440, 550, 660]))

        res = next(res_iter)
        self.assertTrue(all(res[0][0, ...].numpy() == [5, 6, 7, 0]))
        self.assertTrue(all(res[0][1, ...].numpy() == [15, 16, 17, 0]))
        self.assertTrue(all(res[1][0, ...].numpy() == [55, 66, 77, 0]))
        self.assertTrue(all(res[1][1, ...].numpy() == [550, 660, 770, 0]))

        self.assertRaises(StopIteration, next, res_iter)


    def test_sliding_window_2D_padding_value(self):
        inputs = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [11, 12, 13, 14, 15, 16, 17]
        ])

        output = cp.sliding_window(inputs, window_size=4, step_size=2, padding_value=1000)
        res_iter = iter(output)

        res = next(res_iter)
        self.assertTrue(all(res[0, ...].numpy() == [1, 2, 3, 4]))
        self.assertTrue(all(res[1, ...].numpy() == [11, 12, 13, 14]))

        res = next(res_iter)
        self.assertTrue(all(res[0, ...].numpy() == [3, 4, 5, 6]))
        self.assertTrue(all(res[1, ...].numpy() == [13, 14, 15, 16]))

        res = next(res_iter)
        self.assertTrue(all(res[0, ...].numpy() == [5, 6, 7, 1000]))
        self.assertTrue(all(res[1, ...].numpy() == [15, 16, 17, 1000]))

        self.assertRaises(StopIteration, next, res_iter)




if __name__ == "__main__":
    unittest.main(verbosity="b")
