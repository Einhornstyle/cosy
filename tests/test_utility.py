# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import unittest
import cosy.utils as cu




class TestFunctionInspect(unittest.TestCase):
    def test_arg_names(self):
        def my_func(x, y, z=2):
            return x + y + z

        self.assertEqual(cu.arg_names(my_func), ["x", "y", "z"])


    def test_arg_names_no_args(self):
        def my_func():
            return None

        self.assertEqual(cu.arg_names(my_func), [])


    def test_arg_names_lambda(self):
        self.assertEqual(cu.arg_names(lambda x, y: x + y), ["x", "y"])


    def test_has_arg(self):
        def my_func(x, y, z=2):
            return x + y + z

        self.assertTrue(cu.has_arg(my_func, "x"))
        self.assertTrue(cu.has_arg(my_func, "y"))
        self.assertTrue(cu.has_arg(my_func, "z"))


    def test_has_arg_no_args(self):
        def my_func():
            return None

        self.assertFalse(cu.has_arg(my_func, "x"))


    def test_has_arg_lambda(self):
        self.assertTrue(cu.has_arg(lambda x, y: x + y, "x"))
        self.assertTrue(cu.has_arg(lambda x, y: x + y, "y"))


    def test_applicable_kwargs(self):
        def my_func(x, y, z=2):
            return x + y + z

        self.assertEqual(cu.applicable_kwargs(my_func, x=3), {"x": 3})
        self.assertEqual(cu.applicable_kwargs(my_func, x=3, z=5), {"x": 3, "z": 5})
        self.assertEqual(cu.applicable_kwargs(my_func, x=3, y=7), {"x": 3, "y": 7})


    def test_applicable_kwargs_lambda(self):
        self.assertEqual(cu.applicable_kwargs(lambda x, y, z: x+y+z, x=3), {"x": 3})
        self.assertEqual(cu.applicable_kwargs(lambda x, y, z: x+y+z, x=3, z=5), {"x": 3, "z": 5})
        self.assertEqual(cu.applicable_kwargs(lambda x, y, z: x+y+z, x=3, y=7), {"x": 3, "y": 7})




if __name__ == "__main__":
    unittest.main(verbosity=2)

