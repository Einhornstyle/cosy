#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import os
import numpy as np
import tensorflow as tf
import cosy.preprocessing as cp
import cosy.data as cds




class SingleRecordWriter(cds.RecordWriter):
    def make_record(self, alpha):
        features = tf.train.Features(feature={"alpha": cp.int_feature(alpha)})
        return tf.train.Example(features=features)


class SingleRecordReader(cds.RecordReader):
    def preprocess(self, dataset, num_threads):
        return dataset.map(self._parse, num_threads)


    def _parse(self, serialized):
        feature = tf.io.parse_single_example(
            serialized,
            features={"alpha": tf.io.FixedLenFeature([], tf.int64)}
        )

        return feature["alpha"]


class DoubleRecordWriter(cds.RecordWriter):
    def make_record(self, alpha, beta):
        features = tf.train.Features(feature={
            "alpha": cp.int_feature(alpha),
            "beta": cp.str_feature(beta)
        })

        return tf.train.Example(features=features)


class DoubleRecordReader(cds.RecordReader):
    def preprocess(self, dataset, num_threads):
        return dataset.map(self._parse, num_threads)

    def _parse(self, serialized):
        feature = tf.io.parse_single_example(
            serialized,
            features={
                "alpha": tf.io.FixedLenFeature([], tf.int64),
                "beta": tf.io.FixedLenFeature([], tf.string)
            }
        )

        return feature["alpha"], feature["beta"]


class SequenceRecordWriter(cds.RecordWriter):
    def make_record(self, sequence, beta):
        features = tf.train.Features(feature={"beta": cp.str_feature(beta)})

        feature_lists = tf.train.FeatureLists(
            feature_list={"sequence": cp.float_feature_list(sequence)}
        )

        return tf.train.SequenceExample(context=features, feature_lists=feature_lists)


class SequenceRecordReader(cds.RecordReader):
    def preprocess(self, dataset, num_threads):
        return dataset.map(self._parse, num_threads)


    def _parse(self, serialized):
        context, sequence = tf.io.parse_single_sequence_example(
            serialized,
            context_features={"beta": tf.io.FixedLenFeature([], tf.string)},
            sequence_features={"sequence": tf.io.RaggedFeature(tf.float32)}
        )

        return context["beta"], sequence["sequence"]




class TestRecordWriter(unittest.TestCase):
    def setUp(self):
        self.filename = os.path.join("tests", "data", "writer_file.tfrecord")


    def test_open_no_compression(self):
        try:
            writer = SingleRecordWriter(self.filename)

            self.assertIsInstance(writer._writer, tf.io.TFRecordWriter)

            writer.close()
        finally:
            os.remove(self.filename)


    def test_open_with_compression(self):
        try:
            writer = SingleRecordWriter(self.filename, "GZIP")

            self.assertIsInstance(writer._writer, tf.io.TFRecordWriter)

            writer.close()
        finally:
            os.remove(self.filename)


    def test_set_filename(self):
        try:
            writer = SingleRecordWriter()
            self.assertEqual(writer._writer, None)

            writer.filename = self.filename
            self.assertIsInstance(writer._writer, tf.io.TFRecordWriter)

            writer.close()
        finally:
            os.remove(self.filename)


    def test_set_filename_to_none(self):
        try:
            writer = SingleRecordWriter(self.filename)
            self.assertIsInstance(writer._writer, tf.io.TFRecordWriter)

            writer.filename = None
            self.assertEqual(writer._writer, None)

            writer.close()
        finally:
            os.remove(self.filename)


    def test_set_compression(self):
        try:
            writer = SingleRecordWriter(self.filename)
            self.assertIsInstance(writer._writer, tf.io.TFRecordWriter)

            writer.compression = "GZIP"
            self.assertIsInstance(writer._writer, tf.io.TFRecordWriter)

            writer.close()
        finally:
            os.remove(self.filename)


    def test_set_compression_to_none(self):
        try:
            writer = SingleRecordWriter(self.filename, "GZIP")
            self.assertIsInstance(writer._writer, tf.io.TFRecordWriter)

            writer.compression = None
            self.assertIsInstance(writer._writer, tf.io.TFRecordWriter)

            writer.close()
        finally:
            os.remove(self.filename)


    def test_set_compression_no_filename(self):
        writer = SingleRecordWriter()
        self.assertEqual(writer._writer, None)

        writer.compression = "GZIP"
        self.assertEqual(writer._writer, None)

        writer.close()


    def test_set_compression_to_none_no_filename(self):
        writer = SingleRecordWriter(compression="GZIP")
        self.assertEqual(writer._writer, None)

        writer.compression = None
        self.assertEqual(writer._writer, None)

        writer.close()


    def test_close(self):
        try:
            writer = SingleRecordWriter(self.filename)
            self.assertNotEqual(writer._writer, None)

            writer.close()
            self.assertEqual(writer._writer, None)
        finally:
            os.remove(self.filename)


    def test_close_twice(self):
        try:
            writer = SingleRecordWriter(self.filename)
            self.assertNotEqual(writer._writer, None)

            writer.close()
            self.assertEqual(writer._writer, None)

            writer.close()
            self.assertEqual(writer._writer, None)
        finally:
            os.remove(self.filename)


    def test_flush_success(self):
        try:
            writer = SingleRecordWriter(self.filename)
            writer.flush()
            writer.close()
        finally:
            os.remove(self.filename)


    def test_flush_failure(self):
        try:
            writer = SingleRecordWriter(self.filename)
            writer.close()
            self.assertRaises(ValueError, writer.flush)
        finally:
            os.remove(self.filename)


    def test_with_block(self):
        try:
            with SingleRecordWriter(self.filename) as writer:
                writer.flush()
        finally:
            os.remove(self.filename)


    def disabled_test_broadcast(self):
        data = {
            "alpha": [42],
            "beta": ["foobar"],
            "sequence": [[1.2, 1.3, 1], [], [7.2, 5.1]]
        }

        filenames = [
                os.path.join("tests", "data", "single_writer.tfrecord"),
                os.path.join("tests", "data", "double_writer.tfrecord"),
                os.path.join("tests", "data", "sequence_writer.tfrecord")
        ]

        try:
            writers = [
                SingleRecordWriter(filenames[0]),
                DoubleRecordWriter(filenames[1]),
                SequenceRecordWriter(filenames[2])
            ]

            cds.broadcast(writers, **data)

            single = next(iter(SingleRecordReader()(filenames[0])))
            self.assertEqual(single.numpy(), 42)

            double = next(iter(DoubleRecordReader()(filenames[1])))
            self.assertEqual(double[0].numpy(), 42)
            self.assertEqual(double[1].numpy(), b"foobar")

            sequence = next(iter(SequenceRecordReader()(filenames[2])))
            self.assertEqual(sequence[0].numpy(), b"foobar")

            seq_list = sequence[1].to_list()
            for idx in range(3):
                np.testing.assert_almost_equal(seq_list[idx], data["sequence"][idx], 1)
        finally:
            for filename in filenames:
                os.remove(filename)




class TestRecordReader(unittest.TestCase):
    def setUp(self):
        self.filename = os.path.join(
            "tests", "data", "tiny_with_docs", "eval", "eval_10_20_0.tfrecord"
        )


    def test_default_load(self):
        reader = cds.RecordReader()
        dataset = reader([self.filename])


    def test_shuffle_load(self):
        reader = cds.RecordReader(shuffle_buffer_size=32)
        dataset = reader([self.filename])


    def test_prefetch_load(self):
        reader = cds.RecordReader(prefetch_buffer_size=16)
        dataset = reader([self.filename])


    def test_shuffle_and_prefetch_load(self):
        reader = cds.RecordReader(32, 16)
        dataset = reader([self.filename])




if __name__ == "__main__":
    unittest.main(verbosity=2)
