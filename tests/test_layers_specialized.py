#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import numpy as np
import tensorflow as tf
from cosy import layers

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)




class TestKarel(unittest.TestCase):
    def setUp(self):
        self.dataset = np.load("tests/data/tensor/karel_train.npz", allow_pickle=True)
        self.inputs = self.dataset["inputs"].astype(np.float32)
        self.outputs = self.dataset["outputs"].astype(np.float32)
        self.programs = self.dataset["codes"]
        self.program_lengths = self.dataset["code_lengths"]


    def test_residual_block_forward_pass(self):
        inputs = self.inputs.reshape([-1] + list(self.inputs.shape)[2:])
        outputs = self.outputs.reshape([-1] + list(self.outputs.shape)[2:])
        io = np.concatenate([inputs, outputs], axis=-1)

        # In pratice we feed the I/O pairs through a Conv2D layer before feeding them into
        # the res block. This duplication yields the same shapes as the convolution would.
        io = np.concatenate([io, io], axis=-1)
        res_block = layers.KarelResidualBlock()

        self.assertEqual(res_block(io).shape, list(io.shape[0:3]) + [64])


    def test_encoder_forward_pass(self):
        N = self.inputs.shape[0] * self.inputs.shape[1]
        encoder = layers.KarelEncoder()
        self.assertEqual(encoder((self.inputs, self.outputs)).shape, [N, 512])


    def test_decoder_forward_pass(self):
        N = 10 * self.inputs.shape[1]
        encoder = layers.KarelEncoder()
        encoding =  encoder((self.inputs[:10], self.outputs[:10]))

        codes = np.zeros((10, np.max(self.program_lengths)))
        for i, program in enumerate(self.programs[:10]):
            for j, token in enumerate(program):
                codes[i, j] = token

        decoder = layers.KarelDecoder(52, self.inputs.shape[1])
        self.assertEqual(decoder((encoding, codes)).shape, [10, 52])


    def test_decoder_explicit_masking(self):
        N = 10 * self.inputs.shape[1]
        encoder = layers.KarelEncoder()
        encoding =  encoder((self.inputs[:10], self.outputs[:10]))

        codes = np.zeros((10, np.max(self.program_lengths)))
        for i, program in enumerate(self.programs[:10]):
            for j, token in enumerate(program):
                codes[i, j] = token

        mask = tf.sequence_mask(self.program_lengths[:10], maxlen=28)

        decoder = layers.KarelDecoder(52, self.inputs.shape[1])
        self.assertEqual(decoder((encoding, codes), mask).shape, [10, 52])




if __name__ == "__main__":
    unittest.main(verbosity=2)
