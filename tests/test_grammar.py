# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import unittest
import json
import numpy as np
import igraph as ig

import tensorflow as tf

from cosy.tree import TreeAttributes, add_child_index
from cosy.grammar import NumericBNF, BNF
from cosy.preprocessing import DynamicSymbolEncoder




class TestNumericBNF(unittest.TestCase):
    def setUp(self):
        self.test_grammar = {
            1: [[1, 2, 1], [4, 2]],
            2: [[], [3]]
        }


    def test_default_init(self):
        numeric_bnf = NumericBNF()
        self.assertEqual(numeric_bnf._start_symbol, None)
        self.assertEqual(numeric_bnf._grammar, {})


    def test_init(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertEqual(numeric_bnf._start_symbol, 1)
        self.assertEqual(numeric_bnf._grammar, self.test_grammar)


    def test_compute_terminal_symbols(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertEqual(numeric_bnf._cached_terminal_symbols, None)
        numeric_bnf._compute_terminal_symbols()
        self.assertEqual(numeric_bnf._cached_terminal_symbols, {3, 4})


    def test_terminal_symbols(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertEqual(numeric_bnf._cached_terminal_symbols, None)
        self.assertEqual(numeric_bnf.terminal_symbols, {3, 4})
        self.assertEqual(numeric_bnf._cached_terminal_symbols, {3, 4})


    def test_nonterminal_symbols(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertEqual(numeric_bnf.nonterminal_symbols, {1, 2})


    def test_set_start_sybmol(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertEqual(numeric_bnf._start_symbol, 1)
        numeric_bnf.start_symbol = 2
        self.assertEqual(numeric_bnf._start_symbol, 2)


    def test_raise_set_start_sybmol(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)

        def set_to_3(numeric_bnf):
            numeric_bnf.start_symbol = 3

        self.assertRaises(ValueError, set_to_3, numeric_bnf)


    def test_iter(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        nonterminals = set(iter(numeric_bnf))
        self.assertEqual(nonterminals, {1, 2})


    def test_getitem(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        for nonterminal in self.test_grammar:
            self.assertEqual(numeric_bnf[nonterminal], self.test_grammar[nonterminal])


    def test_getitem_missing(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertRaises(KeyError, numeric_bnf.__getitem__, 7)


    def test_add(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)

        result_grammar = {
            1: [[1, 2, 1], [4, 2]],
            2: [[], [3]],
            7: [[3, 4, 1], [], [2,32, 12]]
        }

        self.assertEqual(numeric_bnf.grammar, self.test_grammar)
        numeric_bnf.add(7, result_grammar[7])
        self.assertEqual(numeric_bnf.grammar, result_grammar)


    def test_remove(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)

        result_grammar = {
            1: [[4, 2]],
            2: [[], [3]],
        }

        self.assertEqual(numeric_bnf.grammar, self.test_grammar)
        numeric_bnf.remove(1, [[1, 2, 1]])
        self.assertEqual(numeric_bnf.grammar, result_grammar)


    def test_remove_entire_rhs(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)

        result_grammar = {
            2: [[], [3]]
        }

        self.assertEqual(numeric_bnf.grammar, self.test_grammar)
        numeric_bnf.remove(1, [[1, 2, 1], [4, 2]])
        self.assertEqual(numeric_bnf.grammar, result_grammar)


    def test_remove_no_rhs(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertEqual(numeric_bnf.grammar, self.test_grammar)
        numeric_bnf.remove(1, [])
        self.assertEqual(numeric_bnf.grammar, self.test_grammar)


    def test_raises_remove_lhs(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertRaises(KeyError, numeric_bnf.remove, 7, [[1, 2, 1]])


    def test_raises_remove_rhs(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertRaises(ValueError, numeric_bnf.remove, 1, [[1, 2]])


    def test_pop(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)

        result_grammar = {
            1: [[4, 2]],
            2: [[], [3]],
        }

        self.assertEqual(numeric_bnf.grammar, self.test_grammar)
        lhs, rhs = numeric_bnf.pop(1, [[1, 2, 1]])
        self.assertEqual(numeric_bnf.grammar, result_grammar)

        self.assertEqual(lhs, None)
        self.assertEqual(rhs, [[1, 2, 1]])


    def test_pop_entire_rhs(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)

        result_grammar = {
            2: [[], [3]]
        }

        self.assertEqual(numeric_bnf.grammar, self.test_grammar)
        lhs, rhs = numeric_bnf.pop(1, [[1, 2, 1], [4, 2]])
        self.assertEqual(numeric_bnf.grammar, result_grammar)

        self.assertEqual(lhs, 1)
        self.assertEqual(rhs, [[1, 2, 1], [4, 2]])


    def test_pop_no_rhs(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)

        self.assertEqual(numeric_bnf.grammar, self.test_grammar)
        lhs, rhs = numeric_bnf.pop(1, [])
        self.assertEqual(numeric_bnf.grammar, self.test_grammar)

        self.assertEqual(lhs, None)
        self.assertEqual(rhs, [])


    def test_raises_pop_lhs(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertRaises(KeyError, numeric_bnf.pop, 7, [[1, 2, 1]])


    def test_raises_pop_rhs(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertRaises(ValueError, numeric_bnf.pop, 1, [[1, 2]])


    def test_get_config(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        config = numeric_bnf.get_config()
        self.assertEqual(config["grammar"], numeric_bnf.grammar)
        self.assertEqual(config["start_symbol"], numeric_bnf.start_symbol)


    def test_save_and_load(self):
        save_numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        load_numeric_bnf = NumericBNF()

        path = "tests/data/grammar/tmp_save_numeric.json"

        try:
            save_numeric_bnf.save(path)
            load_numeric_bnf = load_numeric_bnf.load(path)
        finally:
            os.remove(path)

        self.assertEqual(save_numeric_bnf._start_symbol, load_numeric_bnf._start_symbol)
        self.assertEqual(save_numeric_bnf._grammar, load_numeric_bnf._grammar)


    def test_save_and_load_cls(self):
        save_numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)

        path = "tests/data/grammar/tmp_save_numeric.json"

        try:
            save_numeric_bnf.save(path)
            load_numeric_bnf = NumericBNF.load(path)
        finally:
            os.remove(path)

        self.assertEqual(save_numeric_bnf._start_symbol, load_numeric_bnf._start_symbol)
        self.assertEqual(save_numeric_bnf._grammar, load_numeric_bnf._grammar)


    def test_raises_load(self):
        numeric_bnf = NumericBNF()

        path = "tests/data/grammar/tmp_save_numeric_invalid.json"
        data = {"start_symbol": 7, "grammar": self.test_grammar}

        try:
            with open(path, "w") as save_file:
                json.dump(data, save_file)

            self.assertRaises(ValueError, numeric_bnf.load, path)
        finally:
            os.remove(path)


    def test_make_sentence(self):
        numeric_bnf = NumericBNF(1, self.test_grammar)

        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), [1, 1, 2, 2, 4, 3, []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        sentence = np.array([4, 3], dtype=np.int32)

        self.assertTrue(np.all(numeric_bnf.make_sentence(tree) == sentence))


    def test_make_sentence_cls(self):
        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), [1, 1, 2, 2, 4, 3, []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        sentence = np.array([4, 3], dtype=np.int32)

        self.assertTrue(np.all(NumericBNF.make_sentence(tree) == sentence))


    def test_make_sentence_child_index(self):
        numeric_bnf = NumericBNF(1, self.test_grammar)

        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), [1, 1, 2, 2, 4, 3, []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        add_child_index(tree)
        tree.es[0][TreeAttributes.CHILD_INDEX] = 1
        tree.es[1][TreeAttributes.CHILD_INDEX] = 0

        sentence = np.array([3, 4], dtype=np.int32)

        self.assertTrue(np.all(numeric_bnf.make_sentence(tree) == sentence))


    def test_make_record(self):
        numeric_bnf = NumericBNF(1, self.test_grammar)

        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), [1, 1, 2, 2, 4, 3, []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        sentence = np.array([4, 3], dtype=np.int32)

        content = tf.io.parse_single_example(
            numeric_bnf.make_record(tree),
            {"program": tf.io.RaggedFeature(tf.int64)}
        )

        self.assertTrue(np.all(content["program"].numpy() == sentence))


    def test_make_record_cls(self):
        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), [1, 1, 2, 2, 4, 3, []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        sentence = np.array([4, 3], dtype=np.int32)

        content = tf.io.parse_single_example(
            NumericBNF.make_record(tree),
            {"program": tf.io.RaggedFeature(tf.int64)}
        )

        self.assertTrue(np.all(content["program"].numpy() == sentence))


    def test_make_record_child_index(self):
        numeric_bnf = NumericBNF(1, self.test_grammar)

        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), [1, 1, 2, 2, 4, 3, []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        add_child_index(tree)
        tree.es[0][TreeAttributes.CHILD_INDEX] = 1
        tree.es[1][TreeAttributes.CHILD_INDEX] = 0

        sentence = np.array([3, 4], dtype=np.int32)

        content = tf.io.parse_single_example(
            numeric_bnf.make_record(tree),
            {"program": tf.io.RaggedFeature(tf.int64)}
        )

        self.assertTrue(np.all(content["program"].numpy() == sentence))


    def test_is_normalized(self):
        grammar = {
            4: [[1, 2], [4, 1]],
            5: [[], [1, 3], [3], [5]]
        }

        numeric_bnf = NumericBNF(start_symbol=4, grammar=grammar)
        self.assertTrue(numeric_bnf.is_normalized())


    def test_is_normalized_lhs_unordered(self):
        grammar = {
            5: [[3, 2], [4, 3]],
            4: [[], [3, 1], [1], [5]]
        }

        numeric_bnf = NumericBNF(start_symbol=4, grammar=grammar)
        self.assertTrue(numeric_bnf.is_normalized())


    def test_is_normalized_rhs_unordered(self):
        grammar = {
            4: [[3, 2], [4, 3]],
            5: [[], [3, 1], [1], [5]]
        }

        numeric_bnf = NumericBNF(start_symbol=4, grammar=grammar)
        self.assertTrue(numeric_bnf.is_normalized())


    def test_is_not_normalized(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertFalse(numeric_bnf.is_normalized())


    def test_normalize(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        normalized_grammar = {3: [[3, 4, 3], [2, 4]], 4: [[], [1]] }

        self.assertEqual(numeric_bnf.grammar, self.test_grammar)
        normalization_map = numeric_bnf.normalize()
        self.assertEqual(numeric_bnf.grammar, normalized_grammar)
        self.assertEqual(normalization_map, {1:3, 2:4, 3:1, 4:2})


    def test_normalize_and_eval(self):
        numeric_bnf = NumericBNF(start_symbol=1, grammar=self.test_grammar)
        self.assertFalse(numeric_bnf.is_normalized())
        numeric_bnf.normalize()
        self.assertTrue(numeric_bnf.is_normalized())




class TestBNF(unittest.TestCase):
    def setUp(self):
        self.symbol_grammar = {
            "a": [["a", "b", "a"], ["d", "b"]],
            "b": [[], ["c"]]
        }

        self.numeric_grammar = {
            1: [[1, 2, 1], [4, 2]],
            2: [[], [3]]
        }

        self.encoder = DynamicSymbolEncoder()
        self.encoder._str2int = {"a": 1, "b": 2, "c": 3, "d": 4}
        self.encoder._int2str = [None, "a", "b", "c", "d"]

        self.bnf = BNF()
        self.bnf._numeric_bnf = NumericBNF(1, self.numeric_grammar)
        self.bnf._symbol_encoder = self.encoder


    def test_compute_terminal_symbols(self):
        self.assertEqual(self.bnf._cached_terminal_symbols, None)
        self.bnf._compute_terminal_symbols()
        self.assertEqual(self.bnf._cached_terminal_symbols, {"c", "d"})


    def test_terminal_symbols(self):
        self.assertEqual(self.bnf._cached_terminal_symbols, None)
        self.assertEqual(self.bnf.terminal_symbols, {"c", "d"})
        self.assertEqual(self.bnf._cached_terminal_symbols, {"c", "d"})


    def test_compute_nonterminal_symbols(self):
        self.assertEqual(self.bnf._cached_nonterminal_symbols, None)
        self.bnf._compute_nonterminal_symbols()
        self.assertEqual(self.bnf._cached_nonterminal_symbols, {"a", "b"})


    def test_nonterminal_symbols(self):
        self.assertEqual(self.bnf._cached_nonterminal_symbols, None)
        self.assertEqual(self.bnf.nonterminal_symbols, {"a", "b"})


    def test_compute_grammar(self):
        self.assertEqual(self.bnf._cached_grammar, None)
        self.bnf._compute_grammar()
        self.assertEqual(self.bnf._cached_grammar, self.symbol_grammar)


    def test_grammar(self):
        self.assertEqual(self.bnf._cached_grammar, None)
        self.assertEqual(self.bnf.grammar, self.symbol_grammar)


    def test_set_start_sybmol(self):
        self.assertEqual(self.bnf.start_symbol, "a")
        self.bnf.start_symbol = "b"
        self.assertEqual(self.bnf.start_symbol, "b")


    def test_raise_set_start_sybmol(self):
        def set_to_c(bnf):
            bnf.start_symbol = "c"

        self.assertRaises(ValueError, set_to_c, self.bnf)


    def test_encode(self):
        lhs, rhs = "a", [["b", "a"], [], ["c"]]
        encoded_lhs, encoded_rhs = self.bnf._encode(lhs, rhs)
        self.assertEqual(encoded_lhs, 1)
        self.assertEqual(encoded_rhs, [[2, 1], [], [3]])


    def test_decode(self):
        lhs, rhs = 1, [[2, 1], [], [3]]
        decoded_lhs, decoded_rhs = self.bnf._decode(lhs, rhs)
        self.assertEqual(decoded_lhs, "a")
        self.assertEqual(decoded_rhs, [["b", "a"], [], ["c"]])


    def test_raises_decode_missing(self):
        lhs, rhs = 1, [[7, 1], [], [3]]
        self.assertRaises(IndexError, self.bnf._decode, lhs, rhs)


    def test_raises_decode_removed(self):
        lhs, rhs = 1, [[0, 1], [], [3]]
        self.assertRaises(ValueError, self.bnf._decode, lhs, rhs)


    def test_remove_unused_symbols(self):
        lhs, rhs = self.bnf._numeric_bnf.pop(1, [[4, 2]])
        self.assertEquals(self.bnf._symbol_encoder._str2int, self.encoder._str2int)
        self.assertEquals(self.bnf._symbol_encoder._int2str, self.encoder._int2str)

        self.bnf._remove_unused_symbols(lhs, rhs)
        self.assertEquals(self.bnf._symbol_encoder._str2int, {"a": 1, "b": 2, "c": 3})
        self.assertEquals(self.bnf._symbol_encoder._int2str, [None, "a", "b", "c", None])


    def test_iter(self):
        self.assertEqual(set(iter(self.bnf)), {"a", "b"})


    def test_getitem(self):
        for nonterminal in self.symbol_grammar:
            self.assertEqual(self.bnf[nonterminal], self.symbol_grammar[nonterminal])


    def test_getitem_missing(self):
        self.assertRaises(KeyError, self.bnf.__getitem__, "k")


    def test_add(self):
        result_grammar = {
            "a": [["a", "b", "a"], ["d", "b"]],
            "b": [[], ["c"]],
            "m": [["c", "d", "a"], [], ["b", "f", "k"]]
        }

        self.assertEqual(self.bnf.grammar, self.symbol_grammar)
        self.bnf.add("m", result_grammar["m"])
        self.assertEqual(self.bnf.grammar, result_grammar)


    def test_remove(self):
        result_grammar = {
            "a": [["d", "b"]],
            "b": [[], ["c"]],
        }

        self.assertEqual(self.bnf.grammar, self.symbol_grammar)
        self.bnf.remove("a", [["a", "b", "a"]])
        self.assertEqual(self.bnf.grammar, result_grammar)


    def test_remove_entire_rhs(self):
        result_grammar = {
            "b": [[], ["c"]]
        }

        self.assertEqual(self.bnf.grammar, self.symbol_grammar)
        self.bnf.remove("a", [["a", "b", "a"], ["d", "b"]])
        self.assertEqual(self.bnf.grammar, result_grammar)


    def test_remove_no_rhs(self):
        self.assertEqual(self.bnf.grammar, self.symbol_grammar)
        self.bnf.remove("a", [])
        self.assertEqual(self.bnf.grammar, self.symbol_grammar)


    def test_raises_remove_lhs(self):
        self.assertRaises(KeyError, self.bnf.remove, "m", [["a", "b", "a"]])


    def test_raises_remove_rhs(self):
        self.assertRaises(ValueError, self.bnf.remove, "a", [["a", "b"]])


    def test_pop(self):
        result_grammar = {
            "a": [["d", "b"]],
            "b": [[], ["c"]],
        }

        self.assertEqual(self.bnf.grammar, self.symbol_grammar)
        lhs, rhs = self.bnf.pop("a", [["a", "b", "a"]])
        self.assertEqual(self.bnf.grammar, result_grammar)

        self.assertEqual(lhs, None)
        self.assertEqual(rhs, [["a", "b", "a"]])


    def test_pop_entire_rhs(self):
        result_grammar = {
            "b": [[], ["c"]]
        }

        self.assertEqual(self.bnf.grammar, self.symbol_grammar)
        lhs, rhs = self.bnf.pop("a", [["a", "b", "a"], ["d", "b"]])
        self.assertEqual(self.bnf.grammar, result_grammar)

        self.assertEqual(lhs, "a")
        self.assertEqual(rhs, [["a", "b", "a"], ["d", "b"]])


    def test_pop_no_rhs(self):
        self.assertEqual(self.bnf.grammar, self.symbol_grammar)
        lhs, rhs = self.bnf.pop("a", [])
        self.assertEqual(self.bnf.grammar, self.symbol_grammar)

        self.assertEqual(lhs, None)
        self.assertEqual(rhs, [])


    def test_raises_pop_lhs(self):
        self.assertRaises(KeyError, self.bnf.pop, "m", [["a", "b", "a"]])


    def test_raises_pop_rhs(self):
        self.assertRaises(ValueError, self.bnf.pop, "a", [["a", "b"]])


    def test_normalize(self):
        self.assertFalse(self.bnf.is_normalized())
        self.bnf.normalize()
        self.assertTrue(self.bnf.is_normalized())
        self.assertEqual(self.bnf._symbol_encoder._str2int, {"a": 3, "b": 4, "c": 1, "d": 2})
        self.assertEqual(self.bnf._symbol_encoder._int2str, [None, "c", "d", "a", "b"])


    def test_get_config(self):
        config = self.bnf.get_config()
        self.assertEqual(config["grammar"], self.bnf.grammar)
        self.assertEqual(config["start_symbol"], self.bnf.start_symbol)
        self.assertEqual(config["encoding"], self.bnf._symbol_encoder._str2int)
        self.assertEqual(config["decoding"], self.bnf._symbol_encoder._int2str)


    def test_save_and_load(self):
        load_bnf = BNF()
        path = "tests/data/grammar/tmp_save_symbolic.json"

        self.bnf.save(path)
        load_bnf = load_bnf.load(path)
        os.remove(path)

        self.assertEqual(self.bnf.get_config(), load_bnf.get_config())


    def test_save_and_load_cls(self):
        path = "tests/data/grammar/tmp_save_symbolic.json"

        self.bnf.save(path)
        load_bnf = BNF.load(path)
        os.remove(path)

        self.assertEqual(self.bnf.get_config(), load_bnf.get_config())


    def test_save_and_load_without_encoding(self):
        load_bnf = BNF()
        path = "tests/data/grammar/tmp_save_symbolic.json"

        try:
            self.bnf.save(path)
            load_bnf = load_bnf.load(path, with_encoding=False)
        finally:
            os.remove(path)

        self.assertEqual(self.bnf.grammar, load_bnf.grammar)
        self.assertEqual(self.bnf.start_symbol, load_bnf.start_symbol)
        self.assertTrue(load_bnf.is_normalized())

        self.assertNotEqual(
            self.bnf._symbol_encoder.get_config(),
            load_bnf._symbol_encoder.get_config()
        )


    def test_save_and_load_without_encoding_cls(self):
        path = "tests/data/grammar/tmp_save_symbolic.json"

        try:
            self.bnf.save(path)
            load_bnf = BNF.load(path, with_encoding=False)
        finally:
            os.remove(path)

        self.assertEqual(self.bnf.grammar, load_bnf.grammar)
        self.assertEqual(self.bnf.start_symbol, load_bnf.start_symbol)
        self.assertTrue(load_bnf.is_normalized())

        self.assertNotEqual(
            self.bnf._symbol_encoder.get_config(),
            load_bnf._symbol_encoder.get_config()
        )


    def test_raises_load(self):
        bnf = BNF()

        path = "tests/data/grammar/tmp_save_numeric_invalid.json"
        data = {"start_symbol": "x", "grammar": self.symbol_grammar}
        data.update(bnf._symbol_encoder.get_config())

        try:
            with open(path, "w") as save_file:
                json.dump(data, save_file)

            self.assertRaises(ValueError, bnf.load, path)
        finally:
            os.remove(path)


    def test_make_sentence(self):
        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), ["a", "a", "b", "b", "d", "c", []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        self.assertTrue(np.all(self.bnf.make_sentence(tree) == ["d", "c"]))


    def test_make_sentence_cls(self):
        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), ["a", "a", "b", "b", "d", "c", []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        self.assertTrue(np.all(BNF.make_sentence(tree) == ["d", "c"]))


    def test_make_sentence_child_index(self):
        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), ["a", "a", "b", "b", "d", "c", []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        add_child_index(tree)
        tree.es[0][TreeAttributes.CHILD_INDEX] = 1
        tree.es[1][TreeAttributes.CHILD_INDEX] = 0

        self.assertTrue(np.all(self.bnf.make_sentence(tree) == ["c", "d"]))


    def test_make_record(self):
        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), ["a", "a", "b", "b", "d", "c", []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        sentence = [bytes("d", "utf-8"), bytes("c", "utf-8")]

        content = tf.io.parse_single_example(
            self.bnf.make_record(tree),
            {"program": tf.io.RaggedFeature(tf.string)}
        )

        self.assertTrue(np.all(content["program"].numpy() == sentence))


    def test_make_record(self):
        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), ["a", "a", "b", "b", "d", "c", []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        sentence = [bytes("d", "utf-8"), bytes("c", "utf-8")]

        content = tf.io.parse_single_example(
            BNF.make_record(tree),
            {"program": tf.io.RaggedFeature(tf.string)}
        )

        self.assertTrue(np.all(content["program"].numpy() == sentence))


    def test_make_record_child_index(self):
        tree = ig.Graph(directed=True)
        tree.add_vertices(7)
        tree.add_edges([(0, 1), (0, 2), (0, 3), (1, 4), (2, 5), (3, 6)])

        for idx, val in zip(range(7), ["a", "a", "b", "b", "d", "c", []]):
            tree.vs[idx][TreeAttributes.SYMBOL] = val

        add_child_index(tree)
        tree.es[0][TreeAttributes.CHILD_INDEX] = 1
        tree.es[1][TreeAttributes.CHILD_INDEX] = 0

        sentence = [bytes("c", "utf-8"), bytes("d", "utf-8")]

        content = tf.io.parse_single_example(
            self.bnf.make_record(tree),
            {"program": tf.io.RaggedFeature(tf.string)}
        )

        self.assertTrue(np.all(content["program"].numpy() == sentence))


    def test_extract_nonterminal(self):
        bnf = BNF()
        symbol = bnf._extract("<nonterminal>", nonterminal_only=True)
        self.assertEqual(symbol, "nonterminal")


    def test_raises_extract_nonterminal(self):
        bnf = BNF()
        self.assertRaises(ValueError, bnf._extract, "<my nonterminal>", True)


    def test_extract_symbol(self):
        bnf = BNF()
        symbol = bnf._extract(" 'my terminal'")
        self.assertEqual(symbol, "my terminal")


    def test_raises_extract_symbol(self):
        bnf = BNF()
        self.assertRaises(ValueError, bnf._extract, "<my nonterminal>")


    def test_raises_extract_terminal(self):
        bnf = BNF()
        self.assertRaises(ValueError, bnf._extract, "my nonterminal'")




class TestBNFParse(unittest.TestCase):
    def setUp(self):
        self.bnf = BNF()
        self.rules = [
            "   <S> ::= <A> | <B>",
            " <A> ::= 'a' <A> 'a' | 'a' ",
            "   # <C> ::= 'c' <C> 'c' | 'c'  ",
            "<B> ::= 'b' <B> 'b' | 'b'  ",
        ]
        self.grammar = {
            "S": [["A"], ["B"]],
            "A": [["a", "A", "a"], ["a"]],
            "B": [["b", "B", "b"], ["b"]]
        }
        self.numeric_grammar = {
            0: [[1], [2]],
            1: [[3, 1, 3], [3]],
            2: [[4, 2, 4], [4]]
        }
        self.start_symbol = "S"
        self.nonterminals = {"S", "A", "B"}
        self.terminals = {"a", "b"}


    def test_parse_list(self):
        self.bnf.parse(self.rules)
        self.assertEqual(self.bnf.grammar, self.grammar)
        self.assertEqual(self.bnf.numeric.grammar, self.numeric_grammar)
        self.assertEqual(self.bnf.terminal_symbols, self.terminals)
        self.assertEqual(self.bnf.nonterminal_symbols, self.nonterminals)


    def test_parse_str(self):
        self.bnf.parse("\n".join(self.rules))
        self.assertEqual(self.bnf.grammar, self.grammar)
        self.assertEqual(self.bnf.numeric.grammar, self.numeric_grammar)
        self.assertEqual(self.bnf.terminal_symbols, self.terminals)
        self.assertEqual(self.bnf.nonterminal_symbols, self.nonterminals)


    def test_parse_file(self):
        path = "tests/data/grammar/tmp_parse.json"

        try:
            with open(path, "w") as f:
                f.write("\n".join(self.rules))

            self.bnf.parse(path)
        finally:
            os.remove(path)

        self.assertEqual(self.bnf.grammar, self.grammar)
        self.assertEqual(self.bnf.numeric.grammar, self.numeric_grammar)
        self.assertEqual(self.bnf.terminal_symbols, self.terminals)
        self.assertEqual(self.bnf.nonterminal_symbols, self.nonterminals)


    def test_from_file(self):
        path = "tests/data/grammar/tmp_from_file.json"

        try:
            with open(path, "w") as f:
                f.write("\n".join(self.rules))

            bnf = BNF.from_file(path)
        finally:
            os.remove(path)

        self.assertEqual(bnf.start_symbol, self.start_symbol)
        self.assertEqual(bnf.grammar, self.grammar)
        self.assertEqual(bnf.numeric.grammar, self.numeric_grammar)
        self.assertEqual(bnf.terminal_symbols, self.terminals)
        self.assertEqual(bnf.nonterminal_symbols, self.nonterminals)


    def test_parse_empty_word(self):
        self.rules[0] = "   <S> ::= "" | <A> | <B>"
        self.grammar["S"] = [[], ["A"], ["B"]]
        self.numeric_grammar[0] = [[], [1], [2]]
        self.bnf.parse(self.rules)
        self.assertEqual(self.bnf.grammar, self.grammar)
        self.assertEqual(self.bnf.numeric.grammar, self.numeric_grammar)
        self.assertEqual(self.bnf.terminal_symbols, self.terminals)
        self.assertEqual(self.bnf.nonterminal_symbols, self.nonterminals)



    def test_init_list(self):
        bnf = BNF(self.start_symbol, self.rules)
        self.assertEqual(bnf.grammar, self.grammar)
        self.assertEqual(bnf.terminal_symbols, self.terminals)
        self.assertEqual(bnf.nonterminal_symbols, self.nonterminals)
        self.assertTrue(bnf.is_normalized())


    def test_init_str(self):
        bnf = BNF(self.start_symbol, "\n".join(self.rules))
        self.assertEqual(bnf.grammar, self.grammar)
        self.assertEqual(bnf.terminal_symbols, self.terminals)
        self.assertEqual(bnf.nonterminal_symbols, self.nonterminals)
        self.assertTrue(bnf.is_normalized())


    def test_init_file(self):
        path = "tests/data/grammar/tmp_init.json"

        try:
            with open(path, "w") as f:
                f.write("\n".join(self.rules))

            bnf = BNF(self.start_symbol, path)
        finally:
            os.remove(path)

        self.assertEqual(bnf.grammar, self.grammar)
        self.assertEqual(bnf.terminal_symbols, self.terminals)
        self.assertEqual(bnf.nonterminal_symbols, self.nonterminals)
        self.assertTrue(bnf.is_normalized())


    def test_init_empty_word(self):
        self.rules[0] = "   <S> ::= "" | <A> | <B>"
        self.grammar["S"] = [[], ["A"], ["B"]]
        self.numeric_grammar[0] = [[], [1], [2]]
        bnf = BNF(self.start_symbol, self.rules)
        self.assertEqual(bnf.grammar, self.grammar)
        self.assertEqual(bnf.terminal_symbols, self.terminals)
        self.assertEqual(bnf.nonterminal_symbols, self.nonterminals)
        self.assertTrue(bnf.is_normalized())




if __name__ == "__main__":
    unittest.main(verbosity="b")
