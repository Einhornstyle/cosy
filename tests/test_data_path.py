#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import os

import numpy as np
import tensorflow as tf

import cosy.preprocessing as cp
import cosy.data as cds
import cosy.data.record_io as cdr




def padded_path(nested_lists, padding_value=0):
    maxlen = max(len(l) for l in nested_lists)

    return [l + [padding_value]*(maxlen - len(l)) for l in nested_lists]




class TestPath(unittest.TestCase):
    def test_path_writer_init_single_path(self):
        writer = cds.PathWriter()
        self.assertEqual(writer.dtypes, ["int"])
        self.assertEqual(writer.num_paths, 1)
        self.assertEqual(writer._handler, [cp.int_feature_list])


    def test_path_writer_init_multi_path_single_dtype(self):
        writer = cds.PathWriter(dtypes="float", num_paths=3)
        self.assertEqual(writer.dtypes, ["float"]*3)
        self.assertEqual(writer.num_paths, 3)
        self.assertEqual(writer._handler, [cp.float_feature_list]*3)


    def test_path_writer_init_multi_path_multi_dtype(self):
        writer = cds.PathWriter(dtypes=["str", "int32", "float"], num_paths=3)
        handlers = [cp.str_feature_list, cp.int_feature_list, cp.float_feature_list]
        self.assertEqual(writer.dtypes, ["str", "int32", "float"])
        self.assertEqual(writer.num_paths, 3)
        self.assertEqual(writer._handler, handlers)


    def test_path_reader_init_single_path(self):
        reader = cds.PathReader(dtypes="int8")
        self.assertEqual(reader.dtypes, ["int8"])
        self.assertEqual(reader.num_paths, 1)
        self.assertEqual(reader._features, {"path_0": tf.io.RaggedFeature(tf.int64)})
        self.assertEqual(reader._cast_dtype_map, {0: tf.int8})


    def test_path_reader_init_multi_path_single_dtype(self):
        reader = cds.PathReader(dtypes="int16", num_paths=4)
        self.assertEqual(reader.dtypes, ["int16"]*4)
        self.assertEqual(reader.num_paths, 4)
        self.assertEqual(reader._features, {
            "path_0": tf.io.RaggedFeature(tf.int64),
            "path_1": tf.io.RaggedFeature(tf.int64),
            "path_2": tf.io.RaggedFeature(tf.int64),
            "path_3": tf.io.RaggedFeature(tf.int64)
        })
        self.assertEqual(reader._cast_dtype_map, {
            0: tf.int16,
            1: tf.int16,
            2: tf.int16,
            3: tf.int16
        })


    def test_path_reader_init_multi_path_multi_dtype(self):
        reader = cds.PathReader(dtypes=["string", "int32", "float", "uint8"], num_paths=4)
        self.assertEqual(reader.dtypes, ["string", "int32", "float", "uint8"])
        self.assertEqual(reader.num_paths, 4)
        self.assertEqual(reader._features, {
            "path_0": tf.io.RaggedFeature(tf.string),
            "path_1": tf.io.RaggedFeature(tf.int64),
            "path_2": tf.io.RaggedFeature(tf.float32),
            "path_3": tf.io.RaggedFeature(tf.int64)
        })
        self.assertEqual(reader._cast_dtype_map, {
            1: tf.int32,
            3: tf.uint8
        })


    def test_record_roundtrip_single_path(self):
        writer = cds.PathWriter()
        reader = cds.PathReader()

        path = [[i*j for j in range(1, i)] for i in (4, 3, 5)]

        record = writer.make_record(paths=[path])
        context, res_paths, shapes = reader._parse_example(record.SerializeToString())

        self.assertFalse(context)
        self.assertEqual(res_paths["path_0"].to_list(), path)
        self.assertFalse(shapes)


    def test_record_roundtrip_single_path_no_wrapper(self):
        writer = cds.PathWriter()
        reader = cds.PathReader()

        path = [[i*j for j in range(1, i)] for i in (4, 3, 5)]

        record = writer.make_record(paths=path) # <= no brackets

        context, res_paths, shapes = reader._parse_example(record.SerializeToString())

        self.assertFalse(context)
        self.assertEqual(res_paths["path_0"].to_list(), path)
        self.assertFalse(shapes)


    def test_record_roundtrip_multi_path_single_dtype(self):
        writer = cds.PathWriter(dtypes="int16", num_paths=3)
        reader = cds.PathReader(dtypes="int16", num_paths=3)

        path_0 = [[i*j for j in range(1, i)] for i in (4, 3, 5)]
        path_1 = [[i*j for j in range(1, i)] for i in (2, 7, 1, 5)]
        path_2 = [[i*j for j in range(1, i)] for i in (3, 8)]

        record = writer.make_record(paths=[path_0, path_1, path_2])
        context, res_paths, shapes = reader._parse_example(record.SerializeToString())

        self.assertFalse(context)
        self.assertEqual(res_paths["path_0"].to_list(), path_0)
        self.assertEqual(res_paths["path_1"].to_list(), path_1)
        self.assertEqual(res_paths["path_2"].to_list(), path_2)
        self.assertFalse(shapes)


    def test_record_roundtrip_multi_path_multi_dtype(self):
        writer = cds.PathWriter(dtypes=["int8", "int16", "string"], num_paths=3)
        reader = cds.PathReader(dtypes=["int8", "int16", "string"], num_paths=3)

        path_0 = [[i*j for j in range(1, i)] for i in (4, 3, 5)]
        path_1 = [[i*j for j in range(1, i)] for i in (2, 7, 1, 5)]
        path_2 = [["x"*j for j in range(1, i)] for i in (3, 8)]
        path_2_target = [[b"x"*j for j in range(1, i)] for i in (3, 8)]

        record = writer.make_record(paths=[path_0, path_1, path_2])
        context, res_paths, shapes = reader._parse_example(record.SerializeToString())

        self.assertFalse(context)
        self.assertEqual(res_paths["path_0"].to_list(), path_0)
        self.assertEqual(res_paths["path_1"].to_list(), path_1)
        self.assertEqual(res_paths["path_2"].to_list(), path_2_target)
        self.assertFalse(shapes)


    def test_cast_dtype_multi_path_multi_dtype(self):
        writer = cds.PathWriter(dtypes=["int8", "int16", "string"], num_paths=3)
        reader = cds.PathReader(dtypes=["int8", "int16", "string"], num_paths=3)

        path_0 = [[i*j for j in range(1, i)] for i in (4, 3, 5)]
        path_1 = [[i*j for j in range(1, i)] for i in (2, 7, 1, 5)]
        path_2 = [["x"*j for j in range(1, i)] for i in (3, 8)]
        path_2_target = [[b"x"*j for j in range(1, i)] for i in (3, 8)]

        record = writer.make_record(paths=[path_0, path_1, path_2])
        example = reader._parse_example(record.SerializeToString())
        tensors = reader._extract_tensors(*example)

        self.assertTrue(np.all(tensors[0].numpy() == padded_path(path_0)))
        self.assertTrue(np.all(tensors[1].numpy() == padded_path(path_1)))
        self.assertTrue(np.all(tensors[2].numpy() == padded_path(path_2_target, b"")))

        self.assertEqual(tensors[0].dtype, tf.int8)
        self.assertEqual(tensors[1].dtype, tf.int16)
        self.assertEqual(tensors[2].dtype, tf.string)


    def test_cast_dtype_multi_path_multi_dtype_with_bool(self):
        writer = cds.PathWriter(dtypes=["int8", "int16", "bool"], num_paths=3)
        reader = cds.PathReader(dtypes=["int8", "int16", "bool"], num_paths=3)

        path_0 = [[i*j for j in range(1, i)] for i in (4, 3, 5)]
        path_1 = [[i*j for j in range(1, i)] for i in (2, 7, 1, 5)]
        path_2 = [[(i + j) % 2 == 0 for j in range(i, i)] for i in (3, 8)]

        record = writer.make_record(paths=[path_0, path_1, path_2])
        example = reader._parse_example(record.SerializeToString())
        tensors = reader._extract_tensors(*example)

        self.assertTrue(np.all(tensors[0].numpy() == padded_path(path_0)))
        self.assertTrue(np.all(tensors[1].numpy() == padded_path(path_1)))
        self.assertTrue(np.all(tensors[2].numpy() == padded_path(path_2)))

        self.assertEqual(tensors[0].dtype, tf.int8)
        self.assertEqual(tensors[1].dtype, tf.int16)
        self.assertEqual(tensors[2].dtype, tf.bool)


    def test_preprocess_single_path(self):
        filename = os.path.join("tests", "data", "path_preprocess_single_path.tfrecord")

        paths = [[[i*j*n for j in range(1, i)] for i in (4, 3, 5)] for n in (5, 6, 1)]

        try:
            with cds.PathWriter(filename=filename) as writer:
                for path in paths:
                    writer(paths=path)

            dataset = cds.PathReader()(filenames=filename)

            for tensor in dataset:
                idx = None

                for i, path in enumerate(paths):
                    if np.all(tensor.numpy() == padded_path(path)):
                        idx = i
                        break

                paths.pop(idx)

            self.assertFalse(paths)
        finally:
            os.remove(filename)


    def test_preprocess_single_path_batch(self):
        filename = os.path.join("tests", "data", "path_preprocess_single_path_batch.tfrecord")

        paths = [[[i*j*n for j in range(1, i)] for i in (4, 3, 5)] for n in (5, 6, 1, 7)]

        try:
            with cds.PathWriter(filename=filename) as writer:
                for path in paths:
                    writer(paths=path)

            dataset = cds.PathReader()(filenames=filename, batch_size=2)

            for tensor in dataset:
                idx = None
                for i, path in enumerate(paths):
                    if np.all(tensor[0, ...].numpy() == padded_path(path)):
                        idx = i
                        break
                paths.pop(idx)

                idx = None
                for i, path in enumerate(paths):
                    if np.all(tensor[1, ...].numpy() == padded_path(path)):
                        idx = i
                        break
                paths.pop(idx)

            self.assertFalse(paths)
        finally:
            os.remove(filename)


    def test_preprocess_multi_path_single_dtype(self):
        filename = os.path.join("tests", "data", "path_preprocess_ms_st.tfrecord")

        paths = [
            [[[k*i*j*n for j in range(1, i)] for i in (4, 3, 5)] for k in range(3)]
            for n in (5, 6, 1, 7)
        ]

        try:
            with cds.PathWriter(dtypes="int32", num_paths=3, filename=filename) as writer:
                for path in paths:
                    writer(paths=path)

            dataset = cds.PathReader(dtypes="int32", num_paths=3)(filenames=filename)

            for tensors in dataset:
                idx = None

                for i, path_list in enumerate(paths):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(tensors[0].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[1].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[2].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)

            self.assertFalse(paths)
        finally:
            os.remove(filename)


    def test_preprocess_multi_path_single_dtype_batch(self):
        filename = os.path.join("tests", "data", "path_preprocess_ms_st_batch.tfrecord")

        paths = [
            [[[k*i*j*n for j in range(1, i)] for i in (4, 3, 5)] for k in range(3)]
            for n in (5, 6, 1, 7)
        ]

        try:
            with cds.PathWriter(dtypes="int32", num_paths=3, filename=filename) as writer:
                for path in paths:
                    writer(paths=path)

            reader = cds.PathReader(dtypes="int16", num_paths=3)
            dataset = reader(filenames=filename, batch_size=2)

            for tensors in dataset:
                idx = None

                for i, path_list in enumerate(paths):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(tensors[0][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[1][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[2][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)

                idx = None

                for i, path_list in enumerate(paths):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(tensors[0][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[1][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[2][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)

            self.assertFalse(paths)
        finally:
            os.remove(filename)


    def test_preprocess_multi_path_multi_dtype(self):
        filename = os.path.join("tests", "data", "path_preprocess_ms_mt.tfrecord")

        paths = [
            [[[k*i*j*n for j in range(1, i)] for i in (4, 3, 5)] for k in range(3)]
            for n in (5, 6, 1, 7)
        ]

        dtypes = ["int16", "int", "int32"]

        try:
            with cds.PathWriter(dtypes=dtypes, num_paths=3, filename=filename) as writer:
                for path in paths:
                    writer(paths=path)

            dataset = cds.PathReader(dtypes=dtypes, num_paths=3)(filenames=filename)

            for tensors in dataset:
                idx = None

                for i, path_list in enumerate(paths):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(tensors[0].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[1].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[2].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)

            self.assertFalse(paths)
        finally:
            os.remove(filename)


    def test_preprocess_multi_path_multi_dtype_batch(self):
        filename = os.path.join("tests", "data", "path_preprocess_ms_mt_batch.tfrecord")

        paths = [
            [[[k*i*j*n for j in range(1, i)] for i in (4, 3, 5)] for k in range(3)]
            for n in (5, 6, 1, 7)
        ]

        dtypes = ["int16", "int", "int32"]

        try:
            with cds.PathWriter(dtypes=dtypes, num_paths=3, filename=filename) as writer:
                for path in paths:
                    writer(paths=path)

            reader = cds.PathReader(dtypes=dtypes, num_paths=3)
            dataset = reader(filenames=filename, batch_size=2)

            for tensors in dataset:
                idx = None

                for i, path_list in enumerate(paths):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(tensors[0][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[1][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[2][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)

                idx = None

                for i, path_list in enumerate(paths):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(tensors[0][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[1][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(tensors[2][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)

            self.assertFalse(paths)
        finally:
            os.remove(filename)




class TestLabeledPath(unittest.TestCase):
    def test_path_writer_init_single_path(self):
        writer = cds.LabeledPathWriter()
        self.assertEqual(writer.dtypes, ["int"])
        self.assertEqual(writer.num_paths, 1)
        self.assertEqual(writer._handler, [cp.int_feature_list])
        self.assertEqual(writer.label_dtype, "int")
        self.assertEqual(writer._label_handler, cp.int_feature)


    def test_path_writer_init_multi_path_single_dtype(self):
        writer = cds.LabeledPathWriter(dtypes="float", num_paths=3, label_dtype="int16")
        self.assertEqual(writer.dtypes, ["float"]*3)
        self.assertEqual(writer.num_paths, 3)
        self.assertEqual(writer._handler, [cp.float_feature_list]*3)
        self.assertEqual(writer.label_dtype, "int16")
        self.assertEqual(writer._label_handler, cp.int_feature)


    def test_path_writer_init_multi_path_multi_dtype(self):
        writer = cds.LabeledPathWriter(
            dtypes=["str", "int32", "float"],
            num_paths=3,
            label_dtype="int32"
        )

        handlers = [cp.str_feature_list, cp.int_feature_list, cp.float_feature_list]

        self.assertEqual(writer.dtypes, ["str", "int32", "float"])
        self.assertEqual(writer.num_paths, 3)
        self.assertEqual(writer._handler, handlers)
        self.assertEqual(writer.label_dtype, "int32")
        self.assertEqual(writer._label_handler, cp.int_feature)


    def test_path_reader_init_single_path(self):
        reader = cds.LabeledPathReader(dtypes="int8")
        self.assertEqual(reader.dtypes, ["int8"])
        self.assertEqual(reader.num_paths, 1)
        self.assertEqual(reader._features, {"path_0": tf.io.RaggedFeature(tf.int64)})
        self.assertEqual(reader._label_feature, {"label": tf.io.FixedLenFeature([], tf.int64)})
        self.assertEqual(reader._cast_dtype_map, {0: tf.int8})
        self.assertEqual(reader.label_dtype, "int")
        self.assertEqual(reader._cast_label_dtype, tf.int64)


    def test_path_reader_init_multi_path_single_dtype(self):
        reader = cds.LabeledPathReader(dtypes="int16", num_paths=4, label_dtype="int32")
        self.assertEqual(reader.dtypes, ["int16"]*4)
        self.assertEqual(reader.num_paths, 4)
        self.assertEqual(reader._features, {
            "path_0": tf.io.RaggedFeature(tf.int64),
            "path_1": tf.io.RaggedFeature(tf.int64),
            "path_2": tf.io.RaggedFeature(tf.int64),
            "path_3": tf.io.RaggedFeature(tf.int64)
        })
        self.assertEqual(reader._label_feature, {"label": tf.io.FixedLenFeature([], tf.int64)})
        self.assertEqual(reader._cast_dtype_map, {
            0: tf.int16,
            1: tf.int16,
            2: tf.int16,
            3: tf.int16
        })
        self.assertEqual(reader.label_dtype, "int32")
        self.assertEqual(reader._cast_label_dtype, tf.int32)


    def test_path_reader_init_multi_path_multi_dtype(self):
        reader = cds.LabeledPathReader(
            dtypes=["string", "int32", "float", "uint8"],
            num_paths=4,
            label_dtype="uint8"
        )
        self.assertEqual(reader.dtypes, ["string", "int32", "float", "uint8"])
        self.assertEqual(reader.num_paths, 4)
        self.assertEqual(reader._features, {
            "path_0": tf.io.RaggedFeature(tf.string),
            "path_1": tf.io.RaggedFeature(tf.int64),
            "path_2": tf.io.RaggedFeature(tf.float32),
            "path_3": tf.io.RaggedFeature(tf.int64)
        })
        self.assertEqual(reader._label_feature, {"label": tf.io.FixedLenFeature([], tf.int64)})
        self.assertEqual(reader._cast_dtype_map, {
            1: tf.int32,
            3: tf.uint8
        })
        self.assertEqual(reader.label_dtype, "uint8")
        self.assertEqual(reader._cast_label_dtype, tf.uint8)


    def test_record_roundtrip_single_path(self):
        writer = cds.LabeledPathWriter()
        reader = cds.LabeledPathReader()

        path = [[i*j for j in range(1, i)] for i in (4, 3, 5)]
        label = 7

        record = writer.make_record(paths=[path], label=7)
        context, res_paths, shapes = reader._parse_example(record.SerializeToString())

        self.assertTrue(np.all(res_paths["path_0"].to_list() == path))
        self.assertEqual(context["label"], label)
        self.assertFalse(shapes)


    def test_record_roundtrip_single_path_no_wrapper(self):
        writer = cds.LabeledPathWriter()
        reader = cds.LabeledPathReader()

        path = [[i*j for j in range(1, i)] for i in (4, 3, 5)]
        label = 7

        record = writer.make_record(paths=path, label=7) # <= no brackets
        context, res_paths, shapes = reader._parse_example(record.SerializeToString())

        self.assertTrue(np.all(res_paths["path_0"].to_list() == path))
        self.assertEqual(context["label"], label)
        self.assertFalse(shapes)


    def test_record_roundtrip_multi_path_single_dtype(self):
        writer = cds.LabeledPathWriter(dtypes="int16", num_paths=3, label_dtype="int16")
        reader = cds.LabeledPathReader(dtypes="int16", num_paths=3, label_dtype="int16")

        path_0 = [[i*j for j in range(1, i)] for i in (4, 3, 5)]
        path_1 = [[i*j for j in range(1, i)] for i in (2, 7, 1, 5)]
        path_2 = [[i*j for j in range(1, i)] for i in (3, 8)]
        label = 7

        record = writer.make_record(paths = [path_0, path_1, path_2], label=7)
        context, res_paths, shapes = reader._parse_example(record.SerializeToString())

        self.assertTrue(np.all(res_paths["path_0"].to_list() == path_0))
        self.assertTrue(np.all(res_paths["path_1"].to_list() == path_1))
        self.assertTrue(np.all(res_paths["path_2"].to_list() == path_2))
        self.assertEqual(context["label"], label)
        self.assertFalse(shapes)


    def test_record_roundtrip_multi_path_multi_dtype(self):
        writer = cds.LabeledPathWriter(dtypes=["int8", "int16", "string"], num_paths=3)
        reader = cds.LabeledPathReader(dtypes=["int8", "int16", "string"], num_paths=3)

        path_0 = [[i*j for j in range(1, i)] for i in (4, 3, 5)]
        path_1 = [[i*j for j in range(1, i)] for i in (2, 7, 1, 5)]
        path_2 = [["x"*j for j in range(1, i)] for i in (3, 8)]
        path_2_target = [[b"x"*j for j in range(1, i)] for i in (3, 8)]
        label = 12

        record = writer.make_record(paths=[path_0, path_1, path_2], label=12)
        context, res_paths, shapes = reader._parse_example(record.SerializeToString())

        self.assertEqual(res_paths["path_0"].to_list(), path_0)
        self.assertEqual(res_paths["path_1"].to_list(), path_1)
        self.assertEqual(res_paths["path_2"].to_list(), path_2_target)
        self.assertEqual(context["label"], label)
        self.assertFalse(shapes)


    def test_cast_dtype_multi_path_multi_dtype(self):
        writer = cds.LabeledPathWriter(
            dtypes=["int8", "int16", "string"],
            num_paths=3,
            label_dtype="int16"
        )

        reader = cds.LabeledPathReader(
            dtypes=["int8", "int16", "string"],
            num_paths=3,
            label_dtype="int16"
        )

        path_0 = [[i*j for j in range(1, i)] for i in (4, 3, 5)]
        path_1 = [[i*j for j in range(1, i)] for i in (2, 7, 1, 5)]
        path_2 = [["x"*j for j in range(1, i)] for i in (3, 8)]
        path_2_target = [[b"x"*j for j in range(1, i)] for i in (3, 8)]
        label = 7

        record = writer.make_record(paths=[path_0, path_1, path_2], label=7)
        example = reader._parse_example(record.SerializeToString())
        tensors, label_tensor = reader._extract_tensors(*example)

        self.assertTrue(np.all(tensors[0].numpy() == padded_path(path_0)))
        self.assertTrue(np.all(tensors[1].numpy() == padded_path(path_1)))
        self.assertTrue(np.all(tensors[2].numpy() == padded_path(path_2_target, b"")))
        self.assertEqual(label_tensor, label)

        self.assertEqual(tensors[0].dtype, tf.int8)
        self.assertEqual(tensors[1].dtype, tf.int16)
        self.assertEqual(tensors[2].dtype, tf.string)
        self.assertEqual(label_tensor.dtype, tf.int16)


    def test_preprocess_single_path(self):
        filename = os.path.join("tests", "data", "l_path_preprocess_single_path.tfrecord")

        paths = [[[i*j*n for j in range(1, i)] for i in (4, 3, 5)] for n in (5, 6, 1)]
        labels = [7, 12, 3]

        try:
            with cds.LabeledPathWriter(filename=filename) as writer:
                for path, label in zip(paths, labels):
                    writer(paths=path, label=label)

            dataset = cds.LabeledPathReader()(filenames=filename)

            for t, l in dataset:
                idx = None

                for i, (path, label) in enumerate(zip(paths, labels)):
                    if np.all(t.numpy() == padded_path(path)) and (l.numpy() == label):
                        idx = i
                        break

                paths.pop(idx)
                labels.pop(idx)

            self.assertFalse(paths)
            self.assertFalse(labels)
        finally:
            os.remove(filename)


    def test_preprocess_single_path_batch(self):
        filename = os.path.join("tests", "data", "l_path_preprocess_single_path_batch.tfrecord")

        paths = [[[i*j*n for j in range(1, i)] for i in (4, 3, 5)] for n in (5, 6, 1, 7)]
        labels = [7, 12, 3, 5]

        try:
            with cds.LabeledPathWriter(filename=filename) as writer:
                for path, label in zip(paths, labels):
                    writer(paths=path, label=label)

            dataset = cds.LabeledPathReader()(filenames=filename, batch_size=2)

            for t, l in dataset:
                idx = None
                for i, (path, label) in enumerate(zip(paths, labels)):
                    if np.all(t[0, ...].numpy() == padded_path(path)) and (l[0].numpy() == label):
                        idx = i
                        break
                paths.pop(idx)
                labels.pop(idx)

                idx = None
                for i, (path, label) in enumerate(zip(paths, labels)):
                    if np.all(t[1, ...].numpy() == padded_path(path)) and (l[1].numpy() == label):
                        idx = i
                        break
                paths.pop(idx)
                labels.pop(idx)

            self.assertFalse(paths)
            self.assertFalse(labels)
        finally:
            os.remove(filename)


    def test_preprocess_multi_path_single_dtype(self):
        filename = os.path.join("tests", "data", "l_path_preprocess_ms_st.tfrecord")

        paths = [
            [[[k*i*j*n for j in range(1, i)] for i in (4, 3, 5)] for k in range(3)]
            for n in (5, 6, 1, 7)
        ]

        labels = [3, 2, 1, 4]

        try:
            with cds.LabeledPathWriter(dtypes="int32", num_paths=3, filename=filename) as writer:
                for path, label in zip(paths, labels):
                    writer(paths=path, label=label)

            dataset = cds.LabeledPathReader(dtypes="int32", num_paths=3)(filenames=filename)

            for t, l in dataset:
                idx = None

                for i, (path_list, label) in enumerate(zip(paths, labels)):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(t[0].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[1].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[2].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx and (l.numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)
                labels.pop(idx)

            self.assertFalse(paths)
        finally:
            os.remove(filename)


    def test_preprocess_multi_path_single_dtype_batch(self):
        filename = os.path.join("tests", "data", "l_path_preprocess_ms_st_batch.tfrecord")

        paths = [
            [[[k*i*j*n for j in range(1, i)] for i in (4, 3, 5)] for k in range(3)]
            for n in (5, 6, 1, 7)
        ]

        labels = [3, 2, 1, 4]

        try:
            with cds.LabeledPathWriter(dtypes="int32", num_paths=3, filename=filename) as writer:
                for path, label in zip(paths, labels):
                    writer(paths=path, label=label)

            reader = cds.LabeledPathReader(dtypes="int32", num_paths=3)
            dataset = reader(filenames=filename, batch_size=2)

            for t, l in dataset:
                idx = None

                for i, (path_list, label) in enumerate(zip(paths, labels)):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(t[0][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[1][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[2][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx and (l[0].numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)
                labels.pop(idx)

                idx = None

                for i, (path_list, label) in enumerate(zip(paths, labels)):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(t[0][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[1][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[2][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx and (l[1].numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)
                labels.pop(idx)

            self.assertFalse(paths)
        finally:
            os.remove(filename)


    def test_preprocess_multi_path_multi_dtype(self):
        filename = os.path.join("tests", "data", "l_path_preprocess_ms_mt.tfrecord")

        paths = [
            [[[k*i*j*n for j in range(1, i)] for i in (4, 3, 5)] for k in range(3)]
            for n in (5, 6, 1, 7)
        ]

        labels = [3, 2, 1, 4]

        dtypes = ["int", "int32", "int16"]

        try:
            with cds.LabeledPathWriter(dtypes=dtypes, num_paths=3, filename=filename) as writer:
                for path, label in zip(paths, labels):
                    writer(paths=path, label=label)

            dataset = cds.LabeledPathReader(dtypes=dtypes, num_paths=3)(filenames=filename)

            for t, l in dataset:
                idx = None

                for i, (path_list, label) in enumerate(zip(paths, labels)):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(t[0].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[1].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[2].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx and (l.numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)
                labels.pop(idx)

            self.assertFalse(paths)
        finally:
            os.remove(filename)


    def test_preprocess_multi_path_multi_dtype_batch(self):
        filename = os.path.join("tests", "data", "l_path_preprocess_ms_mt_batch.tfrecord")

        paths = [
            [[[k*i*j*n for j in range(1, i)] for i in (4, 3, 5)] for k in range(3)]
            for n in (5, 6, 1, 7)
        ]

        labels = [3, 2, 1, 4]

        dtypes = ["int64", "int32", "int16"]

        try:
            with cds.LabeledPathWriter(dtypes=dtypes, num_paths=3, filename=filename) as writer:
                for path, label in zip(paths, labels):
                    writer(paths=path, label=label)

            reader = cds.LabeledPathReader(dtypes=dtypes, num_paths=3)
            dataset = reader(filenames=filename, batch_size=2)

            for t, l in dataset:
                idx = None

                for i, (path_list, label) in enumerate(zip(paths, labels)):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(t[0][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[1][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[2][0, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx and (l[0].numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)
                labels.pop(idx)

                idx = None

                for i, (path_list, label) in enumerate(zip(paths, labels)):
                    match_idx = set()

                    for j, path in enumerate(path_list):
                        if np.all(t[0][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[1][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                        if np.all(t[2][1, ...].numpy() == padded_path(path)):
                            match_idx.add(j)

                    if match_idx and (l[1].numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                paths.pop(idx)
                labels.pop(idx)

            self.assertFalse(paths)
        finally:
            os.remove(filename)




if __name__ == "__main__":
    unittest.main(verbosity=2)
