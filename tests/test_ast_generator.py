# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest
import math

from cosy.tree import TreeAttributes
from cosy.grammar import BNF
from cosy.generator import ASTGenerator




class TestASTGenerator(unittest.TestCase):
    def setUp(self):
        self.bnf = BNF()
        self.bnf.add("S", [["A"], ["B"]])
        self.bnf.add("A", [["A"], ["a"]])
        self.bnf.add("B", [["C"]])
        self.bnf.add("C", [["c"]])
        self.bnf.start_symbol = "S"


    def test_compute_depth_minima(self):
        gen = ASTGenerator(self.bnf)
        self.assertEqual(len(gen._min_depth_map), 4)
        self.assertEqual(gen._min_depth_map["S"], [2, 3])
        self.assertEqual(gen._min_depth_map["A"], [2, 1])
        self.assertEqual(gen._min_depth_map["B"], [2])
        self.assertEqual(gen._min_depth_map["C"], [1])


    def test_numeric_compute_depth_minima(self):
        gen = ASTGenerator(self.bnf.numeric)
        self.assertEqual(len(gen._min_depth_map), 4)
        self.assertEqual(gen._min_depth_map[0], [2, 3])
        self.assertEqual(gen._min_depth_map[1], [2, 1])
        self.assertEqual(gen._min_depth_map[2], [2])
        self.assertEqual(gen._min_depth_map[4], [1])


    def test_compute_depth_maxima(self):
        gen = ASTGenerator(self.bnf)
        self.assertEqual(len(gen._max_depth_map), 4)
        self.assertEqual(gen._max_depth_map["S"], [math.inf, 3])
        self.assertEqual(gen._max_depth_map["A"], [math.inf, 1])
        self.assertEqual(gen._max_depth_map["B"], [2])
        self.assertEqual(gen._max_depth_map["C"], [1])


    def test_numeric_compute_depth_maxima(self):
        gen = ASTGenerator(self.bnf.numeric)
        self.assertEqual(len(gen._max_depth_map), 4)
        self.assertEqual(gen._max_depth_map[0], [math.inf, 3])
        self.assertEqual(gen._max_depth_map[1], [math.inf, 1])
        self.assertEqual(gen._max_depth_map[2], [2])
        self.assertEqual(gen._max_depth_map[4], [1])


    def test_select_rule(self):
        gen = ASTGenerator(self.bnf)
        self.assertEqual(gen._select_rule(self.bnf["S"], [1.0, 0.0]), ["A"])
        self.assertEqual(gen._select_rule(self.bnf["A"], [0.0, 1.0]), ["a"])


    def test_numeric_select_rule(self):
        gen = ASTGenerator(self.bnf.numeric)
        self.assertEqual(gen._select_rule(self.bnf.numeric[0], [1.0, 0.0]), [1])
        self.assertEqual(gen._select_rule(self.bnf.numeric[1], [0.0, 1.0]), [3])


    def test_compute_candidates_multi(self):
        gen = ASTGenerator(self.bnf, min_depth=1, max_depth=4)
        rules, probs = gen._compute_candidates("A", current_depth=1)
        self.assertEqual(rules, [["A"], ["a"]])
        self.assertEqual(probs, [0.5, 0.5])


    def test_numeric_compute_candidates_multi(self):
        gen = ASTGenerator(self.bnf.numeric, min_depth=1, max_depth=4)
        rules, probs = gen._compute_candidates(1, current_depth=1)
        self.assertEqual(rules, [[1], [3]])
        self.assertEqual(probs, [0.5, 0.5])


    def test_compute_candidates_single(self):
        gen = ASTGenerator(self.bnf, min_depth=3, max_depth=3)
        rules, probs = gen._compute_candidates("B", current_depth=1)
        self.assertEqual(rules, [["C"]])
        self.assertEqual(probs, [1.0])


    def test_nummeric_compute_candidates_single(self):
        gen = ASTGenerator(self.bnf.numeric, min_depth=3, max_depth=3)
        rules, probs = gen._compute_candidates(2, current_depth=1)
        self.assertEqual(rules, [[4]])
        self.assertEqual(probs, [1.0])


    def test_compute_candidates_none(self):
        gen = ASTGenerator(self.bnf, min_depth=5, max_depth=5)
        self.assertRaises(ValueError, gen._compute_candidates, "B", 1)


    def test_numeric_compute_candidates_none(self):
        gen = ASTGenerator(self.bnf.numeric, min_depth=5, max_depth=5)
        self.assertRaises(ValueError, gen._compute_candidates, 2, 1)


    def test_recursive_generate_terminal(self):
        gen = ASTGenerator(self.bnf)
        gen._recursive_generate_ast("a", 3)
        self.assertEqual(gen._vertices, ["a"])
        self.assertEqual(gen._edges, [])


    def test_numeric_recursive_generate_terminal(self):
        gen = ASTGenerator(self.bnf.numeric)
        gen._recursive_generate_ast(3, 3)
        self.assertEqual(gen._vertices, [3])
        self.assertEqual(gen._edges, [])


    def test_recursive_generate_nonterminal(self):
        gen = ASTGenerator(self.bnf, min_depth=4, max_depth=4)
        gen._recursive_generate_ast("A", 3)
        self.assertEqual(gen._vertices, ["A", "a"])
        self.assertEqual(gen._edges, [(0, 1)])


    def test_numeric_recursive_generate_nonterminal(self):
        gen = ASTGenerator(self.bnf.numeric, min_depth=4, max_depth=4)
        gen._recursive_generate_ast(1, 3)
        self.assertEqual(gen._vertices, [1, 3])
        self.assertEqual(gen._edges, [(0, 1)])


    def test_recursive_generate_tree(self):
        gen = ASTGenerator(self.bnf, min_depth=5, max_depth=5)
        gen._recursive_generate_ast("A", 1)
        self.assertEqual(gen._vertices, ["A", "A", "A", "A", "a"])
        self.assertEqual(gen._edges, [(0, 1), (1, 2), (2, 3), (3, 4)])


    def test_numeric_recursive_generate_tree(self):
        gen = ASTGenerator(self.bnf.numeric, min_depth=5, max_depth=5)
        gen._recursive_generate_ast(1, 1)
        self.assertEqual(gen._vertices, [1, 1, 1, 1, 3])
        self.assertEqual(gen._edges, [(0, 1), (1, 2), (2, 3), (3, 4)])


    def test_next(self):
        gen = ASTGenerator(self.bnf, min_depth=5, max_depth=5)
        ast = gen.__next__()
        self.assertEqual([v[TreeAttributes.SYMBOL] for v in ast.vs], ["S", "A", "A", "A", "a"])
        self.assertEqual([(e.source, e.target) for e in ast.es], [(0, 1), (1, 2), (2, 3), (3, 4)])


    def test_numeric_next(self):
        gen = ASTGenerator(self.bnf.numeric, min_depth=5, max_depth=5)
        ast = gen.__next__()
        self.assertEqual([v[TreeAttributes.SYMBOL] for v in ast.vs], [0, 1, 1, 1, 3])
        self.assertEqual([(e.source, e.target) for e in ast.es], [(0, 1), (1, 2), (2, 3), (3, 4)])


    def test_iter(self):
        gen = ASTGenerator(self.bnf, min_depth=5, max_depth=5)

        v_res = ["S", "A", "A", "A", "a"]
        e_res = [(0, 1), (1, 2), (2, 3), (3, 4)]

        for i, ast in zip(range(3), gen):
            self.assertEqual([v[TreeAttributes.SYMBOL] for v in ast.vs], v_res)
            self.assertEqual([(e.source, e.target) for e in ast.es], e_res)


    def test_numeric_iter(self):
        gen = ASTGenerator(self.bnf.numeric, min_depth=5, max_depth=5)

        v_res = [0, 1, 1, 1, 3]
        e_res = [(0, 1), (1, 2), (2, 3), (3, 4)]

        for i, ast in zip(range(3), gen):
            self.assertEqual([v[TreeAttributes.SYMBOL] for v in ast.vs], v_res)
            self.assertEqual([(e.source, e.target) for e in ast.es], e_res)


    def test_generate(self):
        gen = ASTGenerator(self.bnf, min_depth=5, max_depth=5)

        counter = 0
        v_res = ["S", "A", "A", "A", "a"]
        e_res = [(0, 1), (1, 2), (2, 3), (3, 4)]

        for ast in gen.generate(3):
            counter += 1
            self.assertEqual([v[TreeAttributes.SYMBOL] for v in ast.vs], v_res)
            self.assertEqual([(e.source, e.target) for e in ast.es], e_res)

        self.assertEqual(counter, 3)


    def test_numeric_generate(self):
        gen = ASTGenerator(self.bnf.numeric, min_depth=5, max_depth=5)

        counter = 0
        v_res = [0, 1, 1, 1, 3]
        e_res = [(0, 1), (1, 2), (2, 3), (3, 4)]

        for ast in gen.generate(3):
            counter += 1
            self.assertEqual([v[TreeAttributes.SYMBOL] for v in ast.vs], v_res)
            self.assertEqual([(e.source, e.target) for e in ast.es], e_res)

        self.assertEqual(counter, 3)


    def test_multiple_children(self):
        bnf = BNF()
        bnf.add("S", [["A", "B"]])
        bnf.add("A", [["a", "a"]])
        bnf.add("B", [["b"]])
        bnf.start_symbol = "S"

        gen = ASTGenerator(bnf)
        ast = next(iter(gen))

        vertex_soll = [(0, "S"), (1, "A"), (2, "a"), (3, "a"), (4, "B"), (5, "b")]
        edge_soll = [(0, 1), (1, 2), (1, 3), (0, 4), (4, 5)]

        self.assertEqual([(v.index, v["symbol"]) for v in ast.vs], vertex_soll)
        self.assertEqual([(e.source, e.target) for e in ast.es], edge_soll)


    def test_numeric_multiple_children(self):
        bnf = BNF()
        bnf.add("S", [["A", "B"]])
        bnf.add("A", [["a", "a"]])
        bnf.add("B", [["b"]])
        bnf.start_symbol = "S"

        gen = ASTGenerator(bnf.numeric)
        ast = next(iter(gen))

        vertex_soll = [(0, 0), (1, 1), (2, 3), (3, 3), (4, 2), (5, 4)]
        edge_soll = [(0, 1), (1, 2), (1, 3), (0, 4), (4, 5)]

        self.assertEqual([(v.index, v["symbol"]) for v in ast.vs], vertex_soll)
        self.assertEqual([(e.source, e.target) for e in ast.es], edge_soll)




if __name__ == "__main__":
    unittest.main(verbosity=2)
