#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import os

import numpy as np
import tensorflow as tf

import cosy.preprocessing as cp
import cosy.data as cds




class TestASTPaths(unittest.TestCase):
    def setUp(self):
        self.padded_path_symbols = [
            list(range(1, 5)),
            list(range(1, 3)) + [0, 0],
            list(range(3, 7))
        ]

        self.padded_path_indices = [
            list(range(4, 8)),
            list(range(1, 3)) + [0, 0],
            list(range(2, 6))
        ]

        self.path_symbols = [
            list(range(1, 5)),
            list(range(1, 3)),
            list(range(3, 7))
        ]

        self.path_indices = [
            list(range(4, 8)),
            list(range(1, 3)),
            list(range(2, 6))
        ]

        self.padded_seperated_path_symbols = [
            list(range(2, 5)),
            list(range(2, 3)) + [0, 0],
            list(range(4, 7))
        ]

        self.padded_seperated_path_indices = [
            list(range(5, 8)),
            list(range(2, 3)) + [0, 0],
            list(range(3, 6))
        ]

        self.seperated_path_symbols = [
            list(range(2, 5)),
            list(range(2, 3)),
            list(range(4, 7))
        ]

        self.seperated_path_indices = [
            list(range(5, 8)),
            list(range(2, 3)),
            list(range(3, 6))
        ]

        self.terminal_symbols = [p[0] for p in self.path_symbols]
        self.terminal_indices = [p[0] for p in self.path_indices]

        self.root_symbols = list(range(3, 6))
        self.root_indices = list(range(2, 5))

        self.leaf_symbol = 7

        self.terminal_vocab_size = 7
        self.nonterminal_vocab_size = 6
        self.max_sibling_index = 8


    def test_set_terminal_vocab_size(self):
        dsf = cds.ASTPathsReader()
        self.assertEqual(dsf.terminal_vocab_size, None)
        self.assertEqual(dsf._terminal_type, tf.int64)

        dsf.terminal_vocab_size = 24
        self.assertEqual(dsf.terminal_vocab_size, 24)
        self.assertEqual(dsf._terminal_type, tf.int8)


    def test_set_nonterminal_vocab_size(self):
        dsf = cds.ASTPathsReader()
        self.assertEqual(dsf.nonterminal_vocab_size, None)
        self.assertEqual(dsf._nonterminal_type, tf.int64)

        dsf.nonterminal_vocab_size = 2**13
        self.assertEqual(dsf.nonterminal_vocab_size, 2**13)
        self.assertEqual(dsf._nonterminal_type, tf.int16)


    def test_set_max_sibling_index(self):
        dsf = cds.ASTPathsReader()
        self.assertEqual(dsf.max_sibling_index, None)
        self.assertEqual(dsf._index_type, tf.int64)

        dsf.max_sibling_index = 2**55
        self.assertEqual(dsf.max_sibling_index, 2**55)
        self.assertEqual(dsf._index_type, tf.int64)


    def test_normalize_datatypes(self):
        dsf = cds.ASTPathsReader()
        dsf.terminal_vocab_size = 24
        dsf.nonterminal_vocab_size = 2**13
        dsf.max_sibling_index = 2**24

        self.assertEqual(dsf.terminal_vocab_size, 24)
        self.assertEqual(dsf._terminal_type, tf.int8)

        self.assertEqual(dsf.nonterminal_vocab_size, 2**13)
        self.assertEqual(dsf._nonterminal_type, tf.int16)

        self.assertEqual(dsf.max_sibling_index, 2**24)
        self.assertEqual(dsf._index_type, tf.int32)

        dsf.normalize_datatypes()

        self.assertEqual(dsf.terminal_vocab_size, 24)
        self.assertEqual(dsf._terminal_type, tf.int32)

        self.assertEqual(dsf.nonterminal_vocab_size, 2**13)
        self.assertEqual(dsf._nonterminal_type, tf.int32)

        self.assertEqual(dsf.max_sibling_index, 2**24)
        self.assertEqual(dsf._index_type, tf.int32)


    def test_make_context_features(self):
        features = cds.ASTPathsWriter()._make_context_features(
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        self.assertEqual(features["root_symbols"].int64_list.value, self.root_symbols)
        self.assertEqual(features["root_indices"].int64_list.value, self.root_indices)
        self.assertEqual(features["leaf_symbol"].int64_list.value, [self.leaf_symbol])


    def test_make_terminal_features(self):
        features = cds.ASTPathsWriter()._make_terminal_features(
            self.terminal_symbols,
            self.terminal_indices
        )

        self.assertEqual(features["terminal_symbols"].int64_list.value, self.terminal_symbols)
        self.assertEqual(features["terminal_indices"].int64_list.value, self.terminal_indices)


    def test_make_sequence_features(self):
        features = cds.ASTPathsWriter()._make_sequence_features(
            self.path_symbols,
            self.path_indices
        )

        for idx, symbols in enumerate(self.path_symbols):
            self.assertEqual(features["path_symbols"].feature[idx].int64_list.value, symbols)

        for idx, indices in enumerate(self.path_indices):
            self.assertEqual(features["path_indices"].feature[idx].int64_list.value, indices)


    def test_make_features(self):
        context, sequence = cds.ASTPathsWriter(separate_terminals=False)._make_features(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        self.assertEqual(len(context), 3)
        self.assertEqual(len(sequence), 2)

        self.assertEqual(context["root_symbols"].int64_list.value, self.root_symbols)
        self.assertEqual(context["root_indices"].int64_list.value, self.root_indices)
        self.assertEqual(context["leaf_symbol"].int64_list.value, [self.leaf_symbol])

        for idx, symbols in enumerate(self.path_symbols):
            self.assertEqual(sequence["path_symbols"].feature[idx].int64_list.value, symbols)

        for idx, indices in enumerate(self.path_indices):
            self.assertEqual(sequence["path_indices"].feature[idx].int64_list.value, indices)


    def test_make_features_separate(self):
        context, sequence = cds.ASTPathsWriter(separate_terminals=True)._make_features(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        self.assertEqual(len(context), 5)
        self.assertEqual(len(sequence), 2)

        self.assertEqual(context["terminal_symbols"].int64_list.value, self.terminal_symbols)
        self.assertEqual(context["terminal_indices"].int64_list.value, self.terminal_indices)
        self.assertEqual(context["root_symbols"].int64_list.value, self.root_symbols)
        self.assertEqual(context["root_indices"].int64_list.value, self.root_indices)
        self.assertEqual(context["leaf_symbol"].int64_list.value, [self.leaf_symbol])

        for idx, symbols in enumerate(self.path_symbols):
            self.assertEqual(sequence["path_symbols"].feature[idx].int64_list.value, symbols[1:])

        for idx, indices in enumerate(self.path_indices):
            self.assertEqual(sequence["path_indices"].feature[idx].int64_list.value, indices[1:])


    def test_record_roundtrip(self):
        record = cds.ASTPathsWriter(separate_terminals=False).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        context, sequence = tf.io.parse_single_sequence_example(
            record.SerializeToString(),
            context_features=cds.ASTPathsReader.CONTEXT_DESCRIPTION,
            sequence_features=cds.ASTPathsReader.SEQUENCE_DESCRIPTION
        )

        self.assertEqual(len(context), 3)
        self.assertEqual(len(sequence), 2)

        self.assertEqual(sequence["path_symbols"].to_list(), self.path_symbols)
        self.assertEqual(sequence["path_indices"].to_list(), self.path_indices)

        self.assertTrue(np.all(context["root_symbols"].numpy() == self.root_symbols))
        self.assertTrue(np.all(context["root_indices"].numpy() == self.root_indices))

        self.assertEqual(context["leaf_symbol"].numpy(), self.leaf_symbol)


    def test_separated_record_roundtrip(self):
        factory = cds.ASTPathsWriter(separate_terminals=True)

        record = factory.make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        context_description = {
            **cds.ASTPathsReader.SEPARATED_CONTEXT_DESCRIPTION,
            **cds.ASTPathsReader.CONTEXT_DESCRIPTION
        }

        context, sequence = tf.io.parse_single_sequence_example(
            record.SerializeToString(),
            context_features=context_description,
            sequence_features=cds.ASTPathsReader.SEQUENCE_DESCRIPTION
        )

        self.assertEqual(len(context), 5)
        self.assertEqual(len(sequence), 2)

        self.assertEqual(sequence["path_symbols"].to_list(), self.seperated_path_symbols)
        self.assertEqual(sequence["path_indices"].to_list(), self.seperated_path_indices)

        self.assertTrue(np.all(context["terminal_symbols"].numpy() == self.terminal_symbols))
        self.assertTrue(np.all(context["terminal_indices"].numpy() == self.terminal_indices))

        self.assertTrue(np.all(context["root_symbols"].numpy() == self.root_symbols))
        self.assertTrue(np.all(context["root_indices"].numpy() == self.root_indices))

        self.assertEqual(context["leaf_symbol"].numpy(), self.leaf_symbol)


    def test_prepare_joint_features(self):
        context = {
            "root_symbols": tf.constant(self.root_symbols),
            "root_indices": tf.constant(self.root_indices),
            "leaf_symbol": tf.constant(self.leaf_symbol)
        }

        sequence = {
            "path_symbols": tf.ragged.constant(self.path_symbols),
            "path_indices": tf.ragged.constant(self.path_indices)
        }

        dataset = tf.data.TFRecordDataset.from_tensors((context, sequence))
        dataset = dataset.map(cds.ASTPathsReader(separate_terminals=False)._prepare_features)

        for idx, (data, label) in enumerate(dataset):
            self.assertEqual(idx, 0) # The dataset should only contain one element
            self.assertEqual(len(data), 4)

            self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))
            self.assertTrue(np.all(data[1].numpy() == self.padded_path_indices))

            self.assertTrue(np.all(data[2].numpy() == self.root_symbols))
            self.assertTrue(np.all(data[3].numpy() == self.root_indices))

            self.assertEqual(label.numpy(), self.leaf_symbol)


    def test_prepare_separated_features(self):
        context = {
            "terminal_symbols": tf.constant(self.terminal_symbols),
            "terminal_indices": tf.constant(self.terminal_indices),
            "root_symbols": tf.constant(self.root_symbols),
            "root_indices": tf.constant(self.root_indices),
            "leaf_symbol": tf.constant(self.leaf_symbol)
        }

        sequence = {
            "path_symbols": tf.ragged.constant(self.seperated_path_symbols),
            "path_indices": tf.ragged.constant(self.seperated_path_indices)
        }

        dataset = tf.data.TFRecordDataset.from_tensors((context, sequence))
        dataset = dataset.map(cds.ASTPathsReader(separate_terminals=True)._prepare_features)

        for idx, (data, label) in enumerate(dataset):
            self.assertEqual(idx, 0) # The dataset should only contain one element
            self.assertEqual(len(data), 6)

            self.assertTrue(np.all(data[0].numpy() == self.terminal_symbols))
            self.assertTrue(np.all(data[1].numpy() == self.terminal_indices))

            self.assertTrue(np.all(data[2].numpy() == self.padded_seperated_path_symbols))
            self.assertTrue(np.all(data[3].numpy() == self.padded_seperated_path_indices))

            self.assertTrue(np.all(data[4].numpy() == self.root_symbols))
            self.assertTrue(np.all(data[5].numpy() == self.root_indices))

            self.assertEqual(label.numpy(), self.leaf_symbol)


    def test_apply_mask(self):
        factory = cds.ASTPathsWriter(separate_terminals=False)

        record = factory.make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        context, sequence = tf.io.parse_single_sequence_example(
            record.SerializeToString(),
            context_features=cds.ASTPathsReader.CONTEXT_DESCRIPTION,
            sequence_features=cds.ASTPathsReader.SEQUENCE_DESCRIPTION
        )

        mask = [True, False, True, True]

        dataset = tf.data.TFRecordDataset.from_tensors((context, sequence))
        dataset = dataset.map(cds.ASTPathsReader(separate_terminals=False)._prepare_features)
        dataset = dataset.map(cds.ASTPathsReader(separate_terminals=False, mask=mask)._apply_mask)

        for idx, (data, label) in enumerate(dataset):
            self.assertEqual(idx, 0) # The dataset should only contain one element
            self.assertEqual(len(data), 3)

            self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))

            self.assertTrue(np.all(data[1].numpy() == self.root_symbols))
            self.assertTrue(np.all(data[2].numpy() == self.root_indices))

            self.assertEqual(label.numpy(), self.leaf_symbol)


    def test_apply_mask_separated(self):
        factory = cds.ASTPathsWriter(separate_terminals=True)

        record = factory.make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        context_description = {
            **cds.ASTPathsReader.SEPARATED_CONTEXT_DESCRIPTION,
            **cds.ASTPathsReader.CONTEXT_DESCRIPTION
        }

        context, sequence = tf.io.parse_single_sequence_example(
            record.SerializeToString(),
            context_features=context_description,
            sequence_features=cds.ASTPathsReader.SEQUENCE_DESCRIPTION
        )

        mask = [True, False, True, True, False, False]

        dataset = tf.data.TFRecordDataset.from_tensors((context, sequence))
        dataset = dataset.map(cds.ASTPathsReader(separate_terminals=True)._prepare_features)
        dataset = dataset.map(cds.ASTPathsReader(separate_terminals=True, mask=mask)._apply_mask)

        for idx, (data, label) in enumerate(dataset):
            self.assertEqual(idx, 0) # The dataset should only contain one element
            self.assertEqual(len(data), 3)

            self.assertTrue(np.all(data[0].numpy() == self.terminal_symbols))

            self.assertTrue(np.all(data[1].numpy() == self.padded_seperated_path_symbols))
            self.assertTrue(np.all(data[2].numpy() == self.padded_seperated_path_indices))

            self.assertEqual(label.numpy(), self.leaf_symbol)


    def test_load(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_load.tfrecord")

        record = cds.ASTPathsWriter(separate_terminals=False).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsReader(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                separate_terminals=False
            )([filename], compression="GZIP")

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 4)

                self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.padded_path_indices))

                self.assertTrue(np.all(data[2].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.root_indices))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)
                self.assertEqual(data[1].dtype, tf.int8)

                self.assertEqual(data[2].dtype, tf.int8)
                self.assertEqual(data[3].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)


    def test_load_separated(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_seperated_load.tfrecord")

        record = cds.ASTPathsWriter(separate_terminals=True).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsReader(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                separate_terminals=True
            )([filename], compression="GZIP")

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 6)

                self.assertTrue(np.all(data[0].numpy() == self.terminal_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.terminal_indices))

                self.assertTrue(np.all(data[2].numpy() == self.padded_seperated_path_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.padded_seperated_path_indices))

                self.assertTrue(np.all(data[4].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[5].numpy() == self.root_indices))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)
                self.assertEqual(data[1].dtype, tf.int8)

                self.assertEqual(data[2].dtype, tf.int8)
                self.assertEqual(data[3].dtype, tf.int8)

                self.assertEqual(data[4].dtype, tf.int8)
                self.assertEqual(data[5].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)


    def test_load_default_types(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_load.tfrecord")

        record = cds.ASTPathsWriter(separate_terminals=False).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsReader(separate_terminals=False)([filename], compression="GZIP")

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 4)

                self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.padded_path_indices))

                self.assertTrue(np.all(data[2].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.root_indices))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int64)
                self.assertEqual(data[1].dtype, tf.int64)

                self.assertEqual(data[2].dtype, tf.int64)
                self.assertEqual(data[3].dtype, tf.int64)

                self.assertEqual(label.dtype, tf.int64)
        finally:
            os.remove(filename)


    def test_load_separated_default_types(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_seperated_test_load.tfrecord")

        record = cds.ASTPathsWriter(separate_terminals=True).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsReader(separate_terminals=True)([filename], compression="GZIP")

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 6)

                self.assertTrue(np.all(data[0].numpy() == self.terminal_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.terminal_indices))

                self.assertTrue(np.all(data[2].numpy() == self.padded_seperated_path_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.padded_seperated_path_indices))

                self.assertTrue(np.all(data[4].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[5].numpy() == self.root_indices))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int64)
                self.assertEqual(data[1].dtype, tf.int64)

                self.assertEqual(data[2].dtype, tf.int64)
                self.assertEqual(data[3].dtype, tf.int64)

                self.assertEqual(data[4].dtype, tf.int64)
                self.assertEqual(data[5].dtype, tf.int64)

                self.assertEqual(label.dtype, tf.int64)
        finally:
            os.remove(filename)


    def test_load_joint_to_separated(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_load.tfrecord")

        record = cds.ASTPathsWriter(separate_terminals=False).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsReader(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                separate_terminals=False,
                apply_conversion=True
            )([filename], compression="GZIP")

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 6)

                self.assertTrue(np.all(data[0].numpy() == self.terminal_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.terminal_indices))

                self.assertTrue(np.all(data[2].numpy() == self.padded_seperated_path_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.padded_seperated_path_indices))

                self.assertTrue(np.all(data[4].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[5].numpy() == self.root_indices))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)
                self.assertEqual(data[1].dtype, tf.int8)

                self.assertEqual(data[2].dtype, tf.int8)
                self.assertEqual(data[3].dtype, tf.int8)

                self.assertEqual(data[4].dtype, tf.int8)
                self.assertEqual(data[5].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)


    def test_load_separated_to_joint(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_seperated_load.tfrecord")

        record = cds.ASTPathsWriter(separate_terminals=True).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsReader(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                separate_terminals=True,
                apply_conversion=True
            )([filename], compression="GZIP")

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 4)

                self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))
                self.assertTrue(np.all(data[1].numpy() == self.padded_path_indices))

                self.assertTrue(np.all(data[2].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[3].numpy() == self.root_indices))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)
                self.assertEqual(data[1].dtype, tf.int8)

                self.assertEqual(data[2].dtype, tf.int8)
                self.assertEqual(data[3].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)


    def test_load_masked(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_load.tfrecord")

        record = cds.ASTPathsWriter(separate_terminals=False).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsReader(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                separate_terminals=False,
                mask=[True, False, True, True]
            )([filename], compression="GZIP")

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 3)

                self.assertTrue(np.all(data[0].numpy() == self.padded_path_symbols))

                self.assertTrue(np.all(data[1].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[2].numpy() == self.root_indices))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)

                self.assertEqual(data[1].dtype, tf.int8)
                self.assertEqual(data[2].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)


    def test_load_separated_masked(self):
        path = os.path.join("tests", "data")
        filename = os.path.join("tests", "data", "python150k_test_seperated_load.tfrecord")

        record = cds.ASTPathsWriter(separate_terminals=True).make_record(
            self.path_symbols,
            self.path_indices,
            self.root_symbols,
            self.root_indices,
            self.leaf_symbol
        )

        try:
            with tf.io.TFRecordWriter(filename, options="GZIP") as writer:
                writer.write(record.SerializeToString())

            dataset = cds.ASTPathsReader(
                self.terminal_vocab_size,
                self.nonterminal_vocab_size,
                self.max_sibling_index,
                separate_terminals=True,
                mask=[True, False, True, True, True, True]
            )([filename], compression="GZIP")

            for idx, (data, label) in enumerate(dataset):
                self.assertEqual(idx, 0) # The dataset should only contain one element
                self.assertEqual(len(data), 5)

                self.assertTrue(np.all(data[0].numpy() == self.terminal_symbols))

                self.assertTrue(np.all(data[1].numpy() == self.padded_seperated_path_symbols))
                self.assertTrue(np.all(data[2].numpy() == self.padded_seperated_path_indices))

                self.assertTrue(np.all(data[3].numpy() == self.root_symbols))
                self.assertTrue(np.all(data[4].numpy() == self.root_indices))

                self.assertEqual(label.numpy(), self.leaf_symbol)

                self.assertEqual(data[0].dtype, tf.int8)

                self.assertEqual(data[1].dtype, tf.int8)
                self.assertEqual(data[2].dtype, tf.int8)

                self.assertEqual(data[3].dtype, tf.int8)
                self.assertEqual(data[4].dtype, tf.int8)

                self.assertEqual(label.dtype, tf.int8)
        finally:
            os.remove(filename)




if __name__ == "__main__":
    unittest.main(verbosity=2)
