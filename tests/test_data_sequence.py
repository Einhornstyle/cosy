#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import os

import numpy as np
import tensorflow as tf

import cosy.preprocessing as cp
import cosy.data as cds
import cosy.data.record_io as cdr




class TestTypeHelpers(unittest.TestCase):
    def test_get_dtype_handler_float(self):
        handler = cdr._get_dtype_handler("float")
        self.assertEqual(handler, cp.float_feature)


    def test_get_feature_dtype_float(self):
        dtype = cdr._get_feature_dtype("float")
        self.assertEqual(dtype, tf.dtypes.float32)


    def test_get_dtype_handler_string(self):
        handler = cdr._get_dtype_handler("str")
        self.assertEqual(handler, cp.str_feature)


    def test_get_feature_dtype_string(self):
        dtype = cdr._get_feature_dtype("str")
        self.assertEqual(dtype, tf.dtypes.string)


    def test_get_dtype_handler_int16(self):
        handler = cdr._get_dtype_handler("int16")
        self.assertEqual(handler, cp.int_feature)


    def test_get_feature_dtype_int16(self):
        dtype = cdr._get_feature_dtype("int16")
        self.assertEqual(dtype, tf.dtypes.int64)


    def test_get_dtype_handler_float_list(self):
        handler = cdr._get_dtype_handler("float", is_list=True)
        self.assertEqual(handler, cp.float_feature_list)


    def test_get_dtype_handler_string_list(self):
        handler = cdr._get_dtype_handler("str", is_list=True)
        self.assertEqual(handler, cp.str_feature_list)


    def test_get_dtype_handler_int16_list(self):
        handler = cdr._get_dtype_handler("int16", is_list=True)
        self.assertEqual(handler, cp.int_feature_list)


    def test_get_dtype_handler_raises(self):
        msg = "dtype must be one of 'int', 'uint8', 'int8', 'int16', 'int32', 'int64'"\
            ", 'float', 'bytes', 'str' or 'bool'"

        self.assertRaisesRegex(ValueError, msg, cdr._get_dtype_handler, "foobar")


    def test_get_feature_dtype_raises(self):
        msg = "dtype must be one of 'int', 'uint8', 'int8', 'int16', 'int32', 'int64'"\
            ", 'float', 'bytes', 'str' or 'bool'"

        self.assertRaisesRegex(ValueError, msg, cdr._get_feature_dtype, "foobar")


    def test_build_dtype_map(self):
        dtype_map = cdr._build_dtype_map(["int", "int32", "string", "uint8"])
        self.assertEqual(dtype_map, {1: tf.int32, 3: tf.uint8})


    def test_build_dtype_map_empty(self):
        dtype_map = cdr._build_dtype_map(["int", "float", "string", "float"])
        self.assertFalse(dtype_map)




class TestSequence(unittest.TestCase):
    def test_sequence_writer_init_single_seq(self):
        writer = cds.SequenceWriter()
        self.assertEqual(writer.dtypes, ["int"])
        self.assertEqual(writer.num_sequences, 1)
        self.assertEqual(writer._handler, [cp.int_feature])


    def test_sequence_writer_init_multi_seq_single_dtype(self):
        writer = cds.SequenceWriter(dtypes="float", num_sequences=3)
        self.assertEqual(writer.dtypes, ["float"]*3)
        self.assertEqual(writer.num_sequences, 3)
        self.assertEqual(writer._handler, [cp.float_feature]*3)


    def test_sequence_writer_init_multi_seq_multi_dtype(self):
        writer = cds.SequenceWriter(dtypes=["str", "int32", "float"], num_sequences=3)
        self.assertEqual(writer.dtypes, ["str", "int32", "float"])
        self.assertEqual(writer.num_sequences, 3)
        self.assertEqual(writer._handler, [cp.str_feature, cp.int_feature, cp.float_feature])


    def test_sequence_reader_init_single_seq(self):
        reader = cds.SequenceReader(dtypes="int8")
        self.assertEqual(reader.dtypes, ["int8"])
        self.assertEqual(reader.num_sequences, 1)
        self.assertEqual(reader._features, {"sequence_0": tf.io.RaggedFeature(tf.int64)})
        self.assertEqual(reader._cast_dtype_map, {0: tf.int8})


    def test_sequence_reader_init_multi_seq_single_dtype(self):
        reader = cds.SequenceReader(dtypes="int16", num_sequences=4)
        self.assertEqual(reader.dtypes, ["int16"]*4)
        self.assertEqual(reader.num_sequences, 4)
        self.assertEqual(reader._features, {
            "sequence_0": tf.io.RaggedFeature(tf.int64),
            "sequence_1": tf.io.RaggedFeature(tf.int64),
            "sequence_2": tf.io.RaggedFeature(tf.int64),
            "sequence_3": tf.io.RaggedFeature(tf.int64)
        })
        self.assertEqual(reader._cast_dtype_map, {
            0: tf.int16,
            1: tf.int16,
            2: tf.int16,
            3: tf.int16
        })


    def test_sequence_reader_init_multi_seq_multi_dtype(self):
        reader = cds.SequenceReader(dtypes=["string", "int32", "float", "uint8"], num_sequences=4)
        self.assertEqual(reader.dtypes, ["string", "int32", "float", "uint8"])
        self.assertEqual(reader.num_sequences, 4)
        self.assertEqual(reader._features, {
            "sequence_0": tf.io.RaggedFeature(tf.string),
            "sequence_1": tf.io.RaggedFeature(tf.int64),
            "sequence_2": tf.io.RaggedFeature(tf.float32),
            "sequence_3": tf.io.RaggedFeature(tf.int64)
        })
        self.assertEqual(reader._cast_dtype_map, {
            1: tf.int32,
            3: tf.uint8
        })


    def test_record_roundtrip_single_seq(self):
        writer = cds.SequenceWriter()
        reader = cds.SequenceReader()

        seq = list(range(10))

        record = writer.make_record(sequences=[seq])
        example = reader._parse_example(record.SerializeToString())

        self.assertTrue(all(example["sequence_0"].numpy() == seq))


    def test_record_roundtrip_single_seq_no_wrapper(self):
        writer = cds.SequenceWriter()
        reader = cds.SequenceReader()

        seq = list(range(10))

        record = writer.make_record(sequences=seq) # <= no brackets
        example = reader._parse_example(record.SerializeToString())

        self.assertTrue(all(example["sequence_0"].numpy() == seq))


    def test_record_roundtrip_multi_seq_single_dtype(self):
        writer = cds.SequenceWriter(dtypes="int16", num_sequences=3)
        reader = cds.SequenceReader(dtypes="int16", num_sequences=3)

        seq_0 = list(range(10))
        seq_1 = list(range(17))
        seq_2 = list(range(12))

        record = writer.make_record(sequences=[seq_0, seq_1, seq_2])
        example = reader._parse_example(record.SerializeToString())

        self.assertTrue(all(example["sequence_0"].numpy() == seq_0))
        self.assertTrue(all(example["sequence_1"].numpy() == seq_1))
        self.assertTrue(all(example["sequence_2"].numpy() == seq_2))


    def test_record_roundtrip_multi_seq_multi_dtype(self):
        writer = cds.SequenceWriter(dtypes=["int8", "int16", "string"], num_sequences=3)
        reader = cds.SequenceReader(dtypes=["int8", "int16", "string"], num_sequences=3)

        seq_0 = list(range(10))
        seq_1 = list(range(17))
        seq_2 = ["x"*i for i in range(7)]
        seq_2_target = [b"x"*i for i in range(7)]

        record = writer.make_record(sequences=[seq_0, seq_1, seq_2])
        example = reader._parse_example(record.SerializeToString())

        self.assertTrue(all(example["sequence_0"].numpy() == seq_0))
        self.assertTrue(all(example["sequence_1"].numpy() == seq_1))
        self.assertTrue(all(example["sequence_2"].numpy() == seq_2_target))


    def test_cast_dtype_multi_seq_multi_dtype(self):
        writer = cds.SequenceWriter(dtypes=["int8", "int16", "string"], num_sequences=3)
        reader = cds.SequenceReader(dtypes=["int8", "int16", "string"], num_sequences=3)

        seq_0 = list(range(10))
        seq_1 = list(range(17))
        seq_2 = ["x"*i for i in range(7)]
        seq_2_target = [b"x"*i for i in range(7)]

        record = writer.make_record(sequences=[seq_0, seq_1, seq_2])
        example = reader._parse_example(record.SerializeToString())
        tensors = reader._extract_tensors(example)

        self.assertTrue(all(tensors[0].numpy() == seq_0))
        self.assertTrue(all(tensors[1].numpy() == seq_1))
        self.assertTrue(all(tensors[2].numpy() == seq_2_target))

        self.assertEqual(tensors[0].dtype, tf.int8)
        self.assertEqual(tensors[1].dtype, tf.int16)
        self.assertEqual(tensors[2].dtype, tf.string)


    def test_cast_dtype_multi_seq_multi_dtype_with_bool(self):
        writer = cds.SequenceWriter(dtypes=["int8", "int16", "bool"], num_sequences=3)
        reader = cds.SequenceReader(dtypes=["int8", "int16", "bool"], num_sequences=3)

        seq_0 = list(range(10))
        seq_1 = list(range(17))
        seq_2 = list(i % 2 == 0 for i in range(12))

        record = writer.make_record(sequences=[seq_0, seq_1, seq_2])
        example = reader._parse_example(record.SerializeToString())
        tensors = reader._extract_tensors(example)

        self.assertTrue(all(tensors[0].numpy() == seq_0))
        self.assertTrue(all(tensors[1].numpy() == seq_1))
        self.assertTrue(all(tensors[2].numpy() == seq_2))

        self.assertEqual(tensors[0].dtype, tf.int8)
        self.assertEqual(tensors[1].dtype, tf.int16)
        self.assertEqual(tensors[2].dtype, tf.bool)


    def test_preprocess_single_seq(self):
        filename = os.path.join("tests", "data", "seq_preprocess_single_seq.tfrecord")

        sequences = [[i*j for j in range(10)] for i in range(12)]

        try:
            with cds.SequenceWriter(filename=filename) as writer:
                for seq in sequences:
                    writer(sequences=[seq])

            dataset = cds.SequenceReader()(filenames=filename)

            for tensor in dataset:
                idx = None

                for i, seq in enumerate(sequences):
                    if all(tensor.numpy() == seq):
                        idx = i
                        break

                sequences.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)


    def test_preprocess_single_seq_batch(self):
        filename = os.path.join("tests", "data", "seq_preprocess_single_seq_batch.tfrecord")

        sequences = [[i*j for j in range(10)] for i in range(12)]

        try:
            with cds.SequenceWriter(filename=filename) as writer:
                for seq in sequences:
                    writer(sequences=[seq])

            dataset = cds.SequenceReader()(filenames=filename, batch_size=2)

            for tensor in dataset:
                idx = None
                for i, seq in enumerate(sequences):
                    if all(tensor[0, ...].numpy() == seq):
                        idx = i
                        break
                sequences.pop(idx)

                idx = None
                for i, seq in enumerate(sequences):
                    if all(tensor[1, ...].numpy() == seq):
                        idx = i
                        break
                sequences.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)


    def test_preprocess_multi_seq_single_dtype(self):
        filename = os.path.join("tests", "data", "seq_preprocess_ms_st.tfrecord")

        sequences = [[[k*i*j for j in range(10)] for k in range(3)] for i in range(12)]

        try:
            with cds.SequenceWriter(dtypes="int32", num_sequences=3, filename=filename) as writer:
                for seqs in sequences:
                    writer(sequences=seqs)

            dataset = cds.SequenceReader(dtypes="int32", num_sequences=3)(filenames=filename)

            for tensors in dataset:
                idx = None

                for i, seqs in enumerate(sequences):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[1].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[2].numpy() == seq):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)


    def test_preprocess_multi_seq_single_dtype_batch(self):
        filename = os.path.join("tests", "data", "seq_preprocess_ms_st_batch.tfrecord")

        sequences = [[[k*i*j for j in range(10)] for k in range(3)] for i in range(12)]

        try:
            with cds.SequenceWriter(dtypes="int16", num_sequences=3, filename=filename) as writer:
                for seqs in sequences:
                    writer(sequences=seqs)

            reader = cds.SequenceReader(dtypes="int16", num_sequences=3)
            dataset = reader(filenames=filename, batch_size=2)

            for tensors in dataset:
                idx = None

                for i, seqs in enumerate(sequences):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0][0, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[1][0, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[2][0, ...].numpy() == seq):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)

                idx = None

                for i, seqs in enumerate(sequences):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0][1, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[1][1, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[2][1, ...].numpy() == seq):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)


    def test_preprocess_multi_seq_multi_dtype(self):
        filename = os.path.join("tests", "data", "seq_preprocess_ms_mt.tfrecord")

        sequences = [[[k*i*j for j in range(10)] for k in range(3)] for i in range(12)]
        dtypes = ["int16", "int", "int32"]

        try:
            with cds.SequenceWriter(dtypes=dtypes, num_sequences=3, filename=filename) as writer:
                for seqs in sequences:
                    writer(sequences=seqs)

            dataset = cds.SequenceReader(dtypes=dtypes, num_sequences=3)(filenames=filename)

            for tensors in dataset:
                idx = None

                for i, seqs in enumerate(sequences):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[1].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[2].numpy() == seq):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)


    def test_preprocess_multi_seq_multi_dtype_batch(self):
        filename = os.path.join("tests", "data", "seq_preprocess_ms_mt_batch.tfrecord")

        sequences = [[[k*i*j for j in range(10)] for k in range(3)] for i in range(12)]
        dtypes = ["int16", "int", "int32"]

        try:
            with cds.SequenceWriter(dtypes=dtypes, num_sequences=3, filename=filename) as writer:
                for seqs in sequences:
                    writer(sequences=seqs)

            reader = cds.SequenceReader(dtypes=dtypes, num_sequences=3)
            dataset = reader(filenames=filename, batch_size=2)

            for tensors in dataset:
                idx = None

                for i, seqs in enumerate(sequences):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0][0, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[1][0, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[2][0, ...].numpy() == seq):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)

                idx = None

                for i, seqs in enumerate(sequences):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0][1, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[1][1, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[2][1, ...].numpy() == seq):
                            match_idx.add(j)

                    if match_idx:
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)




class TestLabeledSequence(unittest.TestCase):
    def test_sequence_writer_init_single_seq(self):
        writer = cds.LabeledSequenceWriter()
        self.assertEqual(writer.dtypes, ["int"])
        self.assertEqual(writer.num_sequences, 1)
        self.assertEqual(writer._handler, [cp.int_feature])
        self.assertEqual(writer.label_dtype, "int")
        self.assertEqual(writer._label_handler, cp.int_feature)


    def test_sequence_writer_init_multi_seq_single_dtype(self):
        writer = cds.LabeledSequenceWriter(dtypes="float", num_sequences=3, label_dtype="int16")
        self.assertEqual(writer.dtypes, ["float"]*3)
        self.assertEqual(writer.num_sequences, 3)
        self.assertEqual(writer._handler, [cp.float_feature]*3)
        self.assertEqual(writer.label_dtype, "int16")
        self.assertEqual(writer._label_handler, cp.int_feature)


    def test_sequence_writer_init_multi_seq_multi_dtype(self):
        writer = cds.LabeledSequenceWriter(
            dtypes=["str", "int32", "float"],
            num_sequences=3,
            label_dtype="int32"
        )

        self.assertEqual(writer.dtypes, ["str", "int32", "float"])
        self.assertEqual(writer.num_sequences, 3)
        self.assertEqual(writer._handler, [cp.str_feature, cp.int_feature, cp.float_feature])
        self.assertEqual(writer.label_dtype, "int32")
        self.assertEqual(writer._label_handler, cp.int_feature)


    def test_sequence_reader_init_single_seq(self):
        reader = cds.LabeledSequenceReader(dtypes="int8")
        self.assertEqual(reader.dtypes, ["int8"])
        self.assertEqual(reader.num_sequences, 1)
        self.assertEqual(reader._features, {
            "sequence_0": tf.io.RaggedFeature(tf.int64),
            "label": tf.io.FixedLenFeature([], tf.int64)
        })
        self.assertEqual(reader._cast_dtype_map, {0: tf.int8})
        self.assertEqual(reader.label_dtype, "int")
        self.assertEqual(reader._cast_label_dtype, tf.int64)


    def test_sequence_reader_init_multi_seq_single_dtype(self):
        reader = cds.LabeledSequenceReader(dtypes="int16", num_sequences=4, label_dtype="int32")
        self.assertEqual(reader.dtypes, ["int16"]*4)
        self.assertEqual(reader.num_sequences, 4)
        self.assertEqual(reader._features, {
            "sequence_0": tf.io.RaggedFeature(tf.int64),
            "sequence_1": tf.io.RaggedFeature(tf.int64),
            "sequence_2": tf.io.RaggedFeature(tf.int64),
            "sequence_3": tf.io.RaggedFeature(tf.int64),
            "label": tf.io.FixedLenFeature([], tf.int64)
        })
        self.assertEqual(reader._cast_dtype_map, {
            0: tf.int16,
            1: tf.int16,
            2: tf.int16,
            3: tf.int16
        })
        self.assertEqual(reader.label_dtype, "int32")
        self.assertEqual(reader._cast_label_dtype, tf.int32)


    def test_sequence_reader_init_multi_seq_multi_dtype(self):
        reader = cds.LabeledSequenceReader(
            dtypes=["string", "int32", "float", "uint8"],
            num_sequences=4,
            label_dtype="uint8"
        )
        self.assertEqual(reader.dtypes, ["string", "int32", "float", "uint8"])
        self.assertEqual(reader.num_sequences, 4)
        self.assertEqual(reader._features, {
            "sequence_0": tf.io.RaggedFeature(tf.string),
            "sequence_1": tf.io.RaggedFeature(tf.int64),
            "sequence_2": tf.io.RaggedFeature(tf.float32),
            "sequence_3": tf.io.RaggedFeature(tf.int64),
            "label": tf.io.FixedLenFeature([], tf.int64)
        })
        self.assertEqual(reader._cast_dtype_map, {
            1: tf.int32,
            3: tf.uint8
        })
        self.assertEqual(reader.label_dtype, "uint8")
        self.assertEqual(reader._cast_label_dtype, tf.uint8)


    def test_record_roundtrip_single_seq(self):
        writer = cds.LabeledSequenceWriter()
        reader = cds.LabeledSequenceReader()

        seq = list(range(10))
        label = 7

        record = writer.make_record(sequences=[seq], label=7)
        example = reader._parse_example(record.SerializeToString())

        self.assertTrue(all(example["sequence_0"].numpy() == seq))
        self.assertEqual(example["label"], label)


    def test_record_roundtrip_single_seq_no_wrapper(self):
        writer = cds.LabeledSequenceWriter()
        reader = cds.LabeledSequenceReader()

        seq = list(range(10))
        label = 7

        record = writer.make_record(sequences=seq, label=7) # <= no brackets
        example = reader._parse_example(record.SerializeToString())

        self.assertTrue(all(example["sequence_0"].numpy() == seq))
        self.assertEqual(example["label"], label)


    def test_record_roundtrip_multi_seq_single_dtype(self):
        writer = cds.LabeledSequenceWriter(dtypes="int16", num_sequences=3, label_dtype="int16")
        reader = cds.LabeledSequenceReader(dtypes="int16", num_sequences=3, label_dtype="int16")

        seq_0 = list(range(10))
        seq_1 = list(range(17))
        seq_2 = list(range(12))
        label = 7

        record = writer.make_record(sequences=[seq_0, seq_1, seq_2], label=7)
        example = reader._parse_example(record.SerializeToString())

        self.assertTrue(all(example["sequence_0"].numpy() == seq_0))
        self.assertTrue(all(example["sequence_1"].numpy() == seq_1))
        self.assertTrue(all(example["sequence_2"].numpy() == seq_2))
        self.assertEqual(example["label"], label)


    def test_record_roundtrip_multi_seq_multi_dtype(self):
        writer = cds.LabeledSequenceWriter(dtypes=["int8", "int16", "string"], num_sequences=3)
        reader = cds.LabeledSequenceReader(dtypes=["int8", "int16", "string"], num_sequences=3)

        seq_0 = list(range(10))
        seq_1 = list(range(17))
        seq_2 = ["x"*i for i in range(7)]
        seq_2_target = [b"x"*i for i in range(7)]
        label = 7

        record = writer.make_record(sequences=[seq_0, seq_1, seq_2], label=7)
        example = reader._parse_example(record.SerializeToString())

        self.assertTrue(all(example["sequence_0"].numpy() == seq_0))
        self.assertTrue(all(example["sequence_1"].numpy() == seq_1))
        self.assertTrue(all(example["sequence_2"].numpy() == seq_2_target))
        self.assertEqual(example["label"], label)


    def test_cast_dtype_multi_seq_multi_dtype(self):
        writer = cds.LabeledSequenceWriter(
            dtypes=["int8", "int16", "string"],
            num_sequences=3,
            label_dtype="uint8"
        )
        reader = cds.LabeledSequenceReader(
            dtypes=["int8", "int16", "string"],
            num_sequences=3,
            label_dtype="uint8"
        )

        seq_0 = list(range(10))
        seq_1 = list(range(17))
        seq_2 = ["x"*i for i in range(7)]
        seq_2_target = [b"x"*i for i in range(7)]
        label = 12

        record = writer.make_record(sequences=[seq_0, seq_1, seq_2], label=12)
        example = reader._parse_example(record.SerializeToString())
        tensors = reader._extract_tensors(example)

        self.assertTrue(all(tensors[0][0].numpy() == seq_0))
        self.assertTrue(all(tensors[0][1].numpy() == seq_1))
        self.assertTrue(all(tensors[0][2].numpy() == seq_2_target))
        self.assertEqual(tensors[1], label)

        self.assertEqual(tensors[0][0].dtype, tf.int8)
        self.assertEqual(tensors[0][1].dtype, tf.int16)
        self.assertEqual(tensors[0][2].dtype, tf.string)
        self.assertEqual(tensors[1].dtype, tf.uint8)


    def test_preprocess_single_seq(self):
        filename = os.path.join("tests", "data", "l_seq_preprocess_single_seq.tfrecord")

        sequences = [[i*j for j in range(10)] for i in range(12)]
        labels = list(range(12))

        try:
            with cds.LabeledSequenceWriter(filename=filename) as writer:
                for seq, label in zip(sequences, labels):
                    writer(sequences=[seq], label=label)

            dataset = cds.LabeledSequenceReader()(filenames=filename)

            for tensor in dataset:
                idx = None

                for i, (seq, label) in enumerate(zip(sequences, labels)):
                    if all(tensor[0].numpy() == seq) and (tensor[1].numpy() == label):
                        idx = i
                        break

                sequences.pop(idx)
                labels.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)


    def test_preprocess_single_seq_batch(self):
        filename = os.path.join("tests", "data", "l_seq_preprocess_single_seq_batch.tfrecord")

        sequences = [[i*j for j in range(10)] for i in range(12)]
        labels = list(range(12))

        try:
            with cds.LabeledSequenceWriter(filename=filename) as writer:
                for seq, label in zip(sequences, labels):
                    writer(sequences=[seq], label=label)

            dataset = cds.LabeledSequenceReader()(filenames=filename, batch_size=2)

            for tensor in dataset:
                idx = None
                for i, (seq, label) in enumerate(zip(sequences, labels)):
                    if all(tensor[0][0, ...].numpy() == seq) and (tensor[1][0].numpy() == label):
                        idx = i
                        break
                sequences.pop(idx)
                labels.pop(idx)

                idx = None
                for i, (seq, label) in enumerate(zip(sequences, labels)):
                    if all(tensor[0][1, ...].numpy() == seq) and (tensor[1][1].numpy() == label):
                        idx = i
                        break
                sequences.pop(idx)
                labels.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)


    def test_preprocess_multi_seq_single_dtype(self):
        filename = os.path.join("tests", "data", "l_seq_preprocess_ms_st.tfrecord")

        sequences = [[[k*i*j for j in range(10)] for k in range(3)] for i in range(12)]
        labels = list(range(12))

        try:
            with cds.LabeledSequenceWriter(dtypes="int32", num_sequences=3, filename=filename) as writer:
                for seqs, label in zip(sequences, labels):
                    writer(sequences=seqs, label=label)

            dataset = cds.LabeledSequenceReader(dtypes="int32", num_sequences=3)(filenames=filename)

            for tensors in dataset:
                idx = None

                for i, (seqs, label) in enumerate(zip(sequences, labels)):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0][0].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][1].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][2].numpy() == seq):
                            match_idx.add(j)

                    if match_idx and (tensors[1].numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)
                labels.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)


    def test_preprocess_multi_seq_single_dtype_batch(self):
        filename = os.path.join("tests", "data", "l_seq_preprocess_ms_st_batch.tfrecord")

        sequences = [[[k*i*j for j in range(10)] for k in range(3)] for i in range(12)]
        labels = list(range(12))

        try:
            with cds.LabeledSequenceWriter("int16", 3, filename=filename) as writer:
                for seqs, label in zip(sequences, labels):
                    writer(sequences=seqs, label=label)

            reader = cds.LabeledSequenceReader(dtypes="int16", num_sequences=3)
            dataset = reader(filenames=filename, batch_size=2)

            for tensors in dataset:
                idx = None

                for i, (seqs, label) in enumerate(zip(sequences, labels)):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0][0][0, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][1][0, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][2][0, ...].numpy() == seq):
                            match_idx.add(j)

                    if match_idx and (tensors[1][0].numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)
                labels.pop(idx)

                idx = None

                for i, (seqs, label) in enumerate(zip(sequences, labels)):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0][0][1, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][1][1, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][2][1, ...].numpy() == seq):
                            match_idx.add(j)

                    if match_idx and (tensors[1][1].numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)
                labels.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)


    def test_preprocess_multi_seq_multi_dtype(self):
        filename = os.path.join("tests", "data", "l_seq_preprocess_ms_mt.tfrecord")

        sequences = [[[k*i*j for j in range(10)] for k in range(3)] for i in range(12)]
        labels = list(range(12))
        dtypes = ["int16", "int", "int32"]

        try:
            with cds.LabeledSequenceWriter(dtypes, 3, filename=filename) as writer:
                for seqs, label in zip(sequences, labels):
                    writer(sequences=seqs, label=label)

            dataset = cds.LabeledSequenceReader(dtypes=dtypes, num_sequences=3)(filenames=filename)

            for tensors in dataset:
                idx = None

                for i, (seqs, label) in enumerate(zip(sequences, labels)):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0][0].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][1].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][2].numpy() == seq):
                            match_idx.add(j)

                    if match_idx and (tensors[1].numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)
                labels.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)


    def test_preprocess_multi_seq_multi_dtype_batch(self):
        filename = os.path.join("tests", "data", "l_seq_preprocess_ms_mt_batch.tfrecord")

        sequences = [[[k*i*j for j in range(10)] for k in range(3)] for i in range(12)]
        labels = list(range(12))
        dtypes = ["int16", "int", "int32"]

        try:
            with cds.LabeledSequenceWriter(dtypes, 3, filename=filename) as writer:
                for seqs, label in zip(sequences, labels):
                    writer(sequences=seqs, label=label)

            reader = cds.LabeledSequenceReader(dtypes=dtypes, num_sequences=3)
            dataset = reader(filenames=filename, batch_size=2)

            for tensors in dataset:
                idx = None

                for i, (seqs, label) in enumerate(zip(sequences, labels)):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0][0][0, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][1][0, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][2][0, ...].numpy() == seq):
                            match_idx.add(j)

                    if match_idx and (tensors[1][0].numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)
                labels.pop(idx)

                idx = None

                for i, (seqs, label) in enumerate(zip(sequences, labels)):
                    match_idx = set()

                    for j, seq in enumerate(seqs):
                        if all(tensors[0][0][1, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][1][1, ...].numpy() == seq):
                            match_idx.add(j)

                        if all(tensors[0][2][1, ...].numpy() == seq):
                            match_idx.add(j)

                    if match_idx and (tensors[1][1].numpy() == label):
                        self.assertEqual(len(match_idx), 3)
                        idx = i
                        break

                sequences.pop(idx)
                labels.pop(idx)

            self.assertFalse(sequences)
        finally:
            os.remove(filename)




if __name__ == "__main__":
    unittest.main(verbosity=2)
