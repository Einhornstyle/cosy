#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import unittest
import numpy as np
import tensorflow as tf
from cosy import layers




class TestNCEMasking(unittest.TestCase):
    def _make_terminals(self):
        self.terminal_symbols = tf.random.uniform((64, 7), dtype=np.int64, minval=1, maxval=1000)

        self.terminal_mask = tf.concat([
            tf.constant(1, dtype=np.int64, shape=(24, 7)),
            tf.constant(0, dtype=np.int64, shape=(12, 7)),
            tf.constant(1, dtype=np.int64, shape=(18, 7)),
            tf.constant(0, dtype=np.int64, shape=(10, 7))
        ], axis=0)

        self.terminal_symbols = tf.multiply(self.terminal_symbols, self.terminal_mask)

        self.terminal_inputs = self.terminal_symbols
        self.terminal_shapes = self.terminal_symbols.shape


    def _make_nonterminals(self):
        self.nonterminal_symbols = tf.random.uniform((64, 7, 14), dtype=np.int64, minval=1, maxval=300)
        self.nonterminal_indices = tf.random.uniform((64, 7, 14), dtype=np.int64, minval=1, maxval=200)

        self.nonterminal_mask = tf.concat([
            tf.constant(1, dtype=np.int64, shape=(24, 7, 14)),
            tf.concat([
                tf.constant(1, dtype=np.int64, shape=(30, 7, 10)),
                tf.constant(0, dtype=np.int64, shape=(30, 7, 4)),
            ], axis=2),
            tf.constant(0, dtype=np.int64, shape=(10, 7, 14))
        ], axis=0)

        self.nonterminal_symbols = tf.multiply(self.nonterminal_symbols, self.nonterminal_mask)
        self.nonterminal_indices = tf.multiply(self.nonterminal_indices, self.nonterminal_mask)

        self.nonterminal_inputs = (self.nonterminal_symbols, self.nonterminal_indices)
        self.nonterminal_shapes = (self.nonterminal_symbols.shape, self.nonterminal_indices.shape)


    def _make_roots(self):
        self.root_symbols = tf.random.uniform((64, 5), dtype=np.int64, minval=1, maxval=300)
        self.root_indices = tf.random.uniform((64, 5), dtype=np.int64, minval=1, maxval=200)

        self.root_mask = tf.concat([
            tf.constant(1, dtype=np.int64, shape=(10, 5)),
            tf.concat([
                tf.constant(1, dtype=np.int64, shape=(32, 2)),
                tf.constant(0, dtype=np.int64, shape=(32, 3)),
            ], axis=1),
            tf.concat([
                tf.constant(1, dtype=np.int64, shape=(12, 4)),
                tf.constant(0, dtype=np.int64, shape=(12, 1)),
            ], axis=1),
            tf.constant(0, dtype=np.int64, shape=(10, 5))
        ], axis=0)

        self.root_symbols = tf.multiply(self.root_symbols, self.root_mask)
        self.root_indices = tf.multiply(self.root_indices, self.root_mask)

        self.root_inputs = (self.root_symbols, self.root_indices)
        self.root_shapes = (self.root_symbols.shape, self.root_indices.shape)


    def _init(self):
        self._make_terminals()
        self._make_nonterminals()
        self._make_roots()

        assert tf.math.count_nonzero(self.terminal_symbols) == 294
        assert tf.math.count_nonzero(self.nonterminal_symbols) == 4452
        assert tf.math.count_nonzero(self.nonterminal_indices) == 4452
        assert tf.math.count_nonzero(self.root_symbols) == 162
        assert tf.math.count_nonzero(self.root_indices) == 162

        self.inputs = (self.terminal_inputs, self.nonterminal_inputs, self.root_inputs)
        self.inputs_shape = (self.terminal_shapes, self.nonterminal_shapes, self.root_shapes)

        self.input_symbols = (self.terminal_symbols, self.nonterminal_symbols, self.root_symbols)
        self.input_symbols_shape = (
            self.terminal_shapes[0],
            self.nonterminal_shapes[0],
            self.root_shapes[0]
        )

        self.terminal_vocab_size = 1000
        self.nonterminal_vocab_size = 300

        self.max_terminal_index = 200
        self.max_nonterminal_index = 200

        self.terminal_vocab_dimension = 256
        self.nonterminal_vocab_dimension = 256

        self.terminal_index_dimension = 128
        self.nonterminal_index_dimension = 128

        self.terminal_embedding = tf.keras.layers.Embedding(
            self.terminal_vocab_size,
            self.terminal_vocab_dimension + self.terminal_index_dimension,
            mask_zero=True
        )

        self.nonterminal_embedding = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
        )

        self.terminal_embedding_no_masking = tf.keras.layers.Embedding(
            self.terminal_vocab_size,
            self.terminal_vocab_dimension + self.terminal_index_dimension,
            mask_zero=False
        )

        self.nonterminal_embedding_no_masking = layers.SymbolEmbedding(
            self.nonterminal_vocab_size,
            self.max_nonterminal_index,
            self.nonterminal_vocab_dimension,
            self.nonterminal_index_dimension,
            mask_zero=False
        )

        self.leaf_path_embedding = tf.keras.layers.GlobalAveragePooling1D()

        self.root_path_embedding = tf.keras.layers.GlobalAveragePooling1D()


    def setUp(self):
        self._init()
        self.use_joint_embedding = False


    def test_root_mask_shape(self):
        nce = layers.NCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=True,
            apply_root_mask=False,
            use_joint_embedding=self.use_joint_embedding
        )

        mask = nce.nonterminal_embedding.compute_mask(self.root_inputs)

        self.assertEqual(mask.shape, (64, 5))


    def test_root_mask(self):
        nce = layers.NCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=False,
            apply_root_mask=True,
            use_joint_embedding=self.use_joint_embedding
        )

        mask = tf.cast(nce.nonterminal_embedding.compute_mask(self.root_inputs), tf.int64)
        self.assertTrue(tf.reduce_all(tf.equal(mask, self.root_mask)).numpy())


    def test_leaf_mask_shape(self):
        nce = layers.NCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=True,
            apply_root_mask=False,
            use_joint_embedding=self.use_joint_embedding
        )

        mask = nce._compute_joint_mask(self.terminal_inputs, self.nonterminal_inputs)

        self.assertEqual(mask.shape, (64, 7, 15))


    def test_leaf_mask(self):
        nce = layers.NCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=True,
            apply_root_mask=False,
            use_joint_embedding=self.use_joint_embedding
        )

        mask = tf.cast(nce._compute_joint_mask(self.terminal_inputs, self.nonterminal_inputs), tf.int64)
        target = tf.concat([self.terminal_mask[:,:,tf.newaxis], self.nonterminal_mask], axis=2)

        self.assertEqual(mask.shape, target.shape)
        self.assertTrue(tf.reduce_all(tf.equal(mask, target)).numpy())


    def test_no_root_masking(self):
        nce = layers.NCE(
            self.terminal_embedding_no_masking,
            self.nonterminal_embedding_no_masking,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=False,
            apply_root_mask=False,
            use_joint_embedding=self.use_joint_embedding
        )

        nce_no_apply = layers.NCE(
            self.terminal_embedding_no_masking,
            self.nonterminal_embedding_no_masking,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=False,
            apply_root_mask=True,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce(self.inputs)
        output_no_apply = nce_no_apply(self.inputs)

        self.assertEqual(output[1].shape, output_no_apply[1].shape)
        self.assertTrue(tf.reduce_all(tf.equal(output[1], output_no_apply[1])).numpy())


    def test_no_leaf_masking(self):
        nce = layers.NCE(
            self.terminal_embedding_no_masking,
            self.nonterminal_embedding_no_masking,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=False,
            apply_root_mask=False,
            use_joint_embedding=self.use_joint_embedding
        )

        nce_no_apply = layers.NCE(
            self.terminal_embedding_no_masking,
            self.nonterminal_embedding_no_masking,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=True,
            apply_root_mask=False,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce(self.inputs)
        output_no_apply = nce_no_apply(self.inputs)

        self.assertEqual(output[0].shape, output_no_apply[0].shape)
        self.assertTrue(tf.reduce_all(tf.equal(output[0], output_no_apply[0])).numpy())


    def test_with_root_masking(self):
        nce = layers.NCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=False,
            apply_root_mask=False,
            use_joint_embedding=self.use_joint_embedding
        )

        nce_no_apply = layers.NCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=False,
            apply_root_mask=True,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce(self.inputs)
        output_no_apply = nce_no_apply(self.inputs)

        self.assertEqual(output[1].shape, output_no_apply[1].shape)
        self.assertFalse(tf.reduce_all(tf.equal(output[1], output_no_apply[1])).numpy())


    def test_with_leaf_masking(self):
        nce = layers.NCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=False,
            apply_root_mask=False,
            use_joint_embedding=self.use_joint_embedding
        )

        nce_no_apply = layers.NCE(
            self.terminal_embedding,
            self.nonterminal_embedding,
            self.leaf_path_embedding,
            self.root_path_embedding,
            apply_leaf_mask=True,
            apply_root_mask=False,
            use_joint_embedding=self.use_joint_embedding
        )

        output = nce(self.inputs)
        output_no_apply = nce_no_apply(self.inputs)

        self.assertEqual(output[0].shape, output_no_apply[0].shape)
        self.assertFalse(tf.reduce_all(tf.equal(output[0], output_no_apply[0])).numpy())




class TestJointNCEMasking(TestNCEMasking):
    def setUp(self):
        self._init()
        self.use_joint_embedding = True




if __name__ == "__main__":
    unittest.main(verbosity=2)
