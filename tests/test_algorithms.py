# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import math
import unittest
import numpy as np
import tensorflow as tf
from tensorflow import keras as K

from cosy.grammar import BNF
from cosy.layers import TransformerEncoder

from cosy.algorithms import (
    min_depths,
    min_widths,
    max_depths,
    max_widths,
    uniform_rule_probabilities,
    greedy_sequence_prediction,
    FeedbackBeamSearch
)




class TestDepths(unittest.TestCase):
    def setUp(self):
        self.bnf = BNF()
        self.bnf.add("S", [["A"], ["B"]])
        self.bnf.add("A", [["A"], ["a"]])
        self.bnf.add("B", [["C"]])
        self.bnf.add("C", [["c"]])
        self.bnf.start_symbol = "S"


    def test_min_depths(self):
        depths = min_depths(self.bnf)
        self.assertEqual(depths["S"], 2)
        self.assertEqual(depths["A"], 1)
        self.assertEqual(depths["B"], 2)
        self.assertEqual(depths["C"], 1)


    def test_numeric_min_depths(self):
        depths = min_depths(self.bnf.numeric)
        self.assertEqual(depths[0], 2)
        self.assertEqual(depths[1], 1)
        self.assertEqual(depths[2], 2)
        self.assertEqual(depths[4], 1)


    def test_raises_min_depths(self):
        bnf = BNF()
        bnf.add("S", [["A"], ["B"]])
        bnf.add("A", [["A"], ["'a'"]])
        bnf.add("B", [["C"]])
        bnf.add("C", [["C"]])

        depths = min_depths(bnf)
        self.assertEqual(depths["S"], 2)
        self.assertEqual(depths["A"], 1)
        self.assertEqual(depths["B"], math.inf)
        self.assertEqual(depths["C"], math.inf)


    def test_numeric_raises_min_depths(self):
        bnf = BNF()
        bnf.add("S", [["A"], ["B"]])
        bnf.add("A", [["A"], ["'a'"]])
        bnf.add("B", [["C"]])
        bnf.add("C", [["C"]])

        depths = min_depths(bnf.numeric)
        self.assertEqual(depths[0], 2)
        self.assertEqual(depths[1], 1)
        self.assertEqual(depths[2], math.inf)
        self.assertEqual(depths[4], math.inf)


    def test_max_depths(self):
        depths = max_depths(self.bnf)
        self.assertEqual(depths["S"], math.inf)
        self.assertEqual(depths["A"], math.inf)
        self.assertEqual(depths["B"], 2)
        self.assertEqual(depths["C"], 1)


    def test_numeric_max_depths(self):
        depths = max_depths(self.bnf.numeric)
        self.assertEqual(depths[0], math.inf)
        self.assertEqual(depths[1], math.inf)
        self.assertEqual(depths[2], 2)
        self.assertEqual(depths[4], 1)




class TestWidths(unittest.TestCase):
    def setUp(self):
        self.bnf = BNF()
        self.bnf.add("S", [["A"], ["B"]])
        self.bnf.add("A", [["A"], ["a"]])
        self.bnf.add("B", [["C"], []])
        self.bnf.add("C", [["c1", "c2"]])
        self.bnf.start_symbol = "S"


    def test_min_widths(self):
        widths = min_widths(self.bnf)
        self.assertEqual(widths["S"], 0)
        self.assertEqual(widths["A"], 1)
        self.assertEqual(widths["B"], 0)
        self.assertEqual(widths["C"], 2)


    def test_numeric_min_widths(self):
        widths = min_widths(self.bnf.numeric)
        self.assertEqual(widths[0], 0)
        self.assertEqual(widths[1], 1)
        self.assertEqual(widths[2], 0)
        self.assertEqual(widths[4], 2)


    def test_raises_min_widths(self):
        bnf = BNF()
        bnf.add("S", [["A"], ["B"]])
        bnf.add("A", [["A"], ["'a'"]])
        bnf.add("B", [["C"]])
        bnf.add("C", [["C"]])

        widths = min_widths(bnf)
        self.assertEqual(widths["S"], 1)
        self.assertEqual(widths["A"], 1)
        self.assertEqual(widths["B"], math.inf)
        self.assertEqual(widths["C"], math.inf)


    def test_numeric_raises_min_widths(self):
        bnf = BNF()
        bnf.add("S", [["A"], ["B"]])
        bnf.add("A", [["A"], ["'a'"]])
        bnf.add("B", [["C"]])
        bnf.add("C", [["C"]])

        widths = min_widths(bnf.numeric)
        self.assertEqual(widths[0], 1)
        self.assertEqual(widths[1], 1)
        self.assertEqual(widths[2], math.inf)
        self.assertEqual(widths[4], math.inf)


    def test_max_widths(self):
        widths = max_widths(self.bnf)
        self.assertEqual(widths["S"], 2)
        self.assertEqual(widths["A"], 1)
        self.assertEqual(widths["B"], 2)
        self.assertEqual(widths["C"], 2)


    def test_numeric_max_widths(self):
        widths = max_widths(self.bnf.numeric)
        self.assertEqual(widths[0], 2)
        self.assertEqual(widths[1], 1)
        self.assertEqual(widths[2], 2)
        self.assertEqual(widths[4], 2)


    def test_max_widths_infinity(self):
        bnf = BNF()
        bnf.add("S", [["a", "S", "a"], ["b", "S", "b"], ["A"], ["B"]])
        bnf.add("A", [["a", "a"]])
        bnf.add("B", [["b"]])
        bnf.start_symbol = "S"

        widths = max_widths(bnf)
        self.assertEqual(widths["S"], math.inf)
        self.assertEqual(widths["A"], 2)
        self.assertEqual(widths["B"], 1)


    def test_numeric_max_widths_infinity(self):
        bnf = BNF()
        bnf.add("S", [["a", "S", "a"], ["b", "S", "b"], ["A"], ["B"]])
        bnf.add("A", [["a", "a"]])
        bnf.add("B", [["b"]])
        bnf.start_symbol = "S"

        widths = max_widths(bnf.numeric)
        self.assertEqual(widths[0], math.inf)
        self.assertEqual(widths[3], 2)
        self.assertEqual(widths[4], 1)




def _greedy_sequence_prediction_trivial_model(x):
    res = []
    for i in range(tf.size(x)):
        res.append([0.0] * 11)
        res[-1][min(i + 1 , 10)] = 1.0

    return tf.constant(res, dtype=tf.float32)


class TestGreedySequencePrediction(unittest.TestCase):
    def setUp(self):
        self.symbol_list = tf.constant(list(range(11)), dtype=tf.int32)
        self.end_token = self.symbol_list[-1]


    def test_raises_rank_assertion(self):
        self.assertRaises(
            AssertionError,
            greedy_sequence_prediction,
            _greedy_sequence_prediction_trivial_model,
            tf.constant(1, shape=[3, 3, 3]),
            self.end_token
        )


    def test_prediction(self):
        result = greedy_sequence_prediction(
            _greedy_sequence_prediction_trivial_model,
            self.symbol_list[:3],
            self.end_token
        )

        self.assertTrue(tf.reduce_all(tf.equal(result, self.symbol_list[3:])))


    def test_raises_prediction_no_end_token(self):
        self.assertRaises(
            ValueError,
            greedy_sequence_prediction,
            _greedy_sequence_prediction_trivial_model,
            self.symbol_list[:3],
            end_token=21,
            max_length=50
        )


    def test_redundant_3th_dimension(self):
        result = greedy_sequence_prediction(
            lambda x: _greedy_sequence_prediction_trivial_model(x)[tf.newaxis, ...],
            self.symbol_list[:3],
            end_token=self.end_token
        )

        self.assertTrue(tf.reduce_all(tf.equal(result, self.symbol_list[3:])))


    def test_raises_invalid_3th_dimension(self):
        self.assertRaises(
            AssertionError,
            greedy_sequence_prediction,
            lambda x: tf.stack([_greedy_sequence_prediction_trivial_model(x)] * 3),
            self.symbol_list[:3],
            self.end_token
        )


    def test_raises_prediction_too_short(self):
        self.assertRaises(
            ValueError,
            greedy_sequence_prediction,
            _greedy_sequence_prediction_trivial_model,
            self.symbol_list[:3],
            self.end_token,
            max_length=2
        )




class TestFeedbackBeamSearchAuxiliaries(unittest.TestCase):
    def feedback_fn(self, inputs, mask=None):
        output = self.encoder(inputs, mask=mask, training=False)
        output = self.dense(output)

        return output


    def toy_feedback_fn(self, inputs, mask=None):
        output = tf.tile(inputs[:, :, tf.newaxis], [1, 1, 5])
        output = tf.add(output, list(range(5)))

        if mask is not None:
            mask = tf.tile(mask[:, :, tf.newaxis], [1, 1, 5])
            output = tf.multiply(output, mask)

        return output


    def emb_toy_feedback_fn(self, inputs, mask=None):
        val = tf.constant([1.0, 100.0, 2.0, 200.0, 10.0])[tf.newaxis, tf.newaxis, :]
        return inputs + tf.tile(val, [inputs.shape[0], 3, 1])


    def setUp(self):
        self.rtol = 1e-05
        self.atol = 1e-05

        self.inputs = tf.random.uniform((8, 16), minval=1, maxval=100, dtype=tf.int32)
        self.mask = tf.sequence_mask([10]*3 + [16]*3 + [4]*2, maxlen=16)

        self.toy_inputs = tf.constant([
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ], dtype=tf.float32)

        self.toy_mask = tf.sequence_mask([3, 2, 2], maxlen=3)

        self.encoder = TransformerEncoder(
            num_layers=2,
            dimension=52,
            num_heads=4,
            hidden_units=52
        )

        self.dense = K.layers.Dense(100)
        self.embedding_layer = K.layers.Embedding(100, 52, mask_zero=True)
        self.beam_width=4


    def test_create_beams(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None
        )

        beams = beam_search._create_beams(self.inputs)

        self.assertEqual(beams.shape, (8, self.beam_width, 16))

        for i in range(self.beam_width):
            self.assertTrue(np.all(beams[:,i,:] == self.inputs))


    def test_beam_roundtrip(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None
        )

        beams = beam_search._create_beams(self.inputs)

        split_beams = beam_search._split_beams(beams)
        merge_beams = beam_search._merge_beams(split_beams)

        self.assertTrue(np.all(beams == merge_beams))


    def test_beam_roundtrip_forward_pass(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None
        )

        beams = beam_search._create_beams(self.inputs)
        split_beams = beam_search._split_beams(beams)

        split_emb_beams = self.embedding_layer(split_beams)
        split_res_beams = self.feedback_fn(split_emb_beams)

        emb_target = self.embedding_layer(self.inputs)
        res_target = self.feedback_fn(emb_target)

        merge_emb_beams = beam_search._merge_beams(split_emb_beams)
        merge_res_beams = beam_search._merge_beams(split_res_beams)

        for i in range(self.beam_width):
            self.assertTrue(np.all(merge_emb_beams[:,i,...] == emb_target))

            self.assertTrue(np.all(np.isclose(
                merge_res_beams[:,i,...],
                res_target,
                rtol=self.rtol,
                atol=self.atol)
            ))


    def test_beam_roundtrip_forward_pass_masked(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None
        )

        beams = beam_search._create_beams(self.inputs)
        split_beams = beam_search._split_beams(beams)

        mask_beams = beam_search._create_beams(self.mask)
        split_mask_beams = beam_search._split_beams(mask_beams)

        split_emb_beams = self.embedding_layer(split_beams)
        split_res_beams = self.feedback_fn(split_emb_beams, mask=split_mask_beams)

        emb_target = self.embedding_layer(self.inputs)
        res_target = self.feedback_fn(emb_target, mask=self.mask)

        merge_emb_beams = beam_search._merge_beams(split_emb_beams)
        merge_res_beams = beam_search._merge_beams(split_res_beams)

        for i in range(self.beam_width):
            self.assertTrue(np.all(merge_emb_beams[:,i,...] == emb_target))

            self.assertTrue(np.all(np.isclose(
                merge_res_beams[:,i,...],
                res_target,
                rtol=self.rtol,
                atol=self.atol)
            ))


    def test_grow_sequences_2D_speed_1(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None
        )

        output = beam_search._grow_sequences(self.toy_inputs)

        self.assertEqual(output.shape.as_list(), [3, 4])

        for i in range(3):
            self.assertTrue(np.all(output[i,:-1] == self.toy_inputs[i,:]))
            self.assertEqual(output[i,-1], 0.0)


    def test_grow_sequences_2D_speed_3(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None,
            memory_growth_speed=3
        )

        output = beam_search._grow_sequences(self.toy_inputs)

        self.assertEqual(output.shape.as_list(), [3, 6])

        for i in range(3):
            self.assertTrue(np.all(output[i,:-3] == self.toy_inputs[i,:]))
            self.assertTrue(np.all(output[i,3:] == [0.0] * 3))


    def test_grow_sequences_3D_speed_1(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None
        )

        toy_inputs = tf.constant(self.toy_inputs)[..., tf.newaxis]
        toy_inputs = tf.tile(toy_inputs, [1, 1, 7])

        output = beam_search._grow_sequences(toy_inputs)

        self.assertEqual(output.shape.as_list(), [3, 4, 7])

        for i in range(3):
            self.assertTrue(np.all(output[i,:-1,:] == toy_inputs[i,:,:]))
            self.assertTrue(np.all(output[i,-1, :] == 0.0))


    def test_grow_sequences_3D_speed_3(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None,
            memory_growth_speed=3
        )

        toy_inputs = tf.constant(self.toy_inputs)[..., tf.newaxis]
        toy_inputs = tf.tile(toy_inputs, [1, 1, 7])

        output = beam_search._grow_sequences(toy_inputs)

        self.assertEqual(output.shape.as_list(), [3, 6, 7])

        for i in range(3):
            self.assertTrue(np.all(output[i,:-3,:] == toy_inputs[i,:,:]))
            self.assertTrue(np.all(output[i,-3, :] == 0.0))


    def test_initialize_variables(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None
        )

        emb_inputs = self.embedding_layer(self.inputs)
        beam_search._initialize_variables(emb_inputs)

        length = (8 * self.beam_width)

        self.assertTrue(np.all(beam_search._inputs_offset == [15] * length))
        self.assertTrue(np.all(beam_search._pred_lengths == [0] * length))
        self.assertTrue(np.all(beam_search._log_probs == [0] * length))
        self.assertTrue(np.all(beam_search._active_beams == [True] * length))


    def test_initialize_variables_masked(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None
        )

        emb_inputs = self.embedding_layer(self.inputs)
        beam_search._initialize_variables(emb_inputs, mask=self.mask)

        length = (8 * self.beam_width)
        offset = [9]*(3*self.beam_width) + [15]*(3*self.beam_width) + [3]*(2*self.beam_width)

        self.assertTrue(np.all(beam_search._inputs_offset == offset))
        self.assertTrue(np.all(beam_search._pred_lengths == [0] * length))
        self.assertTrue(np.all(beam_search._log_probs == [0] * length))
        self.assertTrue(np.all(beam_search._active_beams == [True] * length))


    def test_initialize_variables_empty_sequence(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=None
        )

        mask = tf.sequence_mask([16]*3 + [0]*3 + [4]*2, maxlen=16)

        emb_inputs = self.embedding_layer(self.inputs)
        beam_search._initialize_variables(emb_inputs, mask=mask)

        length = (8 * self.beam_width)
        offset = [15]*(3*self.beam_width) + [0]*(3*self.beam_width) + [3]*(2*self.beam_width)

        self.assertTrue(np.all(beam_search._inputs_offset == offset))
        self.assertTrue(np.all(beam_search._pred_lengths == [0] * length))
        self.assertTrue(np.all(beam_search._log_probs == [0] * length))
        self.assertTrue(np.all(beam_search._active_beams == [True] * length))


    def test_initialize_beams(self):
        class EmbLayer:
            def __call__(self, inputs):
                return tf.tile(inputs[..., tf.newaxis], [1] * len(inputs.shape) + [5])

            def compute_mask(self, inputs, mask=None):
                return None

        emb_layer = EmbLayer()

        beam_search = FeedbackBeamSearch(
            feedback_fn=self.emb_toy_feedback_fn,
            embedding_layer=emb_layer,
            beam_width=self.beam_width,
            end_token=100
        )

        emb_inputs = emb_layer(self.toy_inputs)

        beams, mask = beam_search._initialize_beams(emb_inputs)

        for i in range(3):
            for j in range(4):
                self.assertTrue(np.all(beams[i*4 + j,:-1,:] == emb_inputs[i, ...]))

        self.assertEqual(mask, None)

        score_ref = tf.constant([
                5.313206, 4.634729, 2.5649493, 1.609438, 5.327876, 4.6634393,
                2.7725887, 2.0794415, 5.3423343, 4.691348, 2.944439, 2.3978953
        ])

        self.assertTrue(np.all(beam_search._pred_lengths == 1))
        self.assertTrue(np.all(beam_search._active_beams == True))
        self.assertTrue(np.all(beam_search._log_probs == beam_search._scores))
        self.assertTrue(np.all(beam_search._log_probs == score_ref))


    def test_feed(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=self.feedback_fn,
            embedding_layer=self.embedding_layer,
            beam_width=self.beam_width,
            end_token=100,
        )

        emb_inputs = self.embedding_layer(self.inputs)
        beam_search._initialize_variables(emb_inputs)

        input_beams = beam_search._create_beams(emb_inputs)
        split_input_beams = beam_search._split_beams(input_beams)

        pred = beam_search._feed(split_input_beams)

        self.assertEqual(pred.shape, (8 * self.beam_width, 16, 100))


    def test_feed_masked(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=self.feedback_fn,
            embedding_layer=self.embedding_layer,
            beam_width=self.beam_width,
            end_token=100,
        )

        emb_inputs = self.embedding_layer(self.inputs)
        beam_search._initialize_variables(emb_inputs)

        input_beams = beam_search._create_beams(emb_inputs)
        split_input_beams = beam_search._split_beams(input_beams)

        mask_beams = beam_search._create_beams(self.mask)
        split_mask_beams = beam_search._split_beams(mask_beams)

        pred = beam_search._feed(split_input_beams, mask=split_mask_beams)

        self.assertEqual(pred.shape, (8 * self.beam_width, 16, 100))


    def test_feed_loop(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=self.feedback_fn,
            embedding_layer=self.embedding_layer,
            beam_width=self.beam_width,
            end_token=100,
            num_parallel_beams=3
        )

        emb_inputs = self.embedding_layer(self.inputs)
        beam_search._initialize_variables(emb_inputs)

        input_beams = beam_search._create_beams(emb_inputs)
        split_input_beams = beam_search._split_beams(input_beams)

        pred = beam_search._feed(split_input_beams)

        self.assertEqual(pred.shape, (8 * self.beam_width, 16, 100))


    def test_feed_loop_masked(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=self.feedback_fn,
            embedding_layer=self.embedding_layer,
            beam_width=self.beam_width,
            end_token=100,
            num_parallel_beams=3
        )

        emb_inputs = self.embedding_layer(self.inputs)
        beam_search._initialize_variables(emb_inputs)

        input_beams = beam_search._create_beams(emb_inputs)
        split_input_beams = beam_search._split_beams(input_beams)

        mask_beams = beam_search._create_beams(self.mask)
        split_mask_beams = beam_search._split_beams(mask_beams)

        pred = beam_search._feed(split_input_beams, mask=split_mask_beams)

        self.assertEqual(pred.shape, (8 * self.beam_width, 16, 100))


    def test_score(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=100,
        )

        beam_search._initialize_variables(self.toy_inputs)

        input_beams = beam_search._create_beams(self.toy_inputs)
        split_input_beams = beam_search._split_beams(input_beams)

        pred_beams = self.toy_feedback_fn(split_input_beams)

        shape = self.toy_inputs.shape.as_list()
        offset = tf.constant([shape[-1] - 1] * (shape[0] * self.beam_width), dtype=tf.int64)

        token_probs = tf.gather(pred_beams, offset, batch_dims=1)

        scores = beam_search._score(token_log_probs=token_probs)
        self.assertTrue(np.all(scores == token_probs))


    def test_score_weighted(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=100,
            alpha=2.0,
            beta=3.0
        )

        beam_search._initialize_variables(self.toy_inputs)

        input_beams = beam_search._create_beams(self.toy_inputs)
        split_input_beams = beam_search._split_beams(input_beams)

        pred_beams = self.toy_feedback_fn(split_input_beams)

        shape = self.toy_inputs.shape.as_list()
        offset = tf.constant([shape[-1] - 1] * (shape[0] * self.beam_width), dtype=tf.int64)

        token_probs = tf.gather(pred_beams, offset, batch_dims=1)

        beam_search._pred_lengths.assign_add([1] * 12)
        scores = beam_search._score(token_log_probs=token_probs)

        self.assertTrue(np.all(np.isclose(
            scores,
            token_probs / (25 / 16),
            rtol=self.rtol,
            atol=self.atol)
        ))


    def test_score_inactive_beams(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=None,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=100,
        )

        beam_search._initialize_variables(self.toy_inputs)

        shape = self.toy_inputs.shape.as_list()
        length = shape[0] * self.beam_width

        beam_search._active_beams = tf.constant(
            [True] * (length//2) + [False] * (length//2),
            dtype=tf.bool
        )

        input_beams = beam_search._create_beams(self.toy_inputs)
        split_input_beams = beam_search._split_beams(input_beams)

        pred_beams = self.toy_feedback_fn(split_input_beams)

        offset = tf.constant([shape[-1] - 1] * length, dtype=tf.int64)

        token_probs = tf.gather(pred_beams, offset, batch_dims=1)

        active = tf.tile(beam_search._active_beams[..., tf.newaxis], [1, token_probs.shape[-1]])
        final_probs = token_probs * tf.cast(active, dtype=tf.float32)

        scores = beam_search._score(token_log_probs=token_probs)

        self.assertTrue(np.all(scores == final_probs))


    def test_compute_score_mask_pad_eq_0(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=self.toy_feedback_fn,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=100,
            padding_token=0,
        )

        beam_search._active_beams.assign(tf.constant([True, False, True, False]))

        scores = tf.constant([
            [1.0, 2.0, 3.0],
            [4.0, 5.0, 6.0],
            [7.0, 8.0, 9.0],
            [10.0, 11.0, 12.0]
        ])

        mask = beam_search._compute_score_mask(scores)

        target = tf.constant([
            [1.0] * 3,
            [1.0, beam_search._SCORE_PENALTY, beam_search._SCORE_PENALTY],
            [1.0] * 3,
            [1.0, beam_search._SCORE_PENALTY, beam_search._SCORE_PENALTY]
        ])

        self.assertTrue(np.all(mask == target))


    def test_compute_score_mask_pad_eq_1(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=self.toy_feedback_fn,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=100,
            padding_token=1,
        )

        beam_search._active_beams.assign(tf.constant([False, False, True, False]))

        scores = tf.constant([
            [1.0, 2.0, 3.0],
            [4.0, 5.0, 6.0],
            [7.0, 8.0, 9.0],
            [10.0, 11.0, 12.0]
        ])

        mask = beam_search._compute_score_mask(scores)

        target = tf.constant([
            [beam_search._SCORE_PENALTY, 1.0, beam_search._SCORE_PENALTY],
            [beam_search._SCORE_PENALTY, 1.0, beam_search._SCORE_PENALTY],
            [1.0] * 3,
            [beam_search._SCORE_PENALTY, 1.0, beam_search._SCORE_PENALTY]
        ])

        self.assertTrue(np.all(mask == target))


    def test_evaluate_beams(self):
        beam_search = FeedbackBeamSearch(
            feedback_fn=self.toy_feedback_fn,
            embedding_layer=None,
            beam_width=self.beam_width,
            end_token=100,
        )

        beam_search._initialize_variables(self.toy_inputs)

        input_beams = beam_search._create_beams(self.toy_inputs)
        split_input_beams = beam_search._split_beams(input_beams)

        pred_beams = self.toy_feedback_fn(split_input_beams)
        log_pred_beams = tf.math.log(pred_beams)

        beam_indices, tokens, log_probs, scores = beam_search._evaluate_beams(pred_beams)

        self.assertTrue(np.all(beam_indices == list(range(12))))
        self.assertTrue(np.all(tokens == [4] * 12))

        self.assertTrue(np.all(log_probs[0:4] == log_pred_beams[0][2][-1]))
        self.assertTrue(np.all(log_probs[4:8] == log_pred_beams[4][2][-1]))
        self.assertTrue(np.all(log_probs[8:12] == log_pred_beams[8][2][-1]))

        self.assertTrue(np.all(log_probs == scores))


    def test_update_beams(self):
        def emb_fun(x):
            return tf.tile(x[..., tf.newaxis], [1] * len(x.shape) + [5])

        beam_search = FeedbackBeamSearch(
            feedback_fn=self.toy_feedback_fn,
            embedding_layer=emb_fun,
            beam_width=self.beam_width,
            end_token=100
        )

        beam_search._initialize_variables(self.toy_inputs)

        input_beams = beam_search._create_beams(self.toy_inputs)
        split_input_beams = beam_search._split_beams(input_beams)

        pred_beams = self.toy_feedback_fn(split_input_beams)

        beam_indices, tokens, log_probs, scores = beam_search._evaluate_beams(pred_beams)

        emb_inputs = emb_fun(split_input_beams)
        output = beam_search._update_beams(emb_inputs, beam_indices, tokens, log_probs, scores)

        self.assertTrue(np.all(beam_search._log_probs == log_probs))
        self.assertTrue(np.all(beam_search._pred_lengths == 1))
        self.assertTrue(np.all(beam_search._active_beams == True))
        self.assertTrue(np.all(beam_search._scores == scores))

        self.assertEqual(output.shape, (12, 4, 5))
        self.assertTrue(np.all(output[:,:-1,:] == emb_inputs))
        self.assertTrue(np.all(output[:,-1,:] == 4.0))




class TestFeedbackBeamSearch(unittest.TestCase):
    def setUp(self):
        self.beam_width = 2
        self.end_token = 4
        self.padding_token = 0

        self.inputs = tf.constant([1, 2, 3])[:, tf.newaxis]

        self.targets = tf.constant([
            [
                [1, 2, 4, 0],
                [1, 3, 2, 4]
            ],
            [
                [2, 2, 3, 4],
                [2, 1, 3, 4]
            ],
            [
                [3, 2, 4, 0],
                [3, 1, 4, 0]
            ]
        ])

        pred = [
            [
                [0.0, 0.1, 0.8, 0.7, 0.2],
                [0.0, 0.4, 0.4, 0.1, 0.8],
                [0.7, 0.2, 0.4, 0.2, 0.1], # irrelevant
                [0.1, 0.2, 0.4, 0.6, 0.5]  # irrelevant
            ],
            [
                [0.0, 0.3, 0.7, 0.1, 0.2],
                [0.1, 0.2, 0.1, 0.8, 0.1],
                [0.3, 0.1, 0.2, 0.3, 0.9],
                [0.1, 0.7, 0.6, 0.1, 0.2]  #irrelevant
            ],
            [
                [0.0, 0.7, 0.8, 0.3, 0.2],
                [0.0, 0.4, 0.4, 0.2, 0.8],
                [0.7, 0.2, 0.4, 0.2, 0.1], #irrelevant
                [0.1, 0.2, 0.4, 0.6, 0.5]  #irrelevant
            ],
        ]

        self.pred = tf.constant(pred)

        self.pred_beams = tf.constant([
            pred[0],
            [
                [0.0, 0.1, 0.3, 0.9, 0.2],
                [0.1, 0.3, 0.8, 0.1, 0.1],
                [0.0, 0.2, 0.4, 0.5, 0.9],
                [0.0, 0.4, 0.5, 0.7, 0.1]  # irrelevant
            ],
            pred[1],
            [
                [0.0, 0.3, 0.6, 0.1, 0.8],
                [0.1, 0.2, 0.1, 0.8, 0.1], #irrelevant
                [0.3, 0.1, 0.2, 0.3, 0.9], #irrelevant
                [0.1, 0.7, 0.6, 0.1, 0.2]  #irrelevant
            ],
            pred[2],
            [
                [0.0, 0.9, 0.5, 0.3, 0.2],
                [0.0, 0.4, 0.4, 0.2, 0.8],
                [0.7, 0.2, 0.4, 0.2, 0.1], #irrelevant
                [0.1, 0.2, 0.4, 0.6, 0.5]  #irrelevant
            ],
        ])

        class EmbLayer:
            def __init__(self, with_mask=False):
                self.with_mask = with_mask

            def __call__(self, inputs):
                multiples = [1] * len(inputs.shape) + [3]
                return tf.cast(tf.tile(inputs[..., tf.newaxis], multiples), tf.float32)

            def compute_mask(self, inputs, mask=None):
                return inputs != 0 if self.with_mask else None

        self.emb_layer = EmbLayer(True)


    def test_default(self):
        def feedback_fn(inputs, mask=None):
            if inputs.shape[0] == 3:
                return self.pred[:, :inputs.shape[1], :]

            return self.pred_beams[:, :inputs.shape[1], :]

        beam_search = FeedbackBeamSearch(
            feedback_fn,
            self.emb_layer,
            self.beam_width,
            self.end_token,
            self.padding_token
        )

        seq, mask, scores = beam_search(self.inputs)

        self.assertTrue(np.all(seq[0, ...] == self.emb_layer(self.targets[0, ...])))
        self.assertTrue(np.all(seq[1, ...] == self.emb_layer(self.targets[1, ...])))
        self.assertTrue(np.all(seq[2, ...] == self.emb_layer(self.targets[2, ...])))




class TestUniformProbabilities(unittest.TestCase):
    def setUp(self):
        self.bnf = BNF()
        self.bnf.add("S", [["A"], ["B"]])
        self.bnf.add("A", [["A"], ["a"]])
        self.bnf.add("B", [["C"]])
        self.bnf.add("C", [["c"]])


    def test_nonterminal_coverage(self):
        probas = uniform_rule_probabilities(self.bnf)
        self.assertEqual(len(probas), 4)


    def test_derivation_coverage(self):
        probas = uniform_rule_probabilities(self.bnf)
        self.assertEqual(len(probas["S"]), 2)
        self.assertEqual(len(probas["A"]), 2)
        self.assertEqual(len(probas["B"]), 1)
        self.assertEqual(len(probas["C"]), 1)


    def test_probability_correctness(self):
        probas = uniform_rule_probabilities(self.bnf)
        self.assertEqual(probas["S"], [0.5, 0.5])
        self.assertEqual(probas["A"], [0.5, 0.5])
        self.assertEqual(probas["B"], [1.0])
        self.assertEqual(probas["C"], [1.0])




if __name__ == "__main__":
    unittest.main(verbosity="b")
