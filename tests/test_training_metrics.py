# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest

import numpy as np
import tensorflow as tf
from tensorflow import keras as K

from cosy import training as ct




class TestMeanReciprocalRank(unittest.TestCase):
    def test_init(self):
        metric = ct.MeanReciprocalRank()

        self.assertEqual(metric._sample_count.numpy(), 0.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 0.0)


    def test_update_state_1D(self):
        metric = ct.MeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state([0, 0, 1, 0, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state([0, 1, 0, 0, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 2.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.25)

        metric.update_state([0, 0, 0, 1, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.75)


    def test_properties(self):
        metric = ct.MeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state([0, 0, 1, 0, 0], y_pred)
        self.assertEqual(metric._sample_count, metric.count)
        self.assertEqual(metric._reciprocal_ranks, metric.total)

        metric.update_state([0, 1, 0, 0, 0], y_pred)
        self.assertEqual(metric._sample_count, metric.count)
        self.assertEqual(metric._reciprocal_ranks, metric.total)

        metric.update_state([0, 0, 0, 1, 0], y_pred)
        self.assertEqual(metric._sample_count, metric.count)
        self.assertEqual(metric._reciprocal_ranks, metric.total)


    def test_update_state_1D_max_rank(self):
        metric = ct.MeanReciprocalRank(max_rank=2)

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state([0, 0, 1, 0, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state([0, 1, 0, 0, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 2.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state([0, 0, 0, 1, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.5)


    def test_update_state_1D_float64(self):
        metric = ct.MeanReciprocalRank(dtype=tf.float64)

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state([0, 0, 1, 0, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state([0, 1, 0, 0, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 2.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.25)

        metric.update_state([0, 0, 0, 1, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.75)


    def test_update_state_1D_sample_weight(self):
        epsilon = 0.0000000001

        metric = ct.MeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state([0, 0, 1, 0, 0], y_pred, sample_weight=0.5)
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 0.5))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.5) < epsilon)

        metric.update_state([0, 1, 0, 0, 0], y_pred, sample_weight=0.1)
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 0.6))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.525) < epsilon)

        metric.update_state([0, 0, 0, 1, 0], y_pred, sample_weight=0.4)
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 1.0))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.725) < epsilon)


    def test_update_state_1D_sample_weight_float16(self):
        epsilon = 0.0000000001

        metric = ct.MeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state(
            [0, 0, 1, 0, 0],
            y_pred,
            sample_weight=tf.constant(0.5, dtype=tf.float16)
        )
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 0.5))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.5) < epsilon)

        metric.update_state(
            [0, 1, 0, 0, 0],
            y_pred,
            sample_weight=tf.constant(0.1, dtype=tf.float16)
        )
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 0.59, rtol=0.01, atol=0.01))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.525) < epsilon)

        metric.update_state(
            [0, 0, 0, 1, 0],
            y_pred,
            sample_weight=tf.constant(0.4, dtype=tf.float16)
        )
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 1.0, rtol=0.01, atol=0.01))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.725) < epsilon)


    def test_reset_states_1D(self):
        metric = ct.MeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state([0, 0, 1, 0, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state([0, 1, 0, 0, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 2.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.25)

        metric.reset_states()
        self.assertEqual(metric._sample_count.numpy(), 0.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 0.0)

        metric.update_state([0, 0, 0, 1, 0], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 0.5)


    def test_result_1D(self):
        metric = ct.MeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state([0, 0, 1, 0, 0], y_pred)
        self.assertEqual(metric.result(), 1.0)

        metric.update_state([0, 1, 0, 0, 0], y_pred)
        self.assertEqual(metric.result(), 1.25/2.0)

        metric.update_state([0, 1, 0, 0, 0], y_pred)
        self.assertEqual(metric.result(), 1.5/3.0)


    def test_update_state_2D(self):
        metric = ct.MeanReciprocalRank()

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        y_true_1 = [
            [0, 0, 1, 0, 0],
            [1, 0, 0, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_2 = [
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_3 = [
            [0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0]
        ]

        metric.update_state(y_true_1, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 2.0)

        metric.update_state(y_true_2, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 6.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 4.5)

        metric.update_state(y_true_3, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 9.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 6.25)


    def test_update_state_2D_max_rank(self):
        metric = ct.MeanReciprocalRank(max_rank=1)

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        y_true_1 = [
            [0, 0, 1, 0, 0],
            [1, 0, 0, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_2 = [
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_3 = [
            [0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0]
        ]

        metric.update_state(y_true_1, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state(y_true_2, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 6.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 3.0)

        metric.update_state(y_true_3, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 9.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 4.0)


    def test_update_state_2D_float64(self):
        metric = ct.MeanReciprocalRank(dtype=tf.float64)

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        y_true_1 = [
            [0, 0, 1, 0, 0],
            [1, 0, 0, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_2 = [
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_3 = [
            [0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0]
        ]

        metric.update_state(y_true_1, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 2.0)

        metric.update_state(y_true_2, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 6.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 4.5)

        metric.update_state(y_true_3, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 9.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 6.25)


    def test_update_state_2D_sample_weight_scalar(self):
        epsilon = 0.0000000001

        metric = ct.MeanReciprocalRank()

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        y_true_1 = [
            [0, 0, 1, 0, 0],
            [1, 0, 0, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_2 = [
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_3 = [
            [0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0]
        ]

        metric.update_state(y_true_1, y_pred, sample_weight=0.25)
        self.assertEqual(metric._sample_count.numpy(), 0.75)
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.5) < epsilon)

        metric.update_state(y_true_2, y_pred, sample_weight=0.5)
        self.assertEqual(metric._sample_count.numpy(), 2.25)
        self.assertTrue((metric._reciprocal_ranks.numpy() - 1.75) < epsilon)

        metric.update_state(y_true_3, y_pred, sample_weight=0.1)
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 2.55))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 1.925) < epsilon)


    def test_update_state_2D_sample_weight(self):
        epsilon = 0.0000000001

        metric = ct.MeanReciprocalRank()

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        y_true_1 = [
            [0, 0, 1, 0, 0],
            [1, 0, 0, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_2 = [
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_3 = [
            [0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0]
        ]

        metric.update_state(y_true_1, y_pred, sample_weight=[0.2, 0.4, 1.0])
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 1.6))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.9) < epsilon)

        metric.update_state(y_true_2, y_pred, sample_weight=[0.1, 0.4, 0.0])
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 2.1))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 1.5) < epsilon)

        metric.update_state(y_true_3, y_pred, sample_weight=[0.5, 0.5, 2.0])
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 5.1))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 3.875) < epsilon)


    def test_reset_states_2D(self):
        metric = ct.MeanReciprocalRank()

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        y_true_1 = [
            [0, 0, 1, 0, 0],
            [1, 0, 0, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_2 = [
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_3 = [
            [0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0]
        ]

        metric.update_state(y_true_1, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 2.0)

        metric.update_state(y_true_2, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 6.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 4.5)

        metric.reset_states()
        self.assertEqual(metric._sample_count.numpy(), 0.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 0.0)

        metric.update_state(y_true_3, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.75)


    def test_result_2D(self):
        metric = ct.MeanReciprocalRank()

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        y_true_1 = [
            [0, 0, 1, 0, 0],
            [1, 0, 0, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_2 = [
            [0, 0, 1, 0, 0],
            [0, 0, 1, 0, 0],
            [0, 1, 0, 0, 0]
        ]

        y_true_3 = [
            [0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0],
            [0, 0, 0, 1, 0]
        ]

        metric.update_state(y_true_1, y_pred)
        self.assertEqual(metric.result(), 2.0/3.0)

        metric.update_state(y_true_2, y_pred)
        self.assertEqual(metric.result(), 4.5/6.0)

        metric.update_state(y_true_3, y_pred)
        self.assertEqual(metric.result(), 6.25/9.0)




class TestSparseMeanReciprocalRank(unittest.TestCase):
    def test_init(self):
        metric = ct.SparseMeanReciprocalRank()

        self.assertEqual(metric._sample_count.numpy(), 0.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 0.0)


    def test_update_state_1D(self):
        metric = ct.SparseMeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state(2, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state(1, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 2.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.25)

        metric.update_state(3, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.75)


    def test_properties(self):
        metric = ct.MeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state(2, y_pred)
        self.assertEqual(metric._sample_count, metric.count)
        self.assertEqual(metric._reciprocal_ranks, metric.total)

        metric.update_state(1, y_pred)
        self.assertEqual(metric._sample_count, metric.count)
        self.assertEqual(metric._reciprocal_ranks, metric.total)

        metric.update_state(3, y_pred)
        self.assertEqual(metric._sample_count, metric.count)
        self.assertEqual(metric._reciprocal_ranks, metric.total)


    def test_update_state_1D_max_rank(self):
        metric = ct.SparseMeanReciprocalRank(max_rank=2)

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state(2, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state(1, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 2.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state(3, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.5)


    def test_update_state_1D_float64(self):
        metric = ct.SparseMeanReciprocalRank(dtype=tf.float64)

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state(2, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state(1, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 2.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.25)

        metric.update_state(3, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.75)


    def test_update_state_1D_sample_weight(self):
        epsilon = 0.0000000001

        metric = ct.SparseMeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state(2, y_pred, sample_weight=0.5)
        self.assertEqual(metric._sample_count.numpy(), 0.5)
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.5) < epsilon)

        metric.update_state(1, y_pred, sample_weight=0.1)
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 0.6))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.525) < epsilon)

        metric.update_state(3, y_pred, sample_weight=0.4)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.725) < epsilon)


    def test_update_state_1D_sample_weight_float16(self):
        epsilon = 0.0000000001

        metric = ct.SparseMeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state(2, y_pred, sample_weight=tf.constant(0.5, dtype=tf.float16))
        self.assertEqual(metric._sample_count.numpy(), 0.5)
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.5) < epsilon)

        metric.update_state(1, y_pred, sample_weight=tf.constant(0.1, dtype=tf.float16))
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 0.6, atol=0.001, rtol=0.001))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.525) < epsilon)

        metric.update_state(3, y_pred, sample_weight=tf.constant(0.4, dtype=tf.float16))
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 1.0, atol=0.001, rtol=0.001))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.725) < epsilon)


    def test_reset_states_1D(self):
        metric = ct.SparseMeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state(2, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state(1, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 2.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.25)

        metric.reset_states()
        self.assertEqual(metric._sample_count.numpy(), 0.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 0.0)

        metric.update_state(3, y_pred)
        self.assertEqual(metric._sample_count.numpy(), 1.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 0.5)


    def test_result_1D(self):
        metric = ct.SparseMeanReciprocalRank()

        y_pred = [0.2, 0.1, 0.7, 0.3, 0.1]

        metric.update_state(2, y_pred)
        self.assertEqual(metric.result(), 1.0)

        metric.update_state(1, y_pred)
        self.assertEqual(metric.result(), 1.25/2.0)

        metric.update_state(1, y_pred)
        self.assertEqual(metric.result(), 1.5/3.0)


    def test_update_state_2D(self):
        metric = ct.SparseMeanReciprocalRank()

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        metric.update_state([2, 0, 1], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 2.0)

        metric.update_state([2, 2, 1], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 6.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 4.5)

        metric.update_state([4, 0, 3], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 9.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 6.25)


    def test_update_state_2D_max_rank(self):
        metric = ct.SparseMeanReciprocalRank(max_rank=1)

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        metric.update_state([2, 0, 1], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.0)

        metric.update_state([2, 2, 1], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 6.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 3.0)

        metric.update_state([4, 0, 3], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 9.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 4.0)


    def test_update_state_2D_float64(self):
        metric = ct.SparseMeanReciprocalRank(dtype=tf.float64)

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        metric.update_state([2, 0, 1], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 2.0)

        metric.update_state([2, 2, 1], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 6.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 4.5)

        metric.update_state([4, 0, 3], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 9.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 6.25)


    def test_update_state_2D_sample_weight_scalar(self):
        epsilon = 0.0000000001

        metric = ct.SparseMeanReciprocalRank()

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        metric.update_state([2, 0, 1], y_pred, sample_weight=0.25)
        self.assertEqual(metric._sample_count.numpy(), 0.75)
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.5) < epsilon)

        metric.update_state([2, 2, 1], y_pred, sample_weight=0.5)
        self.assertEqual(metric._sample_count.numpy(), 2.25)
        self.assertTrue((metric._reciprocal_ranks.numpy() - 1.75) < epsilon)

        metric.update_state([4, 0, 3], y_pred, sample_weight=0.1)
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 2.55))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 1.925) < epsilon)


    def test_update_state_2D_sample_weight(self):
        epsilon = 0.0000000001

        metric = ct.SparseMeanReciprocalRank()

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        metric.update_state([2, 0, 1], y_pred, sample_weight=[0.2, 0.4, 1.0])
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 1.6))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 0.9) < epsilon)

        metric.update_state([2, 2, 1], y_pred, sample_weight=[0.1, 0.4, 0.0])
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 2.1))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 1.5) < epsilon)

        metric.update_state([4, 0, 3], y_pred, sample_weight=[0.5, 0.5, 2.0])
        self.assertTrue(np.isclose(metric._sample_count.numpy(), 5.1))
        self.assertTrue((metric._reciprocal_ranks.numpy() - 3.875) < epsilon)


    def test_reset_states_2D(self):
        metric = ct.SparseMeanReciprocalRank()

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        metric.update_state([2, 0, 1], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 2.0)

        metric.update_state([2, 2, 1], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 6.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 4.5)

        metric.reset_states()
        self.assertEqual(metric._sample_count.numpy(), 0.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 0.0)

        metric.update_state([4, 0, 3], y_pred)
        self.assertEqual(metric._sample_count.numpy(), 3.0)
        self.assertEqual(metric._reciprocal_ranks.numpy(), 1.75)


    def test_result_2D(self):
        metric = ct.SparseMeanReciprocalRank()

        y_pred = tf.constant([
            [0.5, 0.1, 0.7, 0.3, 0.1],
            [0.6, 0.4, 0.8, 0.0, 0.3],
            [0.2, 0.4, 0.2, 0.9, 0.1]
        ])

        metric.update_state([2, 0, 1], y_pred)
        self.assertEqual(metric.result(), 2.0/3.0)

        metric.update_state([2, 2, 1], y_pred)
        self.assertEqual(metric.result(), 4.5/6.0)

        metric.update_state([4, 0, 3], y_pred)
        self.assertEqual(metric.result(), 6.25/9.0)




if __name__ == "__main__":
    unittest.main(verbosity=2)
