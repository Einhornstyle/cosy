# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import os
import glob
import shutil
import unittest

import numpy as np
import tensorflow as tf
from tensorflow import keras as K
from tensorboard.plugins.hparams import api as hp

import cosy
from cosy import training as ct




def compare_hparams(x, y):
    if isinstance(x.domain, hp.RealInterval):
        if not isinstance(y.domain, hp.RealInterval):
            return False

        if x.domain.min_value != y.domain.min_value:
            return False

        if x.domain.max_value != y.domain.max_value:
            return False

    elif isinstance(x.domain, hp.IntInterval):
        if not isinstance(y.domain, hp.IntInterval):
            return False

        if x.domain.min_value != y.domain.min_value:
            return False

        if x.domain.max_value != y.domain.max_value:
            return False

    elif isinstance(x.domain, hp.Discrete):
        if not isinstance(y.domain, hp.Discrete):
            return False

        if len(x.domain.values) != len(y.domain.values):
            return False

        for x_val, y_val in zip(x.domain.values, y.domain.values):
            if x_val != y_val:
                return False
    else:
        return False

    if x.name != y.name:
        return False

    if x.display_name != y.display_name:
        return False

    if x.description != y.description:
        return False

    return True




class TestHyperParameterGenerators(unittest.TestCase):
    def setUp(self):
        self.hparams = {
            "foo": hp.HParam("foo-alias", hp.Discrete([12, 42, 7])),
            "bar": hp.HParam("bar-alias", hp.RealInterval(1.2, 5.5))
        }


    def test_grid_search(self):
        targets = [
            {"foo": 12, "bar": 1.2},
            {"foo": 12, "bar": 5.5},
            {"foo": 42, "bar": 1.2},
            {"foo": 42, "bar": 5.5},
            {"foo": 7, "bar": 1.2},
            {"foo": 7, "bar": 5.5},
        ]

        params = list(ct.grid_search(self.hparams))

        self.assertEqual(len(params), 6)

        for target in targets:
            self.assertTrue(target in params)




class TestHyperParameterTuningSetup(unittest.TestCase):
    def test_make_discrete_hparam(self):
        hparam = ct.HyperParameterTuning._make_hparam("foo", [1, 2, 3], True, alias="bar")
        target = hp.HParam("bar", hp.Discrete([1, 2, 3]))
        self.assertTrue(compare_hparams(hparam, target))


    def test_make_int_interval_hparam(self):
        hparam = ct.HyperParameterTuning._make_hparam("foo", [12, 42], False, alias="bar")
        target = hp.HParam("bar", hp.IntInterval(12, 42))
        self.assertTrue(compare_hparams(hparam, target))


    def test_make_real_interval_hparam(self):
        hparam = ct.HyperParameterTuning._make_hparam("foo", [1.2, 4.2], False, alias="bar")
        target = hp.HParam("bar", hp.RealInterval(1.2, 4.2))
        self.assertTrue(compare_hparams(hparam, target))


    def test_raises_make_interval_hparam(self):
        self.assertRaises(
            AssertionError,
            ct.HyperParameterTuning._make_hparam,
            "foo", [1, 2, 3], False, alias="bar"
        )


    def test_add_model_param(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertEqual(hpt._model_params, {})

        hpt.add_param("foo", 42)
        self.assertEqual(hpt._model_params, {"foo": 42})


    def test_add_compile_param(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertEqual(hpt._compile_params, {})

        hpt.add_param("foo", 42, model_param=False)
        self.assertEqual(hpt._compile_params, {"foo": 42})


    def test_add_discrete_model_hparam(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertEqual(hpt._model_hparams, {})

        hpt.add_hparam("foo", [12, 42], alias="bar")
        target = hp.HParam("bar", hp.Discrete([12, 42]))

        self.assertTrue("foo" in hpt._model_hparams)
        self.assertTrue(compare_hparams(hpt._model_hparams["foo"], target))


    def test_add_discrete_compile_hparam(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertEqual(hpt._compile_hparams, {})

        hpt.add_hparam("foo", [12, 42], alias="bar", model_param=False)
        target = hp.HParam("bar", hp.Discrete([12, 42]))

        self.assertTrue("foo" in hpt._compile_hparams)
        self.assertTrue(compare_hparams(hpt._compile_hparams["foo"], target))


    def test_add_real_interval_model_hparam(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertEqual(hpt._model_hparams, {})

        hpt.add_hparam("foo", [1.2, 4.2], discrete=False, alias="bar")
        target = hp.HParam("bar", hp.RealInterval(1.2, 4.2))

        self.assertTrue("foo" in hpt._model_hparams)
        self.assertTrue(compare_hparams(hpt._model_hparams["foo"], target))


    def test_add_real_interval_compile_hparam(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertEqual(hpt._compile_hparams, {})

        hpt.add_hparam("foo", [1.2, 4.2], discrete=False, alias="bar", model_param=False)
        target = hp.HParam("bar", hp.RealInterval(1.2, 4.2))

        self.assertTrue("foo" in hpt._compile_hparams)
        self.assertTrue(compare_hparams(hpt._compile_hparams["foo"], target))


    def test_raises_add_real_interval_model_hparam(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertEqual(hpt._model_hparams, {})

        self.assertRaises(AssertionError, hpt.add_hparam, "foo", [1.2, 3.1, 1.2], discrete=False)


    def test_raises_add_real_interval_compile_hparam(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertEqual(hpt._compile_hparams, {})

        self.assertRaises(
            AssertionError,
            hpt.add_hparam,
            "foo", [1.2, 3.1, 1.2], discrete=False, model_param=False
        )


    def test_remove_model_param(self):
        hpt = ct.HyperParameterTuning(None, None, None)

        hpt._model_params["foo"] = 42
        self.assertEqual(hpt._model_params, {"foo": 42})

        hpt.remove_param("foo")
        self.assertEqual(hpt._model_params, {})


    def test_remove_compile_param(self):
        hpt = ct.HyperParameterTuning(None, None, None)

        hpt._compile_params["foo"] = 42
        self.assertEqual(hpt._compile_params, {"foo": 42})

        hpt.remove_param("foo", model_param=False)
        self.assertEqual(hpt._compile_params, {})


    def test_raises_remove_model_param(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertRaises(KeyError, hpt.remove_param, "foo")


    def test_raises_remove_compile_param(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertRaises(KeyError, hpt.remove_param, "foo", model_param=False)


    def test_remove_model_hparam(self):
        hpt = ct.HyperParameterTuning(None, None, None)

        target = hp.HParam("bar", hp.Discrete([12, 42]))
        hpt._model_hparams["foo"] = target

        self.assertTrue("foo" in hpt._model_hparams)
        self.assertTrue(compare_hparams(hpt._model_hparams["foo"], target))

        hpt.remove_hparam("foo")
        self.assertEqual(hpt._model_hparams, {})


    def test_remove_compile_hparam(self):
        hpt = ct.HyperParameterTuning(None, None, None)

        target = hp.HParam("bar", hp.Discrete([12, 42]))
        hpt._compile_hparams["foo"] = target

        self.assertTrue("foo" in hpt._compile_hparams)
        self.assertTrue(compare_hparams(hpt._compile_hparams["foo"], target))


        hpt.remove_hparam("foo", model_param=False)
        self.assertEqual(hpt._compile_hparams, {})


    def test_raises_remove_model_hparam(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertRaises(KeyError, hpt.remove_hparam, "foo")


    def test_raises_remove_compile_hparam(self):
        hpt = ct.HyperParameterTuning(None, None, None)
        self.assertRaises(KeyError, hpt.remove_hparam, "foo", model_param=False)


    def test_update_arguments_no_callbacks(self):
        args = [1, 2]
        kwargs = {"foo": 3, "bar": 4}

        res_args, res_kwargs = ct.HyperParameterTuning._update_arguments("xxx", args, kwargs)

        self.assertEqual(res_args, args)
        self.assertEqual(res_kwargs, kwargs)


    def test_update_arguments_args_callbacks(self):
        callbacks = [K.callbacks.ProgbarLogger(), lambda x: K.callbacks.TensorBoard(x)]
        args = [1, 2, 3, 4, 5, callbacks]
        kwargs = {}

        res_args, res_kwargs = ct.HyperParameterTuning._update_arguments("foo", args, kwargs)

        self.assertEqual(res_args[0:5], args[0:5])
        self.assertIsInstance(res_args[5][0], K.callbacks.ProgbarLogger)
        self.assertIsInstance(res_args[5][1], K.callbacks.TensorBoard)
        self.assertEqual(res_args[5][1].log_dir, "foo")
        self.assertEqual(res_kwargs, kwargs)


    def test_update_arguments_kwargs_callbacks(self):
        callbacks = [K.callbacks.ProgbarLogger(), lambda x: K.callbacks.TensorBoard(x)]
        args = [1, 2, 3]
        kwargs = {"schmock": 123, "callbacks": callbacks}

        res_args, res_kwargs = ct.HyperParameterTuning._update_arguments("foo", args, kwargs)

        self.assertIsInstance(res_kwargs["callbacks"][0], K.callbacks.ProgbarLogger)
        self.assertIsInstance(res_kwargs["callbacks"][1], K.callbacks.TensorBoard)
        self.assertEqual(res_kwargs["callbacks"][1].log_dir, "foo")
        self.assertEqual(res_args, args)




class TestHyperParameterTuningRun(unittest.TestCase):
    def setUp(self):
        self.terminal_symbols = tf.random.uniform((64, 123), dtype=tf.int64, minval=0, maxval=300)
        self.terminal_indices = tf.random.uniform((64, 123), dtype=tf.int64, minval=0, maxval=200)

        self.nonterminal_symbols = tf.random.uniform(
            (64, 123, 15),
            dtype=tf.int64,
            minval=0,
            maxval=300
        )

        self.nonterminal_indices = tf.random.uniform(
            (64, 123, 15),
            dtype=tf.int64,
            minval=0,
            maxval=200
        )

        self.nonterminal_inputs = (self.nonterminal_symbols, self.nonterminal_indices)

        self.root_symbols = tf.random.uniform((64, 28), dtype=tf.int64, minval=0, maxval=300)
        self.root_indices = tf.random.uniform((64, 28), dtype=tf.int64, minval=0, maxval=200)
        self.root_inputs = (self.root_symbols, self.root_indices)

        self.labels = tf.random.uniform((64,), dtype=tf.int64, minval=0, maxval=300)

        self.inputs = [self.terminal_symbols, *self.nonterminal_inputs, *self.root_inputs]
        self.dataset = tf.data.Dataset.from_tensors((tuple(self.inputs), self.labels))

        self.log_dir = os.path.join("tests", "data", "logs")


    def test_model_hparam_tuning_forward_pass(self):
        hpt = ct.HyperParameterTuning(
            cosy.models.CodeCompletionXS,
            self.log_dir,
            model_generator=ct.grid_search
        )

        # model params
        hpt.add_param("terminal_vocab_size", 1000)
        hpt.add_param("nonterminal_vocab_size", 300)
        hpt.add_param("max_sibling_index", 200)

        # model hparams
        hpt.add_hparam("embedding_dimension", [128, 256, 512])

        # compile params
        hpt.add_param("optimizer", "adam", model_param=False)
        hpt.add_param("loss", "sparse_categorical_crossentropy", model_param=False)
        hpt.add_param("metrics", ["accuracy"], model_param=False)

        try:
            history_stack = hpt.run(self.dataset, verbose=0)

            meta_file = glob.glob(os.path.join(self.log_dir, "events*"))
            self.assertEqual(len(meta_file), 0)

            for i in range(3):
                name_components = [self.log_dir, "run-{}".format(i), "hparams", "events*"]
                log_file = glob.glob(os.path.join(*name_components))
                self.assertEqual(len(log_file), 1)
                self.assertTrue(os.path.isfile(log_file[0]))

                self.assertEqual(len(history_stack[i].history["loss"]), 1)
                self.assertEqual(len(history_stack[i].history["accuracy"]), 1)
        finally:
            shutil.rmtree(self.log_dir)


    def test_compile_hparam_tuning_forward_pass(self):
        def compiler(model, learning_rate, **kwargs):
            model.compile(optimizer=K.optimizers.Adam(learning_rate), **kwargs)

        hpt = ct.HyperParameterTuning(
            cosy.models.CodeCompletionXS,
            self.log_dir,
            compiler=compiler,
            model_generator=ct.grid_search
        )

        # model params
        hpt.add_param("terminal_vocab_size", 1000)
        hpt.add_param("nonterminal_vocab_size", 300)
        hpt.add_param("max_sibling_index", 200)
        hpt.add_param("embedding_dimension", 256)

        # compile params
        hpt.add_param("loss", "sparse_categorical_crossentropy", model_param=False)
        hpt.add_param("metrics", ["accuracy"], model_param=False)

        # compile hparams
        hpt.add_hparam("learning_rate", [0.01, 0.005, 0.001], model_param=False)

        try:
            history_stack = hpt.run(self.dataset, verbose=0)

            meta_file = glob.glob(os.path.join(self.log_dir, "events*"))
            self.assertEqual(len(meta_file), 0)

            for i in range(3):
                name_components = [self.log_dir, "run-{}".format(i), "hparams", "events*"]
                log_file = glob.glob(os.path.join(*name_components))

                self.assertEqual(len(log_file), 1)
                self.assertTrue(os.path.isfile(log_file[0]))

                self.assertEqual(len(history_stack[i].history["loss"]), 1)
                self.assertEqual(len(history_stack[i].history["accuracy"]), 1)
        finally:
            shutil.rmtree(self.log_dir)


    def test_combined_hparam_tuning_forward_pass(self):
        def compiler(model, learning_rate, **kwargs):
            model.compile(optimizer=K.optimizers.Adam(learning_rate), **kwargs)

        hpt = ct.HyperParameterTuning(
            cosy.models.CodeCompletionXS,
            self.log_dir,
            compiler=compiler,
            model_generator=ct.grid_search
        )

        # model params
        hpt.add_param("terminal_vocab_size", 1000)
        hpt.add_param("nonterminal_vocab_size", 300)
        hpt.add_param("max_sibling_index", 200)

        # model hparams
        hpt.add_hparam("embedding_dimension", [256, 512])

        # compile params
        hpt.add_param("loss", "sparse_categorical_crossentropy", model_param=False)
        hpt.add_param("metrics", ["accuracy"], model_param=False)

        # compile hparams
        hpt.add_hparam("learning_rate", [0.01, 0.005, 0.001], model_param=False)

        try:
            history_stack = hpt.run(self.dataset, verbose=0)

            meta_file = glob.glob(os.path.join(self.log_dir, "events*"))
            self.assertEqual(len(meta_file), 0)

            for i in range(6):
                name_components = [self.log_dir, "run-{}".format(i), "hparams", "events*"]
                log_file = glob.glob(os.path.join(*name_components))
                self.assertEqual(len(log_file), 1)
                self.assertTrue(os.path.isfile(log_file[0]))

                self.assertEqual(len(history_stack[i].history["loss"]), 1)
                self.assertEqual(len(history_stack[i].history["accuracy"]), 1)
        finally:
            shutil.rmtree(self.log_dir)


    def test_hparam_tuning_with_callbacks(self):
        def tb_callback(log_dir):
            return K.callbacks.TensorBoard(log_dir)

        def compiler(model, learning_rate, **kwargs):
            model.compile(optimizer=K.optimizers.Adam(learning_rate), **kwargs)

        hpt = ct.HyperParameterTuning(
            cosy.models.CodeCompletionXS,
            self.log_dir,
            compiler=compiler,
            model_generator=ct.grid_search
        )

        # model params
        hpt.add_param("terminal_vocab_size", 1000)
        hpt.add_param("nonterminal_vocab_size", 300)
        hpt.add_param("max_sibling_index", 200)

        # model hparams
        hpt.add_hparam("embedding_dimension", [256, 512])

        # compile params
        hpt.add_param("loss", "sparse_categorical_crossentropy", model_param=False)
        hpt.add_param("metrics", ["accuracy"], model_param=False)

        # compile hparams
        hpt.add_hparam("learning_rate", [0.01, 0.005, 0.001], model_param=False)

        try:
            history_stack = hpt.run(self.dataset, verbose=0, callbacks=[tb_callback])

            meta_file = glob.glob(os.path.join(self.log_dir, "events*"))
            self.assertEqual(len(meta_file), 0)

            for i in range(6):
                name_components = [self.log_dir, "run-{}".format(i), "hparams", "events*"]
                log_file = glob.glob(os.path.join(*name_components))
                self.assertEqual(len(log_file), 1)
                self.assertTrue(os.path.isfile(log_file[0]))

                self.assertEqual(len(history_stack[i].history["loss"]), 1)
                self.assertEqual(len(history_stack[i].history["accuracy"]), 1)

                tb_folder = os.path.join(self.log_dir, "run-{}".format(i), "train")
                self.assertTrue(os.path.isdir(tb_folder))
        finally:
            shutil.rmtree(self.log_dir)




if __name__ == "__main__":
    unittest.main(verbosity=2)
