# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import unittest
import numpy as np
import tensorflow as tf
from tensorflow import keras as K
import cosy.preprocessing as cp




class TestMasking(unittest.TestCase):
    def test_compute_value_mask_1D(self):
        y_true = tf.constant([1, 2, 3, 4, 5, 6, 7])
        mask_true = [1, 1, 0, 1, 1, 1, 1]

        mask = cp.mask_sample(y_true, mask_values=3, mask_dtype=tf.int32)

        self.assertTrue(all(mask.numpy() == mask_true))


    def test_compute_value_mask_2D(self):
        y_true = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [3, 4, 5, 3, 1, 2, 2],
        ])

        mask_true = [
            [1, 1, 0, 1, 1, 1, 1],
            [0, 1, 1, 0, 1, 1, 1]
        ]

        mask = cp.mask_sample(y_true, mask_values=3, mask_dtype=tf.int32)

        self.assertTrue(np.all(mask.numpy() == mask_true))


    def test_compute_value_mask_1D_sample_weight(self):
        y_true = tf.constant([1, 2, 3, 4, 5, 6, 7])
        mask_true = [1, 1, 0, 1, 1, 1, 1]

        sample_weight = tf.constant([0.5, 2.0, 0.5, 2.0, 0.5, 2.0, 0.5])

        mask = cp.mask_sample(y_true, sample_weight, mask_values=3, mask_dtype=tf.float32)

        self.assertTrue(all(mask.numpy() == (mask_true * sample_weight)))


    def test_compute_value_mask_2D_sample_weight(self):
        y_true = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [3, 4, 5, 3, 1, 2, 2],
        ])

        mask_true = [
            [1, 1, 0, 1, 1, 1, 1],
            [0, 1, 1, 0, 1, 1, 1]
        ]

        sample_weight = tf.constant([
            [0.5, 2.0, 0.5, 2.0, 0.5, 2.0, 0.5],
            [0.3, 3.0, 0.3, 3.0, 0.3, 3.0, 0.3],
        ])

        mask = cp.mask_sample(y_true, sample_weight, mask_values=3, mask_dtype=tf.float32)

        self.assertTrue(np.all(mask.numpy() == (mask_true * sample_weight)))


    def test_compute_value_list_mask_1D(self):
        y_true = tf.constant([1, 2, 3, 4, 5, 6, 7])
        mask_true = [1, 1, 0, 1, 0, 1, 1]

        mask = cp.mask_sample(y_true, mask_values=[3, 5], mask_dtype=tf.int32)

        self.assertTrue(all(mask.numpy() == mask_true))


    def test_compute_value_list_mask_2D(self):
        y_true = tf.constant([
            [1, 2, 3, 4, 5, 6, 7],
            [3, 4, 5, 3, 1, 2, 2],
        ])

        mask_true = [
            [1, 0, 0, 1, 1, 1, 1],
            [0, 1, 1, 0, 1, 0, 0],
        ]

        mask = cp.mask_sample(y_true, mask_values=[2, 3], mask_dtype=tf.int32)

        self.assertTrue(np.all(mask.numpy() == mask_true))


    def test_loss_sparse_categorical_crossentropy_1D_value(self):
        y_pred = tf.constant([0.1, 0.2, 0.1, 0.8, 0.3])

        loss = K.losses.SparseCategoricalCrossentropy(from_logits=True)

        mask_3 = cp.mask_sample(3, mask_values=3)
        mask_4 = cp.mask_sample(4, mask_values=3)

        self.assertEqual(loss(3, y_pred, sample_weight=mask_3).numpy(), 0.0)
        self.assertEqual(loss(4, y_pred, sample_weight=mask_4).numpy(), loss(4, y_pred))


    def test_loss_sparse_categorical_crossentropy_2D_value(self):
        y_pred = tf.constant([
            [0.1, 0.2, 0.1, 0.8, 0.3],
            [0.3, 0.9, 0.2, 0.1, 0.2],
            [0.7, 0.2, 0.7, 0.1, 0.1]
        ])

        loss = K.losses.SparseCategoricalCrossentropy(from_logits=True)

        mask_0 = cp.mask_sample([3, 0, 2], mask_values=1)
        mask_1 = cp.mask_sample([3, 1, 2], mask_values=1)
        mask_2 = cp.mask_sample([3, 2, 2], mask_values=1)

        res_0 = loss([3, 0, 2], y_pred).numpy()
        res_1 = loss([3, 1, 2], y_pred).numpy()
        res_2 = loss([3, 2, 2], y_pred).numpy()

        masked_res_0 = loss([3, 0, 2], y_pred, sample_weight=mask_0).numpy()
        masked_res_1 = loss([3, 1, 2], y_pred, sample_weight=mask_1).numpy()
        masked_res_2 = loss([3, 2, 2], y_pred, sample_weight=mask_2).numpy()

        self.assertEqual(masked_res_0, res_0)
        self.assertNotEqual(masked_res_1, res_1)
        self.assertEqual(masked_res_2, res_2)


    def test_loss_sparse_categorical_crossentropy_1D_value_list(self):
        y_pred = tf.constant([0.1, 0.2, 0.1, 0.8, 0.3])

        loss = K.losses.SparseCategoricalCrossentropy(from_logits=True)

        mask_1 = cp.mask_sample(1, mask_values=[1, 3])
        mask_3 = cp.mask_sample(3, mask_values=[1, 3])
        mask_4 = cp.mask_sample(4, mask_values=[1, 3])

        masked_res_1 = loss(1, y_pred, sample_weight=mask_1).numpy()
        masked_res_3 = loss(3, y_pred, sample_weight=mask_3).numpy()
        masked_res_4 = loss(4, y_pred, sample_weight=mask_4).numpy()

        self.assertEqual(masked_res_1, 0.0)
        self.assertEqual(masked_res_3, 0.0)
        self.assertEqual(masked_res_4, loss(4, y_pred))


    def test_loss_sparse_categorical_crossentropy_2D_value_list(self):
        y_pred = tf.constant([
            [0.1, 0.2, 0.1, 0.8, 0.3],
            [0.3, 0.9, 0.2, 0.1, 0.2],
            [0.7, 0.2, 0.7, 0.1, 0.1]
        ])

        loss = K.losses.SparseCategoricalCrossentropy(from_logits=True)

        mask_0 = cp.mask_sample([3, 1, 0], mask_values=[0, 2])
        mask_1 = cp.mask_sample([3, 1, 1], mask_values=[0, 2])
        mask_2 = cp.mask_sample([3, 1, 2], mask_values=[0, 2])

        res_0 = loss([3, 1, 0], y_pred).numpy()
        res_1 = loss([3, 1, 1], y_pred).numpy()
        res_2 = loss([3, 1, 2], y_pred).numpy()

        masked_res_0 = loss([3, 1, 0], y_pred, sample_weight=mask_0).numpy()
        masked_res_1 = loss([3, 1, 1], y_pred, sample_weight=mask_1).numpy()
        masked_res_2 = loss([3, 1, 2], y_pred, sample_weight=mask_2).numpy()

        self.assertNotEqual(masked_res_0, res_0)
        self.assertEqual(masked_res_1, res_1)
        self.assertNotEqual(masked_res_2, res_2)


    def test_loss_mse_1D_value(self):
        y_pred = tf.constant([0.1, 0.2, 0.1, 0.8, 0.3])

        loss = K.losses.MeanSquaredError()

        mask_3 = cp.mask_sample(3, mask_values=3)
        mask_4 = cp.mask_sample(4, mask_values=3)

        self.assertEqual(loss(3, y_pred, sample_weight=mask_3).numpy(), 0.0)
        self.assertEqual(loss(4, y_pred, sample_weight=mask_4).numpy(), loss(4, y_pred))


    def test_loss_mse_2D_value(self):
        y_pred = tf.constant([
            [0.1, 0.2, 0.1, 0.8, 0.3],
            [0.3, 0.9, 0.2, 0.1, 0.2],
            [0.7, 0.2, 0.7, 0.1, 0.1]
        ])

        loss = K.losses.MeanSquaredError()

        mask_0 = cp.mask_sample([[3], [0], [2]], mask_values=1)
        mask_1 = cp.mask_sample([[3], [1], [2]], mask_values=1)
        mask_2 = cp.mask_sample([[3], [2], [2]], mask_values=1)

        res_0 = loss([[3], [0], [2]], y_pred).numpy()
        res_1 = loss([[3], [1], [2]], y_pred).numpy()
        res_2 = loss([[3], [2], [2]], y_pred).numpy()

        masked_res_0 = loss([[3], [0], [2]], y_pred, sample_weight=mask_0).numpy()
        masked_res_1 = loss([[3], [1], [2]], y_pred, sample_weight=mask_1).numpy()
        masked_res_2 = loss([[3], [2], [2]], y_pred, sample_weight=mask_2).numpy()

        self.assertEqual(masked_res_0, res_0)
        self.assertNotEqual(masked_res_1, res_1)
        self.assertEqual(masked_res_2, res_2)


    def test_loss_mse_1D_value_list(self):
        y_pred = tf.constant([0.1, 0.2, 0.1, 0.8, 0.3])

        loss = K.losses.MeanSquaredError()

        mask_1 = cp.mask_sample(1, mask_values=[1, 3])
        mask_3 = cp.mask_sample(3, mask_values=[1, 3])
        mask_4 = cp.mask_sample(4, mask_values=[1, 3])

        masked_res_1 = loss(1, y_pred, sample_weight=mask_1).numpy()
        masked_res_3 = loss(3, y_pred, sample_weight=mask_3).numpy()
        masked_res_4 = loss(4, y_pred, sample_weight=mask_4).numpy()

        self.assertEqual(masked_res_1, 0.0)
        self.assertEqual(masked_res_3, 0.0)
        self.assertEqual(masked_res_4, loss(4, y_pred))


    def test_loss_mse_2D_value_list(self):
        y_pred = tf.constant([
            [0.1, 0.2, 0.1, 0.8, 0.3],
            [0.3, 0.9, 0.2, 0.1, 0.2],
            [0.7, 0.2, 0.7, 0.1, 0.1]
        ])

        loss = K.losses.MeanSquaredError()

        mask_0 = cp.mask_sample([[3], [1], [0]], mask_values=[0, 2])
        mask_1 = cp.mask_sample([[3], [1], [1]], mask_values=[0, 2])
        mask_2 = cp.mask_sample([[3], [1], [2]], mask_values=[0, 2])

        res_0 = loss([[3], [1], [0]], y_pred).numpy()
        res_1 = loss([[3], [1], [1]], y_pred).numpy()
        res_2 = loss([[3], [1], [2]], y_pred).numpy()

        masked_res_0 = loss([[3], [1], [0]], y_pred, sample_weight=mask_0).numpy()
        masked_res_1 = loss([[3], [1], [1]], y_pred, sample_weight=mask_1).numpy()
        masked_res_2 = loss([[3], [1], [2]], y_pred, sample_weight=mask_2).numpy()

        self.assertNotEqual(masked_res_0, res_0)
        self.assertEqual(masked_res_1, res_1)
        self.assertNotEqual(masked_res_2, res_2)


    def test_metric_update_state_value_1D(self):
        metric = K.metrics.SparseCategoricalAccuracy()
        ref_metric = K.metrics.SparseCategoricalAccuracy()

        y_pred = tf.constant([0.1, 0.2, 0.1, 0.8, 0.3])

        mask_2 = cp.mask_sample(2, mask_dtype=tf.int32, mask_values=3)
        mask_3 = cp.mask_sample(3, mask_dtype=tf.int32, mask_values=3)

        metric.update_state(2, y_pred, sample_weight=mask_2)
        ref_metric.update_state(2, y_pred)
        self.assertEqual(metric.result().numpy(), ref_metric.result().numpy())

        metric.reset_states()
        ref_metric.reset_states()

        metric.update_state(3, y_pred, sample_weight=mask_3)
        ref_metric.update_state(3, y_pred)
        self.assertEqual(metric.result().numpy(), 0.0)
        self.assertNotEqual(metric.result().numpy(), ref_metric.result().numpy())


    def test_metric_update_state_value_2D(self):
        metric = K.metrics.SparseCategoricalAccuracy()
        ref_metric = K.metrics.SparseCategoricalAccuracy()

        y_pred = tf.constant([
            [0.1, 0.2, 0.1, 0.8, 0.3],
            [0.3, 0.9, 0.2, 0.1, 0.2],
            [0.7, 0.2, 0.7, 0.1, 0.1]
        ])

        for label in [1, 2, 3]:
            metric.reset_states()
            ref_metric.reset_states()

            mask = cp.mask_sample([label, 1, 0], mask_dtype=tf.int32, mask_values=3)

            metric.update_state([label, 1, 0], y_pred, sample_weight=mask)
            masked_res = metric.result().numpy()

            ref_metric.update_state([label, 1, 0], y_pred)
            res = ref_metric.result().numpy()

            adjust = 1 if label == 3 else 0

            self.assertEqual(metric.count + adjust, ref_metric.count)
            self.assertEqual(metric.total + adjust, ref_metric.total)
            self.assertEqual(masked_res, res)


    def test_metric_update_state_value_list_1D(self):
        metric = K.metrics.SparseCategoricalAccuracy()
        ref_metric = K.metrics.SparseCategoricalAccuracy()

        y_pred = tf.constant([0.1, 0.2, 0.1, 0.8, 0.3])

        mask_2 = cp.mask_sample(2, mask_dtype=tf.int32, mask_values=[3, 4])
        mask_3 = cp.mask_sample(3, mask_dtype=tf.int32, mask_values=[3, 4])
        mask_4 = cp.mask_sample(4, mask_dtype=tf.int32, mask_values=[3, 4])

        metric.update_state(2, y_pred, sample_weight=mask_2)
        ref_metric.update_state(2, y_pred)
        self.assertEqual(metric.result().numpy(), ref_metric.result().numpy())

        metric.reset_states()
        ref_metric.reset_states()

        metric.update_state(3, y_pred, sample_weight=mask_3)
        ref_metric.update_state(3, y_pred)
        self.assertEqual(metric.result().numpy(), 0.0)
        self.assertNotEqual(metric.result().numpy(), ref_metric.result().numpy())

        metric.reset_states()
        ref_metric.reset_states()

        metric.update_state(4, y_pred, sample_weight=mask_4)
        ref_metric.update_state(4, y_pred)
        self.assertEqual(metric.result().numpy(), 0.0)
        self.assertEqual(metric.result().numpy(), ref_metric.result().numpy())


    def test_metric_update_state_value_list_2D(self):
        metric = K.metrics.SparseCategoricalAccuracy()
        ref_metric = K.metrics.SparseCategoricalAccuracy()

        y_pred = tf.constant([
            [0.1, 0.2, 0.1, 0.8, 0.3],
            [0.3, 0.9, 0.2, 0.1, 0.2],
            [0.7, 0.2, 0.7, 0.1, 0.1]
        ])

        for label in [1, 2, 3]:
            metric.reset_states()
            ref_metric.reset_states()

            mask = cp.mask_sample([label, 1, 0], mask_dtype=tf.int32, mask_values=[0, 3])

            metric.update_state([label, 1, 0], y_pred, sample_weight=mask)
            masked_res = metric.result().numpy()

            ref_metric.update_state([label, 1, 0], y_pred)
            res = ref_metric.result().numpy()

            adjust = 2 if label == 3 else 1

            self.assertEqual(metric.count + adjust, ref_metric.count)
            self.assertEqual(metric.total + adjust, ref_metric.total)

            if adjust == 1:
                self.assertNotEqual(masked_res, res)
            else:
                self.assertEqual(masked_res, res)




if __name__ == "__main__":
    unittest.main(verbosity="b")
