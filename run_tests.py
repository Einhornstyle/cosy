#Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

import os
import sys
import argparse
import unittest




def discover_and_run(template, pattern="*", start_dir="tests"):
    suite = unittest.TestSuite()

    for test in unittest.defaultTestLoader.discover("tests", template.format(pattern)):
        suite.addTest(test)

    test_results = None
    num_test_cases = suite.countTestCases()

    if num_test_cases > 0:
        test_results = unittest.TextTestRunner(verbosity=2).run(suite)

    return test_results, num_test_cases




if __name__ == "__main__":
    test_desc = "only tests from files that match 'tests/test_[TEST_PATTERN].py' will be run"
    eval_desc = "only evaluations from files that match "\
                "'tests/evaluations/eval_[EVAL_PATTERN].py' will be run"
    gpu_desc = "specify the GPU to be used (default = 0)"

    parser = argparse.ArgumentParser(description="Run the cosy tests and evaluations.")
    parser.add_argument("-t", "--test_pattern", help=test_desc)
    parser.add_argument("-e", "--eval_pattern", help=eval_desc)
    parser.add_argument("-g", "--gpu_device", type=int, default=0, help=gpu_desc)
    args = parser.parse_args()

    if args.test_pattern is None and args.eval_pattern is None:
        args.test_pattern = "*"

    # prohibit Tensorflow log messages from polluting the test output
    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

    # cuda will now treat args.gpu_device as if it would be device 0
    os.environ["CUDA_VISIBLE_DEVICES"] = "{}".format(args.gpu_device)

    num_failed_tests = 0

    test_results, num_test_cases = discover_and_run("test_{}.py", args.test_pattern)

    if test_results is not None:
        num_failed_tests += len(test_results.errors) + len(test_results.failures)

    if args.eval_pattern is not None:
        if num_test_cases > 0:
            print()

        eval_results, _ = discover_and_run("eval_{}.py", args.eval_pattern)

        if eval_results is not None:
            num_failed_tests += len(eval_results.errors) + len(eval_results.failures)

    sys.exit(0 if num_failed_tests == 0 else 1)
