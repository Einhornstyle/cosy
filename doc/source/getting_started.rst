.. Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
..
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
..
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
..
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
.. THE SOFTWARE.


Getting Started
***************
We start this tutorial with an overview over the Cosy preprocessing pipeline in the first section.
In the second section we introduce the notion of syntax trees and their realization in Cosy.
And lastly, we describe how to write customized pipeline components in the third section


Cosy Preprocessing Pipeline
===========================
The aim of the preprocessing is to convert the raw data into tensors which will be feed into our
model. In Cosy this process is split into 4 primary steps:

1. Download the raw data.
2. Convert each data point into a set of features.
3. Write the features into Tensorflow record files.
4. Load the features from the record files and convert them to tensors.

Step 2 as well as 4 allow for feature engineering. However, step 4 will usually be part of your
trainings script and therefore repeated every time you train your model. The heavy lifting, such as
the processing of ASTs or the encoding of symbols should be done in step 2 before writing the record
files. The overall structure of a preprocessing script, with an emphasize on the interplay of the
different Cosy components, might look like this:

.. code-block:: python
    :linenos:

    import os
    import cosy

    TRAIN_FILENAME = "python100k_train.json"

    # Download raw data files if they do not exist.
    if not os.path.isfile(TRAIN_FILENAME):
        cosy.data.Python150kDataset.download()

    # Create your dataset. We can iterate over train_data to get one AST at a time.
    train_data = cosy.data.Python150kDataset(TRAIN_FILENAME)

    # We will use the record_writer to create and write the Tensorflow records.
    with cosy.data.ASTPathsWriter() as record_writer:
        for idx, ast in enumerate(train_data):
            # Preprocessing would happen here (e.g. map each ast to a set of features).
            # ...

            # Usually you will create many files to ensure they are not to large (e.g. < 200MB).
            # In this example we create 100k / 1000 = 100 files.
            if idx % 1000:
                record_writer.filename = "my_record_{}.tfrecord".format(idx)

            # Convert the features to a record and write it to disk.
            record_writer(**my_features)

    # We close the file explicitly. Alternatively we could have used a "with" statement.
    train_data.close()

    # Load the records into a tensorflow dataset (usually this happens in a different script).
    tf_dataset = cosy.data.ASTPathsReader()

The script starts with optionally downloading the raw data files via one of Cosy's dataset classes.
In this case the Python150kDataset. Each dataset class has a static member function called
“download” for this purpose. Furthermore, each dataset implements the iterator interface to stream
the data efficiently into your preprocessing pipeline and may be used inside a “with” statement
instead of opening and closing it explicitly. Most of Cosy’s datasets provide ASTs and converting
them into a set of features is the bulk of what the preprocessing must accomplish. Whilst this step
is not shown in the script template above, since it varies substantially from one script to another,
Cosy provides several tools to help with it. After computing the features,
we use one of Cosy’s record writers to create the record files. Here we used the “ASTPathsWriter”,
which writes a set of paths within an AST to disk. The features which are expected by a specific
record writer as well as their structure is documented in the “make_record” member function of the
writer. Each record writer has an associated record reader that may be used in the trainings script
to load the records form the files and convert them to proper tensors.


Processing Syntax Trees
=======================
The grammar of most programming language is
`context-free <https://en.wikipedia.org/wiki/Context-free_grammar>`_. As such there exists a so-called
parse tree or concrete syntax tree for every word of the language that describes the rule
derivations which are necessary to generate the word. A parse tree has the property that its leaf
nodes read in order from either left to right or vice versa (the direction is convention and we
choose left to right) form the word the parse tree is associated with. In contrast, an
`abstract syntax tree (AST) <https://en.wikipedia.org/wiki/Abstract_syntax_tree>`_ does in general not
have this property anymore. ASTs are usually derived from a concrete syntax tree and aim to simplify
reasoning over the program structure. In Cosy we also employ the notion of a pseudo concrete syntax
tree. These are usually created by taking an AST and modifying it such that the leaf order property
holds again, in the sense that the word defined by the leaf nodes is a meaningful entity, but is not a
word in the original language the AST is associated with.

To represent the syntax trees or any graph for that matter we use the
`igraph package <https://igraph.org/python>`_ and particularly their graph class. However, igraph is
a generic graph library that defines a tree as a connected acyclic undirected graph. This definition
does not carry any information regarding the order of child nodes and without it we are unable to
compute the word represented by a syntax tree. Additionally, since we create syntax trees by
successive application of the grammar rules, there is only one valid candidate for the root of the
tree and every node has exactly one well defined parent. To store this information, we define a
syntax tree T has follows:

1. T is a directed graph
2. T’s undirected representation is connected and acyclic
3. There is an injective mapping s from the set of edges of T into an ordered set (typically the
   natural numbers), such that the leaf order property holds if for every node the children are
   traversed in ascending order of s(edge(node->child)).

For efficiency and convenience, we also establish the following convention which are upheld by any
Cosy algorithm that creates or modifies an ASTs.

1. The first node (the one with id 0) should be the root.
2. The index of every edge e is chosen in such a way that the mapping s(e) = e.index fulfills the
   third property.
3. If a node holds a symbol (= letter in the terminal or nonterminal alphabet of the grammar) the
   corresponding attribute were it is stored will be called “symbol”.

Since the syntax trees usually originate from a Cosy dataset and we provide a series of proxy
methods when working with them, it does usually not require any effort to uphold these properties
and conventions. Yet, when writing syntax tree algorithms with the intention to add them to Cosy
one should never assume that the conventions are upheld. Hence, inside an algorithm the root should
always be computed with the “cosy.tree.root” function and every algorithm that depends on the child
traversal order should have the argument “sort=cosy.tree.EdgeSort()” and propagate its value
internally.


Custom Pipeline Components
==========================
Most Cosy pipelines employ a dataset, record writer and record reader. In this section we will
explore how to implement them yourself, which in most cases is straightforward, since they have been
designed with this purpose in mind.


Dataset
-------
The raw data is usually stored in a file and a Cosy dataset reads the data points from the file,
applies some preprocessing and provides them one at a time. It is also responsible for providing
a method to download the raw data. Hence, it must have at least the following API:

1. __init__(filename) & close()
2. __enter__() & __exit__()
3. __iter__() & __next__()
4. @classmethod download(path)

We provide the class “cosy.data.Dataset” that implements all of these methods except for
“download” and simply provides one line of the raw data file on every iteration. Usually it
is sufficient to subclass the “cosy.data.Dataset” class and implement the __init__, __iter__,
__next__ and “download” methods. The class “cosy.data.Python150kDataset” may serve as an
example implementation.


RecordWriter
------------
A Cosy “RecordWriter” encapsulates the necessary logic to convert a set of features to a Tensorflow
record and write the record to disk. Since the I/O logic does not change between different writers
the only method that needs to be implemented is the abstract “make_record” method.  This method
may take any arguments and must return either a “tensorflow.train.Example” or a
“tensorflow.train.SequenceExample”. We refer to “cosy.data.ASTPathsWriter”,
“cosy.data.ASTPathsDosWriter” and the Tensorflow documentation for examples on how this may be
achieved.


RecordReader
------------
The “RecordReader” class reads a set of files containing Tensorflow records from disk and creates
a Tensorflow dataset, from which the data can be streamed efficiently into your model. The base
class “cosy.data.RecordReader” may be used by itself in which case the dataset will contain
serialized Tensorflow records. This class also can shuffle and prefetch the records if desired.
When deriving from this class only the method “preprocess” needs to be implemented. If shuffling
and/or prefetching is enabled the “preprocess” method will be called after the shuffle but before
the prefetching. Which means in most cases the “preprocess” method will do the following:

1. deserialize the records
2. several calls to “map” which bring the tensors into the desired form
3. batch the data if applicable

The signature of the “preprocess” method is::

    preprocess(dataset: TFRecordDataset, num_threads: int, **kwargs) -> TFRecordDataset

The “num_threads” arguments specifies the number of CPU threads the “preprocess” method can use.
If you need additional parameters, you can simply add them to the prototype as keyword arguments
and provided them later to the RecordReaders __call__ method, which will pass them to the
“preprocess” method. The classes “cosy.data.ASTPathsReader” and “cosy.data.ASTPathsDocsReader”
are examples on how to subclass the “cosy.data.RecordReader”. For further information on
preprocessing “TFRecordDatasets” we refer to the Tensorflow documentation.
