.. Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
..
.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
..
.. The above copyright notice and this permission notice shall be included in
.. all copies or substantial portions of the Software.
..
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
.. THE SOFTWARE.


Cosy documentation
*******************

.. toctree::
   :caption: Tutorial
   :maxdepth: 2
   :hidden:

   Getting Started <getting_started>


.. toctree::
   :caption: Documentation
   :maxdepth: 2
   :hidden:

   Grammar <grammar>
   Tree <tree>
   Algorithms <algorithms>
   Generator <generator>
   Models <models>
   Layers <layers>
   Data <data>
   Preprocessing <preprocessing>
   Training <training>
   Utils <utils>


Installation Instructions
=========================

Open a terminal and type::

    git clone https://gitlab.com/Einhornstyle/nldsl.git
    cd cosy
    pip install .

To efficiently train the provided models, GPU support is essentially required. How to add GPU support
is described in the official `TensorFlow 2 <https://www.tensorflow.org/install>`_ install instructions.

You can verify that the installation was successful by opening the terminal again, navigating into the
cosy folder and executing::

    python run_tests.py

If all tests succeed the installation was successful. Additionally you can run the evaluation tests
which will train a few models and check that they converge via::

   python run_test.py -e *

Please note that the evaluation tests take around 30 minutes up to an hour depending on your GPU.
Running these tests without GPU support is not recommended.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`