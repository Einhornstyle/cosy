# DFS Transformer

This example demonstrates how to train a code prediction model based on the transformer architecture,
to predict the next vertex (in depth-first traversal order) in an AST given the previous vertices.
This approach is described in the paper
[Code Prediction by Feeding Trees to Transformers](https://arxiv.org/abs/2003.13848).
However, we stick closer to the original Transformer architecture and training setup.
We also decrease certain parameters such as the vocabulary and batch size
to reduce the hardware requirements necessary to follow along.

To download the raw data and preprocess it simply run:
```
python dfs_preprocessing.py
```
From the same folder you can then run the trainings script:
```
python dfs_training.py
```
If you want to run the two script from different locations you need to copy the files
that contain the preprocessed data, namely:

- ./data/dfs_train.tfrecord
- ./data/dfs_eval.tfrecord
 
And make sure they are at them same relative locations on the machine where the trainings script
will run.

Note: The preprocessing as well as training take several hours and running the latter requires a GPU.
