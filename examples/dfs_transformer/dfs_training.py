# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from pathlib import Path
import tensorflow as tf
from tensorflow import keras as K
import cosy




class LRSchedule(K.optimizers.schedules.LearningRateSchedule):
    """Learning rate used to when training the model.

    We use the seam learning rate as in the original Transformer paper
    `Attention Is All You Need <https://arxiv.org/abs/1706.03762>`_.
    """
    def __init__(self, dim, warmup_steps=4000):
        super().__init__()

        self.dim = tf.cast(dim, tf.float32)
        self.warmup_steps = warmup_steps


    def __call__(self, step):
        arg1 = tf.math.rsqrt(step)
        arg2 = step * (self.warmup_steps ** -1.5)

        return tf.math.rsqrt(self.dim) * tf.math.minimum(arg1, arg2)




def finalize(vertices, mask, mask_values):
    """Create the labels and mask padding as well as OOV symbols.

    Args:
        vertices (Tensor): The sequences of vertices of the ASTs in DFS order.
            shape = (batch_size, seq_len)
        mask (Tensor): The sliding window mask. shape = (batch_size, seq_len)
        mask_values (list): The symbols to be masked as well.

    Returns:
        (tuple): A triple containing the tensors (vertices, labels, sample_weights)
    """
    # Split the vertices into the pair (vertices[..., :-1], vertices[..., 1:])
    vertices, labels = cosy.preprocessing.add_shifted_labels(vertices)

    # Assign a sample weight of zero to padding and OOV symbols.
    sample_weights = cosy.preprocessing.mask_sample(
        y_true=labels,
        sample_weight=mask[..., 1:],
        mask_values=mask_values,
        mask_dtype="int32"
    )

    return vertices, labels, sample_weights




if __name__ == "__main__":
    VERBOSITY_LEVEL = 1 # 0 = silent, 1 = progress bar, 2 = one line per epoch
    VOCAB_SIZE = 10000
    COMPRESSION = "GZIP"
    NUM_THREADS = 2
    BATCH_SIZE = 8
    MAX_RANK = 10
    EPOCHS = 10
    DATA_DIR = Path("data")

    train_file = str(DATA_DIR / "dfs_train.tfrecord")
    eval_file = str(DATA_DIR / "dfs_eval.tfrecord")

    # Load the data and create the final labels and sample_weights.
    config = {"compression": COMPRESSION, "num_threads": NUM_THREADS, "batch_size": BATCH_SIZE}
    reader = cosy.data.SequenceReader(["int32", "int32"], 2, shuffle_buffer_size=1000)

    # create the trainings dataset
    train_dataset = reader(train_file, **config)
    train_dataset = train_dataset.map(lambda x, mask: finalize(x, mask, [0, VOCAB_SIZE-1]))

    # create the evaluation dataset
    eval_dataset = reader(eval_file, **config)
    eval_dataset = eval_dataset.map(lambda x, mask: finalize(x, mask, [0, VOCAB_SIZE-1]))

    # Define the model. We use the same dropout rate as for the original Transformer
    model = cosy.models.SequenceTransformer(vocab_size=VOCAB_SIZE, dropout=0.1)

    # setup optimizer and metrics
    learning_rate = LRSchedule(300)
    optimizer = K.optimizers.Adam(learning_rate, beta_1=0.9, beta_2=0.98, epsilon=1e-9)
    mrr = cosy.training.SparseMeanReciprocalRank(max_rank=MAX_RANK, name="MRR")

    model.compile(
        optimizer=optimizer,
        loss="sparse_categorical_crossentropy",
        weighted_metrics=["accuracy", mrr]
    )

    # train & evaluate
    model.fit(train_dataset, epochs=EPOCHS, verbose=VERBOSITY_LEVEL)

    model.evaluate(eval_dataset, verbose=VERBOSITY_LEVEL)
