# Copyright (c) 2019 Kevin Kiefer <abc.kiefer@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import json
from collections import defaultdict
from pathlib import Path
import cosy




def build_encoder(in_file, vocab_size):
    """Build the encoder that encodes our symbols as integers.

    Args:
        in_file (Path): The file that contains the raw datapoints.
        vocab_size (int): The size of our encoder vocabulary. We only encode the
            (vocab_size - 2) most frequent symbols together with the padding and OOV symbol.

    Returns:
        (StaticSymbolEncoder) The encoder.
    """
    frequencies = defaultdict(int)

    # Dataset format: [{"type": <str>, "value": <str>, "children": list[int]}, ...]
    # Note: Some datapoints do not have all 3 attributes.
    with cosy.data.Dataset(in_file) as dataset:
        for line in dataset:
            for node in json.loads(line):
                if "value" in node:
                    frequencies[node["value"]] += 1

                if "type" in node:
                    frequencies[node["type"]] += 1

    # sorted_frequencies is a list of (symbol, frequency) pairs.
    sorted_frequencies = sorted(frequencies.items(), key=lambda x: x[1], reverse=True)

    # The encoder will always add the two special symbols <PADDING> and <UNKNOWN>.
    sorted_frequencies = sorted_frequencies[:(vocab_size - 2)]

    return cosy.preprocessing.StaticSymbolEncoder((symbol for symbol, _ in sorted_frequencies))




def run(in_file, out_file, encoder, window_size, step_size, compression):
    """Rune the preprocessing pipeline and create the tfrecord files.

    Args:
        in_file (Path): The name of the file that contains the raw datapoints.
        out_file (Path): The file where the tfrecords will be written to.
        encoder (StaticSymbolEncoder): The encoder.
        window_size (int): Size of the sliding window.
        step_size (int): Step size of the sliding window.
        compression (str): The type of compression that will be applied to the output file.
            Must be either 'GZIP', 'ZLIP' or None.
    """
    # The filename and compression need to proved as keyword arguments.
    writer_config = {"filename": str(out_file), "compression": compression}

    # We map each AST in the Python150kDataset to a sequence of vertices is DFS order.
    with cosy.data.Python150kDataset(in_file, expand_values=True) as dataset,\
         cosy.data.SequenceWriter(["int32", "bool"], 2, **writer_config) as writer:

        for i, ast in enumerate(dataset):
            # Get all vertices in DFS order and encode their respective symbols.
            vertices = [encoder.encode(v["symbol"]) for v in cosy.tree.dfs(ast)]

            # Create the final sequences with a sliding window. mask[i] is True if the symbol at
            # position i in the window is not already part of a previous window and False otherwise.
            for window, mask in cosy.preprocessing.window_iter(vertices, window_size, step_size):
                writer(sequences=[window, mask])




if __name__ == "__main__":
    VOCAB_SIZE = 10000
    WINDOW_SIZE = 1000
    STEP_SIZE = 500
    COMPRESSION = "GZIP"
    FOLDER = Path("data")

    train_in_file = FOLDER / "python100k_train.json"
    eval_in_file = FOLDER / "python50k_eval.json"
    train_out_file = FOLDER / "dfs_train.tfrecord"
    eval_out_file = FOLDER / "dfs_eval.tfrecord"
    encoding_file = FOLDER / "encoder.json"

    # Create the root folder if necessary.
    FOLDER.mkdir(parents=True, exist_ok=True)

    # Download the data if it does not already exist.
    if not (train_in_file.is_file() and eval_in_file.is_file()):
        cosy.data.Python150kDataset.download(path=FOLDER)

    # Create the encoding if it does not already exist.
    if not encoding_file.is_file():
        encoder = build_encoder(train_in_file, VOCAB_SIZE)
        encoder.save(encoding_file)
    else:
        encoder = cosy.preprocessing.StaticSymbolEncoder.load(encoding_file)

    # Create the trainings data.
    run(train_in_file, train_out_file, encoder, WINDOW_SIZE, STEP_SIZE, COMPRESSION)

    # Create the evaluation data.
    run(eval_in_file, eval_out_file, encoder, WINDOW_SIZE, STEP_SIZE, COMPRESSION)
