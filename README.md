# Python package for **Co**de **Sy**nthesis

Cosy is a collection of tools to build and train deep learning models for code synthesis.


## Documentation and Tutorial

The complete documentation is available on the [project web page](https://einhornstyle.gitlab.io/cosy/).


## Installation


Open a terminal and type:
```
git clone https://gitlab.com/Einhornstyle/cosy.git
cd cosy
pip install .
```
To efficiently train the provided models, GPU support is essentially required. How to add GPU support
is described in the official [TensorFlow 2](https://www.tensorflow.org/install) install instructions.


### Verify Installation
You can verify that the installation was successful by opening the terminal again, navigating into the
cosy folder and executing:
```
python run_tests.py
```
If all tests succeed the installation was successful. Additionally you can run the evaluation tests
which will train a few models and check that they converge via:
```
python run_tests.py -e "*"
```
Please note that the evaluation tests take around 30 minutes up to an hour depending on your GPU.
Running these tests without GPU support is not recommended.


### Build Documentation
If you want to build the documentation you will need to install sphinx together with the rtd_theme:
```
pip install sphinx
pip install sphinx_rtd_theme
```
You can then build the documentation from within the cosy folder as follows:
```
make -C doc html
```
In case you are using Windows and do not have the make command available by default,
I recommend installing [Chocolatey](https://chocolatey.org/install)
and then executing the following command in an administrative powershell to install make:
```
choco install make
```


## Breaking Changes

1. \[Version 0.13.7a\] The classes DatasetFactoryABC, DatasetWriter, ASTPaths and
   ASTPathsWithDocs have been moved from cosy.data into cosy.deprecated.data.
2. \[Version 0.14.0a\] The modules cosy.utility and cosy.layers.utility have been renamed
   to cosy.utils and cosy.layers.utils respectively.
3. \[Version 0.14.9a\] The classmethod Python150kDataset.download does not write to stdout
   anymore by default. This behaviour needs to enabled explicitly with the \`verbose\` argument.
4. \[Version 0.15.1a\] The classes TerminalPathEmbedding, NonterminalPathEmbedding
   and PathEmbedding have been moved from cosy.layers into cosy.deprecated.layers.
5. \[Version 0.15.5a\] Changed parameters of TransformerDecoder.call and
   TransformerDecoderLayer.call.
5. \[Version 0.15.5a\] Transformer(Encoder/Decoder)(-Layer)s now work with default boolean sequence
   masks, instead of inverted multi-head broadcastable float masks.
6. \[Version 0.15.8a\] The function cosy.generator.generate has been moved to
   cosy.deprecated.generator.generate
6. \[Version 0.15.9a\] NumericBNF.load and BNF.load are now classmethods.
7. \[Version 0.15.9a\] The class cosy.generator.Generator has been moved to
   cosy.deprecated.generator.Generator
